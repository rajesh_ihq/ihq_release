package pageObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import baseClass.TestBase;
import locators.Locators.basicProcessingLocators;
import locators.Locators.carpeeshClientPolicyLocators;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

public class Basic_Processing_Page extends TestBase implements basicProcessingLocators, carpeeshClientPolicyLocators {

	CommonFunction commonFunction = new CommonFunction();
	ReadPropertyConfig readPro = new ReadPropertyConfig();
	CP_Client_Policy_Page cpPolicyPage = new CP_Client_Policy_Page();
	String excelPath = readPro.getReleaseAutomationAdditionalFormMV();
	Xls_Reader reader = new Xls_Reader(excelPath);

	// Clear Client Transaction Data
	@FindBy(xpath = CLIENT_NAME)
	WebElement clientName_field;
	@FindBy(xpath = KEY)
	WebElement key_field;
	@FindBy(xpath = SUBMIT_BUTTON)
	WebElement submitButton_field;

	// Editing A Client Locators
	@FindBy(xpath = EDIT_BUTTON)
	WebElement editButton_field;
	@FindBy(xpath = GENDER)
	WebElement gender_field;
	@FindBy(xpath = SAVE_CHANGES_BUTTON)
	WebElement saveChangesButton_field;
	@FindBy(xpath = VIEW_ICON)
	WebElement viewIcon_field;
	@FindBy(xpath = VIEW_MALE)
	WebElement viewMale_field;

	// Add New Task Locators
	@FindBy(xpath = NAVIGATE_DASHBOARD)
	WebElement navigateDashboard_field;
	@FindBy(xpath = ALL_TASKS)
	WebElement allTasks_field;
	@FindBy(xpath = ADD_TASK_BUTTON)
	WebElement addTaskButton_field;
	@FindBy(xpath = ADD_TASK_CLIENT_NAME)
	WebElement addTaskClientName_field;
	@FindBy(xpath = ASSIGNED_TO)
	WebElement assignedTo_field;
	@FindBy(xpath = ADD_TASK_SUBJECT)
	WebElement addTaskSubject_field;
	@FindBy(xpath = ADD_TASK_DESCRIPTION)
	WebElement addTaskDescription_field;
	@FindBy(xpath = ADD_TASK_NOTE_SECTION)
	WebElement addTaskNoteSection_field;
	@FindBy(xpath = ADD_TASK_TIME)
	WebElement addTaskTime_field;
	@FindBy(xpath = ADD_TASK_SAVE_BUTTON)
	WebElement addTaskSaveButton_field;

	// Add new note locators
	@FindBy(xpath = ALL_NOTES)
	WebElement allNotes_field;

	// Create new client locators
	@FindBy(xpath = CLIENT_BUTTON)
	WebElement clientButton_field;
	@FindBy(id = "salutation")
	WebElement salutation_field;
	@FindBy(id = "first_name")
	WebElement firstName_field;
	@FindBy(id = "last_name")
	WebElement lastName_field;
	@FindBy(id = "company_name_client")
	WebElement legalName_field;
	@FindBy(id = "client_email")
	WebElement emailAddress_field;
	@FindBy(id = "client_dob")
	WebElement clientDOB_field;
	@FindBy(id = "physical_same_as_mailing_client")
	WebElement mailingAddressSameAsPhysicalAddress_field;
	@FindBy(id = "mailing_address1_client")
	WebElement mailingAddress1_field;
	@FindBy(id = "mobileareacode")
	WebElement mobileNumber_field;
	@FindBy(xpath = SEARCH_BUTTON)
	WebElement searchButton_field;
	@FindBy(xpath = IHQ_CLIENT_NAME)
	WebElement ihqClientName_field;

	// Create Contact to Ghost Client locators.
	@FindBy(xpath = CLIENT_CONTACT_TAB)
	WebElement clientContactTab_field;
	@FindBy(xpath = CONTACT_NAV)
	WebElement contactNavigation_field;
	@FindBy(xpath = ADD_CONTACT_BUTTON)
	WebElement addContactButton_field;
	@FindBy(id = "position")
	WebElement contactPosition_field;
	@FindBy(id = "mobile")
	WebElement contactMobile_field;
	@FindBy(id = "email")
	WebElement contactEmail_field;
	@FindBy(id = "address")
	WebElement contactAddress_field;
	@FindBy(id = "dob")
	WebElement contactDOB_field;
	@FindBy(id = "contactrelationships")
	WebElement contactReleationship_field;
	@FindBy(id = "btn_add_edit_contact")
	WebElement addButton_field;

	// Create new Quote Locators
	@FindBy(xpath = NEW_QUOTE_BUTTON)
	WebElement newQuoteButton_field;
	@FindBy(xpath = SELECT_POLICY_CLASS)
	WebElement selectPolicyClass_field;
	@FindBy(xpath = ADD_LOB_TOWN_VILLAGE)
	WebElement addLobTownVillage_field;
	@FindBy(xpath = ADD_LOB_SUM_INSURED)
	WebElement addLobSumInsured_field;
	@FindBy(xpath = ADD_LOB_FUTURE_ANNUAL_PREMIER)
	WebElement addLobFutureAnnualPremier_field;
	@FindBy(xpath = ADD_LOB_SAVE_BUTTON)
	WebElement addLobSaveButton_field;

	// New policy SM Additional field locators
	@FindBy(xpath = ADD_LOB_DESCRIPTION)
	WebElement addLobDescription_field;
	@FindBy(xpath = ADD_LOB_EDIT_ICON)
	WebElement addLobEditIcon_field;
	@FindBy(xpath = UPDATE_ADDITIONAL_FIELDS_BUTTON)
	WebElement updateAdditionalFieldButton_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_MAKE_01)
	WebElement makeField01_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_MAKE_02)
	WebElement makeField02_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_YEAR)
	WebElement year_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_MODIFICATION_YES)
	WebElement notificationYes_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_MODIFICATION_NO)
	WebElement notificationNo_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_ANY_FINANCE_ON_THE_CAR_YES)
	WebElement anyFinanceOnTheCarYes_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_ANY_FINANCE_ON_THE_CAR_NO)
	WebElement anyFinanceOnTheCarNo_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_DOES_THE_CAR_HAVE_IMMOBILISER_FACTORY)
	WebElement doesTheCarHaveAnImmobiliserFactory_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_DOES_THE_CAR_HAVE_IMMOBILISER_MARKET)
	WebElement doesTheCarHaveAnImmobiliserAfterMarket_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_DOES_THE_CAR_HAVE_IMMOBILISER_NO)
	WebElement doesTheCarHaveAnImmobiliserNo_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_HELD_LICENCE_OVER_10_YEAR_YES)
	WebElement heldLicenceForOver10YearsYes_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_HELD_LICENCE_OVER_10_YEAR_NO)
	WebElement heldLicenceForOver10YearsNo_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_NONE)
	WebElement howManyFaulyClaimHadInLast3YearNone_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_1)
	WebElement howManyFaulyClaimHadInLast3Year1_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_2)
	WebElement howManyFaulyClaimHadInLast3Year2_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_3)
	WebElement howManyFaulyClaimHadInLast3Year3_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_4_OR_MORE)
	WebElement howManyFaulyClaimHadInLast3Year4OrMore_field;
	@FindBy(xpath = HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_NONE)
	WebElement howManyLicenceSuspensionCancellationDisqualificationInLast3YearNone_field;
	@FindBy(xpath = HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_1)
	WebElement howManyLicenceSuspensionCancellationDisqualificationInLast3Year1_field;
	@FindBy(xpath = HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_2)
	WebElement howManyLicenceSuspensionCancellationDisqualificationInLast3Year2_field;
	@FindBy(xpath = HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_3)
	WebElement howManyLicenceSuspensionCancellationDisqualificationInLast3Year3_field;
	@FindBy(xpath = HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_4_OR_MORE)
	WebElement howManyLicenceSuspensionCancellationDisqualificationInLast3Year4OrMore_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_AGE_OF_MAIN_DRIVER)
	WebElement ageOfMainDriver_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_CAR_MAIN_USE_PERSONAL)
	WebElement carMainUsePersonal_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_CAR_MAIN_USE_BUSINESS)
	WebElement carMainUseBusiness_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_WHERE_IS_CAR_PARKED_GARAGED)
	WebElement whereIsCarParkedGaraged_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_WHERE_IS_CAR_PARKED_DRIVEWAY)
	WebElement whereIsCarParkedDriveway_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_WHERE_IS_CAR_PARKED_STREET)
	WebElement whereIsCarParkedStreet_field;
	@FindBy(xpath = SM_ADDITIONAL_FIELD_QUOTE_BUTTON)
	WebElement quoteButton_field;

	// New Legacy Locators
	@FindBy(xpath = NEW_LEGACY)
	WebElement newLegacyButton_field;

	// New Cover Note Locators
	@FindBy(xpath = COVER_NOTE)
	WebElement coverNoteButton_field;

	// New Insurere Quote Locators
	@FindBy(xpath = NEW_INSURER_QUOTE)
	WebElement newInsurerQuoteButton_field;
	@FindBy(xpath = EXCESS)
	WebElement excess_field;
	@FindBy(xpath = ADD_INSURER_BUTTON)
	WebElement addInsurerButton_field;
	@FindBy(xpath = SELECT_CREDITOR_FROM_ADD_INSURER)
	WebElement selectCreditorFromAddInsurer_field;
	@FindBy(xpath = SAVE_BUTTON_ADD_INSURER_FROM_QUOTE)
	WebElement saveButtonAddInsurerFromQuote_field;
	@FindBy(xpath = ADD_INSURER_BROKERAGE)
	WebElement addInsurerBrokerage_field;

	// Endorsing Policy
	@FindBy(xpath = MANAGE_ICON_FOR_ENDORSE_POLICY)
	WebElement manageIconForEndorsePolicy_field;
	@FindBy(xpath = ENDORSE_OPTION_FROM_CONVERT_POLICY)
	WebElement endorseOptionFromConvertPolicy_field;
	@FindBy(xpath = CONFIRM_BUTTON_WHILE_CONVERT_POLICY)
	WebElement confirmButtonWhileConvertPolicy_field;
	@FindBy(xpath = BIND_POLICY_BUTTON_CONVERT_POLICY)
	WebElement bindPolicyConvertPolicy_field;

	// Create a Manual Invoice
	@FindBy(xpath = CREATE_INVOICE_BUTTON)
	WebElement createInvoiceButton_field;
	@FindBy(xpath = TRANSACTION_TYPE)
	WebElement transactionType_field;
	@FindBy(xpath = CURRENCY)
	WebElement currency_field;
	@FindBy(xpath = SEARCH_FOR_CLIENT_DEBATOR)
	WebElement searchForClientDebator_field;
	@FindBy(xpath = BRANCH)
	WebElement branch_field;
	@FindBy(xpath = INVOICE_DETAIL)
	WebElement detail_field;
	@FindBy(xpath = ADD_INVOICE_LINE_BUTTON)
	WebElement addInvoiceLineButton_field;
	@FindBy(xpath = INVOICE_LINE_DESCRIPTION)
	WebElement invoiceLineDescription_field;
	@FindBy(xpath = EXCLUSIVE_AMOUNT)
	WebElement exclusiveAmount_field;
	@FindBy(xpath = VAGST_AMOUNT)
	WebElement vagstAmount_field;
	@FindBy(xpath = MANUAL_INVOICE_CREDIT_NOTE_BUTTON)
	WebElement manualInvoiceCreditNoteButton_field;

	// Client Receipt Payment Locators
	@FindBy(xpath = RECEIPT_PAYMENT_BUTTON)
	WebElement receiptPaymentButton_field;
	@FindBy(xpath = PAYMENT_AMOUNT)
	WebElement paymentAmount_field;
	@FindBy(xpath = PAYMENT_TYPE)
	WebElement paymentType_field;
	@FindBy(xpath = DEPARTMENT)
	WebElement department_field;
	@FindBy(xpath = REFERENCE)
	WebElement reference_field;
	@FindBy(xpath = RECEIPT_PAYMENT_SUBMIT_BUTTON)
	WebElement receiptPaymentSubmitButton_field;
	@FindBy(xpath = ALLOCATE_CHECKBOX_1)
	WebElement allocateCheckBox1_field;

	// Unapproved Receipt Locators
	@FindBy(xpath = UNAPPROVED_RECEIPT)
	WebElement unapprovedReceipt_field;
	@FindBy(xpath = CLIENT_PAID)
	WebElement clientPaid_field;

	// Run Statement Locators
	@FindBy(xpath = CLIENT_RUN_STATEMENT)
	WebElement clientRunStatement_field;
	@FindBy(xpath = CREDITOR_RUN_STATEMENT)
	WebElement creditorRunStatement_field;
	@FindBy(xpath = PREVIEW_STATEMENT_BUTTON)
	WebElement previewStatementButton_field;

	// Refund Payment Locators
	@FindBy(xpath = REFOUND_PAYMENT)
	WebElement refundPayment_field;
	@FindBy(xpath = REFUND_PAYMENT_BUTTONN)
	WebElement refundPaymentButton_field;

	// New Claim Locators
	@FindBy(xpath = NEW_CLAIM)
	WebElement newClaims_field;
	@FindBy(xpath = SELECT_POLICY_TRANSACTION)
	WebElement selectPolicyTransaction_field;
	@FindBy(xpath = SELECT_POLICY)
	WebElement selectPolicy_field;
	@FindBy(xpath = ADD_NEW_RESERVE)
	WebElement addNewReserve_field;
	@FindBy(xpath = ADD_THIRD_PARTY)
	WebElement addThirdParty_field;
	@FindBy(xpath = ADD_NEW_THIRD_PARTY)
	WebElement addNewThirdParty_field;
	@FindBy(xpath = ADD_NOTE_IN_CLAIM)
	WebElement addNoteInClaim_field;
	@FindBy(xpath = ADD_TASK_IN_CLAIM)
	WebElement addTaskInClaim_field;
	@FindBy(xpath = LOSS_SUMMARY)
	WebElement lossSummary_field;
	@FindBy(xpath = SUBMIT_CLAIM)
	WebElement submitClaim_field;
	@FindBy(xpath = ADD_RESERVE_DESCRIPTION)
	WebElement addReserveDescription_field;
	@FindBy(xpath = ADD_RESERVE_AMOUNT)
	WebElement addReserveAmount_field;
	@FindBy(xpath = ADD_RESERVE_EXCESS)
	WebElement addReserveExcess_field;
	@FindBy(xpath = APPROVED_CHECKBOX)
	WebElement approvedCheckBox_field;
	@FindBy(xpath = REASON_FOR_RESERVE_ADJUSTMENT)
	WebElement reasonForReserveAdjustment_field;
	@FindBy(xpath = ADD_RESERVE_SAVE_BUTTON)
	WebElement addReserveSaveButton_field;
	@FindBy(xpath = ADD_PAYMENT_ICON)
	WebElement addPaymentIcon_field;
	@FindBy(xpath = AUTHORISE_CHECHBOX)
	WebElement authoriseCheckbox_field;
	@FindBy(xpath = ADD_CLAIM_PAYMENT_SAVE_BUTTON)
	WebElement addClaimPaymentSaveButton_field;

	// Create New Creditor
	@FindBy(xpath = CREDITOR_TAB)
	WebElement creditorTab_field;
	@FindBy(xpath = CREDITOR_BUTTON)
	WebElement creditorButton_field;
	@FindBy(xpath = CREDITOR_TYPE)
	WebElement creditorType_field;
	@FindBy(xpath = TRADE_TERMS)
	WebElement tradeTerm_field;
	@FindBy(xpath = CREDITOR_CODE)
	WebElement creditorCode_field;
	@FindBy(xpath = CREDITOR_BRANCH)
	WebElement creditorBranch_field;
	@FindBy(xpath = CREDITOR_COMPANY_NAME)
	WebElement creditorCompanyName_field;
	@FindBy(xpath = CREDITOR_EMAIL)
	WebElement creditorEmail_field;
	@FindBy(xpath = CREDITOR_MAILING_SAME_PHYSICAL_CHECKBOX)
	WebElement creditorMailingSamePhysicalCheckbox_field;
	@FindBy(xpath = MAILING_ADDRESS_CREDITOR)
	WebElement mailingAddressCreditor_field;
	@FindBy(xpath = CREDITOR_SUBMIT_BUTTON)
	WebElement creditorSubmitButton_field;
	@FindBy(xpath = CREDITOR_TEXT_INPUT)
	WebElement creditorTextInput_field;
	@FindBy(xpath = CREDITOR_LIST_MANAGE_ICON)
	WebElement creditorListManageIcon_field;
	@FindBy(xpath = EDIT_CREDITOR_ICON)
	WebElement editCreditorIcon_field;
	@FindBy(xpath = CREDITOR_SAVE_CHANGE_BUTTON)
	WebElement creditorSaveChangeButton_field;
	@FindBy(xpath = CREDITOR_VIEW_ICON)
	WebElement creditorViewIcon_field;
	@FindBy(xpath = CREDITOR_DELETE_ICON)
	WebElement creditorDeleteIcon_field;
	@FindBy(xpath = CREDITOR_DELETE_CONFIRM_BUTTON)
	WebElement creditorDeleteConfirmButton_field;
	@FindBy(xpath = CREDITOR_SEARCH_BUTTON)
	WebElement creditorSearchButton_field;
	@FindBy(xpath = CREDITOR_CONTACTS_TAB)
	WebElement creditorContactsTab_field;

	public Basic_Processing_Page() {
		PageFactory.initElements(driver, this);
	}

	// Clear Client Transaction Data (Clear_Client_Data.java)
	public void clearClientTransactionData(String clientName, String key) throws Exception {
		clientName_field.sendKeys(clientName);
		Thread.sleep(2000);
		clientName_field.sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		key_field.sendKeys(key);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		submitButton_field.click();
		Thread.sleep(1000);
		driver.switchTo().alert().accept();
		Thread.sleep(1000);
		driver.switchTo().alert().accept();

		String clearClientData = driver.findElement(By.xpath("//div[@class='alert alert-info']")).getText();

		if (clearClientData.contains("Cleared transactional data for Ghost Inspector")) {
			ReportsClass.logStat(Status.PASS, "Clear Client Transactional Data is Successfully !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "Clear Client Transactional Data is Not Successfully !!!");
		}

		// Verify the client transactional data is cleared or not
		Assert.assertTrue(clearClientData.contains("Cleared transactional data for Ghost Inspector"),
				"Clear Client Transactional Data is Not Successfully !!!");
	}

	// Editing A Client (Editing_A_Client.java)
	public void editingAClient(String ihqClientName) throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFunction.navigateToClient(ihqClientName);
		driver.findElement(By.cssSelector(
				"body.nav-md:nth-child(2) div.container.body:nth-child(1) div.main_container div.right_col:nth-child(5) div.x_panel:nth-child(3) div.x_content div.row div.col-md-4.col-sm-12.col-xs-12.profile_details:nth-child(1) div.front-title:nth-child(2) > a.btn.btn-primary:nth-child(2)"))
				.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		Select drpGender = new Select(gender_field);
		try {
			drpGender.selectByValue("Male");
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		saveChangesButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(4000);
		viewIcon_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);
		String genderMale = driver.findElement(By.xpath("//div[contains(text(),'Male')]")).getText();
		if (genderMale.contains("Male")) {
			ReportsClass.logStat(Status.PASS, "Edit Client Successfully !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "Edit Client is not succesfully ");
		}

		// Verify the Client is edit successfully or not.
		Assert.assertTrue(genderMale.contains("Male"), "Edit Client Failed !!!");
	}

	// Add New Task (Add_New_Task.java)
	public void addNewTaskNotes(String TaskOrNote, String clientName, String assignedTo, String subject,
			String noteSection, String description) throws Exception {
		// Navigate to Dashboard
		navigateDashboard_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Click on AllTask Tab

		if (TaskOrNote.contains("Task")) {
			allTasks_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else {
			allNotes_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}

		// Click on Add Task Button
		addTaskButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Client Name
		addTaskClientName_field.sendKeys(clientName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
		addTaskClientName_field.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(1000);
		addTaskClientName_field.sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Assigned Task To
		Select drpAssignedTaskTo = new Select(assignedTo_field);

		try {
			drpAssignedTaskTo.selectByValue(assignedTo);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Select Note Section
		Select drpNoteSection = new Select(addTaskNoteSection_field);

		try {
			drpNoteSection.selectByValue(noteSection);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Enter Time
		addTaskTime_field.sendKeys("10:00");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Enter Subject
		addTaskSubject_field.sendKeys(subject);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Enter Description
		addTaskDescription_field.sendKeys(description);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Click on Save button
		addTaskSaveButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
		String alertMessage = driver.switchTo().alert().getText();

		if (TaskOrNote.contains("Task")) {
			if (alertMessage.contains("Manual Task added")) {
				ReportsClass.logStat(Status.PASS, "Task Added Successfully !!!");
			} else {
				ReportsClass.logStat(Status.FAIL, "Task is Not Added Successfully !!!");
			}

			// Verify Task is added successfully Or Not
			Assert.assertTrue(alertMessage.contains("Manual Task added"), "Task is Not Added Successfully !!!");
		} else {
			if (alertMessage.contains("Manual Note added")) {
				ReportsClass.logStat(Status.PASS, "Note Added Successfully !!!");
			} else {
				ReportsClass.logStat(Status.FAIL, "Note is Not Added Successfully !!!");
			}

			// Verify Task is added successfully Or Not
			Assert.assertTrue(alertMessage.contains("Manual Note added"), "Note is Not Added Successfully !!!");
		}

		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
	}

	// Create a New Client
	public void createNewClient(String salutation, String firstName, String lastName, String legalName, String gender,
			String emailAddress, String clientDOB, String mailingAddress) throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		clientButton_field.click();
		Thread.sleep(2000);

		// Create new client
		String sTempSalutation = CommonFunction.capitaliseFirstLetterSingleWord(salutation);
		System.out.println("Salutation : " + sTempSalutation);
		Select drpSalutation = new Select(salutation_field);
		drpSalutation.selectByValue(sTempSalutation);
		firstName_field.sendKeys(firstName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		lastName_field.sendKeys(lastName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		legalName_field.sendKeys(legalName);
		waitFor1Sec();

		String sTempGender = CommonFunction.capitaliseFirstLetterSingleWord(gender);
		System.out.println("Gender : " + sTempGender);

		Select drpGender = new Select(gender_field);
		drpGender.selectByValue(sTempGender);
		emailAddress_field.sendKeys(emailAddress);
		clientDOB_field.sendKeys(clientDOB, Keys.ESCAPE);
		Thread.sleep(2000);

		mailingAddress1_field.sendKeys(mailingAddress);
		Thread.sleep(2000);
		mailingAddress1_field.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		mailingAddress1_field.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		mailingAddressSameAsPhysicalAddress_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		saveChangesButton_field.click();
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String clientAddedMessage = driver
				.findElement(By.xpath("//div[contains(text(),'Client Has Been Successfully Added')]")).getText();
		if (clientAddedMessage.contains("Client Has Been Successfully Added")) {
			ReportsClass.logStat(Status.PASS, "Client Has Been Successfully Added !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "Client Has Not Been Successfully added !!!");
		}

		// Verify the client has been successfully added or not
		Assert.assertTrue(clientAddedMessage.contains("Client Has Been Successfully Added"),
				"Client Has Not Been Successfully added !!!");
		Thread.sleep(2000);
	}

	// Delete Contact
	public void deleteContact(String clientName) throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		ihqClientName_field.sendKeys(clientName);
		Thread.sleep(2000);
		searchButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='btnRemove']/i")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='_btnConfirm']")).click();
		Thread.sleep(3000);
	}

	// Create Contact To Ghost Client Or Creditor
	public void createContactToGhostClient_Or_Creditor(String clientOrCreditor, String clientOrCreditorName,
			String fName, String lName, String position, String mobile, String email, String address, String dob,
			String contactRelationship) throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		if (clientOrCreditor.equalsIgnoreCase("Creditor")) {
			creditorTab_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorTextInput_field.sendKeys(clientOrCreditorName);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorSearchButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorListManageIcon_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(2000);

			creditorContactsTab_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(2000);
		} else if (clientOrCreditor.equalsIgnoreCase("Client")) {
			ihqClientName_field.sendKeys(clientOrCreditorName);
			Thread.sleep(2000);
			ihqClientName_field.sendKeys(Keys.ENTER);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Thread.sleep(2000);

			clientContactTab_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		} else {
			System.out.println("Failed : " + clientOrCreditor);
		}
		addContactButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		firstName_field.sendKeys(fName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='last_name']")).sendKeys(lName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		contactPosition_field.sendKeys(position);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		contactMobile_field.sendKeys(mobile);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		contactEmail_field.sendKeys(email);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		contactAddress_field.sendKeys(address);
		Thread.sleep(2000);
		contactAddress_field.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		contactAddress_field.sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		contactDOB_field.sendKeys(dob, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		Select drpContactReleationship = new Select(contactReleationship_field);

		try {
			drpContactReleationship.selectByValue(contactRelationship);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		addButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		String contactAddedSuccessfully = driver
				.findElement(By.xpath("//div[contains(text(),'Contact Has Been Successfully Added')]")).getText();

		if (contactAddedSuccessfully.contains("Contact Has Been Successfully Added")) {
			if (clientOrCreditor.equalsIgnoreCase("Client")) {
				ReportsClass.logStat(Status.PASS, "Contact Has been Successfully Added for Ghost Client !!!");
			} else {
				ReportsClass.logStat(Status.PASS, "Contact Has been Successfully Added for Ghost Creditor !!!");
			}

		} else {
			if (clientOrCreditor.equalsIgnoreCase("Client")) {
				ReportsClass.logStat(Status.FAIL, "Contact Added Failed for Ghost Client !!!");
			} else {
				ReportsClass.logStat(Status.FAIL, "Contact Added Failed for Ghost Creditor !!!");
			}
		}

		// Verify contact has been successfully added for ghost client or
		// not
		if (clientOrCreditor.equalsIgnoreCase("Client")) {
			Assert.assertTrue(contactAddedSuccessfully.contains("Contact Has Been Successfully Added"),
					"Contact Added Failed for Ghost Client !!!");
		} else {
			Assert.assertTrue(contactAddedSuccessfully.contains("Contact Has Been Successfully Added"),
					"Contact Added Failed for Ghost Creditor !!!");
		}
		if (clientOrCreditor.equalsIgnoreCase("Client")) {

			// Delete Contact
			driver.findElement(By.xpath("//div[@class='container']//a[2]")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.switchTo().alert().accept();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(2000);
		} else if (clientOrCreditor.equalsIgnoreCase("Creditor")) {
			// Delete Creditor
			driver.findElement(By.xpath("//i[@class='fa fa-trash']")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.switchTo().alert().accept();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(2000);
		}

	}

	// Create A New Policy, Quote and Legacy
	public void createNewPolicy_Quote_Legacy_CoverNote(String type, String clientName, String policyClass,
			String townVillage, String sumInsured, String futureAnnualPremier, String sheetName, int rowNum)
			throws Exception {
		String make01 = reader.getCellData(sheetName, "Make_01", rowNum);
		String make02 = reader.getCellData(sheetName, "Make_02", rowNum);
		String year = reader.getCellData(sheetName, "Year", rowNum);
		String notification = reader.getCellData(sheetName, "Modification", rowNum);
		String anyFinanceOnCar = reader.getCellData(sheetName, "Any finance on the car?", rowNum);
		String doesTheCarHaveAnImmobiliser = reader.getCellData(sheetName, "Does the car have an immobiliser?", rowNum);
		String heldLicenceForOver10Years = reader.getCellData(sheetName, "Held licence for over 10 years", rowNum);
		String howManyAtFaultClaimHadInLast3Year = reader.getCellData(sheetName,
				"How many at fault claims have you had in the last 3 years", rowNum);
		String howManyLicenceSuspensionCancellationDisqualificationLast3Year = reader.getCellData(sheetName,
				"How many licence suspensions, cancellations, disqualifications, restrictions, or good driving periods have you had in the last 3 years",
				rowNum);
		String ageOfMainDriver = reader.getCellData(sheetName, "Age of main driver", rowNum);
		String carMainUse = reader.getCellData(sheetName, "Car main use", rowNum);
		String whereIsCarParked = reader.getCellData(sheetName, "Where is car parked", rowNum);

		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFunction.navigateToClient(clientName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.addTransactionButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		if (type.equalsIgnoreCase("Quote")) {
			newQuoteButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if (type.equalsIgnoreCase("Policy")) {
			cpPolicyPage.newPolicyButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if (type.equalsIgnoreCase("Ghost Policy")) {
			cpPolicyPage.newPolicyButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if (type.equalsIgnoreCase("Legacy")) {
			newLegacyButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if (type.equalsIgnoreCase("CoverNote")) {
			coverNoteButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else {
			Assert.assertTrue(
					type.equalsIgnoreCase("Quote") || type.equalsIgnoreCase("Policy") || type.equalsIgnoreCase("Legacy")
							|| type.equalsIgnoreCase("Ghost Policy"),
					"Failed to click on Quote OR Policy OR Legacy OR Ghost Policy from Add Transaction button !!!");
		}

		Select drpSelectPolicyClass = new Select(selectPolicyClass_field);

		try {
			drpSelectPolicyClass.selectByValue(policyClass);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		String getCurrentDate = CommonFunction.getCurrentDate();
		cpPolicyPage.inceptionDate_field.sendKeys(getCurrentDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.newPolicyContinueButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		cpPolicyPage.addLobButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		if (type.equalsIgnoreCase("Quote")) {
			addLobTownVillage_field.clear();
			addLobTownVillage_field.sendKeys(townVillage);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			addLobSumInsured_field.sendKeys(Keys.CONTROL, "a");
			addLobSumInsured_field.sendKeys(Keys.BACK_SPACE);
			addLobSumInsured_field.sendKeys(sumInsured);
			Thread.sleep(1000);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			addLobFutureAnnualPremier_field.sendKeys(Keys.CONTROL, "a");
			addLobFutureAnnualPremier_field.sendKeys(Keys.BACK_SPACE);
			addLobFutureAnnualPremier_field.sendKeys(futureAnnualPremier);
			Thread.sleep(1000);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if (type.equalsIgnoreCase("Ghost Policy")) {
			addLobTownVillage_field.clear();
			addLobTownVillage_field.sendKeys(townVillage);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			addLobSumInsured_field.sendKeys(Keys.CONTROL, "a");
			addLobSumInsured_field.sendKeys(Keys.BACK_SPACE);
			addLobSumInsured_field.sendKeys(sumInsured);
			Thread.sleep(1000);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			addLobFutureAnnualPremier_field.sendKeys(Keys.CONTROL, "a");
			addLobFutureAnnualPremier_field.sendKeys(Keys.BACK_SPACE);
			addLobFutureAnnualPremier_field.sendKeys(futureAnnualPremier);
			Thread.sleep(1000);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if (type.equalsIgnoreCase("CoverNote")) {
			addLobDescription_field.sendKeys("New Cover Note For MV");
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			addLobSumInsured_field.sendKeys(Keys.CONTROL, "a");
			addLobSumInsured_field.sendKeys(Keys.BACK_SPACE);
			addLobSumInsured_field.sendKeys(sumInsured);
			Thread.sleep(1000);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			addLobFutureAnnualPremier_field.sendKeys(Keys.CONTROL, "a");
			addLobFutureAnnualPremier_field.sendKeys(Keys.BACK_SPACE);
			addLobFutureAnnualPremier_field.sendKeys(futureAnnualPremier);
			Thread.sleep(1000);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if (type.equalsIgnoreCase("Policy") || type.equalsIgnoreCase("Legacy")) {
			updateAdditionalFieldButton_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Thread.sleep(3000);

			// Make Field 01
			Select drpMake01 = new Select(makeField01_field);

			try {
				drpMake01.selectByValue(make01);
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}

			// Make Field 02
			Select drpMake02 = new Select(makeField02_field);

			try {
				drpMake02.selectByValue(make02);
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}

			// Year
			year_field.sendKeys(year);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			// Nodifications
			if (notification.equalsIgnoreCase("Yes")) {
				notificationYes_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else {
				notificationNo_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}

			// Any finance on the car?
			if (anyFinanceOnCar.equalsIgnoreCase("Yes")) {
				anyFinanceOnTheCarYes_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else {
				anyFinanceOnTheCarNo_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}

			// Does the car have an immobiliser?
			if (doesTheCarHaveAnImmobiliser.equalsIgnoreCase("Factory")) {
				doesTheCarHaveAnImmobiliserFactory_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (doesTheCarHaveAnImmobiliser.equalsIgnoreCase("After Market")) {
				doesTheCarHaveAnImmobiliserAfterMarket_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else {
				doesTheCarHaveAnImmobiliserNo_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}

			// Insurance History

			// Held licence for over 10 years
			if (heldLicenceForOver10Years.equalsIgnoreCase("Yes")) {
				heldLicenceForOver10YearsYes_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else {
				heldLicenceForOver10YearsNo_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}

			// How many at fault claims have you had in the last 3 years
			if (howManyAtFaultClaimHadInLast3Year.equalsIgnoreCase("None")) {
				howManyFaulyClaimHadInLast3YearNone_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (howManyAtFaultClaimHadInLast3Year.equalsIgnoreCase("1")) {
				howManyFaulyClaimHadInLast3Year1_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (howManyAtFaultClaimHadInLast3Year.equalsIgnoreCase("2")) {
				howManyFaulyClaimHadInLast3Year2_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (howManyAtFaultClaimHadInLast3Year.equalsIgnoreCase("3")) {
				howManyFaulyClaimHadInLast3Year3_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else {
				howManyFaulyClaimHadInLast3Year4OrMore_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}

			// How many licence suspensions, cancellations, disqualifications,
			// restrictions, or good driving periods have you had in the last 3
			// years

			if (howManyLicenceSuspensionCancellationDisqualificationLast3Year.equalsIgnoreCase("None")) {
				howManyLicenceSuspensionCancellationDisqualificationInLast3YearNone_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (howManyLicenceSuspensionCancellationDisqualificationLast3Year.equalsIgnoreCase("1")) {
				howManyLicenceSuspensionCancellationDisqualificationInLast3Year1_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (howManyLicenceSuspensionCancellationDisqualificationLast3Year.equalsIgnoreCase("2")) {
				howManyLicenceSuspensionCancellationDisqualificationInLast3Year2_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (howManyLicenceSuspensionCancellationDisqualificationLast3Year.equalsIgnoreCase("3")) {
				howManyLicenceSuspensionCancellationDisqualificationInLast3Year3_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else {
				howManyLicenceSuspensionCancellationDisqualificationInLast3Year4OrMore_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}

			// Insured Details

			// Age of main driver
			ageOfMainDriver_field.sendKeys(ageOfMainDriver);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			// Car main use
			if (carMainUse.equalsIgnoreCase("Personal")) {
				carMainUsePersonal_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else {
				carMainUseBusiness_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}

			// Where is car parked
			if (whereIsCarParked.equalsIgnoreCase("Garaged/locked")) {
				whereIsCarParkedGaraged_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (whereIsCarParked.equalsIgnoreCase("Driveway")) {
				whereIsCarParkedDriveway_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else {
				whereIsCarParkedStreet_field.click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}

			quoteButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(2000);

			if (type.equalsIgnoreCase("Policy")) {
				addLobDescription_field.sendKeys("New Policy For MV");
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				addLobSumInsured_field.sendKeys(Keys.CONTROL, "a");
				addLobSumInsured_field.sendKeys(Keys.BACK_SPACE);
				addLobSumInsured_field.sendKeys(sumInsured);
				Thread.sleep(1000);
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

				addLobFutureAnnualPremier_field.sendKeys(Keys.CONTROL, "a");
				addLobFutureAnnualPremier_field.sendKeys(Keys.BACK_SPACE);
				addLobFutureAnnualPremier_field.sendKeys(futureAnnualPremier);
				Thread.sleep(1000);
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (type.equalsIgnoreCase("Legacy")) {
				addLobDescription_field.sendKeys("New Legacy For MV");
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else {
				System.out.println("Failed to Enter value in Description : " + type);
			}
		} else {
			Assert.assertTrue(type.equalsIgnoreCase("Quote") || type.equalsIgnoreCase("Type"),
					"Failed to select value for Quote or Policy after ADD LOB !!!");
		}

		commonFunction.addInsurer("131", "11", "AAA1212", "30 Days", "11", "10.00");

		addLobSaveButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(4000);

		if (type.equalsIgnoreCase("CoverNote")) {
			driver.switchTo().alert().accept();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Thread.sleep(4000);
		} else {
			System.out.println("");
		}

		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.switchTo().alert().accept();
		Thread.sleep(3000);

		// Verify that Policy, Quote, New Insurer Quote, Legancy is Created
		// successfully or Not
		commonFunction.verifyAddTransactionTypeCreatedSuccessfully(type);

		driver.findElement(By.xpath("//button[contains(text(),'Policy Overview')]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(4000);
	}

	// Create New Policy for FIRE Class
	public void createNewPolicy_FIRE(String clientName, String policyClass, String townVillage, String sumInsured,
			String futureAnnualPremier) throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFunction.navigateToClient(clientName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.addTransactionButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		cpPolicyPage.newPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		Select drpSelectPolicyClass = new Select(selectPolicyClass_field);

		try {
			drpSelectPolicyClass.selectByValue(policyClass);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		String getCurrentDate = CommonFunction.getCurrentDate();
		System.out.println("Current Date : " + getCurrentDate);
		cpPolicyPage.inceptionDate_field.sendKeys(getCurrentDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.newPolicyContinueButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		cpPolicyPage.addLobButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		addLobTownVillage_field.clear();
		addLobTownVillage_field.sendKeys(townVillage);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		addLobSumInsured_field.sendKeys(Keys.CONTROL, "a");
		addLobSumInsured_field.sendKeys(Keys.BACK_SPACE);
		addLobSumInsured_field.sendKeys(sumInsured);
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		addLobFutureAnnualPremier_field.sendKeys(Keys.CONTROL, "a");
		addLobFutureAnnualPremier_field.sendKeys(Keys.BACK_SPACE);
		addLobFutureAnnualPremier_field.sendKeys(futureAnnualPremier);
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		commonFunction.addInsurer("131", "11", "AAA1212", "30 Days", "11", "10.00");

		addLobSaveButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(4000);

		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.switchTo().alert().accept();
		Thread.sleep(3000);

		String policySuccessful = driver.findElement(By.xpath("//div//h3[contains(text(),'Policy Documents')]"))
				.getText();
		if (policySuccessful.contains("Policy Documents")) {
			ReportsClass.logStat(Status.PASS, "New Policy FIRE Successfully Created !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "New Policy FIRE Failed to Create !!!");
		}
		// Verify to Create New Quote Or Not
		Assert.assertTrue(policySuccessful.contains("Policy Documents"), "New Policy FIRE Failed to Create !!!");

		driver.findElement(By.xpath("//button[contains(text(),'Policy Overview')]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(4000);
	}

	// Create New Insurer Quote
	public void createNewInsurerQuote(String clientName, String policyClass, String sumInsured, String totalExcess,
			String creditorName, String brokerage) throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFunction.navigateToClient(clientName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.addTransactionButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		newInsurerQuoteButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);

		Select drpSelectPolicyClass = new Select(selectPolicyClass_field);

		try {
			drpSelectPolicyClass.selectByValue(policyClass);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		cpPolicyPage.newPolicyContinueButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		cpPolicyPage.addLobButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		addLobDescription_field.sendKeys("New Cover Note For MV");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		addLobSumInsured_field.sendKeys(Keys.CONTROL, "a");
		addLobSumInsured_field.sendKeys(Keys.BACK_SPACE);
		addLobSumInsured_field.sendKeys(sumInsured);
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		excess_field.sendKeys(Keys.CONTROL, "a");
		excess_field.sendKeys(Keys.BACK_SPACE);
		excess_field.sendKeys(totalExcess);
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		addLobSaveButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);

		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(4000);

		// Add Insurer
		addInsurerButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);

		Select drpInsurer = new Select(selectCreditorFromAddInsurer_field);

		try {
			drpInsurer.selectByValue(creditorName);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		addInsurerBrokerage_field.sendKeys(brokerage);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		saveButtonAddInsurerFromQuote_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(3000);

		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.switchTo().alert().accept();
		Thread.sleep(3000);

		String insurerQuoteSuccessful = driver.findElement(By.xpath("//div//h3[contains(text(),'Policy Documents')]"))
				.getText();
		if (insurerQuoteSuccessful.contains("Policy Documents")) {
			ReportsClass.logStat(Status.PASS, "New Insurer Quote Successfully Created !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "New Insurer Quote Failed to Create !!!");
		}

		// Verify to Create New Quote Or Not
		Assert.assertTrue(insurerQuoteSuccessful.contains("Policy Documents"),
				"New Insurer Quote Failed to Create !!!");

		driver.findElement(By.xpath("//button[contains(text(),'Policy Overview')]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(4000);
	}

	// Converting a Policy
	public void convertPolicy(String convertPolicyOption, String sumInsured, String futureAnnualPremier)
			throws Exception {
		if (convertPolicyOption.contains("Renew") || convertPolicyOption.contains("Reversal")
				|| convertPolicyOption.contains("Reinstate") || convertPolicyOption.contains("Full Cancellation")) {
			contactNavigation_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			commonFunction.navigateToClient("Ghost Inspector");
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}

		manageIconForEndorsePolicy_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
		if (convertPolicyOption.contains("Cancel ")) {
			driver.findElement(By.xpath("//a[contains(text(),'" + convertPolicyOption + "')]")).click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else {
			driver.findElement(By.xpath("//a[contains(text(),'" + convertPolicyOption + "')]")).click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}

		confirmButtonWhileConvertPolicy_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);

		if (convertPolicyOption.contains("Full Cancellation")) {
			System.out.println("");
		} else {
			addLobEditIcon_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(2000);

			driver.findElement(
					By.xpath("//div[@class='text-right']//button[@class='btn btn-primary'][contains(text(),'No')]"))
					.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			
			
			addLobSaveButton_field.click();
			
			
			
			
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Thread.sleep(4000);
		}

		bindPolicyConvertPolicy_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(1000);

		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		Thread.sleep(2000);

		// Verify that Convert Policy Successfully Or Not
		commonFunction.verifyConvertPolicy(convertPolicyOption);

	}

	// Maintain the Policy
	public void maintainPolicy() throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFunction.navigateToClient("Ghost Inspector");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		manageIconForEndorsePolicy_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		driver.findElement(By.xpath("//a[contains(text(),'Maintain')]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		confirmButtonWhileConvertPolicy_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);

		driver.findElement(By.xpath("//button[contains(text(),'Update')]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);

		if (driver.findElement(By.xpath("//div[@id='policy_list']")) != null) {
			ReportsClass.logStat(Status.PASS, "Maintain a Policy Successfully !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "Failed to Maintain a Policy !!!");
		}

		// Verify that Maintain a Policy is successfully or Not
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='policy_list']")) != null,
				"Failed to Maintain a Policy !!!");
	}

	// Create a Manual Invoice
	public void createManualInvoice(String clientOrCreditor, String clientOrCreditorName, String transactionType,
			String currency, String branch, String detail, String description, String exclusiveAmount)
			throws Exception {

		if (clientOrCreditor.equalsIgnoreCase("Creditor")) {
			contactNavigation_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			creditorTab_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorTextInput_field.sendKeys(clientOrCreditorName);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorSearchButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorListManageIcon_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(2000);
		} else if (clientOrCreditor.equalsIgnoreCase("Client")) {
			commonFunction.navigateToAccountTransaction(clientOrCreditorName);
		} else {
			System.out.println("Failed to Navigate Client Or Creditor : " + clientOrCreditor);
		}

		if (clientOrCreditor.equalsIgnoreCase("Creditor")) {
			driver.findElement(By.xpath("//button[@name='create_invoice']")).click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if (clientOrCreditor.equalsIgnoreCase("Client")) {
			createInvoiceButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else {
			System.out.println("Failed : " + clientOrCreditor);
		}

		// Select Invoice Transaction Type
		Select drpTransactionType = new Select(transactionType_field);

		try {
			drpTransactionType.selectByValue(transactionType);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Select Invoice Currency
		Select drpCurrency = new Select(currency_field);

		try {
			drpCurrency.selectByValue(currency);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println();
		}

		// Select Invoice Branch
		Select drpBranch = new Select(branch_field);

		try {
			drpBranch.selectByValue(branch);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println();
		}

		// Due Date
		LocalDate mydate = LocalDate.now();
		mydate = mydate.plusMonths(1);
		String nextMonthDate = mydate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		driver.findElement(By.id("due_date")).sendKeys(nextMonthDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		detail_field.sendKeys(detail);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		addInvoiceLineButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		invoiceLineDescription_field.sendKeys(description);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		exclusiveAmount_field.sendKeys(exclusiveAmount);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		exclusiveAmount_field.sendKeys(Keys.TAB);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		vagstAmount_field.clear();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		manualInvoiceCreditNoteButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		driver.switchTo().alert().accept();
		Thread.sleep(3000);

		// Verify that Create a Manual Invoice Successfully or Not.

		if (clientOrCreditor.equalsIgnoreCase("Creditor")) {
			commonFunction.verifyAccountTransactionModuleFunctionality(detail, "",
					"Create a Manual Invoice Successfully for Creditor !!!",
					"Failed to Create a Manual Invoice for Creditor!!!");
		} else if (clientOrCreditor.equalsIgnoreCase("Client")) {
			commonFunction.verifyAccountTransactionModuleFunctionality(detail, "",
					"Create a Manual Invoice Successfully for Client !!!",
					"Failed to Create a Manual Invoice for Client!!!");
		} else {
			System.out.println("Failed : " + clientOrCreditor);
		}
	}

	// Client Receipt Payment
	public void receiptPayment(String testName, String clientName, String paymentAmount, String paymentType,
			String department, String reference) throws Exception {
		commonFunction.navigateToAccountTransaction(clientName);

		receiptPaymentButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Enter Payment Amount
		paymentAmount_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		paymentAmount_field.sendKeys(Keys.CONTROL, "a");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		paymentAmount_field.sendKeys(Keys.BACK_SPACE);
		paymentAmount_field.sendKeys(Keys.BACK_SPACE);
		paymentAmount_field.sendKeys(paymentAmount);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Payment Type
		Select drpPaymentType = new Select(paymentType_field);

		try {
			drpPaymentType.selectByValue(paymentType);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("");
		}

		// Select Department
		Select drpDepartment = new Select(department_field);

		try {
			drpDepartment.selectByValue(department);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("");
		}

		// Enter Reference
		reference_field.sendKeys(reference);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		allocateCheckBox1_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		receiptPaymentSubmitButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);

		// Verify that Receipt Payment Successfully or Not
		commonFunction.verifyAccountTransactionModuleFunctionality(reference, "", "Receipt Payment Successfully !!!",
				"Failed Receipt Payment !!!");
	}

	// Client Unapproved Receipt
	public void unapprovedReceipt(String clientName, String paymentAmount, String paymentType, String department,
			String reference) throws Exception {
		commonFunction.navigateToAccountTransaction(clientName);
		unapprovedReceipt_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Enter Payment Amount
		paymentAmount_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		paymentAmount_field.sendKeys(Keys.CONTROL, "a");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		paymentAmount_field.sendKeys(Keys.BACK_SPACE);
		paymentAmount_field.sendKeys(Keys.BACK_SPACE);
		paymentAmount_field.sendKeys(paymentAmount);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Payment Type
		Select drpPaymentType = new Select(paymentType_field);

		try {
			drpPaymentType.selectByValue(paymentType);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		clientPaid_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Department
		Select drpDepartment = new Select(department_field);

		try {
			drpDepartment.selectByValue(department);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Enter Reference
		reference_field.sendKeys(reference);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		receiptPaymentSubmitButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);

		// Verify that UnApproved Receipt Payment Successfully or Not.
		commonFunction.verifyAccountTransactionModuleFunctionality(reference, "PAY",
				"UnApproved Receipt Payment Successfully !!!", "Failed UnApproved Receipt Payment !!!");
	}

	// Run Statement
	public void runStatement(String runStatementFor, String client_Creditor_Name) throws Exception {
		if (runStatementFor.equalsIgnoreCase("Creditor")) {
			contactNavigation_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			creditorTab_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorTextInput_field.sendKeys(client_Creditor_Name);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorSearchButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorListManageIcon_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(3000);

			creditorRunStatement_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(3000);

			WebElement creditorCurreny_field = driver.findElement(By.xpath("//select[@id='srh_currency_id']"));
			Select drpCreditorCurrency = new Select(creditorCurreny_field);

			try {
				drpCreditorCurrency.selectByValue("1");
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} catch (Exception e) {

			}
		} else if (runStatementFor.equalsIgnoreCase("Client")) {
			commonFunction.navigateToAccountTransaction(client_Creditor_Name);
			clientRunStatement_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);
		} else {
			System.out.println("Failed to Run Statement for : " + runStatementFor);
		}

		driver.findElement(By.id("from_date")).sendKeys("01/01/2017", Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		String getCurrentDate = CommonFunction.getCurrentDate();
		driver.findElement(By.id("to_date")).sendKeys(getCurrentDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		previewStatementButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);

		if (runStatementFor.equalsIgnoreCase("Creditor")) {
			WebElement getCreditorPreviewStatement = driver.findElement(By.xpath("//div[@class='x_content']"));
			if (getCreditorPreviewStatement != null) {
				ReportsClass.logStat(Status.PASS, "Creditor Run Statement Preview Successfully Displayed !!!");
			} else {
				ReportsClass.logStat(Status.FAIL, "Failed to Preview Creditor Run Statement !!!");
			}

			// Verify that Preview Creditor Run Statement is success or not
			Assert.assertTrue(getCreditorPreviewStatement != null, "Failed to Preview Creditor Run Statement !!!");

		} else if (runStatementFor.equalsIgnoreCase("Client")) {
			WebElement getClientPreviewStatement = driver
					.findElement(By.xpath("//div[@class='col-xs-12 table-responsive']"));
			if (getClientPreviewStatement != null) {
				ReportsClass.logStat(Status.PASS, "Client Run Statement Preview Successfully Displayed !!!");
			} else {
				ReportsClass.logStat(Status.FAIL, "Failed to Preview Client Run Statement !!!");
			}

			// Verify that Preview Client Run Statement is success or not
			Assert.assertTrue(getClientPreviewStatement != null, "Failed to Preview Client Run Statement !!!");
		} else {
			System.out.println("Failed to Run Statement for : " + runStatementFor);
		}
	}

	// Refund Payment
	public void refundPayment(String clientName, String paymentAmount, String paymentType, String department,
			String reference) throws Exception {
		commonFunction.navigateToAccountTransaction(clientName);
		refundPayment_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Enter Payment Amount
		paymentAmount_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		paymentAmount_field.sendKeys(Keys.CONTROL, "a");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		paymentAmount_field.sendKeys(Keys.BACK_SPACE);
		paymentAmount_field.sendKeys(Keys.BACK_SPACE);
		paymentAmount_field.sendKeys(paymentAmount);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Payment Type
		Select drpPaymentType = new Select(paymentType_field);

		try {
			drpPaymentType.selectByValue(paymentType);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		clientPaid_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Department
		Select drpDepartment = new Select(department_field);

		try {
			drpDepartment.selectByValue(department);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Enter Reference
		reference_field.sendKeys(reference);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		refundPaymentButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);

		commonFunction.verifyAccountTransactionModuleFunctionality(reference, "REFUND",
				"Refund Payment Successfully !!!", "Failed Refund Payment !!!");
	}

	// Create New Claim
	public void createNewClaim_OR_AddReserve_OR_AddPayment(String type, String clientName, String description,
			String reserveAmount, String excess, String reasonForReserveAdjustment, String paymentAmount,
			String paymentType) throws Exception {
		commonFunction.navigateToClaims(clientName);
		newClaims_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		Thread.sleep(1000);
		/*
		 * driver.close(); driver.switchTo().window(tabs2.get(0));
		 */
		String todayDate = CommonFunction.getCurrentDate();
		driver.findElement(By.id("loss_date")).sendKeys(todayDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Policy
		selectPolicyTransaction_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		selectPolicy_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		WebElement addNewResearve = driver.findElement(By.xpath("//button[@id='add_new_reserve_btn']"));
		if (type.equalsIgnoreCase("Add Reserve")) {
			// Add Reserve
			addNewReserve_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Thread.sleep(2000);

			addReserveDescription_field.sendKeys(description);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			addReserveAmount_field.click();
			Thread.sleep(1000);
			addReserveAmount_field.sendKeys(Keys.CONTROL, "a");
			Thread.sleep(1000);
			addReserveAmount_field.sendKeys(Keys.BACK_SPACE);
			addReserveAmount_field.sendKeys(Keys.BACK_SPACE);
			addReserveAmount_field.sendKeys(reserveAmount);
			Thread.sleep(1000);
			addReserveAmount_field.sendKeys(Keys.TAB);

			addReserveExcess_field.click();
			Thread.sleep(1000);
			addReserveExcess_field.sendKeys(Keys.CONTROL, "a");
			Thread.sleep(1000);
			addReserveExcess_field.sendKeys(Keys.BACK_SPACE);
			addReserveExcess_field.sendKeys(Keys.BACK_SPACE);
			addReserveExcess_field.sendKeys(excess);

			approvedCheckBox_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			reasonForReserveAdjustment_field.sendKeys(reasonForReserveAdjustment);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			addReserveSaveButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(3000);

			// Add Payment
			addPaymentIcon_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(3000);

			paymentAmount_field.sendKeys(paymentAmount);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			authoriseCheckbox_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);
			/*
			 * driver.switchTo().alert().accept(); Thread.sleep(1000);
			 */

			// Payment Type
			Select drpPaymentType = new Select(paymentType_field);

			try {
				drpPaymentType.selectByValue(paymentType);
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}
			addClaimPaymentSaveButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			String addReservePaymentMessage = "Claim Payment Has Been Added Successfully.";
			String getPaymentTypeSuccess = driver
					.findElement(By
							.xpath("//*[contains(text(),'Claim Payment Has Been Added Successfully.')]"))
					.getText();
			System.out.println("Message form Web :"+getPaymentTypeSuccess);
			if (getPaymentTypeSuccess.contains(addReservePaymentMessage)) {
				ReportsClass.logStat(Status.PASS, "Add Payment Successfully !!!");
				System.out.println("ReservePaymentMessage Test Case Pass :");
			} else {
				ReportsClass.logStat(Status.FAIL, "Failed To Add Payment !!!");
				System.out.println("ReservePaymentMessage Test Case Fail :");
			}

			// Verify Payment is added successfully or Not
			Assert.assertTrue(getPaymentTypeSuccess.contains(addReservePaymentMessage), "Failed To Add Payment !!!");
		}

		lossSummary_field.sendKeys("New Claim Loss Summary");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
		submitClaim_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
		String actualGetClaimSuccessMsg = driver
				.findElement(By.xpath("//*[contains(text(),'Claim Has Been Successfully Submitted.')]")).getText();
		String expectedGetClaimSuccessMsg = "Claim Has Been Successfully Submitted.";

		System.out.println("Cliam Success Message :"+actualGetClaimSuccessMsg);
		
		if (actualGetClaimSuccessMsg.contains(expectedGetClaimSuccessMsg)) {
			ReportsClass.logStat(Status.PASS, "Claims Has Been Created Successfully !!!");
			System.out.println( " Test Case Pass :Claim Has Been Successfully Submitted. !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "Failed to Create Claim !!!");
			System.out.println("Test Case Failed :ClaimSuccessMsg Failed !!!");
		}

		// Verify that Claim is Created or Not
		Assert.assertTrue(actualGetClaimSuccessMsg.contains(expectedGetClaimSuccessMsg),
				"Failed to Create Claim !!!");

		driver.findElement(By.xpath("//button[contains(text(),'Claim Overview')]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);
	}

	// Add New Creditor
	public void addNewCreditor(String creditorType, String tradeTerms, String creditorCode, String branch,
			String companyName, String emailAddress, String mailingAddress) throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		creditorTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		creditorButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Creditor Type
		Select drpCreditorType = new Select(creditorType_field);

		try {
			drpCreditorType.selectByValue(creditorType);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Select Trade Terms
		Select drpTradeTerms = new Select(tradeTerm_field);

		try {
			drpTradeTerms.selectByValue(tradeTerms);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Enter Creditor Code
		creditorCode_field.sendKeys(creditorCode);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Branch
		Select drpBranch = new Select(creditorBranch_field);

		try {
			drpBranch.selectByValue(branch);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Enter Company Name
		creditorCompanyName_field.sendKeys(companyName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Enter Email Address
		creditorEmail_field.sendKeys(emailAddress);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Enter Mailing Address 1
		mailingAddressCreditor_field.sendKeys(mailingAddress);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		mailingAddressCreditor_field.sendKeys(Keys.ARROW_DOWN);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		mailingAddressCreditor_field.sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		creditorMailingSamePhysicalCheckbox_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		creditorSubmitButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		creditorTextInput_field.sendKeys(companyName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		creditorSearchButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		String getCreditorDetails = driver.findElement(By.xpath("//table[@class='table table-striped']")).getText();

		if (getCreditorDetails.contains(companyName)) {
			ReportsClass.logStat(Status.PASS, "Creditor Created Successfully !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "Failed to Created Creditor !!!");
		}

		// Verify that creditor is created or Not.
		Assert.assertTrue(getCreditorDetails.contains(companyName), "Failed to Created Creditor !!!");
	}

	// Edit Creditor
	public void edit_Delete_Creditor(String operation, String companyName, String creditorType, String emailAddress)
			throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		creditorTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		creditorTextInput_field.sendKeys(companyName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		creditorSearchButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		if (operation.equalsIgnoreCase("Delete Creditor")) {
			creditorDeleteIcon_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);
			creditorDeleteConfirmButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			String getCreditorDeleteMsg = driver
					.findElement(By.xpath("//div[contains(text(),'Creditor Has Been Successfully Deleted')]"))
					.getText();

			if (getCreditorDeleteMsg.contains("Creditor Has Been Successfully Deleted")) {
				ReportsClass.logStat(Status.PASS, "Creditor Has Been Successfully Deleted !!!");
			} else {
				ReportsClass.logStat(Status.FAIL, "Failed to delete Creditor !!!");
			}

			// Verify that Creditor has been successfully deleted or Not.
			Assert.assertTrue(getCreditorDeleteMsg.contains("Creditor Has Been Successfully Deleted"),
					"Failed to delete Creditor !!!");
		} else {
			creditorListManageIcon_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(2000);

			editCreditorIcon_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			// Select Creditor Type
			Select drpCreditorType = new Select(creditorType_field);

			try {
				drpCreditorType.selectByValue(creditorType);
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}

			// Enter Email Address
			creditorEmail_field.sendKeys(Keys.CONTROL, "a");
			Thread.sleep(1000);
			creditorEmail_field.sendKeys(Keys.BACK_SPACE);
			Thread.sleep(1000);
			creditorEmail_field.sendKeys(Keys.BACK_SPACE);
			creditorEmail_field.sendKeys(emailAddress);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			creditorSaveChangeButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(1000);

			creditorViewIcon_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			String getEmailAfterEdit = driver
					.findElement(By.xpath("//div[contains(text(),'edit_ghost@insuredhq.com')]")).getText();

			if (getEmailAfterEdit.contains(emailAddress)) {
				ReportsClass.logStat(Status.PASS, "Creditor is Updated Successfully !!!");
			} else {
				ReportsClass.logStat(Status.FAIL, "Failed to Edit Creditor !!!");
			}

			// Verify that Creditor has been updated or Not
			Assert.assertTrue(getEmailAfterEdit.contains(emailAddress), "Failed to Edit Creditor !!!");
		}
	}

	// Create New Policy for FIRE Class
	public void navigateToAddLOBForCustomAPILogs(String clientName, String policyClass, String townVillage,
			String sumInsured, String futureAnnualPremier) throws Exception {
		cpPolicyPage.addTransactionButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		cpPolicyPage.newPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		String getCurrentDate = CommonFunction.getCurrentDate();
		System.out.println("Current Date : " + getCurrentDate);
		cpPolicyPage.inceptionDate_field.sendKeys(getCurrentDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.newPolicyContinueButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		cpPolicyPage.addLobButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
}
}