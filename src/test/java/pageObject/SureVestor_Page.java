package pageObject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import baseClass.TestBase;
import locators.Locators.baileysLocators;
import locators.Locators.sureVestorLocators;
import utility.ReadPropertyConfig;
import utility.Xls_Reader;

public class SureVestor_Page extends TestBase implements sureVestorLocators, baileysLocators {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String sureVestorExcelPath = readPro.getSureVestorExcel();
	Xls_Reader reader = new Xls_Reader(sureVestorExcelPath);

	// Scheer Landlord Protection Insurance Policy Locators
	@FindBy(id = "company_name_client")
	WebElement landlordName_field;
	@FindBy(id = "gender")
	WebElement gender_field;
	@FindBy(id = "mailing_address1_client")
	WebElement mailingAddress1_field;
	@FindBy(id = "mailing_city_client")
	WebElement mailingCity_field;
	@FindBy(id = "mailing_postcode_client")
	WebElement mailingRegion_field;
	@FindBy(id = "physical_same_as_mailing_client")
	WebElement physicalSameAsMailingAddressCheckbox_field;
	@FindBy(id = "country")
	WebElement country_field;
	@FindBy(id = "phonecountrycode")
	WebElement phoneCountryCode_field;
	@FindBy(id = "phoneareacode")
	WebElement phoneAreaCode_field;
	@FindBy(id = "phone")
	WebElement phone_field;
	@FindBy(id = "email")
	WebElement emailAddress_field;
	@FindBy(xpath = S_NEXT_BUTTON)
	WebElement nextButton_field;

	// Property Management Agent
	@FindBy(xpath = NAME_OF_PROPERTY_MANAGEMENT_AGENCY)
	WebElement nameOfPropertyManagementAgency_field;
	@FindBy(xpath = PROPERTY_MANAGER)
	WebElement propertyManager_field;

	// Residential Rental Property to be Insured
	@FindBy(xpath = TYPE_OF_DWELLING)
	WebElement typeOfDwelling_field;
	@FindBy(xpath = ADDRESS1)
	WebElement address1_field;
	@FindBy(xpath = CITY)
	WebElement city_field;
	@FindBy(xpath = STATE)
	WebElement state_field;
	@FindBy(xpath = STATE_CODE)
	WebElement stateCode_field;
	@FindBy(xpath = ZIP_CODE)
	WebElement zipCode_field;

	// Please answer the following questions.
	@FindBy(xpath = HAS_THIS_TENANT_BEEN_BEHIND_MORE_THAN_5_DAYS_LAST_2_MONTH_NO)
	WebElement hasThisTenantBeenBehindMoreThan5DayLast2MonthNo_field;
	@FindBy(xpath = WHAT_IS_THE_MONTHLY_RENT_FOR_RENTAL_PROPERTY)
	WebElement whatIsTheMonthlyRentForRentalProperty_field;
	@FindBy(xpath = HAS_THE_RENTAL_PROPERTY_UNOCCUPIED_FOR_60_DAYS_MORE_NO)
	WebElement hasTheRentalPropertyUnoccupiedFor60DayMoreNo_field;

	// Insurance Benefits Required
	@FindBy(xpath = LEVEL_OF_BENEFITS_REQUIRED)
	WebElement levelOfBenefitsRequired_field;
	@FindBy(xpath = DO_YOU_WISH_TO_ADD_TERRORISM_POLICY_AN_ADDITIONAL_PREMIUM_NO)
	WebElement doYouWishToAddTerrorismPolicyAnAdditionalPremiumNo_field;
	@FindBy(xpath = DO_YOU_WISH_TO_ADD_TERRORISM_POLICY_AN_ADDITIONAL_PREMIUM_YES)
	WebElement doYouWishToAddTerrorismPolicyAnAdditionalPremiumYes_field;
	@FindBy(xpath = SUBMIT_BUTTON)
	WebElement submitButton_field;

	public SureVestor_Page() {
		PageFactory.initElements(driver, this);
	}

	public void scheerLandlordProtectionInsurancePolicy(String sheetName, int rowNum) {
		String landLordName = reader.getCellData(sheetName, "Landlord Name", rowNum);
		String gender = reader.getCellData(sheetName, "Gender", rowNum);
		String mailingAddress1 = reader.getCellData(sheetName, "Mailing Address1", rowNum);
		String mailingCity = reader.getCellData(sheetName, "Mailing City", rowNum);
		String mailingRegion = reader.getCellData(sheetName, "Mailing Region", rowNum);
		String country = reader.getCellData(sheetName, "Country", rowNum);
		String phone = reader.getCellData(sheetName, "Phone", rowNum);
		String emailAddress = reader.getCellData(sheetName, "Email Address", rowNum);

		// Landlord Name
		landlordName_field.sendKeys(landLordName);

		// Gender
		Select drpGender = new Select(gender_field);
		try {
			drpGender.selectByValue(gender);
		} catch (Exception e) {
			System.out.println("");
		}

		// Mailing Address 1
		mailingAddress1_field.sendKeys(mailingAddress1);

		// Mailing City
		mailingCity_field.sendKeys(mailingCity);

		// Mailing Region
		mailingRegion_field.sendKeys(mailingRegion);

		// Physical same as Mailing address
		physicalSameAsMailingAddressCheckbox_field.click();

		// Country
		Select drpCountry = new Select(country_field);
		try {
			drpCountry.selectByValue(country);
		} catch (Exception e) {
			System.out.println("");
		}

		// Phone
		String strPhone[] = phone.split("-");
		String countryCode = strPhone[0];
		String areaCode = strPhone[1];
		String phoneNum = strPhone[2];

		phoneCountryCode_field.sendKeys("+" + countryCode);
		phoneAreaCode_field.sendKeys(areaCode);
		phone_field.sendKeys(phoneNum);

		// Email Address
		emailAddress_field.sendKeys(emailAddress);

		// nextButton_field.click();
		nextButton_field.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	// Property Management Agent
	public void propertyManagementAgent(String sheetName, int rowNum) throws Exception {
		String nameOfPropertyManagementAgency = reader.getCellData(sheetName, "Name of Property Management Agency",
				rowNum);
		String propertyManager = reader.getCellData(sheetName, "Property Manager", rowNum);

		// Name of Property Management Agency
		nameOfPropertyManagementAgency_field.sendKeys(nameOfPropertyManagementAgency);
		Thread.sleep(1000);
		nameOfPropertyManagementAgency_field.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(1000);
		nameOfPropertyManagementAgency_field.sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Property Manager
		Select drpPropertyManager = new Select(propertyManager_field);
		try {
			drpPropertyManager.selectByValue(propertyManager);
		} catch (Exception e) {
			System.out.println("");
		}
	}

	// Residential Rental Property to be Insured
	public void residentialRentalPropertyToBeInsured(String sheetName, int rowNum) {
		String typeOfDwelling = reader.getCellData(sheetName, "Type of Dwelling", rowNum);
		String address1 = reader.getCellData(sheetName, "Address1", rowNum);
		String city = reader.getCellData(sheetName, "City", rowNum);
		String state = reader.getCellData(sheetName, "State", rowNum);
		String stateCode = reader.getCellData(sheetName, "State Code", rowNum);
		String zipCode = reader.getCellData(sheetName, "ZipCode", rowNum);

		// Type of Dwelling
		Select drpTypeOfDwelling = new Select(typeOfDwelling_field);
		try {
			drpTypeOfDwelling.selectByValue(typeOfDwelling);
		} catch (Exception e) {
			System.out.println("");
		}

		// Address 1
		address1_field.clear();
		address1_field.sendKeys(address1);

		// City
		city_field.clear();
		city_field.sendKeys(city);

		// State
		Select drpState = new Select(state_field);
		try {
			drpState.selectByValue(state);
		} catch (Exception e) {
			System.out.println("");
		}

		// State code
		stateCode_field.clear();
		stateCode_field.sendKeys(stateCode);

		// Zip Code
		zipCode_field.clear();
		zipCode_field.sendKeys(zipCode);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	// Please answer the following questions
	public void pleaseAnswerFollowingQuestions(String sheetName, int rowNum) throws Exception {
		String whatIsTheMonthlyRentForRentalProperty = reader.getCellData(sheetName,
				"What is the monthly rent for rental property?", rowNum);

		hasThisTenantBeenBehindMoreThan5DayLast2MonthNo_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		whatIsTheMonthlyRentForRentalProperty_field.clear();
		whatIsTheMonthlyRentForRentalProperty_field.sendKeys(whatIsTheMonthlyRentForRentalProperty);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		hasTheRentalPropertyUnoccupiedFor60DayMoreNo_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(2000);
	}

	// Insurance Benefits Required
	public void insuranceBenefitsRequired(String sheetName, int rowNum) throws Exception {
		String levelOfBenefitsRequired = reader.getCellData(sheetName, "Level of benefits required", rowNum);
		String doYouWishToAddTerrorismCoverToYourPolicyAdditionalForm = reader.getCellData(sheetName,
				"Do you wish to add terrorism cover to your policy additional form", rowNum);

		// Level of benefits required
		Select drpLevelOfRequiredBenefits = new Select(levelOfBenefitsRequired_field);
		try {
			drpLevelOfRequiredBenefits.selectByValue(levelOfBenefitsRequired);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Do you wish to add terrorism cover to your policy for an additional
		// premium?
		if (doYouWishToAddTerrorismCoverToYourPolicyAdditionalForm.equalsIgnoreCase("Yes")) {
			doYouWishToAddTerrorismPolicyAnAdditionalPremiumYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} else {
			doYouWishToAddTerrorismPolicyAnAdditionalPremiumNo_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		// Commencement Date
		driver.findElement(By.xpath("//*[@id='date_input_46']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div[4]/div[1]/table/tbody/tr[4]/td[2]")).click();

		Thread.sleep(2000);
		submitButton_field.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id='form_ul']/div[2]/table/tbody/tr/td/button[1]")).click();
	}
}