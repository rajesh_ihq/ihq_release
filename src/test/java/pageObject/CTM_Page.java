package pageObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import baseClass.TestBase;
import locators.Locators.carpeeshLocators;
import locators.Locators.ctmLocators;
import utility.ReadPropertyConfig;
import utility.Xls_Reader;

public class CTM_Page extends TestBase implements ctmLocators, carpeeshLocators {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String ctmExcel = readPro.getCtmExcel();
	String jsonExcel = readPro.getJSONExcelFile();

	Xls_Reader reader = new Xls_Reader(ctmExcel);
	Xls_Reader reader1 = new Xls_Reader(jsonExcel);

	JavascriptExecutor js = (JavascriptExecutor) driver;

	// CTM JSON Form Locators
	@FindBy(xpath = CTM_JSON_TEXTAREA) WebElement ctmJsonTextArea_field;
	@FindBy(xpath = CTM_PASSCODE) WebElement ctmPasscode_field;
	@FindBy(xpath = CTM_SUBMIT_BUTTON) WebElement ctmSubmitButton_field;
	@FindBy(id = "pwbox-377") WebElement carpeeshProtectedPassword_field;
	@FindBy(xpath = CARPEESH_ENTER_BUTTON) WebElement carPeeshEnterButton_field;

	// CTM Online Quote Form Locators
	@FindBy(xpath = CTM_I_AGREE_BUTTON) WebElement ctmIAgreeButton_field;
	@FindBy(id = "salutation") WebElement saluTation;
	@FindBy(id = "mobile") WebElement mobile_field;
	@FindBy(id = "email") WebElement email_field;
	@FindBy(id = "confirm_email") WebElement confirmEmail_field;
	@FindBy(id = "btn_save_client") WebElement Step1_Next_Button;
	@FindBy(xpath = PREVIOUS_INSURER) WebElement previousInsurer_field;
	@FindBy(xpath = ESTIMATED_ANNUAL_KILOMETER) WebElement estimatedAnnualKM_field;
	@FindBy(xpath = CAR_REGISTRATION_PLAT) WebElement carRegistrationPlat_field;
	@FindBy(xpath = CAR_REGISTRATION_MONTH) WebElement carRegistrationMonth_field;
	@FindBy(xpath = CAR_FINANCE) WebElement carFinance_field;
	@FindBy(xpath = CAR_ANTI_THEFT_DEVICE) WebElement carAntiTheftDevice_field;
	@FindBy(xpath = CAR_HAS_RUST_DAMAGE_YES) WebElement carHasRustDamageYes_field;
	@FindBy(xpath = CAR_HAS_RUST_DAMAGE_NO) WebElement carHasRustDamageNo_field;
	@FindBy(xpath = CAR_HAS_WINDSCREEN_DAMAGE_YES) WebElement carHasWindscreenDamageYes_field;
	@FindBy(xpath = CAR_HAS_WINDSCREEN_DAMAGE_NO) WebElement carHasWindscreenDamageNo_field;
	@FindBy(xpath = CAR_HAS_HAIL_DAMAGE_YES) WebElement carHasHailDamageYes_field;
	@FindBy(xpath = CAR_HAS_HAIL_DAMAGE_NO) WebElement carHasHailDamageNo_field;

	/* Car Usage Locators */
	@FindBy(xpath = CTM_CAR_USAGE_NEXT_BUTTON) WebElement carUsageNextButton_field;

	/* Kept address details Locators */
	@FindBy(xpath = CTM_SAME_AS_POSTAL_ADDRESS) WebElement sameAsPostalAddress_field;
	@FindBy(xpath = CTM_SAME_AS_KEPT_AT_NIGHT_ADDRESS) WebElement sameAsKeptAtNightAddress_field;
	@FindBy(xpath = CTM_KEPT_DURING_THE_DAY_SITUATION) WebElement keptDuringTheDaySituation_field;

	/* Driver details Locators */
	@FindBy(xpath = CTM_LICENCE_COUNTRY) WebElement licenceCountry_field;
	@FindBy(xpath = CTM_LICENCE_TYPE) WebElement licenceType_field;
	@FindBy(xpath = CTM_LICENCE_YEAR) WebElement licenceYear_field;
	@FindBy(xpath = CTM_DRIVER_HAS_BEEN_UNDER_LICENCE_SUSPENSIION_PAST_5_YEAR_YES) WebElement driverHasBeenUnderLicenceSuspensionPast5YeaYes_field;
	@FindBy(xpath = CTM_DRIVER_HAS_BEEN_UNDER_LICENCE_SUSPENSIION_PAST_5_YEAR_NO) WebElement driverHasBeenUnderLicenceSuspensionPast5YeaNo_field;
	@FindBy(xpath = CTM_DRIVER_HAD_ALCOHOL_DRUG_PAST_5_YEAR_YES) WebElement driverHadAlcoholDrugPast5YearYes_field;
	@FindBy(xpath = CTM_DRIVER_HAD_ALCOHOL_DRUG_PAST_5_YEAR_NO) WebElement driverHadAlcoholDrugPast5YearNo_field;
	@FindBy(xpath = CTM_DRIVER_HAD_THEIR_CAR_INSURANCE_CANCELLED_YES) WebElement driverHadTheirCarInsuranceCancelledYes_field;
	@FindBy(xpath = CTM_DRIVER_HAD_THEIR_CAR_INSURANCE_CANCELLED_NO) WebElement driverHadTheirCarInsuranceCancelledNo_field;
	@FindBy(xpath = CTM_CRIMINAL_HISTORY_YES) WebElement criminalHistoryYes_field;
	@FindBy(xpath = CTM_CRIMINAL_HISTORY_NO) WebElement criminalHistoryNo_field;
	@FindBy(xpath = CTM_BANKRUPTCY_YES) WebElement bankruptcyYes_field;
	@FindBy(xpath = CTM_BANKRUPTCY_NO) WebElement bankruptcyNo_field;
	@FindBy(xpath = CTM_IS_THERE_2ND_DRIVER_YES) WebElement isThereA2ndDriverYes_field;
	@FindBy(xpath = CTM_IS_THERE_2ND_DRIVER_NO) WebElement isThereA2ndDriverNo_field;

	// Option and Pay page Locators
	@FindBy(xpath = CTM_OPTIONAL_WINDSCREEN_BENEFIT_YES) WebElement optionalWindscreenBenefireYes_field;
	@FindBy(xpath = CTM_OPTIONAL_WINDSCREEN_BENEFIT_NO) WebElement optionalWindscreenBenefireNo_field;
	@FindBy(xpath = CTM_ADD_HIRE_CAN_AFTER_ACCIDENT_BENEFIT_YES) WebElement addHireAfterAccidentBenefitYes_field;
	@FindBy(xpath = CTM_ADD_HIRE_CAN_AFTER_ACCIDENT_BENEFIT_NO) WebElement addHireAfterAccidentBenefitNo_field;
	@FindBy(xpath = CTM_CONTINUE_TO_PAYMENT_BUTTON) WebElement continueToPaymentButton_field;

	// Payment
	@FindBy(xpath = CTM_PAY_NOW_BUTTON) WebElement payNowButton_field;
	@FindBy(id = "cardnumber") WebElement cardNumber_field;
	@FindBy(id = "expirydate") WebElement expiryDate_field;
	@FindBy(id = "cardverificationcode") WebElement cardVerificationCode_field;
	@FindBy(id = "cardholder") WebElement cardHolderName_field;
	@FindBy(xpath = CTM_PAY_BUTTON) WebElement payButton_field;

	public CTM_Page() {
		PageFactory.initElements(driver, this);
	}

	// Enter CTM JSON
	public void enterCTMJson(String sheetName, int rowNum, String password) throws Exception {
		String ctmJsonData = reader1.getCellData(sheetName, "JSON_DATA", rowNum);
		ctmJsonTextArea_field.sendKeys(ctmJsonData);
		ctmPasscode_field.sendKeys(password);
		ctmSubmitButton_field.click();
		waitFor5Sec();
	}

	// Enter Carpeesh Site Protected Password
	public void enterCarpeeshProtectedPassword(String password) throws Exception {
		carpeeshProtectedPassword_field.sendKeys(password);
		carPeeshEnterButton_field.click();
		waitFor3Sec();
	}

	// Enter Policy Holder Details
	public void policyHolderDetails(String sheetName, int rowNum) throws Exception {
		String salutatin = reader.getCellData(sheetName, "Salutation", rowNum);
		String mobile = reader.getCellData(sheetName, "Mobile", rowNum);
		String emailAddress = reader.getCellData(sheetName, "Email_Address", rowNum);
		String confirmEmailAddress = reader.getCellData(sheetName, "Confirm_Email_Address", rowNum);
		js.executeScript("window.scrollBy(0,250)");
		switchToDefaultView();
		ctmIAgreeButton_field.click();
		waitFor2Sec();
		Select drpSalutation = new Select(saluTation);
		drpSalutation.selectByValue(salutatin);
		mobile_field.sendKeys(mobile);
		email_field.sendKeys(emailAddress);
		confirmEmail_field.sendKeys(confirmEmailAddress);
		Step1_Next_Button.click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}

	// Car Details Form
	public void carDetails_Step1(String sheetName, int rowNum) throws Exception {
		String previousInsurer = reader.getCellData(sheetName, "Previous insurer", rowNum);
		String estimatedAnualKM = reader.getCellData(sheetName, "Estimated annual kilometres", rowNum);
		String carRegistrationPlat = reader.getCellData(sheetName, "Car registration plate", rowNum);
		String carRegistrationMonth = reader.getCellData(sheetName, "Car registration month", rowNum);
		String carFinance = reader.getCellData(sheetName, "Car finance", rowNum);
		String carAntiTheftDevice = reader.getCellData(sheetName, "Car anti-theft device", rowNum);
		String carHasRustDamage = reader.getCellData(sheetName, "Car has rust damage", rowNum);
		String carHasWindScreenDamage = reader.getCellData(sheetName, "Car has windscreen damage", rowNum);
		String carHasHailDamage = reader.getCellData(sheetName, "Car has hail damage", rowNum);
		/*
		 * 
		 * */
		Locale locate = new Locale("en", "UK");
		DecimalFormatSymbols symbols = new DecimalFormatSymbols(locate);
		symbols.setDecimalSeparator(',');

		Select drpPreviousInsurer = new Select(previousInsurer_field);
		drpPreviousInsurer.selectByValue(previousInsurer);
		waitFor2Sec();

		/*
		 * Converstion "estimatedAnualKM"
		 */

		String pattern = "#,##0.###";

		Double annualKMParse = Double.parseDouble(estimatedAnualKM);

		DecimalFormat decimalFormat = new DecimalFormat(pattern);

		String estimatedKM = decimalFormat.format(annualKMParse);

		Select drpEstimatedAnualKM = new Select(estimatedAnnualKM_field);
		drpEstimatedAnualKM.selectByValue(estimatedKM);
		carRegistrationPlat_field.sendKeys(carRegistrationPlat);

		/*
		 * */

		Select drpCarRegistrationMonth = new Select(carRegistrationMonth_field);
		drpCarRegistrationMonth.selectByValue(carRegistrationMonth);

		Select drpCarFinance = new Select(carFinance_field);
		drpCarFinance.selectByValue(carFinance);

		Select drpCarAntiTheftDevice = new Select(carAntiTheftDevice_field);
		drpCarAntiTheftDevice.selectByValue(carAntiTheftDevice);

		/* Car has rust damage ****/

		if (carHasRustDamage.equalsIgnoreCase("Yes")) {
			carHasRustDamageYes_field.click();
			waitFor3Sec();
		} else {
			carHasRustDamageNo_field.click();
			waitFor1Sec();
		}

		/* Car has windscreen damage ****/
		if (carHasWindScreenDamage.equalsIgnoreCase("Yes")) {
			carHasWindscreenDamageYes_field.click();
			waitFor3Sec();
		} else {
			carHasWindscreenDamageNo_field.click();
			waitFor1Sec();
		}
		/* Car has hail damage ******/
		if (carHasHailDamage.equalsIgnoreCase("Yes")) {
			carHasHailDamageYes_field.click();
			waitFor3Sec();
		} else {
			carHasHailDamageNo_field.click();
			waitFor1Sec();
		}
		
		// Click on Car Detail Next button
		driver.findElement(By.xpath("//*[@id='custom_tab_p_n_button_li']/a")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	// Kept Address Details
	public void keptAddressDetails_step3(String sheetName, int rowNum) throws Exception {
		String keptDuringTheDaySituation = reader.getCellData(sheetName, "Kept during the day situation", rowNum);
		sameAsPostalAddress_field.click();
		waitFor1Sec();
		sameAsKeptAtNightAddress_field.click();
		waitFor1Sec();

		Select drpKeptDuringTheDaySituation = new Select(keptDuringTheDaySituation_field);
		drpKeptDuringTheDaySituation.selectByValue(keptDuringTheDaySituation);
		js.executeScript("window.scrollBy(0,250)");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		// Click on Kept Address Details Next Button
		driver.switchTo().activeElement().click();
		driver.findElement(By.cssSelector("#custom_tab_p_n_button_li > a.ct_next_step")).click();
		driver.findElement(By.cssSelector("#custom_tab_p_n_button_li > a.ct_next_step")).click();
	}

	// Driver Details Page
	public void driverDetails_step4(String sheetName, int rowNum) throws Exception {
		String licenceCountry = reader.getCellData(sheetName, "Licence country", rowNum);
		String licenceType = reader.getCellData(sheetName, "Licence type", rowNum);
		String licenceYear = reader.getCellData(sheetName, "Licence years", rowNum);
		String driverHasBeenUnderLicenceSuspensionPast5Year = reader.getCellData(sheetName,
				"Has this driver been under a licence suspension or cancellation, or restricted licence during the past 5 years?",
				rowNum);
		String driverHadAlcoholDrugPast5Year = reader.getCellData(sheetName,
				"Has this driver had any alcohol or drug-related driving charges or dangerous driving charges within the last 5 years?",
				rowNum);
		String driverHadTheirCarInsuranceCancelledPast5Year = reader.getCellData(sheetName,
				"Has this driver had their car insurance cancelled or refused at renewal in the past 5 years?", rowNum);
		String criminalHistory = reader.getCellData(sheetName, "Criminal history", rowNum);
		String bankruptcy = reader.getCellData(sheetName, "Bankruptcy", rowNum);
		String isThereA2ndDriver = reader.getCellData(sheetName, "Is there a 2nd driver?", rowNum);

		// Select Lucence Country
		Select drpLicenceCountry = new Select(licenceCountry_field);
		drpLicenceCountry.selectByValue(licenceCountry);
		waitFor1Sec();

		// Select Licence Type
		Select drpLicenceType = new Select(licenceType_field);
		drpLicenceType.selectByValue(licenceType);
		waitFor1Sec();

		// Select Licence Year
		Select drpLicenceYear = new Select(licenceYear_field);
		drpLicenceYear.selectByValue(licenceYear);
		waitFor1Sec();

		// Has this driver been under a licence suspension or cancellation, or
		// restricted licence during the past 5 years?
		String option1 = driverHasBeenUnderLicenceSuspensionPast5Year.toString();
		if (option1.equalsIgnoreCase("No") || option1.trim().length() == 0) {
			driverHasBeenUnderLicenceSuspensionPast5YeaNo_field.click();
			waitFor1Sec();
		} else {
			driverHasBeenUnderLicenceSuspensionPast5YeaYes_field.click();
			waitFor1Sec();
		}

		// Has this driver had any alcohol or drug-related driving charges or
		// dangerous driving charges within the past 5 years?
		String option2 = driverHadAlcoholDrugPast5Year.toString();
		if (option2.equalsIgnoreCase("No") || option2.trim().length() == 0) {
			driverHadAlcoholDrugPast5YearNo_field.click();
			waitFor1Sec();
		} else {
			driverHadAlcoholDrugPast5YearYes_field.click();
			waitFor1Sec();
		}

		// Has this driver had their car insurance cancelled or refused at
		// renewal in the past 5 years?
		String option3 = driverHadTheirCarInsuranceCancelledPast5Year.toString();
		if (option3.equalsIgnoreCase("No") || option3.trim().length() == 0) {
			driverHadTheirCarInsuranceCancelledNo_field.click();
			waitFor1Sec();
		} else {
			driverHadTheirCarInsuranceCancelledYes_field.click();
			waitFor1Sec();
		}

		// Criminal history
		String option5 = criminalHistory.toString();
		if (option5.equalsIgnoreCase("No") || option5.trim().length() == 0) {
			criminalHistoryNo_field.click();
			waitFor1Sec();
		} else {
			criminalHistoryYes_field.click();
			waitFor1Sec();
		}

		// Bankruptcy
		String option6 = bankruptcy.toString();
		if (option6.equalsIgnoreCase("No") || option6.trim().length() == 0) {
			bankruptcyNo_field.click();
			waitFor1Sec();
		} else {
			bankruptcyYes_field.click();
			waitFor1Sec();
		}

		// Is there a 2nd driver?
		String option7 = isThereA2ndDriver.toString();
		if (option7.equalsIgnoreCase("No") || option7.trim().length() == 0) {
			isThereA2ndDriverNo_field.click();
			boolean tempNo = driver.findElement(By.xpath("//*[@id='fe_66_1238']/div/span[2]/span/div[2]/label"))
					.isSelected();
			if (tempNo == false) {
				isThereA2ndDriverNo_field.click();
			} else {
				System.out.println("");
			}
			waitFor1Sec();
		} else {
			isThereA2ndDriverYes_field.click();
			boolean tempYes = driver.findElement(By.xpath("//*[@id='fe_66_1238']/div/span[2]/span/div[1]/label"))
					.isSelected();
			if (tempYes == false) {
				isThereA2ndDriverYes_field.click();
			} else {
				System.out.println("");
			}
			waitFor1Sec();
		}
		js.executeScript("window.scrollBy(0,250)");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		// Click on Driver Details Next Button
		driver.findElement(By.cssSelector("#custom_tab_p_n_button_li > a.ct_next_step")).click();
		waitFor2Sec();
	}

	// Option and Pay details Page
	public void optionAndPay_step6(String sheetName, int rowNum) throws Exception {
		String optionalWindscreenBenefit = reader.getCellData(sheetName, "Reduce windscreen excess", rowNum);
		String addHireCarAfterAccidentBenefit = reader.getCellData(sheetName, "Add hire car", rowNum);

		String optionPay_1 = optionalWindscreenBenefit.toString();
		if (optionPay_1.equalsIgnoreCase("No") || optionPay_1.trim().length() == 0) {
			optionalWindscreenBenefireNo_field.click();
			waitFor1Sec();
		} else {
			optionalWindscreenBenefireYes_field.click();
			waitFor1Sec();
		}

		String optionPay_2 = addHireCarAfterAccidentBenefit.toString();
		if (optionPay_2.equalsIgnoreCase("No") || optionPay_2.trim().length() == 0) {
			addHireAfterAccidentBenefitNo_field.click();
			waitFor1Sec();
		} else {
			addHireAfterAccidentBenefitYes_field.click();
			waitFor1Sec();
		}

		continueToPaymentButton_field.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	// Payment Page
	public void clickOnPayNowButton() {
		payNowButton_field.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	// Enter Payment Card Details
	public void enterCardDetails(String cardNumber, String mmYY, String csc, String cardHolderName) throws Exception {
		cardNumber_field.sendKeys(cardNumber);
		expiryDate_field.sendKeys(mmYY);
		cardVerificationCode_field.sendKeys(csc);
		cardHolderName_field.sendKeys(cardHolderName);
		waitFor2Sec();
		payButton_field.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
}
