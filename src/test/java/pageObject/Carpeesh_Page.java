package pageObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import com.aventstack.extentreports.Status;
import baseClass.TestBase;
import locators.Locators.carpeeshLocators;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

/*
 * Developed by : Bhargav
 * 
 * */
public class Carpeesh_Page extends TestBase implements carpeeshLocators {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
	CommonFunction commonFunction = new CommonFunction();
	CP_Client_Policy_Page cpPolicyPage = new CP_Client_Policy_Page();
	String path = readPro.getExcelPath();
	String renewalVerification = readPro.getRenewalVerificationExcel();
	String renewalPoliciesExcelPath = readPro.getRenewalPolicesDataExcel();
	Xls_Reader reader = new Xls_Reader(renewalPoliciesExcelPath);
	Xls_Reader reader1 = new Xls_Reader(renewalPoliciesExcelPath);
	Xls_Reader renewalVerificationReader = new Xls_Reader(renewalVerification);

	JavascriptExecutor js = (JavascriptExecutor) driver;

	// Policy Holder Page Locators
	@FindBy(xpath = I_AGREE_BUTTON)
	WebElement iAgreeButton;
	@FindBy(xpath = INITIAL_I_AGREE_BUTTON)
	WebElement initialIAgreeButton_field;
	@FindBy(xpath = POLICY_START_DAY)
	WebElement policyStartDay_field;
	@FindBy(xpath = POLICY_START_MONTH)
	WebElement policyStartMonth_field;
	@FindBy(xpath = POLICY_START_YEAR)
	WebElement policyStartYear_field;
	@FindBy(id = "first_name")
	WebElement firstName;
	@FindBy(id = "last_name")
	WebElement familyName;
	@FindBy(id = "salutation")
	WebElement saluTation;
	@FindBy(id = "date_field")
	WebElement dateField;
	@FindBy(id = "month_field")
	WebElement monthField;
	@FindBy(id = "year_field")
	WebElement yearField;
	@FindBy(id = "gender")
	WebElement gender_field;
	@FindBy(id = "mailing_address1_client")
	WebElement postalAddress1_field;
	@FindBy(id = "mailing_city_client")
	WebElement postalCity_field;
	@FindBy(id = "mailing_state_client")
	WebElement postalState_field;
	@FindBy(id = "mailing_postcode_client")
	WebElement postalCode_field;
	@FindBy(id = "mobile")
	WebElement mobile_field;
	@FindBy(id = "email")
	WebElement email_field;
	@FindBy(id = "confirm_email")
	WebElement confirmEmail_field;
	@FindBy(id = "btn_save_client")
	WebElement Step1_Next_Button;

	// Car Details Page Locators
	@FindBy(xpath = PREVIOUS_INSURER)
	WebElement previousInsurer_field;
	@FindBy(xpath = ESTIMATED_ANNUAL_KILOMETER)
	WebElement estimatedAnnualKM_field;
	@FindBy(xpath = CAR_REGISTRATION_PLAT)
	WebElement carRegistrationPlat_field;
	@FindBy(xpath = CAR_MAKE)
	WebElement carMake_field;
	@FindBy(xpath = CAR_MODEL)
	WebElement carModel_field;
	@FindBy(xpath = CAR_YEAR_MANUFACTURE)
	WebElement carYearManufacture_field;
	@FindBy(xpath = CAR_DESCRIPTION)
	WebElement carDescription_field;
	@FindBy(xpath = CAR_REGISTRATION_MONTH)
	WebElement carRegistrationMonth_field;
	@FindBy(xpath = MARKET_VALUE)
	WebElement marketValue_field;
	@FindBy(xpath = AGREED_VALUE)
	WebElement agreedValue_field;
	@FindBy(xpath = CAR_FINANCE)
	WebElement carFinance_field;
	@FindBy(xpath = CAR_ANTI_THEFT_DEVICE)
	WebElement carAntiTheftDevice_field;
	@FindBy(xpath = DOES_CAR_HAVE_NON_STANDARD_MODIFICATIONS_OR_ACCESSORIES_YES)
	WebElement doesCarHaveNonStandardModicationYes_field;
	@FindBy(xpath = DOES_CAR_HAVE_NON_STANDARD_MODIFICATIONS_OR_ACCESSORIES_NO)
	WebElement doesCarHaveNonStandardModicationNo_field;
	@FindBy(xpath = DOES_CAR_HAVE_PRE_EXISTING_DAMAGE_YES)
	WebElement doesCarHavePreExistingDamageYes_field;
	@FindBy(xpath = DOES_CAR_HAVE_PRE_EXISTING_DAMAGE_NO)
	WebElement doesCarHavePreExistingDamageNo_field;
	@FindBy(xpath = CAR_HAS_RUST_DAMAGE_YES)
	WebElement carHasRustDamageYes_field;
	@FindBy(xpath = CAR_HAS_RUST_DAMAGE_NO)
	WebElement carHasRustDamageNo_field;
	@FindBy(xpath = CAR_HAS_WINDSCREEN_DAMAGE_YES)
	WebElement carHasWindscreenDamageYes_field;
	@FindBy(xpath = CAR_HAS_WINDSCREEN_DAMAGE_NO)
	WebElement carHasWindscreenDamageNo_field;
	@FindBy(xpath = AGREED_VALUE_TEXTBOX)
	WebElement agreedValueTextbox_field;
	@FindBy(xpath = CAR_DAMAGE_AREA)
	WebElement carDamageArea_field;
	@FindBy(xpath = IS_DAMAGE_LARGER_THAN_ONE_HAND_SIZE_YES)
	WebElement isDamageLargerThenOneHandSizeYes_field;
	@FindBy(xpath = IS_DAMAGE_LARGER_THAN_ONE_HAND_SIZE_NO)
	WebElement isDamageLargerThenOneHandSizeNo_field;
	@FindBy(xpath = CAR_HAS_HAIL_DAMAGE_YES)
	WebElement carHasHailDamageYes_field;
	@FindBy(xpath = CAR_HAS_HAIL_DAMAGE_NO)
	WebElement carHasHailDamageNo_field;
	@FindBy(xpath = ALCOHOL_INTERLOCK)
	WebElement alcoholInterLock_field;
	@FindBy(xpath = ALLOY_MAG_WHEELS)
	WebElement alloyMagWheels_field;

	// Car Usage Page Locators
	@FindBy(xpath = CAR_USAGE)
	WebElement carUsage_field;
	@FindBy(xpath = BUSINESS_USE)
	WebElement businessUse_field;
	@FindBy(xpath = GREY_FLEET)
	WebElement greyFleet_field;
	@FindBy(xpath = RIDE_SHARE)
	WebElement rideShare_field;
	@FindBy(xpath = CAR_USAGE_NEXT_BUTTON)
	WebElement carUsageNextButton_field;

	// Kept address details Locators
	@FindBy(xpath = SAME_AS_POSTAL_ADDRESS)
	WebElement sameAsPostalAddress_field;
	@FindBy(xpath = KEPT_AT_NIGHT_SITUATION)
	WebElement keptAtNightSituation_field;
	@FindBy(xpath = SAME_AS_KEPT_AT_NIGHT_ADDRESS)
	WebElement sameAsKeptAtNightAddress_field;
	@FindBy(xpath = KEPT_DURING_THE_DAY_SITUATION)
	WebElement keptDuringTheDaySituation_field;

	// Driver1 Details Locators
	@FindBy(xpath = PERMITTED_DRIVER_AGE_RANGE)
	WebElement permittedDriverAgeRange_field;
	@FindBy(xpath = LICENCE_COUNTRY)
	WebElement licenceCountry_field;
	@FindBy(xpath = LICENCE_TYPE)
	WebElement licenceType_field;
	@FindBy(xpath = LICENCE_YEARS)
	WebElement licenceYear_field;
	@FindBy(xpath = HAS_THIS_DRIVER_LICENCE_SUSPENSION_OR_CANCELLATION_YES)
	WebElement hasThisDriverLicenceSuspensionYes_field;
	@FindBy(xpath = HAS_THIS_DRIVER_LICENCE_SUSPENSION_OR_CANCELLATION_NO)
	WebElement hasThisDriverLicenceSuspensionNo_field;
	@FindBy(xpath = HAS_THIS_DRIVER_ANY_ALCOHOL_OR_DRUG_YES)
	WebElement hasThisDriverAlcoholOrDrugYes_field;
	@FindBy(xpath = HAS_THIS_DRIVER_ANY_ALCOHOL_OR_DRUG_NO)
	WebElement hasThisDriverAlcoholOrDrugNo_field;
	@FindBy(xpath = HAS_THIS_DRIVER_THEIR_CAR_INSURANCE_CANCELLED_YES)
	WebElement hasThisDriverCarInsuranceCalcelledYes_field;
	@FindBy(xpath = HAS_THIS_DRIVER_THEIR_CAR_INSURANCE_CANCELLED_NO)
	WebElement hasThisDriverCarInsuranceCalcelledNo_field;
	@FindBy(xpath = HAS_THIS_DRIVER_HAD_ANY_CLAIM_PAST_5YEAR_YES)
	WebElement hasThisDriverHadAnyClaimPast5YearYes_field;
	@FindBy(xpath = HAS_THIS_DRIVER_HAD_ANY_CLAIM_PAST_5YEAR_NO)
	WebElement hasThisDriverHadAnyClaimPast5YearNo_field;
	@FindBy(xpath = CRIMINAL_HISTORY_YES)
	WebElement criminalHistoryYes_field;
	@FindBy(xpath = CRIMINAL_HISTORY_NO)
	WebElement criminalHistoryNo_field;
	@FindBy(xpath = BANKRUPTCY_YES)
	WebElement bankruptcyYes_field;
	@FindBy(xpath = BANKRUPTCY_NO)
	WebElement bankruptcyNo_field;
	@FindBy(xpath = IS_THERE_2ND_DRIVER_YES)
	WebElement isThere2ndDriverYes_field;
	@FindBy(xpath = IS_THERE_2ND_DRIVER_NO)
	WebElement isThere2ndDriverNo_field;
	@FindBy(xpath = DRIVER_DETAIL_NEXT_BUTTON)
	WebElement driverDetailNextButton;
	@FindBy(xpath = REVIEW_PAGE_NEXT_BUTTON)
	WebElement reviewPageNextButton;
	@FindBy(xpath = ADD_CLAIM_BUTTON)
	WebElement addClaimButton_field;
	@FindBy(xpath = CLAIM_TYPE)
	WebElement claimType_field;
	@FindBy(xpath = CLAIM_MONTH)
	WebElement claimMonth_field;
	@FindBy(xpath = CLAIM_YEAR)
	WebElement claimYear_field;
	@FindBy(xpath = DID_YOU_PAY_AN_EXCESS_YES)
	WebElement didYouPayAnExcessYes_field;
	@FindBy(xpath = DID_YOU_PAY_AN_EXCESS_NO)
	WebElement didYouPayAnExcessNo_field;

	// Driver2 Details Locators
	@FindBy(xpath = REMOVE_2ND_DRIVER_FROM_POLICY_YES)
	WebElement remove2ndDriverFromPolicyYes_field;
	@FindBy(xpath = REMOVE_2ND_DRIVER_FROM_POLICY_NO)
	WebElement remove2ndDriverFromPolicyNo_field;
	@FindBy(xpath = DRIVER2_SALUTATION)
	WebElement driver2Salutation_field;
	@FindBy(xpath = DRIVER2_FIRSTNAME)
	WebElement driver2FirstName_field;
	@FindBy(xpath = DRIVER2_FAMILYNAME)
	WebElement driver2FamilyName_field;
	@FindBy(xpath = DRIVER2_GENDER)
	WebElement driver2Gender_field;
	@FindBy(xpath = DRIVER2_DAY)
	WebElement driver2Day_field;
	@FindBy(xpath = DRIVER2_MONTH)
	WebElement driver2Month_field;
	@FindBy(xpath = DRIVER2_YEAR)
	WebElement driver2Year_field;
	@FindBy(xpath = DRIVER2_MOBILE)
	WebElement driver2Mobile_field;
	@FindBy(xpath = DRIVER2_LICENCE_TYPE)
	WebElement driver2LicenceType_field;
	@FindBy(xpath = DRIVER2_LICENCE_COUNTRY)
	WebElement driver2LicenceCountry_field;
	@FindBy(xpath = DRIVER2_LICENCE_YEAR)
	WebElement driver2LicenceYear_field;
	@FindBy(xpath = DRIVER2_HAS_BEEN_LICENCE_SUSPENSION_OR_CANCELLATION_NO)
	WebElement driver2HasBeenSuspensionOrCancellationNo_Field;
	@FindBy(xpath = DRIVER2_HAS_BEEN_LICENCE_SUSPENSION_OR_CANCELLATION_YES)
	WebElement driver2HasBeenSuspensionOrCancellationYes_Field;
	@FindBy(xpath = DRIVER2_HAS_DRIVER_ALCOHOL_OR_DRUG_YES)
	WebElement driver2HasDriverAlcoholOrDrugYes_field;
	@FindBy(xpath = DRIVER2_HAS_DRIVER_ALCOHOL_OR_DRUG_NO)
	WebElement driver2HasDriverAlcoholOrDrugNo_field;
	@FindBy(xpath = DRIVER2_HAS_DRIVER_CAR_INSURANCE_CANCELLED_YES)
	WebElement driver2HasDriverCarInsuranceCancelledYes_field;
	@FindBy(xpath = DRIVER2_HAS_DRIVER_CAR_INSURANCE_CANCELLED_NO)
	WebElement driver2HasDriverCarInsuranceCancelledNo_field;
	@FindBy(xpath = DRIVER2_HAS_DRIVER_HAD_ANY_CLAIM_PAST_5YEAR_NO)
	WebElement driver2HadAnyClaimPast5YearNo_field;
	@FindBy(xpath = DRIVER2_HAS_DRIVER_HAD_ANY_CLAIM_PAST_5YEAR_YES)
	WebElement driver2HadAnyClaimPast5YearYes_field;
	@FindBy(xpath = DRIVER2_CRIMINAL_HISTORY_YES)
	WebElement driver2CriminalHistoryYes_field;
	@FindBy(xpath = DRIVER2_CRIMINAL_HISTORY_NO)
	WebElement driver2CriminalHistoryNo_field;
	@FindBy(xpath = DRIVER2_BANKRUPTCY_YES)
	WebElement driver2BankruptcyYes_field;
	@FindBy(xpath = DRIVER2_BANKRUPTCY_NO)
	WebElement driver2BankruptcyNo_field;
	@FindBy(xpath = IS_THERE_3RD_DRIVER_YES)
	WebElement isThere3rdDriverYes_field;
	@FindBy(xpath = IS_THERE_3RD_DRIVER_NO)
	WebElement isThere3rdDriverNo_field;
	@FindBy(xpath = ADD_CLAIM_BUTTON_DRIVER2)
	WebElement addClaimButtonDriver2_field;
	@FindBy(xpath = DRIVER2_CLAIM_TYPE)
	WebElement driver2ClaimType_field;
	@FindBy(xpath = DRIVER2_CLAIM_MONTH)
	WebElement driver2ClaimMonth_field;
	@FindBy(xpath = DRIVER2_CLAIM_YEAR)
	WebElement driver2ClaimYear_field;
	@FindBy(xpath = DRIVER2_DID_YOU_PAY_AN_EXCESS_YES)
	WebElement driver2DidYouPayAnExcessYes_field;
	@FindBy(xpath = DRIVER2_DID_YOU_PAY_AN_EXCESS_NO)
	WebElement driver2DidYouPayAnExcessNo_field;

	// Driver3 Details Locators
	@FindBy(xpath = DRIVER3_REMOVE_3RD_DRIVER_FROM_POLICY_YES)
	WebElement driver3Remove3rdPolicyYes_field;
	@FindBy(xpath = DRIVER3_REMOVE_3RD_DRIVER_FROM_POLICY_NO)
	WebElement driver3Remove3rdPolicyNo_field;
	@FindBy(xpath = DRIVER3_SALUTATION)
	WebElement driver3Salutation_field;
	@FindBy(xpath = DRIVER3_FIRST_NAME)
	WebElement driver3FirstName_field;
	@FindBy(xpath = DRIVER3_FAMILY_NAME)
	WebElement driver3FamilyName_field;
	@FindBy(xpath = DRIVER3_GENDER)
	WebElement driver3Gender_field;
	@FindBy(xpath = DRIVER3_DAY)
	WebElement driver3Day_field;
	@FindBy(xpath = DRIVER3_MONTH)
	WebElement driver3Month_field;
	@FindBy(xpath = DRIVER3_YEAR)
	WebElement driver3Year_field;
	@FindBy(xpath = DRIVER3_MOBILE)
	WebElement driver3Mobile_field;
	@FindBy(xpath = DRIVER3_LICENCE_TYPE)
	WebElement driver3LicenceType_field;
	@FindBy(xpath = DRIVER3_LICENCE_YEAR)
	WebElement driver3LicenceYear_field;
	@FindBy(xpath = DRIVER3_LICENCE_COUNTRY)
	WebElement driver3LicenceCountry_field;
	@FindBy(xpath = DRIVER3_HAS_BEEN_UNDER_SUSPENSION_OR_CANCELLATION_YES)
	WebElement driver3HasBeenUnderSuspensionOrCancellationYes_field;
	@FindBy(xpath = DRIVER3_HAS_BEEN_UNDER_SUSPENSION_OR_CANCELLATION_NO)
	WebElement driver3HasBeenUnderSuspensionOrCancellationNo_field;
	@FindBy(xpath = DRIVER3_HAD_ANY_ALCOHOL_OR_DRUG_YES)
	WebElement driver3HadAnyAlcoholOrDrugYes_field;
	@FindBy(xpath = DRIVER3_HAD_ANY_ALCOHOL_OR_DRUG_NO)
	WebElement driver3HadAnyAlcoholOrDrugNo_field;
	@FindBy(xpath = DRIVER3_HAD_THEIR_CAR_INSURANCE_CANCELLED_YES)
	WebElement driver3HadTheirCarInsuranceCancelledYes_field;
	@FindBy(xpath = DRIVER3_HAD_THEIR_CAR_INSURANCE_CANCELLED_NO)
	WebElement driver3HadTheirCarInsuranceCancelledNo_field;
	@FindBy(xpath = DRIVER3_HAD_ANY_CLAIM_IN_LAST_5_YEAR_YES)
	WebElement driver3HadAnyClaimInLast5YearYes_field;
	@FindBy(xpath = DRIVER3_HAD_ANY_CLAIM_IN_LAST_5_YEAR_NO)
	WebElement driver3HadAnyClaimInLast5YearNo_field;
	@FindBy(xpath = DRIVER3_CRIMINAL_HISTORY_YES)
	WebElement driver3CriminalHistoryYes_field;
	@FindBy(xpath = DRIVER3_CRIMINAL_HISTORY_NO)
	WebElement driver3CriminalHistoryNo_field;
	@FindBy(xpath = DRIVER3_BANKRUPTCY_YES)
	WebElement driver3BankruptcyYes_field;
	@FindBy(xpath = DRIVER3_BANKRUPTCY_NO)
	WebElement driver3BankruptcyNo_field;
	@FindBy(xpath = IS_THERE_4TH_DRIVER_YES)
	WebElement isThere4thDriverYes_field;
	@FindBy(xpath = IS_THERE_4TH_DRIVER_NO)
	WebElement isThere4thDriverNo_field;
	@FindBy(xpath = DRIVER3_CLAIM_TYPE)
	WebElement driver3ClaimType_field;
	@FindBy(xpath = DRIVER3_CLAIM_MONTH)
	WebElement driver3ClaimMonth_field;
	@FindBy(xpath = DRIVER3_CLAIM_YEAR)
	WebElement driver3ClaimYear_field;
	@FindBy(xpath = DRIVER3_DID_YOU_PAY_EXCESS_YES)
	WebElement driver3DidYouPayExcessYes_field;
	@FindBy(xpath = DRIVER3_DID_YOU_PAY_EXCESS_NO)
	WebElement driver3DidYouPayExcessNo_field;
	@FindBy(xpath = ADD_CLAIM_BUTTON_DRIVER3)
	WebElement addClaimButtonDriver3_field;

	// Options and Pay Locators
	@FindBy(xpath = PAYMENT_TYPE)
	WebElement paymentType_field;
	@FindBy(xpath = ADJUST_EXCESS_BY)
	WebElement adjustExcessBy_field;
	@FindBy(xpath = OPTIONAL_WINDSCREEN_BENEFIT_YES)
	WebElement optionalWindScreenBenefitYes_field;
	@FindBy(xpath = OPTIONAL_WINDSCREEN_BENEFIT_NO)
	WebElement optionalWindScreenBenefitNo_field;
	@FindBy(xpath = ADD_HIRE_CAR_ACCIDENT_BENEFIT_YES)
	WebElement addHireCarAccidentBenefitYes_field;
	@FindBy(xpath = ADD_HIRE_CAR_ACCIDENT_BENEFIT_NO)
	WebElement addHireCarAccidentBenefitNo_field;
	@FindBy(xpath = CONTINUE_TO_PAYMENT_BUTTON)
	WebElement continueToPaymentButton_field;

	// Payment Form Locators
	@FindBy(xpath = PAY_NOW_BUTTON)
	WebElement payNowButton_field;
	@FindBy(xpath = CARD_NUMBER)
	WebElement cardNumber_field;
	@FindBy(xpath = EXPIRY_DATE)
	WebElement expiryDate_field;
	@FindBy(xpath = CARD_VERIFICATION_CODE)
	WebElement cardVerificationCode_field;
	@FindBy(xpath = CARD_HOLDER_NAME)
	WebElement cardHolderName_field;
	@FindBy(xpath = PAY_BUTTON)
	WebElement payButton_field;

	public Carpeesh_Page() {
		PageFactory.initElements(driver, this);
	}

	public void clickIAgreeButton() {
		iAgreeButton.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public void clickInitialIAgreeButton() {
		initialIAgreeButton_field.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	// Policy Holder Details Form
	public void policyHolderDetails(String sheetName, int rowNum) throws Exception {
		String salutatin = reader.getCellData(sheetName, "Salutation", rowNum);
		String fName = reader.getCellData(sheetName, "First Name", rowNum);
		String famlyName = reader.getCellData(sheetName, "Last Name", rowNum);
		String dob = reader.getCellData(sheetName, "Date Of Birth", rowNum);
		String gender = reader.getCellData(sheetName, "Gender", rowNum);
		String postalAddress = reader.getCellData(sheetName, "Mailing Address", rowNum);
		String postalCity = reader.getCellData(sheetName, "City", rowNum);
		String postalState = reader.getCellData(sheetName, "State", rowNum);
		String postalCode = reader.getCellData(sheetName, "Postalcode", rowNum);
		String mobile = reader.getCellData(sheetName, "Mobile", rowNum);
		String emailAddress = reader.getCellData(sheetName, "Email_Address", rowNum);
		String confirmEmailAddress = reader.getCellData(sheetName, "Email_Address", rowNum);

		System.out.println("Date Of Birth : " + dob);

		String sTempSalutation = CommonFunction.capitaliseFirstLetterSingleWord(salutatin);
		System.out.println("Salutation : " + sTempSalutation);
		Select drpSalutation = new Select(saluTation);
		drpSalutation.selectByValue(sTempSalutation);
		firstName.sendKeys(fName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		familyName.sendKeys(famlyName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		String str[] = dob.split("/");
		String day = str[0];
		String month = str[1];
		String year = str[2];

		Select drpDay = new Select(dateField);
		drpDay.selectByValue(day);
		Select drpMonth = new Select(monthField);
		drpMonth.selectByValue(month);
		Select drpYear = new Select(yearField);
		drpYear.selectByValue(year);

		String sTempGender = CommonFunction.capitaliseFirstLetterSingleWord(gender);
		System.out.println("Gender : " + sTempGender);

		Select drpGender = new Select(gender_field);
		drpGender.selectByValue(sTempGender);

		postalAddress1_field.sendKeys(postalAddress);
		postalCity_field.sendKeys(postalCity);
		postalState_field.sendKeys(postalState);
		postalCode_field.sendKeys(postalCode);
		mobile_field.sendKeys(mobile);
		email_field.sendKeys(emailAddress);
		confirmEmail_field.sendKeys(confirmEmailAddress);
		Step1_Next_Button.click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		waitFor5Sec();
	}

	// Car Details Form
	public boolean carDetails_Step1(String sheetName, int rowNum) throws Exception {
		String policyStartDate = reader.getCellData(sheetName, "Policy start date", rowNum);
		String previousInsurer = reader.getCellData(sheetName, "Previous insurer", rowNum);
		String estimatedAnualKM = reader.getCellData(sheetName, "Estimated annual kilometres", rowNum);
		String carRegistrationPlat = reader.getCellData(sheetName, "Car registration plate", rowNum);
		String carMake = reader.getCellData(sheetName, "Car make", rowNum);
		String carModel = reader.getCellData(sheetName, "Car model", rowNum);
		String carYearOfManufacture = reader.getCellData(sheetName, "Car year of manufacture", rowNum);
		String carDescription = reader.getCellData(sheetName, "Car description", rowNum);
		String carRegistrationMonth = reader.getCellData(sheetName, "Car registration month", rowNum);
		String marketValue = reader.getCellData(sheetName, "Market value", rowNum);
		String agreedValue = reader.getCellData(sheetName, "Agreed value", rowNum);
		String carFinance = reader.getCellData(sheetName, "Car finance", rowNum);
		String carAntiTheftDevice = reader.getCellData(sheetName, "Car anti-theft device", rowNum);
		String doesTheCarHaveNonStandardMoficationOrAccessories = reader.getCellData(sheetName,
				"Does the car have non-standard modifications or accessories?", rowNum);
		String carHasRustDamage = reader.getCellData(sheetName, "Car has rust damage", rowNum);
		String doesTheCarHavePreExistingDamage = reader.getCellData(sheetName, "Does the car have pre-existing damage",
				rowNum);
		String carDamageArea = reader.getCellData(sheetName, "Car damage area", rowNum);
		String isDamageLargetThanOneHandSize_Yes = reader.getCellData(sheetName, "Is damage larger than one hand size",
				rowNum);
		String carHasWindScreenDamage = reader.getCellData(sheetName, "Car has windscreen damage", rowNum);
		String carHasHailDamage = reader.getCellData(sheetName, "Car has hail damage", rowNum);
		String carDetailNextButton = "Next >";

		Locale locate = new Locale("en", "UK");
		DecimalFormatSymbols symbols = new DecimalFormatSymbols(locate);
		symbols.setDecimalSeparator(',');

		System.out.println("Previous Insurer : " + previousInsurer);
		System.out.println("Policy Start Date : " + policyStartDate);

		String sDate = CommonFunction.getCurrentDate();
		System.out.println("Current Date is : " +sDate);
		
		String strPolicyStartDate[] = sDate.split("/");
		String dayPolicy = strPolicyStartDate[0];
		String monthPolicy = strPolicyStartDate[1];
		String yearPolicy = strPolicyStartDate[2];

		Select drpDayPolicy = new Select(policyStartDay_field);
		drpDayPolicy.selectByValue(dayPolicy);

		Select drpMonthPolicy = new Select(policyStartMonth_field);
		drpMonthPolicy.selectByValue(monthPolicy);

		Select drpYearPolicy = new Select(policyStartYear_field);
		drpYearPolicy.selectByValue(yearPolicy);

		Select drpPreviousInsurer = new Select(previousInsurer_field);
		try {
			drpPreviousInsurer.selectByValue(previousInsurer);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Previous Insurer
		String getPrevious_Insurer = drpPreviousInsurer.getFirstSelectedOption().getText().trim();

		if (previousInsurer.compareToIgnoreCase(getPrevious_Insurer) != 0) {
			System.out.println("Invalid Value => Previous insurer" + " : " + previousInsurer);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Previous insurer" + " : " + previousInsurer);
			return false;
		}

		waitFor1Sec();

		// Converstion "estimatedAnualKM"
		String pattern = "#,##0.###";

		Double annualKMParse = Double.parseDouble(estimatedAnualKM);

		DecimalFormat decimalFormat = new DecimalFormat(pattern);

		String estimatedKM = decimalFormat.format(annualKMParse);

		System.out.println("Estimated Annual Kilometer : " + estimatedKM);

		Select drpEstimatedAnualKM = new Select(estimatedAnnualKM_field);
		try {
			drpEstimatedAnualKM.selectByValue(estimatedKM);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Estimated Annual Kilometer
		String getEstimatedAnnualKm = drpEstimatedAnualKM.getFirstSelectedOption().getText().trim();
		if (estimatedKM.compareToIgnoreCase(getEstimatedAnnualKm) != 0) {
			System.out.println("Invalid Value => Estimated Annual Kilometer" + " : " + estimatedAnualKM);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Estimated Annual Kilometer" + " : " + estimatedAnualKM);
			return false;
		}

		carRegistrationPlat_field.sendKeys(carRegistrationPlat);

		Select drpCarMake = new Select(carMake_field);

		try {
			drpCarMake.selectByValue(carMake);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Car Make
		String getCar_Make = drpCarMake.getFirstSelectedOption().getText().trim();

		if (carMake.compareToIgnoreCase(getCar_Make) != 0) {
			System.out.println("Invalid Value => Car Make" + " : " + carMake);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Car Make" + " : " + carMake);
			return false;
		}

		System.out.println("Car Models : " + carModel);

		// Conversion : "Car Model"
		String sTempCarModel = carModel.toString().trim();
		Select drpCarModel = new Select(carModel_field);
		try {
			drpCarModel.selectByValue(sTempCarModel);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Car Model
		String getCarModel = drpCarModel.getFirstSelectedOption().getText().trim();

		if (carModel.compareToIgnoreCase(getCarModel) != 0) {
			System.out.println("Invalid Value => Car Model" + " : " + carModel);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Car Model" + " : " + carModel);
			return false;
		}

		// Converstion "carYearOfManufacture"
		String pattern2 = "#";
		Double yearOFManu = Double.parseDouble(carYearOfManufacture);
		DecimalFormat yrOfManufacture = new DecimalFormat(pattern2);
		String carYearOfManufactureConversion = yrOfManufacture.format(yearOFManu);

		System.out.println("Year Manufacture : " + carYearOfManufactureConversion);

		Select drpCarYearOfManufacture = new Select(carYearManufacture_field);
		try {
			drpCarYearOfManufacture.selectByValue(carYearOfManufactureConversion);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Car year of manufacture
		String getCarYearOfManufacture = drpCarYearOfManufacture.getFirstSelectedOption().getText().trim();

		if (carYearOfManufacture.compareToIgnoreCase(getCarYearOfManufacture) != 0) {
			System.out.println("Invalid Value => Car year of manufacture" + " : " + carYearOfManufacture);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Car year of manufacture" + " : " + carYearOfManufacture);
			return false;
		}

		String carDescription_Temp = carDescription.toString().trim();
		Select drpCarDescription = new Select(carDescription_field);
		try {
			drpCarDescription.selectByValue(carDescription_Temp);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Car Description
		String getCarDescription = drpCarDescription.getFirstSelectedOption().getText().trim();
		if (carDescription_Temp.compareToIgnoreCase(getCarDescription) != 0) {
			System.out.println("Invalid Value => Car Description" + " : " + carDescription);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Car Description" + " : " + carDescription);
			return false;
		}

		// Verify Declined Case for Car Description
		String declined_CarDescription1 = "//a[@id='Next2']";
		String declined_CarDescription2 = driver.findElement(By.xpath(declined_CarDescription1)).getText();
		if (carDetailNextButton.compareToIgnoreCase(declined_CarDescription2) != 0) {
			System.out.println("Declined Case => Car Description" + " : " + carDescription);
			ReportsClass.logStat(Status.FATAL, "Declined Case => Car Description" + " : " + carDescription);
			return false;
		}

		String sTempCarRegistrationMonth = CommonFunction.capitaliseFirstLetterSingleWord(carRegistrationMonth);
		System.out.println("Car Registraton Month : " + sTempCarRegistrationMonth);
		Select drpCarRegistrationMonth = new Select(carRegistrationMonth_field);
		try {
			drpCarRegistrationMonth.selectByValue(sTempCarRegistrationMonth);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Car registration month
		String getCarRegistrationMonth = drpCarRegistrationMonth.getFirstSelectedOption().getText().trim();
		if (carRegistrationMonth.compareToIgnoreCase(getCarRegistrationMonth) != 0) {
			System.out.println("Invalid Value => Car registration month" + " : " + carRegistrationMonth);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Car registration month" + " : " + carRegistrationMonth);
			return false;
		}

		if (marketValue != null) {
			marketValue_field.click();
			waitFor1Sec();
		} else {
			agreedValue_field.click();
			waitFor3Sec();
			agreedValue_field.clear();
			agreedValue_field.sendKeys(agreedValue);
			waitFor1Sec();
		}

		String sTempCarFinance = CommonFunction.capitaliseFirstLetterEntireString(carFinance);
		System.out.println("Car Finance : " + sTempCarFinance);

		Select drpCarFinance = new Select(carFinance_field);
		try {
			drpCarFinance.selectByValue(sTempCarFinance);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Car Finance
		String getCarFinance = drpCarFinance.getFirstSelectedOption().getText().trim();

		if (carFinance.compareToIgnoreCase(getCarFinance) != 0) {
			System.out.println("Invalid Value => Car Finance" + " : " + carFinance);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Car Finance" + " : " + carFinance);
			return false;
		}

		String sTempCarAntiDevice = CommonFunction.capitaliseFirstLetterEntireString(carAntiTheftDevice);
		System.out.println("Car Anti Theft Device : " + sTempCarAntiDevice);
		Select drpCarAntiTheftDevice = new Select(carAntiTheftDevice_field);
		try {
			drpCarAntiTheftDevice.selectByValue(sTempCarAntiDevice);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Car anti-theft device
		String getCarAntiTheftDevice = drpCarAntiTheftDevice.getFirstSelectedOption().getText().trim();
		if (carAntiTheftDevice.compareToIgnoreCase(getCarAntiTheftDevice) != 0) {
			System.out.println("Invalid Value => Car anti-theft device" + " : " + carAntiTheftDevice);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Car anti-theft device" + " : " + carAntiTheftDevice);
			return false;
		}

		// Does the car have non-standard modifications or accessories?

		String sTemp = doesTheCarHaveNonStandardMoficationOrAccessories.toString().trim();
		if (sTemp.equalsIgnoreCase("No") || (sTemp.trim().length() == 0)) {
			doesCarHaveNonStandardModicationNo_field.click();
		} else {
			doesCarHaveNonStandardModicationYes_field.click();
			waitFor5Sec();

			String sTempDoesTheCarHaveNonStandard = doesTheCarHaveNonStandardMoficationOrAccessories.toString();
			System.out.println(
					"Does the car have non-standard modifications or accessories? : " + sTempDoesTheCarHaveNonStandard);

			String[] str;
			str = sTempDoesTheCarHaveNonStandard.split(";");

			for (String values : str) {
				values = values.trim();

				System.out.println("Value is : " + values);

				if (values.equalsIgnoreCase("Alcohol Interlock")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[1]/td[1]/div/label"))

							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Alloy/Mag Wheels")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[1]/td[2]/div/label"))
							.click();
				} else if (values.equalsIgnoreCase("Blow Off Valve")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[1]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Body Kit")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[2]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Bonnet Scoops/Bulge")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[2]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Boot Speakers/Amplifer")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[2]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Bull/Roo/Nudge Bar")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[3]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Cam Shaft Alt")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[3]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Campervan Conversion")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[3]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Canopy/Tonneau Cover (Hard Or Soft)")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[4]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Carboretter Alterations")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[4]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Cargo Barrier")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[4]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("CB/UHF Radio")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[5]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Child Seats")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[5]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Computer Chip")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[5]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Custom Modifications")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[6]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Dashcam")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[6]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Dual Control")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[6]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Electric Brakes / Trailer Controls")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[7]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Engine Head/Supercharge")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[7]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Engine Modifications")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[7]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Exhaust Modifications")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[8]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Extractors")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[8]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Fairings")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[8]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Headers")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[9]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Ladder Racks")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[9]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Long Range Fuel Tanks")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[9]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("LPG Conversion/Dual Fuel")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[10]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Nitrous Oxide")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[10]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Non Standard Turbo")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[10]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Non Std Turbo - Diesel")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[11]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Non-factory Conversion")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[11]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Other")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[11]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Paint Custom/Decals/Murals")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[12]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Paint Protection")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[12]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Paint Signwriting/Plastic Wrap")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[12]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Rally Pack")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[13]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Replacement Engine with higher output")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[13]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Roll Bar / Roll Cage / Racing Harness")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[13]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Roof Racks")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[14]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Roof Top Cargo Box")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[14]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Shelving/Storage")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[14]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Side Mirrors")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[15]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Side Skirts")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[15]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Side Steps")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[15]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Snorkel(4WD)")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[16]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Spoiler")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[16]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Sports Steering Wheel")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[16]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Suspension Raised/Lowered")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[17]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Television (Non-Std)")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[17]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Tray")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[17]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Tray Liner (Utility)")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[18]/td[1]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Vinyl Paint Wrap")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[18]/td[2]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Wide Wheels/Rims permitted by RTA")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[18]/td[3]/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else if (values.equalsIgnoreCase("Winch (4WD)")) {
					driver.findElement(
							By.xpath("//*[@id='form_ul']/li[22]/div[2]/span[2]/table/tbody/tr[19]/td/div/label"))
							.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				}
			}

			// Verify Declined Case for Does the car have non-standard
			// modifications or accessories?
			String declined_doesTheStandard1 = "//a[@id='Next2']";
			String declined_doesTheStandard2 = driver.findElement(By.xpath(declined_doesTheStandard1)).getText();
			if (carDetailNextButton.compareToIgnoreCase(declined_doesTheStandard2) != 0) {
				System.out.println("Declined Case => Does the car have non-standard modifications or accessories?"
						+ " : " + doesTheCarHaveNonStandardMoficationOrAccessories);
				ReportsClass.logStat(Status.FATAL,
						"Declined Case => Does the car have non-standard modifications or accessories?" + " : "
								+ doesTheCarHaveNonStandardMoficationOrAccessories);
				return false;
			}
		}

		// Does the car have pre-existing damage
		if (doesTheCarHavePreExistingDamage.equalsIgnoreCase("Yes")) {
			doesCarHavePreExistingDamageYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Verify Declined Case for Does the car have pre-existing damage
			String declined_CarHavePreExistingDamage1 = "//*[@id='custom_tab_p_n_button_li']/a";
			String declined_CarHavePreExistingDamage2 = driver.findElement(By.xpath(declined_CarHavePreExistingDamage1))
					.getText();
			if (carDetailNextButton.compareToIgnoreCase(declined_CarHavePreExistingDamage2) != 0) {
				System.out.println("Declined Case => Does the car have pre-existing damage" + " : "
						+ doesTheCarHavePreExistingDamage);
				ReportsClass.logStat(Status.FATAL, "Declined Case => Does the car have pre-existing damage" + " : "
						+ doesTheCarHavePreExistingDamage);
				return false;
			}

			// Converstion "carDamageArea"
			System.out.println("Damage Area Value Before Parse is : " + carDamageArea);

			String sTempDamageArea = carDamageArea.toString();

			try {
				if (sTempDamageArea.trim().length() == 0) {
					System.out.println("Damage Area value is Empty ........");
				} else {
					String pattern3 = "#";
					Double carDamageAreaParse = Double.parseDouble(sTempDamageArea);
					DecimalFormat carDamageFormat = new DecimalFormat(pattern3);
					String carDamageAreaConversion = carDamageFormat.format(carDamageAreaParse);
					System.out.println("Car Damage Area is " + carDamageAreaConversion);

					Select drpCarDamageArea = new Select(carDamageArea_field);
					drpCarDamageArea.selectByValue(carDamageAreaConversion);
					waitFor2Sec();
					if (isDamageLargetThanOneHandSize_Yes.equalsIgnoreCase("Yes")) {
						isDamageLargerThenOneHandSizeYes_field.click();
						waitFor2Sec();
					} else {
						isDamageLargerThenOneHandSizeNo_field.click();
						waitFor2Sec();
					}
				}
			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			doesCarHavePreExistingDamageNo_field.click();
		}

		// Car has rust damage

		if (carHasRustDamage.equalsIgnoreCase("Yes")) {
			carHasRustDamageYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Verify Declined Case for Car has rust damage
			String declined_CarHasRustDamage1 = "//a[@id='Next2']";
			String declined_CarHasRustDamage2 = driver.findElement(By.xpath(declined_CarHasRustDamage1)).getText();
			if (carDetailNextButton.compareToIgnoreCase(declined_CarHasRustDamage2) != 0) {
				System.out.println("Declined Case => Car has rust damage" + " : " + carHasRustDamage);
				ReportsClass.logStat(Status.FATAL, "Declined Case => Car has rust damage" + " : " + carHasRustDamage);
				return false;
			}

		} else {
			carHasRustDamageNo_field.click();
		}

		// Car has windscreen damage
		if (carHasWindScreenDamage.equalsIgnoreCase("Yes")) {
			carHasWindscreenDamageYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Verify Declined Case for Car has windscreen damage
			String declined_CarWindscreen1 = "//a[@id='Next2']";
			String declined_CarWindscreen2 = driver.findElement(By.xpath(declined_CarWindscreen1)).getText();
			if (carDetailNextButton.compareToIgnoreCase(declined_CarWindscreen2) != 0) {
				System.out.println("Declined Case => Car has windscreen damage" + " : " + carHasWindScreenDamage);
				ReportsClass.logStat(Status.FATAL,
						"Declined Case => Car has windscreen damage" + " : " + carHasWindScreenDamage);
				return false;
			}

		} else {
			carHasWindscreenDamageNo_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		// Car has hail damage
		if (carHasHailDamage.equalsIgnoreCase("Yes")) {
			carHasHailDamageYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Verify Declined Case for Car has hail damage
			String declined_CarHasHailDamage1 = "//a[@id='Next2']";
			String declined_CarHasHailDamage2 = driver.findElement(By.xpath(declined_CarHasHailDamage1)).getText();
			if (carDetailNextButton.compareToIgnoreCase(declined_CarHasHailDamage2) != 0) {
				System.out.println("Declined Case => Car has hail damage" + " : " + carHasHailDamage);
				ReportsClass.logStat(Status.FATAL, "Declined Case => Car has hail damage" + " : " + carHasHailDamage);
				return false;
			}
		} else {
			carHasHailDamageNo_field.click();
		}

		driver.findElement(By.xpath("//*[@id='custom_tab_p_n_button_li']/a")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return true;
	}

	// Car Usage Form
	public boolean carUsage_Step2(String sheetName, int rowNum) throws Exception {
		String carUsage = reader.getCellData(sheetName, "Car usage", rowNum);
		String businessUse = reader.getCellData(sheetName, "Business use", rowNum);
		String rideShare = reader.getCellData(sheetName, "Rideshare", rowNum);
		String carUsageNextButton = "  Next >";

		System.out.println("Car Usage : " + carUsage);

		if (carUsage.toLowerCase().contains("private use only - not")) {
			Select drpCarUsagePrivateUseOnly = new Select(carUsage_field);
			try {
				drpCarUsagePrivateUseOnly.selectByValue("Private Use only - NOT driving to/from work or education");
			} catch (Exception e) {
				System.out.println("");
			}

			// Invalid case for Car Usage = Private Use Only - Not Driving
			String getPrivateUseOnly = drpCarUsagePrivateUseOnly.getFirstSelectedOption().getText().trim();
			if (getPrivateUseOnly
					.compareToIgnoreCase("Private Use only - NOT driving to/from work or education") != 0) {
				System.out.println("Invalid Value => Car Usage" + " : " + getPrivateUseOnly);
				ReportsClass.logStat(Status.FAIL, "Invalid Case => Car Usage" + " : " + getPrivateUseOnly);
				return false;
			}

			// Verify Declined Case for Car Usage = Private Use Only - Not
			String declined_CarPrivateUseOnly1 = "//a[@id='Next3']";
			String declined_CarPrivateUseOnly2 = driver.findElement(By.xpath(declined_CarPrivateUseOnly1)).getText();
			if (carUsageNextButton.compareToIgnoreCase(declined_CarPrivateUseOnly2) != 0) {
				System.out.println("Declined Case => Car Usage" + " : " + getPrivateUseOnly);
				ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getPrivateUseOnly);
				return false;
			}
		} else if (carUsage.toLowerCase().contains("private use including")) {
			Select drpCarUsagePrivateUseIncluding = new Select(carUsage_field);
			try {
				drpCarUsagePrivateUseIncluding
						.selectByValue("Private Use INCLUDING driving to/from place of work or education");
			} catch (Exception e) {
				System.out.println("");
			}
		} else if (carUsage.toLowerCase().contains("private and business use")) {
			Select drpCarUsagePrivateBusiness = new Select(carUsage_field);
			try {
				drpCarUsagePrivateBusiness.selectByValue("Private and Business Use");
			} catch (Exception e) {
				System.out.println("");
			}

			// Invalid Case for Car Usage = Private and Business Use
			String getPrivateAndBusinessUse = drpCarUsagePrivateBusiness.getFirstSelectedOption().getText().trim();
			if (getPrivateAndBusinessUse.compareToIgnoreCase(carUsage) != 0) {
				System.out.println("Invalid Value => Car Usage" + " : " + getPrivateAndBusinessUse);
				ReportsClass.logStat(Status.FAIL, "Invalid Case => Car Usage" + " : " + getPrivateAndBusinessUse);
				return false;
			}

			// Verify Declined Case for Car Usage = Private and Business Use
			String declined_CarPrivateUseBusinessUse1 = "//a[@id='Next3']";
			String declined_CarPrivateUseBusinessUse2 = driver.findElement(By.xpath(declined_CarPrivateUseBusinessUse1))
					.getText();
			if (carUsageNextButton.compareToIgnoreCase(declined_CarPrivateUseBusinessUse2) != 0) {
				System.out.println("Declined Case => Car Usage" + " : " + getPrivateAndBusinessUse);
				ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getPrivateAndBusinessUse);
				return false;
			}

			if (businessUse.toLowerCase().contains("courier")) {
				Select drpCourier = new Select(businessUse_field);
				try {
					drpCourier.selectByValue("Courier / Delivery");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Courier/Delivery
				String getBusinessUseCourier = drpCourier.getFirstSelectedOption().getText().trim();
				if (getBusinessUseCourier.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseCourier);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Business Use" + " : " + getBusinessUseCourier);
					return false;
				}

				// Verify Declined Case for Car Usage = Courier/Delivery
				String declined_Courier1 = "//a[@id='Next3']";
				String declined_Courier2 = driver.findElement(By.xpath(declined_Courier1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Courier2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseCourier);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseCourier);
					return false;
				}

			} else if (businessUse.toLowerCase().contains("domestic")) {
				Select drpDomestic = new Select(businessUse_field);
				try {
					drpDomestic.selectByValue("Domestic / commercial services e.g. nanny, cleaner");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Domestic / commercial
				// services
				String getBusinessUseDomestic = drpDomestic.getFirstSelectedOption().getText().trim();
				if (getBusinessUseDomestic.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseDomestic);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Business Use" + " : " + getBusinessUseDomestic);
					return false;
				}

				// Verify Declined Case for Car Usage = Domestic / commercial
				// services
				String declined_Domestic1 = "//a[@id='Next3']";
				String declined_Domestic2 = driver.findElement(By.xpath(declined_Domestic1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Domestic2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseDomestic);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseDomestic);
					return false;
				}
			} else if (businessUse.toLowerCase().contains("driver")) {
				Select drpDriver = new Select(businessUse_field);
				try {
					drpDriver.selectByValue("Driver education");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Driver education
				String getBusinessUseDriverEducation = drpDriver.getFirstSelectedOption().getText().trim();
				if (getBusinessUseDriverEducation.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseDriverEducation);
					ReportsClass.logStat(Status.FAIL,
							"Invalid Case => Business Use" + " : " + getBusinessUseDriverEducation);
					return false;
				}

				// Verify Declined Case for Car Usage = Driver education
				String declined_Driver1 = "//a[@id='Next3']";
				String declined_Driver2 = driver.findElement(By.xpath(declined_Driver1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Driver2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseDriverEducation);
					ReportsClass.logStat(Status.FATAL,
							"Declined Case => Car Usage" + " : " + getBusinessUseDriverEducation);
					return false;
				}

			} else if (businessUse.toLowerCase().contains("farmer")) {
				Select drpFarmer = new Select(businessUse_field);
				try {
					drpFarmer.selectByValue("Farmer");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Farmer
				String getBusinessUseFarmer = drpFarmer.getFirstSelectedOption().getText().trim();
				if (getBusinessUseFarmer.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseFarmer);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Business Use" + " : " + getBusinessUseFarmer);
					return false;
				}

				// Verify Declined Case for Car Usage = Farmer
				String declined_Farmer1 = "//a[@id='Next3']";
				String declined_Farmer2 = driver.findElement(By.xpath(declined_Farmer1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Farmer2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseFarmer);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseFarmer);
					return false;
				}
			} else if (businessUse.toLowerCase().contains("financial")) {
				Select drpFinancial = new Select(businessUse_field);
				try {
					drpFinancial.selectByValue("Financial Services");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Financial Services
				String getBusinessUseFinancial = drpFinancial.getFirstSelectedOption().getText().trim();
				if (getBusinessUseFinancial.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseFinancial);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Business Use" + " : " + getBusinessUseFinancial);
					return false;
				}

				// Verify Declined Case for Car Usage = Financial Services
				String declined_Financial1 = "//a[@id='Next3']";
				String declined_Financial2 = driver.findElement(By.xpath(declined_Financial1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Financial2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseFinancial);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseFinancial);
					return false;
				}
			} else if (businessUse.toLowerCase().contains("food")) {
				Select drpFood = new Select(businessUse_field);
				try {
					drpFood.selectByValue("Food delivery");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Food
				String getBusinessUseFood = drpFood.getFirstSelectedOption().getText().trim();
				if (getBusinessUseFood.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseFood);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Business Use" + " : " + getBusinessUseFood);
					return false;
				}

				// Verify Declined Case for Car Usage = Food
				String declined_Food1 = "//a[@id='Next3']";
				String declined_Food2 = driver.findElement(By.xpath(declined_Food1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Food2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseFood);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseFood);
					return false;
				}
			} else if (businessUse.toLowerCase().contains("grey")) {
				Select drpGrey = new Select(businessUse_field);
				try {
					drpGrey.selectByValue("Grey Fleet");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Grey Fleet
				String getBusinessUseGreyFleet = drpGrey.getFirstSelectedOption().getText().trim();
				if (getBusinessUseGreyFleet.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseGreyFleet);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Business Use" + " : " + getBusinessUseGreyFleet);
					return false;
				}

				// Verify Declined Case for Car Usage = Grey Fleet
				String declined_Grey1 = "//a[@id='Next3']";
				String declined_Grey2 = driver.findElement(By.xpath(declined_Grey1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Grey2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseGreyFleet);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseGreyFleet);
					return false;
				}

				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				// Select Grey Field Value
				String greyFieldValue = "BJSIB";
				String getGreyFieldValue = greyFieldValue.toString().trim();
				Select drpGreyField = new Select(greyFleet_field);
				try {
					drpGreyField.selectByValue(getGreyFieldValue);
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Grey Field = BJSIB
				String getGreyFleetBJSIB = drpGreyField.getFirstSelectedOption().getText().trim();
				if (getGreyFleetBJSIB.compareToIgnoreCase("BJSIB") != 0) {
					System.out.println("Invalid Value => Grey Fleet " + " : " + getGreyFleetBJSIB);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Grey Fleet " + " : " + getGreyFleetBJSIB);
					return false;
				}

				// Verify Declined Case for Grey Field = BJSIB
				String declined_GreyField1 = "//a[@id='Next3']";
				String declined_GreyField2 = driver.findElement(By.xpath(declined_GreyField1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_GreyField2) != 0) {
					System.out.println("Declined Case => Grey Fleet " + " : " + getGreyFleetBJSIB);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Grey Fleet " + " : " + getGreyFleetBJSIB);
					return false;
				}

			} else if (businessUse.toLowerCase().contains("health care")) {
				Select drpHealthCare = new Select(businessUse_field);
				try {
					drpHealthCare.selectByValue("Health care provider (excluding ambulance)");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Health Care Provider
				String getBusinessUseHealthCare = drpHealthCare.getFirstSelectedOption().getText().trim();
				if (getBusinessUseHealthCare.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseHealthCare);
					ReportsClass.logStat(Status.FAIL,
							"Invalid Case => Business Use" + " : " + getBusinessUseHealthCare);
					return false;
				}

				// Verify Declined Case for Car Usage = Health Care Provider
				String declined_Health1 = "//a[@id='Next3']";
				String declined_Health2 = driver.findElement(By.xpath(declined_Health1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Health2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseHealthCare);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseHealthCare);
					return false;
				}
			} else if (businessUse.toLowerCase().contains("professional")) {
				Select drpProfessional = new Select(businessUse_field);
				try {
					drpProfessional.selectByValue("Professional / Salesperson e.g. real estate agent");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = "Professional / Salesperson
				String getBusinessUseProfessional = drpProfessional.getFirstSelectedOption().getText().trim();
				if (getBusinessUseProfessional.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseProfessional);
					ReportsClass.logStat(Status.FAIL,
							"Invalid Case => Business Use" + " : " + getBusinessUseProfessional);
					return false;
				}

				// Verify Declined Case for Car Usage = "Professional /
				// Salesperson
				String declined_Professional1 = "//a[@id='Next3']";
				String declined_Professional2 = driver.findElement(By.xpath(declined_Professional1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Professional2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseProfessional);
					ReportsClass.logStat(Status.FATAL,
							"Declined Case => Car Usage" + " : " + getBusinessUseProfessional);
					return false;
				}
			} else if (businessUse.toLowerCase().contains("rideshare")) {
				Select drpRideShare = new Select(businessUse_field);
				try {
					drpRideShare.selectByValue("Rideshare [passengers/car pooling]");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Rideshare [passengers/car
				// pooling]
				String getBusinessUseRideShare = drpRideShare.getFirstSelectedOption().getText().trim();
				if (getBusinessUseRideShare.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseRideShare);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Business Use" + " : " + getBusinessUseRideShare);
					return false;
				}

				// Verify Declined Case for Car Usage = Rideshare
				// [passengers/car pooling]
				String declined_RideShare1 = "//a[@id='Next3']";
				String declined_RideShare2 = driver.findElement(By.xpath(declined_RideShare1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_RideShare2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseRideShare);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseRideShare);
					return false;
				}

				if (rideShare.toLowerCase().contains("didi")) {
					Select drpDiDi = new Select(rideShare_field);
					try {
						drpDiDi.selectByValue("DiDi");
					} catch (Exception e) {
						System.out.println("");
					}

					// Invalid Case for RideShare = DiDi
					String getRideShareDiDi = drpDiDi.getFirstSelectedOption().getText().trim();
					if (getRideShareDiDi.compareToIgnoreCase(rideShare) != 0) {
						System.out.println("Invalid Value =>Ride Share" + " : " + getRideShareDiDi);
						ReportsClass.logStat(Status.FAIL, "Invalid Case => Ride Share" + " : " + getRideShareDiDi);
						return false;
					}

					// Declined Case for RideShare = DiDi
					String declined_RideShareDiDi1 = "//a[@id='Next3']";
					String declined_RideShareDiDi2 = driver.findElement(By.xpath(declined_RideShareDiDi1)).getText();
					if (carUsageNextButton.compareToIgnoreCase(declined_RideShareDiDi2) != 0) {
						System.out.println("Declined Case => Ride Share" + " : " + getRideShareDiDi);
						ReportsClass.logStat(Status.FATAL, "Declined Case => Ride Share" + " : " + getRideShareDiDi);
						return false;
					}

				} else if (rideShare.toLowerCase().contains("gocatch")) {
					Select drpGoCatch = new Select(rideShare_field);
					try {
						drpGoCatch.selectByValue("GoCatch");
					} catch (Exception e) {
						System.out.println("");
					}

					// Invalid Case for RideShare = GoCatch
					String getRideShareGoCatch = drpGoCatch.getFirstSelectedOption().getText().trim();
					if (getRideShareGoCatch.compareToIgnoreCase(rideShare) != 0) {
						System.out.println("Invalid Value =>Ride Share" + " : " + getRideShareGoCatch);
						ReportsClass.logStat(Status.FAIL, "Invalid Case => Ride Share " + " : " + getRideShareGoCatch);
						return false;
					}

					// Declined Case for RideShare = GoCatch
					String declined_RideShareGoCatch1 = "//a[@id='Next3']";
					String declined_RideShareGoCatch2 = driver.findElement(By.xpath(declined_RideShareGoCatch1))
							.getText();
					if (carUsageNextButton.compareToIgnoreCase(declined_RideShareGoCatch2) != 0) {
						System.out.println("Declined Case => Ride Share" + " : " + getRideShareGoCatch);
						ReportsClass.logStat(Status.FATAL, "Declined Case => Ride Share" + " : " + getRideShareGoCatch);
						return false;
					}
				} else if (rideShare.toLowerCase().contains("ola")) {
					Select drpOla = new Select(rideShare_field);
					try {
						drpOla.selectByValue("Ola");
					} catch (Exception e) {
						System.out.println("");
					}

					// Invalid Case for RideShare = Ola
					String getRideShareOla = drpOla.getFirstSelectedOption().getText().trim();
					if (getRideShareOla.compareToIgnoreCase(rideShare) != 0) {
						System.out.println("Invalid Value =>Ride Share" + " : " + getRideShareOla);
						ReportsClass.logStat(Status.FAIL, "Invalid Case => Ride Share" + " : " + getRideShareOla);
						return false;
					}

					// Declined Case for RideShare = Ola
					String declined_RideShareOla1 = "//a[@id='Next3']";
					String declined_RideShareOla2 = driver.findElement(By.xpath(declined_RideShareOla1)).getText();
					if (carUsageNextButton.compareToIgnoreCase(declined_RideShareOla2) != 0) {
						System.out.println("Declined Case => Ride Share" + " : " + getRideShareOla);
						ReportsClass.logStat(Status.FATAL, "Declined Case => Ride Share" + " : " + getRideShareOla);
						return false;
					}
				} else if (rideShare.toLowerCase().contains("shofer")) {
					Select drpShofer = new Select(rideShare_field);
					try {
						drpShofer.selectByValue("Shofer");
					} catch (Exception e) {
						System.out.println("");
					}

					// Invalid Case for RideShare = Shofer
					String getRideShareShofer = drpShofer.getFirstSelectedOption().getText().trim();
					if (getRideShareShofer.compareToIgnoreCase(rideShare) != 0) {
						System.out.println("Invalid Value =>Ride Share" + " : " + getRideShareShofer);
						ReportsClass.logStat(Status.FAIL, "Invalid Case => Ride Share" + " : " + getRideShareShofer);
						return false;
					}

					// Declined Case for RideShare = Shofer
					String declined_RideShareShofer1 = "//a[@id='Next3']";
					String declined_RideShareShofer2 = driver.findElement(By.xpath(declined_RideShareShofer1))
							.getText();
					if (carUsageNextButton.compareToIgnoreCase(declined_RideShareShofer2) != 0) {
						System.out.println("Declined Case => Ride Share" + " : " + getRideShareShofer);
						ReportsClass.logStat(Status.FATAL, "Declined Case => Ride Share" + " : " + getRideShareShofer);
						return false;
					}
				} else if (rideShare.toLowerCase().contains("taxify")) {
					Select drpTaxify = new Select(rideShare_field);
					try {
						drpTaxify.selectByValue("Taxify");
					} catch (Exception e) {
						System.out.println("");
					}

					// Invalid Case for RideShare = Taxify
					String getRideShareTaxify = drpTaxify.getFirstSelectedOption().getText().trim();
					if (getRideShareTaxify.compareToIgnoreCase(rideShare) != 0) {
						System.out.println("Invalid Value =>Ride Share" + " : " + getRideShareTaxify);
						ReportsClass.logStat(Status.FAIL, "Invalid Case => Ride Share" + " : " + getRideShareTaxify);
						return false;
					}

					// Declined Case for RideShare = Taxify
					String declined_RideShareTaxify1 = "//a[@id='Next3']";
					String declined_RideShareTaxify2 = driver.findElement(By.xpath(declined_RideShareTaxify1))
							.getText();
					if (carUsageNextButton.compareToIgnoreCase(declined_RideShareTaxify2) != 0) {
						System.out.println("Declined Case => Ride Share" + " : " + getRideShareTaxify);
						ReportsClass.logStat(Status.FATAL, "Declined Case => Ride Share" + " : " + getRideShareTaxify);
						return false;
					}
				} else if (rideShare.equalsIgnoreCase("Uber")) {
					Select drpUber = new Select(rideShare_field);
					try {
						drpUber.selectByValue("Uber");
					} catch (Exception e) {
						System.out.println("");
					}

					// Invalid Case for RideShare = Uber
					String getRideShareUber = drpUber.getFirstSelectedOption().getText().trim();
					if (getRideShareUber.compareToIgnoreCase(rideShare) != 0) {
						System.out.println("Invalid Value =>Ride Share" + " : " + getRideShareUber);
						ReportsClass.logStat(Status.FAIL, "Invalid Case => Ride Share" + " : " + getRideShareUber);
						return false;
					}

					// Declined Case for RideShare = Uber
					String declined_RideShareUber1 = "//a[@id='Next3']";
					String declined_RideShareUber2 = driver.findElement(By.xpath(declined_RideShareUber1)).getText();
					if (carUsageNextButton.compareToIgnoreCase(declined_RideShareUber2) != 0) {
						System.out.println("Declined Case => Ride Share" + " : " + getRideShareUber);
						ReportsClass.logStat(Status.FATAL, "Declined Case => Ride Share" + " : " + getRideShareUber);
						return false;
					}
				} else if (rideShare.toLowerCase().contains("uber eat")) {
					Select drpUberEats = new Select(rideShare_field);
					try {
						drpUberEats.selectByValue("Uber Eats");
					} catch (Exception e) {
						System.out.println("");
					}

					// Invalid Case for RideShare = Uber Eats
					String getRideShareUberEats = drpUberEats.getFirstSelectedOption().getText().trim();
					if (getRideShareUberEats.compareToIgnoreCase(rideShare) != 0) {
						System.out.println("Invalid Value =>Ride Share" + " : " + getRideShareUberEats);
						ReportsClass.logStat(Status.FAIL, "Invalid Case => Ride Share" + " : " + getRideShareUberEats);
						return false;
					}

					// Declined Case for RideShare = Uber Eats
					String declined_RideShareUberEats1 = "//a[@id='Next3']";
					String declined_RideShareUberEats2 = driver.findElement(By.xpath(declined_RideShareUberEats1))
							.getText();
					if (carUsageNextButton.compareToIgnoreCase(declined_RideShareUberEats2) != 0) {
						System.out.println("Declined Case => Ride Share" + " : " + getRideShareUberEats);
						ReportsClass.logStat(Status.FATAL,
								"Declined Case => Ride Share" + " : " + getRideShareUberEats);
						return false;
					}
				} else if (rideShare.toLowerCase().contains("other")) {
					Select drpOther = new Select(rideShare_field);
					try {
						drpOther.selectByValue("Other");
					} catch (Exception e) {
						System.out.println("");
					}

					// Invalid Case for RideShare = Other
					String getRideShareOther = drpOther.getFirstSelectedOption().getText().trim();
					if (getRideShareOther.compareToIgnoreCase(rideShare) != 0) {
						System.out.println("Invalid Value =>Ride Share" + " : " + getRideShareOther);
						ReportsClass.logStat(Status.FAIL, "Invalid Case => Ride Share" + " : " + getRideShareOther);
						return false;
					}

					// Declined Case for RideShare = Other
					String declined_RideShareOther1 = "//a[@id='Next3']";
					String declined_RideShareOther2 = driver.findElement(By.xpath(declined_RideShareOther1)).getText();
					if (carUsageNextButton.compareToIgnoreCase(declined_RideShareOther2) != 0) {
						System.out.println("Declined Case => Ride Share" + " : " + getRideShareOther);
						ReportsClass.logStat(Status.FATAL, "Declined Case => Ride Share" + " : " + getRideShareOther);
						return false;
					}
				}

			} else if (businessUse.toLowerCase().contains("taxi")) {
				Select drpTaxi = new Select(businessUse_field);
				try {
					drpTaxi.selectByValue("Taxi");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Taxi
				String getBusinessUseTaxi = drpTaxi.getFirstSelectedOption().getText().trim();
				if (getBusinessUseTaxi.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseTaxi);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Business Use" + " : " + getBusinessUseTaxi);
					return false;
				}

				// Verify Declined Case for Car Usage = Taxi
				String declined_Taxi1 = "//a[@id='Next3']";
				String declined_Taxi2 = driver.findElement(By.xpath(declined_Taxi1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Taxi2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseTaxi);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseTaxi);
					return false;
				}
			} else if (businessUse.toLowerCase().contains("tradesperson")) {
				Select drpTradePerson = new Select(businessUse_field);
				try {
					drpTradePerson.selectByValue("Tradesperson");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Tradesperson
				String getBusinessUseTradesperson = drpTradePerson.getFirstSelectedOption().getText().trim();
				if (getBusinessUseTradesperson.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseTradesperson);
					ReportsClass.logStat(Status.FAIL,
							"Invalid Case => Business Use" + " : " + getBusinessUseTradesperson);
					return false;
				}

				// Verify Declined Case for Car Usage = Tradesperson
				String declined_Tradesperson1 = "//a[@id='Next3']";
				String declined_Tradesperson2 = driver.findElement(By.xpath(declined_Tradesperson1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Tradesperson2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseTradesperson);
					ReportsClass.logStat(Status.FATAL,
							"Declined Case => Car Usage" + " : " + getBusinessUseTradesperson);
					return false;
				}

			} else if (businessUse.toLowerCase().contains("other")) {
				Select drpOther = new Select(businessUse_field);
				try {
					drpOther.selectByValue("Other");
				} catch (Exception e) {
					System.out.println("");
				}

				// Invalid Case for Business Use = Other
				String getBusinessUseOther = drpOther.getFirstSelectedOption().getText().trim();
				if (getBusinessUseOther.compareToIgnoreCase(businessUse) != 0) {
					System.out.println("Invalid Value =>Business Use" + " : " + getBusinessUseOther);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Business Use" + " : " + getBusinessUseOther);
					return false;
				}

				// Verify Declined Case for Car Usage = Other
				String declined_Other1 = "//a[@id='Next3']";
				String declined_Other2 = driver.findElement(By.xpath(declined_Other1)).getText();
				if (carUsageNextButton.compareToIgnoreCase(declined_Other2) != 0) {
					System.out.println("Declined Case => Car Usage" + " : " + getBusinessUseOther);
					ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessUseOther);
					return false;
				}
			}

		} else if (carUsage.toLowerCase().contains("business only")) {
			Select drpCarUsageBusinessOnly = new Select(carUsage_field);
			try {
				drpCarUsageBusinessOnly.selectByValue("Business Only (no Private Use)");
			} catch (Exception e) {
				System.out.println("");
			}

			// Invalid Case for Car Usage = Business Only
			String getBusinessOnly = drpCarUsageBusinessOnly.getFirstSelectedOption().getText().trim();
			if (getBusinessOnly.compareToIgnoreCase(businessUse) != 0) {
				System.out.println("Invalid Value => Car Usage" + " : " + getBusinessOnly);
				ReportsClass.logStat(Status.FAIL, "Invalid Case => Car Usage" + " : " + getBusinessOnly);
				return false;
			}

			// Verify Declined Case for Car Usage = Business Only
			String declined_CarPrivateUseBusinessOnly1 = "//a[@id='Next3']";
			String declined_CarPrivateUseBusinessOnly2 = driver
					.findElement(By.xpath(declined_CarPrivateUseBusinessOnly1)).getText();
			if (carUsageNextButton.compareToIgnoreCase(declined_CarPrivateUseBusinessOnly2) != 0) {
				System.out.println("Declined Case => Car Usage" + " : " + getBusinessOnly);
				ReportsClass.logStat(Status.FATAL, "Declined Case => Car Usage" + " : " + getBusinessOnly);
				return false;
			}
		}

		driver.findElement(By.xpath("//a[@id='Next3']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		return true;
	}

	// Kept Address Details Form
	public boolean keptAddressDetails_step3(String sheetName, int rowNum) throws Exception {
		String keptAtNightSituation = reader.getCellData(sheetName, "Kept at night situation", rowNum);
		String keptDuringTheDaySituation = reader.getCellData(sheetName, "Kept during the day situation", rowNum);

		sameAsPostalAddress_field.click();
		waitFor1Sec();

		String sTempKeptAtNightSituation = CommonFunction.capitaliseFirstLetterEntireString(keptAtNightSituation);
		System.out.println("Kept at night situation : " + sTempKeptAtNightSituation);

		Select drpKeptAtNightSituation = new Select(keptAtNightSituation_field);
		try {
			drpKeptAtNightSituation.selectByValue(sTempKeptAtNightSituation);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Kept at night situation
		String getKeptAtNightSituation = drpKeptAtNightSituation.getFirstSelectedOption().getText().trim();
		if (sTempKeptAtNightSituation.compareToIgnoreCase(getKeptAtNightSituation) != 0) {
			System.out.println("Invalid Value => Kept at night situation" + " : " + keptAtNightSituation);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Kept at night situation" + " : " + keptAtNightSituation);
			return false;
		}

		sameAsKeptAtNightAddress_field.click();
		waitFor1Sec();

		String sTempKeptDuringTheDaySituation = CommonFunction
				.capitaliseFirstLetterEntireString(keptDuringTheDaySituation);
		Select drpKeptDuringTheDayAddress = new Select(keptDuringTheDaySituation_field);
		try {
			drpKeptDuringTheDayAddress.selectByValue(sTempKeptDuringTheDaySituation);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Kept during the day situation

		System.out.println("Kept During the Day Situation : " + sTempKeptDuringTheDaySituation);

		String getSameAsKeptAtNightSituation = drpKeptDuringTheDayAddress.getFirstSelectedOption().getText().trim();
		if (sTempKeptDuringTheDaySituation.compareToIgnoreCase(getSameAsKeptAtNightSituation) != 0) {
			System.out.println("Invalid Value => Kept during the day situation" + " : " + keptDuringTheDaySituation);
			ReportsClass.logStat(Status.FAIL,
					"Invalid Case => Kept during the day situation" + " : " + keptDuringTheDaySituation);
			return false;
		}

		waitFor1Sec();
		driver.findElement(By.xpath("//a[@id='Next25']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return true;
	}

	// Driver Details Form
	public boolean driverDetails_Step4(String sheetName, int rowNum) throws Exception {
		// Driver 1 Details
		String permittedDriverAge = reader.getCellData(sheetName, "Permitted driver age range", rowNum);
		String licenceCountry = reader.getCellData(sheetName, "Licence country", rowNum);
		String licenceType = reader.getCellData(sheetName, "Licence type", rowNum);
		String licenceYear = reader.getCellData(sheetName, "Licence years", rowNum);
		String hasThisDriverUnderALicenceSuspensionOrCancellation = reader.getCellData(sheetName,
				"Has this driver been under a licence suspension or cancellation, or restricted licence during the past 5 years?",
				rowNum);
		String hasThisDriverHadAnyAlcoholOrDrug = reader.getCellData(sheetName,
				"Has this driver had any alcohol or drug-related driving charges or dangerous driving charges within the last 5 years?",
				rowNum);
		String hasThidDriverHadCarInsuranceCancelled = reader.getCellData(sheetName,
				"Has this driver had their car insurance cancelled or refused at renewal in the past 5 years?", rowNum);
		String hasThisDriverHadAnyClaimInPast5Years = reader.getCellData(sheetName,
				"Has this driver, regardless of who was at fault, had any claims in the past 5 years?", rowNum);
		String criminalHistory = reader.getCellData(sheetName, "Criminal history", rowNum);
		String bankruptcy = reader.getCellData(sheetName, "Bankruptcy", rowNum);
		String isThereA2ndDriver = reader.getCellData(sheetName, "Is there a 2nd driver?", rowNum);
		String claimType = reader.getCellData(sheetName, "Claim Type", rowNum);
		String claimMonth = reader.getCellData(sheetName, "Claim Month", rowNum);
		String claimYear = reader.getCellData(sheetName, "Claim Year", rowNum);
		String didYouPayAnExcess = reader.getCellData(sheetName, "Did you pay an excess", rowNum);

		// Driver 2 Details
		String removed2ndDriverFromPolicy = reader.getCellData(sheetName, "Remove 2nd driver from policy?", rowNum);
		String driver2Salutation = reader.getCellData(sheetName, "Driver 2 Salutation", rowNum);
		String driver2FirstName = reader.getCellData(sheetName, "Driver 2 First name", rowNum);
		String driver2FamilyName = reader.getCellData(sheetName, "Driver 2 Family name", rowNum);
		String driver2Gender = reader.getCellData(sheetName, "Driver 2 Gender", rowNum);
		String driver2DateOfBirth = reader.getCellData(sheetName, "Driver 2 Date of birth", rowNum);
		String driver2Mobile = reader.getCellData(sheetName, "Driver 2 Mobile", rowNum);
		String driver2LicenceType = reader.getCellData(sheetName, "Driver 2 Licence type", rowNum);
		String driver2LicenceCountry = reader.getCellData(sheetName, "Driver 2 Licence country", rowNum);
		String driver2LicenceYear = reader.getCellData(sheetName, "Driver 2 Licence years", rowNum);
		String driver2HasBeenSuspensionOrCancellation = reader.getCellData(sheetName,
				"Has Driver 2 been under a licence suspension or cancellation, or restricted licence during the past 5 years?",
				rowNum);
		String driver2HasAnyAlcoholOrDrug = reader.getCellData(sheetName,
				"Has Driver 2 had any alcohol or drug-related driving charges or dangerous driving charges within the last 5 years?",
				rowNum);
		String driver2HadCarInsuranceCancelled = reader.getCellData(sheetName,
				"Has Driver 2 had their car insurance cancelled or refused at renewal in the past 5 years?", rowNum);
		String driver2HadAnyClaimPast5Year = reader.getCellData(sheetName,
				"Has Driver 2, regardless of who was at fault, had any claims in the past 5 years?", rowNum);
		String driver2CriminalHistory = reader.getCellData(sheetName, "Driver 2 Criminal history", rowNum);
		String driver2Bankruptcy = reader.getCellData(sheetName, "Driver 2 Bankruptcy", rowNum);
		String isThere3rdDriver = reader.getCellData(sheetName, "Is there a 3rd driver?", rowNum);
		String driver2ClaimType = reader.getCellData(sheetName, "Driver 2 Claim Type", rowNum);
		String driver2ClaimMonth = reader.getCellData(sheetName, "Driver 2 Claim Month", rowNum);
		String driver2ClaimYear = reader.getCellData(sheetName, "Driver 2 Claim Year", rowNum);
		String driver2DidYouPayAnExcess = reader.getCellData(sheetName, "Driver 2 Did you pay an excess", rowNum);

		// Driver 3 Details
		String removed3rdDriverFromPolicy = reader.getCellData(sheetName, "Remove 3rd driver from policy?", rowNum);
		String driver3Salutation = reader.getCellData(sheetName, "Driver 3 Salutation", rowNum);
		String driver3FirstName = reader.getCellData(sheetName, "Driver 3 First name", rowNum);
		String driver3FamilyName = reader.getCellData(sheetName, "Driver 3 Family name", rowNum);
		String driver3Gender = reader.getCellData(sheetName, "Driver 3 Gender", rowNum);
		String driver3Mobile = reader.getCellData(sheetName, "Driver 3 Mobile", rowNum);
		String driver3LicenceType = reader.getCellData(sheetName, "Driver 3 Licence type", rowNum);
		String driver3LicenceCountry = reader.getCellData(sheetName, "Driver 3 Licence country", rowNum);
		String driver3LicenceYear = reader.getCellData(sheetName, "Driver 3 Licence years", rowNum);
		String driver3HasBeenLicenceSuspensionOrCancellatin = reader.getCellData(sheetName,
				"Has Driver 3 been under a licence suspension or cancellation, or restricted licence during the past 5 years?",
				rowNum);
		String driver3HadAnyAlcoholOrDrugRelatedDrivingCharges = reader.getCellData(sheetName,
				"Has Driver 3 had any alcohol or drug-related driving charges or dangerous driving charges within the last 5 years?",
				rowNum);
		String driver3HadTheirCarInsuranceCancelledOrRefused = reader.getCellData(sheetName,
				"Has Driver 3 had their car insurance cancelled or refused at renewal in the past 5 years?", rowNum);
		String driver3HadAnyClaimInThePast5Year = reader.getCellData(sheetName,
				"Has Driver 3, regardless of who was at fault, had any claims in the past 5 years?", rowNum);
		String driver3ClaimType = reader.getCellData("Part-3", "Driver 3 Claim Type", rowNum);
		String driver3ClaimMonth = reader.getCellData("Part-3", "Driver 3 Claim Month", rowNum);
		String driver3ClaimYear = reader.getCellData("Part-3", "Driver 3 Claim Year", rowNum);
		String driver3DidYouAnExcess = reader.getCellData("Part-3", "Driver 3 Did you pay an excess", rowNum);
		String driver3CriminalHistory = reader.getCellData("Part-3", "Driver 3 Criminal history", rowNum);
		String driver3Bankruptcy = reader.getCellData("Part-3", "Driver 3 Bankruptcy", rowNum);
		String isThere4ThDriver = reader.getCellData("Part-3", "Is there a 4th driver?", rowNum);

		String driverDetailNextButton = "//a[@class='ct_next_step']";

		// Select Permitted driver age range dropdown values
		String sTempPermittedDriverAge = CommonFunction.capitaliseFirstLetterEntireString(permittedDriverAge);
		Select drpPermittedDriverAge = new Select(permittedDriverAgeRange_field);
		System.out.println("Permitted Driver Age Range is " + sTempPermittedDriverAge);
		try {
			drpPermittedDriverAge.selectByValue(sTempPermittedDriverAge);
		} catch (Exception e) {
			System.out.println("");
		}
		waitFor1Sec();

		// Verify Invalid Case for Permitted driver age range
		String getPermittedDriverAgeRange = drpPermittedDriverAge.getFirstSelectedOption().getText().trim();
		if (sTempPermittedDriverAge.compareToIgnoreCase(getPermittedDriverAgeRange) != 0) {
			System.out.println("Invalid Value => Permitted driver age range" + " : " + permittedDriverAge);
			ReportsClass.logStat(Status.FAIL,
					"Invalid Case => Permitted driver age range" + " : " + permittedDriverAge);
			return false;
		}

		// Select Licence country dropdown values
		if (CommonFunction.selectLicenceCountry(licenceCountry, licenceCountry_field) == false) {
			System.out.println("Invalid Value => Licence Country" + " : " + licenceCountry);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Licence Country : " + licenceCountry);
			return false;
		}

		waitFor1Sec();

		// Select Licence Type dropdown values
		if (CommonFunction.selectLicenceType(licenceType, licenceType_field) == false) {
			System.out.println("Invalid Value => Licence Type" + " : " + licenceType);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Licence Type : " + licenceType);
			return false;
		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Select Licence Year dropdown values
		if (CommonFunction.selectLicenceYear(licenceType, licenceYear, licenceYear_field) == false) {
			System.out.println("Invalid Value => Licence Year" + " : " + licenceYear);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Licence year : " + licenceYear);
			return false;
		}
		waitFor1Sec();

		// Has this driver been under a licence suspension or cancellation, or
		// restricted licence during the past 5 years?
		String option1 = hasThisDriverUnderALicenceSuspensionOrCancellation.toString().trim();
		System.out.println("Option 1 is :==> " + option1);
		if (option1.equalsIgnoreCase("No") || option1.trim().length() == 0) {
			hasThisDriverLicenceSuspensionNo_field.click();
		} else {
			hasThisDriverLicenceSuspensionYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Verify Declined Case for Has this driver been under a licence
			// suspension or cancellation, or restricted licence during the past
			// 5 years?
			String declined_Option1 = "//a[@id='Next26']";
			String declined_Option11 = driver.findElement(By.xpath(declined_Option1)).getText();
			if (driverDetailNextButton.compareToIgnoreCase(declined_Option11) != 0) {
				System.out.println(
						"Declined Case => Has this driver been under a licence suspension or cancellation, or restricted licence during the past 5 years?"
								+ " : " + hasThisDriverUnderALicenceSuspensionOrCancellation);
				ReportsClass.logStat(Status.FATAL,
						"Declined Case => Has this driver been under a licence suspension or cancellation, or restricted licence during the past 5 years?"
								+ " : " + hasThisDriverUnderALicenceSuspensionOrCancellation);
				return false;
			}
		}

		// Has this driver had any alcohol or drug-related driving charges or
		// dangerous driving charges within the past 5 years?
		String option2 = hasThisDriverHadAnyAlcoholOrDrug.toString().trim();
		System.out.println("Option 2 is :==> " + option2);
		if (option2.equalsIgnoreCase("No") || option2.trim().length() == 0) {
			hasThisDriverAlcoholOrDrugNo_field.click();
		} else {
			hasThisDriverAlcoholOrDrugYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Verify Declined Case for Has this driver had any alcohol or
			// drug-related driving charges or dangerous driving charges within
			// the past 5 years?
			String declined_Option2 = "//a[@id='Next26']";
			String declined_Option22 = driver.findElement(By.xpath(declined_Option2)).getText();
			if (driverDetailNextButton.compareToIgnoreCase(declined_Option22) != 0) {
				System.out.println(
						"Declined Case => Has this driver had any alcohol or drug-related driving charges or dangerous driving charges within the past 5 years?"
								+ " : " + hasThisDriverHadAnyAlcoholOrDrug);
				ReportsClass.logStat(Status.FATAL,
						"Declined Case => Has this driver had any alcohol or drug-related driving charges or dangerous driving charges within the past 5 years?"
								+ " : " + hasThisDriverHadAnyAlcoholOrDrug);
				return false;
			}
		}

		// Has this driver had their car insurance cancelled or refused at
		// renewal in the past 5 years?
		String option3 = hasThidDriverHadCarInsuranceCancelled.toString().trim();
		System.out.println("Option 3 is :==> " + option3);
		if (option3.equalsIgnoreCase("No") || option3.trim().length() == 0) {
			hasThisDriverCarInsuranceCalcelledNo_field.click();
		} else {
			hasThisDriverCarInsuranceCalcelledYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Verify Declined Case for Has this driver had their car insurance
			// cancelled or refused at renewal in the past 5 years?
			String declined_Option3 = "//a[@id='Next26']";
			String declined_Option33 = driver.findElement(By.xpath(declined_Option3)).getText();
			if (driverDetailNextButton.compareToIgnoreCase(declined_Option33) != 0) {
				System.out.println(
						"Declined Case => Has this driver had their car insurance cancelled or refused at renewal in the past 5 years?"
								+ " : " + hasThidDriverHadCarInsuranceCancelled);
				ReportsClass.logStat(Status.FATAL,
						"Declined Case => Has this driver had their car insurance cancelled or refused at renewal in the past 5 years?"
								+ " : " + hasThidDriverHadCarInsuranceCancelled);
				return false;
			}
		}

		// Has this driver, regardless of who was at fault, had any claims in
		// the past 5 years?
		String option4 = hasThisDriverHadAnyClaimInPast5Years.toString().trim();

		System.out.println("Option 4 is :=> " + option4);

		if (option4.equalsIgnoreCase("No") || option4.trim().length() == 0) {
			hasThisDriverHadAnyClaimPast5YearNo_field.click();
		} else {
			hasThisDriverHadAnyClaimPast5YearYes_field.click();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

			String strClaimType[] = claimType.split(";");
			String strClaimMonth[] = claimMonth.split(";");
			String strClaimYear[] = claimYear.split(";");
			String strPayExcess[] = didYouPayAnExcess.split(";");
			try {
				for (int nCnt = 0; nCnt < strClaimType.length; nCnt++) {

					addClaimButton_field.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

					int dynamicXpathCount = nCnt + 1;

					// Claim Type Driver 1
					System.out.println("Claim Type : " + strClaimType[nCnt]);
					Select drpClaimType = new Select(
							driver.findElement(By.xpath("//select[@id='ctri-3-" + dynamicXpathCount + "-9']")));
					drpClaimType.selectByValue(CommonFunction.capitaliseFirstLetterSingleWord(strClaimType[nCnt]));

					// Claim Month Driver 1
					System.out.println("Claim Month : " + strClaimMonth[nCnt]);
					Select drpClaimMonth = new Select(
							driver.findElement(By.xpath("//select[@id='ctri-3-" + dynamicXpathCount + "-10']")));
					drpClaimMonth.selectByValue(CommonFunction.capitaliseFirstLetterSingleWord(strClaimMonth[nCnt]));

					// Claim Year Driver 1
					System.out.println("Claim Type : " + strClaimYear[nCnt]);
					Select drpClaimYear = new Select(
							driver.findElement(By.xpath("//select[@id='ctri-3-" + dynamicXpathCount + "-11']")));
					drpClaimYear.selectByValue(strClaimYear[nCnt]);

					// Did you pay an excess Driver 1
					String didYouPayAnExcessOption = strPayExcess[nCnt].trim();
					if (didYouPayAnExcessOption.equalsIgnoreCase("No")
							|| didYouPayAnExcessOption.trim().length() == 0) {
						driver.findElement(By
								.xpath("//li[@class='DELETE_ROW_BUTTON_CLASS nform_li ct_column_control3 show_hide_custom_table custom_table3 CUSTOM_TABLE_FIELD_POSITION inline5 custom_table_row_"
										+ dynamicXpathCount
										+ "-3 left']//span[@class='input_cover text_cover ng-scope']//span[2]//input[1]"))
								.click();
					} else {
						driver.findElement(By
								.xpath("//li[@class='DELETE_ROW_BUTTON_CLASS nform_li ct_column_control3 show_hide_custom_table custom_table3 CUSTOM_TABLE_FIELD_POSITION inline5 custom_table_row_"
										+ dynamicXpathCount + "-3 left']//span[1]//input[1]"))
								.click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}
				}
			} catch (Exception e) {
				System.out.println("");
			}
		}

		// Criminal history
		String criminalHistory_Option = criminalHistory.toString().trim();
		if (criminalHistory_Option.equalsIgnoreCase("No") || criminalHistory_Option.trim().length() == 0) {
			criminalHistoryNo_field.click();
		} else {
			criminalHistoryYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// Verify Declined Case for Criminal history
			String declined_Option4 = "//a[@id='Next26']";
			String declined_Option44 = driver.findElement(By.xpath(declined_Option4)).getText();
			if (driverDetailNextButton.compareToIgnoreCase(declined_Option44) != 0) {
				System.out.println("Declined Case => Criminal history" + " : " + criminalHistory);
				ReportsClass.logStat(Status.FATAL, "Declined Case => Criminal history" + " : " + criminalHistory);
				return false;
			}
		}

		// Bankruptcy
		String bankruptcy_Option = bankruptcy.toString().trim();
		if (bankruptcy_Option.equalsIgnoreCase("No") || bankruptcy_Option.trim().length() == 0) {
			bankruptcyNo_field.click();
		} else {
			bankruptcyYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// Verify Declined Case for Bankruptcy
			String declined_Option5 = "//a[@id='Next26']";
			String declined_Option55 = driver.findElement(By.xpath(declined_Option5)).getText();
			if (driverDetailNextButton.compareToIgnoreCase(declined_Option55) != 0) {
				System.out.println("Declined Case => Bankruptcy" + " : " + bankruptcy);
				ReportsClass.logStat(Status.FATAL, "Declined Case => Bankruptcy" + " : " + bankruptcy);
				return false;
			}
		}

		// Is there a 2nd driver?
		String isThere2ndDriver_Option = isThereA2ndDriver.toString().trim();
		if (isThere2ndDriver_Option.equalsIgnoreCase("No") || isThere2ndDriver_Option.trim().length() == 0) {
			isThere2ndDriverNo_field.click();
		} else {
			isThere2ndDriverYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			/* 2nd Driver Details Form fields */

			// Remove 2nd driver from policy
			String sTempRemovedDriver2FromPolicy = removed2ndDriverFromPolicy.toString().trim();
			if (sTempRemovedDriver2FromPolicy.equalsIgnoreCase("No")
					|| sTempRemovedDriver2FromPolicy.trim().length() == 0) {
				remove2ndDriverFromPolicyNo_field.click();
			} else {
				remove2ndDriverFromPolicyYes_field.click();
			}

			// Driver 2 Salutation
			String sTempDriver2Salutation = driver2Salutation.toString().trim();
			int index_Driver2Salutation = 0;

			if (sTempDriver2Salutation.equalsIgnoreCase("Mr")) {
				index_Driver2Salutation = 1;
			} else if (sTempDriver2Salutation.equalsIgnoreCase("Mrs")) {
				index_Driver2Salutation = 2;
			} else if (sTempDriver2Salutation.equalsIgnoreCase("Ms")) {
				index_Driver2Salutation = 3;
			} else if (sTempDriver2Salutation.equalsIgnoreCase("Dr")) {
				index_Driver2Salutation = 4;
			} else if (sTempDriver2Salutation.equalsIgnoreCase("Miss")) {
				index_Driver2Salutation = 5;
			}

			System.out.println("Driver 2 Salutation is : " + sTempDriver2Salutation);
			Select drpDriver2Salutation = new Select(driver2Salutation_field);
			try {
				drpDriver2Salutation.selectByIndex(index_Driver2Salutation);
			} catch (Exception e) {
				System.out.println("");
			}

			// Verify Invalid Case for Permitted driver age range
			String getDriver2Salutation = drpDriver2Salutation.getFirstSelectedOption().getText().trim();
			if (sTempDriver2Salutation.compareToIgnoreCase(getDriver2Salutation) != 0) {
				System.out.println("Invalid Case => Driver 2 Salutation" + driver2Salutation);
				ReportsClass.logStat(Status.FAIL, "Invalid Case => Driver 2 Salutation" + driver2Salutation);
				return false;
			}

			// Driver 2 First name, Family name
			driver2FirstName_field.sendKeys(driver2FirstName);
			driver2FamilyName_field.sendKeys(driver2FamilyName);

			// Driver 2 Gender
			String sTempDriver2Gender = driver2Gender.toString().trim();
			int index_Driver2Gender = 0;

			if (sTempDriver2Gender.equalsIgnoreCase("Female")) {
				index_Driver2Gender = 1;
			} else if (sTempDriver2Gender.equalsIgnoreCase("Male")) {
				index_Driver2Gender = 2;
			} else if (sTempDriver2Gender.equalsIgnoreCase("Intersex")) {
				index_Driver2Gender = 3;
			}

			System.out.println("Driver 2 Gender :->" + sTempDriver2Gender);
			Select drpDriver2Gender = new Select(driver2Gender_field);
			try {
				drpDriver2Gender.selectByIndex(index_Driver2Gender);
			} catch (Exception e) {
				System.out.println("");
			}

			// Verify Invalid Case for Driver 2 Gender
			String getDriver2Gender = drpDriver2Gender.getFirstSelectedOption().getText().trim();
			if (sTempDriver2Gender.compareToIgnoreCase(getDriver2Gender) != 0) {
				System.out.println("Invalid Case => Driver 2 Gender" + driver2Gender);
				ReportsClass.logStat(Status.FAIL, "Invalid Case => Driver 2 Gender" + driver2Gender);
				return false;
			}

			// Driver 2 Date Of Birth
			String str1[] = driver2DateOfBirth.split("/");
			String day1 = str1[0];
			String month1 = str1[1];
			String year1 = str1[2];

			Select drpDay = new Select(driver2Day_field);
			drpDay.selectByValue(day1);
			Select drpMonth = new Select(driver2Month_field);
			drpMonth.selectByValue(month1);
			Select drpYear = new Select(driver2Year_field);
			drpYear.selectByValue(year1);

			// Driver 2 Mobile
			driver2Mobile_field.sendKeys(driver2Mobile);

			// Driver 2 Licence type
			if (CommonFunction.selectLicenceType(driver2LicenceType, driver2LicenceType_field) == false) {
				System.out.println("Invalid Value => Licence Type" + " : " + driver2LicenceType);
				ReportsClass.logStat(Status.FAIL, "Invalid Case => Licence Type : " + driver2LicenceType);
				return false;
			}

			// Driver 2 Licence country
			if (CommonFunction.selectLicenceCountry(driver2LicenceCountry, driver2LicenceCountry_field) == false) {
				System.out.println("Invalid Value => Licence Country" + " : " + driver2LicenceCountry);
				ReportsClass.logStat(Status.FAIL, "Invalid Case => Licence Country : " + driver2LicenceCountry);
				return false;
			}

			// Driver 2 Licence years
			if (CommonFunction.selectLicenceYear(driver2LicenceType, driver2LicenceYear,
					driver2LicenceYear_field) == false) {
				System.out.println("Invalid Value => Licence Year : " + " : " + driver2LicenceYear);
				ReportsClass.logStat(Status.FAIL, "Invalid Case => Licence year : " + driver2LicenceYear);
				return false;
			}

			// Has Driver 2 been under a licence suspension or cancellation, or
			// restricted licence during the past 5 years?
			String sTempDriver2HasBeenLicenceSuspension = driver2HasBeenSuspensionOrCancellation.toString().trim();

			if (sTempDriver2HasBeenLicenceSuspension.equalsIgnoreCase("No")
					|| sTempDriver2HasBeenLicenceSuspension.trim().length() == 0) {
				driver2HasBeenSuspensionOrCancellationNo_Field.click();
			} else {
				driver2HasBeenSuspensionOrCancellationYes_Field.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				// Declined case forHas Driver 2 been under a licence suspension
				// or cancellation, or restricted licence during the past 5
				// years?
				String declined_driver2_Option1 = "//a[@id='Next26']";
				String declined_driver2_Option11 = driver.findElement(By.xpath(declined_driver2_Option1)).getText();
				if (driverDetailNextButton.compareToIgnoreCase(declined_driver2_Option11) != 0) {
					System.out.println(
							"Declined Case => Has Driver 2 been under a licence suspension or cancellation, or restricted licence during the past 5 years?"
									+ driver2HasBeenSuspensionOrCancellation);
					ReportsClass.logStat(Status.FAIL,
							"Declined Case => Has Driver 2 been under a licence suspension or cancellation, or restricted licence during the past 5 years?"
									+ driver2HasBeenSuspensionOrCancellation);
					return false;
				}
			}

			// Has Driver 2 had any alcohol or drug-related driving charges or
			// dangerous driving charges within the past 5 years?
			String sTempDriver2HadAnyAlcohol = driver2HasAnyAlcoholOrDrug.toString().trim();
			if (sTempDriver2HadAnyAlcohol.equalsIgnoreCase("No") || sTempDriver2HadAnyAlcohol.trim().length() == 0) {
				driver2HasDriverAlcoholOrDrugNo_field.click();
			} else {
				driver2HasDriverAlcoholOrDrugYes_field.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				// Declined Case for Has Driver 2 had any alcohol or
				// drug-related driving charges or dangerous driving charges
				// within the past 5 years?
				if (driverDetailNextButton.compareToIgnoreCase("declined_driver2_Option22") != 0) {
					System.out.println(
							"Declined Case => Has Driver 2 had any alcohol or drug-related driving charges or dangerous driving charges within the past 5 years?"
									+ driver2HasAnyAlcoholOrDrug);
					ReportsClass.logStat(Status.FAIL,
							"Declined Case => Has Driver 2 had any alcohol or drug-related driving charges or dangerous driving charges within the past 5 years?"
									+ driver2HasAnyAlcoholOrDrug);
					return false;
				}
			}

			// Has Driver 2 had their car insurance cancelled or refused at
			// renewal in the past 5 years?
			String sTempDriver2HadCarInsuranceCancelled = driver2HadCarInsuranceCancelled.toString().trim();
			if (sTempDriver2HadCarInsuranceCancelled.equalsIgnoreCase("No")
					|| sTempDriver2HadCarInsuranceCancelled.trim().length() == 0) {
				driver2HasDriverCarInsuranceCancelledNo_field.click();
			} else {
				driver2HasDriverCarInsuranceCancelledYes_field.click();

				// Declined Case for Has Driver 2 had their car insurance
				// cancelled or refused at renewal in the past 5 years?
				String declined_driver2_Option3 = "//a[@id='Next26']";
				String declined_driver2_Option33 = driver.findElement(By.xpath(declined_driver2_Option3)).getText();
				if (driverDetailNextButton.compareToIgnoreCase(declined_driver2_Option33) != 0) {
					System.out.println(
							"Declined Case => Has Driver 2 had their car insurance cancelled or refused at renewal in the past 5 years?"
									+ driver2HadCarInsuranceCancelled);
					ReportsClass.logStat(Status.FAIL,
							"Declined Case => Has Driver 2 had their car insurance cancelled or refused at renewal in the past 5 years?"
									+ driver2HadCarInsuranceCancelled);
					return false;
				}
			}

			// Has Driver 2, regardless of who was at fault, had any claims in
			// the past 5 years?
			String sTempDriver2HadAnyClaimPast5Year = driver2HadAnyClaimPast5Year.toString().trim();
			if (sTempDriver2HadAnyClaimPast5Year.equalsIgnoreCase("No")
					|| sTempDriver2HadAnyClaimPast5Year.trim().length() == 0) {
				driver2HadAnyClaimPast5YearNo_field.click();

				// Declined Case for Driver 2 Criminal history
				String declined_driver2_Option4 = "//a[@id='Next26']";
				String declined_driver2_Option44 = driver.findElement(By.xpath(declined_driver2_Option4)).getText();
				if (driverDetailNextButton.compareToIgnoreCase(declined_driver2_Option44) != 0) {
					System.out.println(
							"Declined Case => Has Driver 2, regardless of who was at fault, had any claims in the past 5 years?"
									+ sTempDriver2HadAnyClaimPast5Year);
					ReportsClass.logStat(Status.FAIL,
							"Declined Case => Has Driver 2, regardless of who was at fault, had any claims in the past 5 years?"
									+ sTempDriver2HadAnyClaimPast5Year);
					return false;
				}

			} else {
				driver2HadAnyClaimPast5YearYes_field.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				String strClaimTypeDriver2[] = driver2ClaimType.split(";");
				String strClaimMonthDriver2[] = driver2ClaimMonth.split(";");
				String strClaimYearDriver2[] = driver2ClaimYear.split(";");
				String strPayExcessDriver2[] = driver2DidYouPayAnExcess.split(";");
				try {
					for (int nCnt = 0; nCnt < strClaimTypeDriver2.length; nCnt++) {

						addClaimButtonDriver2_field.click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

						int dynamicXpathCountDriverTwo = nCnt + 1;

						// Claim Type Driver 2
						System.out.println("Claim Type : " + strClaimTypeDriver2[nCnt]);
						Select drpClaimType = new Select(driver
								.findElement(By.xpath("//select[@id='ctri-4-" + dynamicXpathCountDriverTwo + "-7']")));
						drpClaimType.selectByValue(
								CommonFunction.capitaliseFirstLetterSingleWord(strClaimTypeDriver2[nCnt]));

						// Claim Month Driver 2
						System.out.println("Claim Month : " + strClaimMonthDriver2[nCnt]);
						Select drpClaimMonth = new Select(driver
								.findElement(By.xpath("//select[@id='ctri-4-" + dynamicXpathCountDriverTwo + "-8']")));
						drpClaimMonth.selectByValue(
								CommonFunction.capitaliseFirstLetterSingleWord(strClaimMonthDriver2[nCnt]));

						// Claim Year Driver 2
						System.out.println("Claim Type : " + strClaimYearDriver2[nCnt]);
						Select drpClaimYear = new Select(driver
								.findElement(By.xpath("//select[@id='ctri-4-" + dynamicXpathCountDriverTwo + "-13']")));
						drpClaimYear.selectByValue(strClaimYearDriver2[nCnt]);

						// Did you pay an excess Driver 2
						String didYouPayAnExcessOption = strPayExcessDriver2[nCnt].trim();
						if (didYouPayAnExcessOption.equalsIgnoreCase("No")
								|| didYouPayAnExcessOption.trim().length() == 0) {
							driver.findElement(By
									.xpath("//li[@class='DELETE_ROW_BUTTON_CLASS nform_li ct_column_control4 show_hide_custom_table custom_table4 CUSTOM_TABLE_FIELD_POSITION inline5 custom_table_row_"
											+ dynamicXpathCountDriverTwo
											+ "-4 left']//span[@class='input_cover text_cover ng-scope']//span[2]//input[1]"))
									.click();
						} else {
							driver.findElement(By
									.xpath("//li[@class='DELETE_ROW_BUTTON_CLASS nform_li ct_column_control4 show_hide_custom_table custom_table4 CUSTOM_TABLE_FIELD_POSITION inline5 custom_table_row_"
											+ dynamicXpathCountDriverTwo + "-4 left']//span[1]//input[1]"))
									.click();
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						}
					}
				} catch (Exception e) {
					System.out.println("");
				}
			}

			// Driver 2 - Criminal history
			String sTempDriver2CriminalHistory = driver2CriminalHistory.toString().trim();
			if (sTempDriver2CriminalHistory.equalsIgnoreCase("No")
					|| sTempDriver2CriminalHistory.trim().length() == 0) {
				driver2CriminalHistoryNo_field.click();
			} else {
				driver2CriminalHistoryYes_field.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				// Declined Case for Driver 2 Criminal history
				String declined_driver2_Option4 = "//a[@id='Next26']";
				String declined_driver2_Option44 = driver.findElement(By.xpath(declined_driver2_Option4)).getText();
				if (driverDetailNextButton.compareToIgnoreCase(declined_driver2_Option44) != 0) {
					System.out.println("Declined Case => Driver 2 Criminal History" + driver2CriminalHistory);
					ReportsClass.logStat(Status.FAIL,
							"Declined Case => Driver 2 Criminal History" + driver2CriminalHistory);
					return false;
				}
			}

			// Driver 2 - Bankruptcy
			String sTempDriver2Bankruptcy = driver2Bankruptcy.toString().trim();
			if (sTempDriver2Bankruptcy.equalsIgnoreCase("No") || sTempDriver2Bankruptcy.trim().length() == 0) {
				driver2BankruptcyNo_field.click();
			} else {
				driver2BankruptcyYes_field.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				// Declined Case for Driver 2 Bankruptcy
				String declined_driver2_Option5 = "//a[@id='Next26']";
				String declined_driver2_Option55 = driver.findElement(By.xpath(declined_driver2_Option5)).getText();
				if (driverDetailNextButton.compareToIgnoreCase(declined_driver2_Option55) != 0) {
					System.out.println("Declined Case => Driver 2 Bankruptcy" + driver2Bankruptcy);
					ReportsClass.logStat(Status.FAIL, "Declined Case => Driver 2 Bankruptcy" + driver2Bankruptcy);
					return false;
				}
			}

			// Is there a 3rd driver?
			String sTempIsThere3rdDriver = isThere3rdDriver.toString().trim();
			if (sTempIsThere3rdDriver.equalsIgnoreCase("No") || sTempIsThere3rdDriver.trim().length() == 0) {
				isThere3rdDriverNo_field.click();
			} else {
				isThere3rdDriverYes_field.click();

				/* 3nd Driver Details Form fields */

				// Remove 3nd driver from policy
				String sTempDriver3RemovedDriver2FromPolicy = removed3rdDriverFromPolicy.toString().trim();
				if (sTempDriver3RemovedDriver2FromPolicy.equalsIgnoreCase("No")
						|| sTempDriver3RemovedDriver2FromPolicy.trim().length() == 0) {
					driver3Remove3rdPolicyNo_field.click();
				} else {
					driver3Remove3rdPolicyYes_field.click();
				}

				// Driver 3 Salutation
				String sTempDriver3Salutation = driver3Salutation.toString().trim();
				int index_Driver3Salutation = 0;

				if (sTempDriver3Salutation.equalsIgnoreCase("Mr")) {
					index_Driver3Salutation = 1;
				} else if (sTempDriver3Salutation.equalsIgnoreCase("Mrs")) {
					index_Driver3Salutation = 2;
				} else if (sTempDriver3Salutation.equalsIgnoreCase("Ms")) {
					index_Driver3Salutation = 3;
				} else if (sTempDriver3Salutation.equalsIgnoreCase("Dr")) {
					index_Driver3Salutation = 4;
				} else if (sTempDriver3Salutation.equalsIgnoreCase("Miss")) {
					index_Driver3Salutation = 5;
				}

				System.out.println("Driver 3 Salutation : " + sTempDriver3Salutation);
				Select drpDriver3Salutation = new Select(driver3Salutation_field);
				try {
					drpDriver3Salutation.selectByIndex(index_Driver3Salutation);
				} catch (Exception e) {
					System.out.println("");
				}

				// Verify Invalid Case for Driver 3 Salutation
				String getDriver3Salutation = drpDriver3Salutation.getFirstSelectedOption().getText().trim();
				if (sTempDriver3Salutation.compareToIgnoreCase(getDriver3Salutation) != 0) {
					System.out.println("Invalid Case => Driver 3 Salutation" + driver3Salutation);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Driver 3 Salutation" + driver3Salutation);
					return false;
				}

				// Driver 3 First name, Family name
				driver3FirstName_field.sendKeys(driver3FirstName);
				driver3FamilyName_field.sendKeys(driver3FamilyName);

				// Driver 3 Gender
				String sTempDriver3Gender = driver3Gender.toString().trim();
				int index_Driver3Gender = 0;

				if (sTempDriver3Gender.equalsIgnoreCase("Female")) {
					index_Driver3Gender = 1;
				} else if (sTempDriver3Gender.equalsIgnoreCase("Male")) {
					index_Driver3Gender = 2;
				} else if (sTempDriver3Gender.equalsIgnoreCase("Intersex")) {
					index_Driver3Gender = 3;
				}

				System.out.println("Driver 3 Gender :->" + sTempDriver3Gender);
				Select drpDriver3Gender = new Select(driver3Gender_field);
				try {
					drpDriver3Gender.selectByIndex(index_Driver3Gender);
				} catch (Exception e) {
					System.out.println("");
				}

				// Verify Invalid Case for Driver 3 Gender
				String getDriver3Gender = drpDriver3Gender.getFirstSelectedOption().getText().trim();
				if (sTempDriver3Gender.compareToIgnoreCase(getDriver3Gender) != 0) {
					System.out.println("Invalid Case => Driver 3 Gender" + driver3Gender);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Driver 3 Gender" + driver3Gender);
					return false;
				}

				// Driver 3 Date Of Birth
				String str3[] = driver2DateOfBirth.split("/");
				String day3 = str3[0];
				String month3 = str3[1];
				String year3 = str3[2];

				Select drpDay3 = new Select(driver3Day_field);
				drpDay3.selectByValue(day3);
				Select drpMonth3 = new Select(driver3Month_field);
				drpMonth3.selectByValue(month3);
				Select drpYear3 = new Select(driver3Year_field);
				drpYear3.selectByValue(year3);

				// Driver 3 Mobile
				driver3Mobile_field.sendKeys(driver3Mobile);

				// Driver 3 Licence type
				if (CommonFunction.selectLicenceType(driver3LicenceType, driver3LicenceType_field) == false) {
					System.out.println("Invalid Value => Licence Type" + " : " + driver3LicenceType);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Licence Type : " + driver3LicenceType);
					return false;
				}

				// Driver 3 Licence country
				if (CommonFunction.selectLicenceCountry(driver3LicenceCountry, driver3LicenceCountry_field) == false) {
					System.out.println("Invalid Value => Licence Country" + " : " + driver3LicenceCountry);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Licence Country : " + driver3LicenceCountry);
					return false;
				}

				// Driver 3 Licence years
				if (CommonFunction.selectLicenceYear(driver3LicenceType, driver3LicenceYear,
						driver3LicenceYear_field) == false) {
					System.out.println("Invalid Value => Licence Year : " + " : " + driver3LicenceYear);
					ReportsClass.logStat(Status.FAIL, "Invalid Case => Licence year : " + driver3LicenceYear);
					return false;
				}

				// Has Driver 3 been under a licence suspension or cancellation,
				// or
				// restricted licence during the past 5 years?
				String sTempDriver3HasBeenLicenceSuspension = driver3HasBeenLicenceSuspensionOrCancellatin.toString()
						.trim();

				if (sTempDriver3HasBeenLicenceSuspension.equalsIgnoreCase("No")
						|| sTempDriver3HasBeenLicenceSuspension.trim().length() == 0) {
					driver3HasBeenUnderSuspensionOrCancellationNo_field.click();
				} else {
					driver3HasBeenUnderSuspensionOrCancellationYes_field.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

					// Declined case forHas Driver 3 been under a licence
					// suspension
					// or cancellation, or restricted licence during the past 5
					// years?
					String declined_driver2_Option1 = "//a[@id='Next26']";
					String declined_driver2_Option11 = driver.findElement(By.xpath(declined_driver2_Option1)).getText();
					if (driverDetailNextButton.compareToIgnoreCase(declined_driver2_Option11) != 0) {
						System.out.println(
								"Declined Case => Has Driver 2 been under a licence suspension or cancellation, or restricted licence during the past 5 years?"
										+ sTempDriver3HasBeenLicenceSuspension);
						ReportsClass.logStat(Status.FAIL,
								"Declined Case => Has Driver 2 been under a licence suspension or cancellation, or restricted licence during the past 5 years?"
										+ sTempDriver3HasBeenLicenceSuspension);
						return false;
					}
				}

				// Has Driver 3 had any alcohol or drug-related driving charges
				// or
				// dangerous driving charges within the past 5 years?
				String sTempDriver3HadAnyAlcohol = driver3HadAnyAlcoholOrDrugRelatedDrivingCharges.toString().trim();
				if (sTempDriver3HadAnyAlcohol.equalsIgnoreCase("No")
						|| sTempDriver3HadAnyAlcohol.trim().length() == 0) {
					driver3HadAnyAlcoholOrDrugNo_field.click();
				} else {
					driver3HadAnyAlcoholOrDrugYes_field.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

					// Declined Case for Has Driver 2 had any alcohol or
					// drug-related driving charges or dangerous driving charges
					// within the past 5 years?
					if (driverDetailNextButton.compareToIgnoreCase("declined_driver2_Option22") != 0) {
						System.out.println(
								"Declined Case => Has Driver 2 had any alcohol or drug-related driving charges or dangerous driving charges within the past 5 years?"
										+ driver3HadAnyAlcoholOrDrugRelatedDrivingCharges);
						ReportsClass.logStat(Status.FAIL,
								"Declined Case => Has Driver 2 had any alcohol or drug-related driving charges or dangerous driving charges within the past 5 years?"
										+ driver3HadAnyAlcoholOrDrugRelatedDrivingCharges);
						return false;
					}
				}

				// Has Driver 3 had their car insurance cancelled or refused at
				// renewal in the past 5 years?
				String sTempDriver3HadCarInsuranceCancelled = driver3HadTheirCarInsuranceCancelledOrRefused.toString()
						.trim();
				if (sTempDriver3HadCarInsuranceCancelled.equalsIgnoreCase("No")
						|| sTempDriver3HadCarInsuranceCancelled.trim().length() == 0) {
					driver3HadTheirCarInsuranceCancelledNo_field.click();
				} else {
					driver3HadTheirCarInsuranceCancelledYes_field.click();

					// Declined Case for Has Driver 3 had their car insurance
					// cancelled or refused at renewal in the past 5 years?
					String declined_driver2_Option3 = "//a[@id='Next26']";
					String declined_driver2_Option33 = driver.findElement(By.xpath(declined_driver2_Option3)).getText();
					if (driverDetailNextButton.compareToIgnoreCase(declined_driver2_Option33) != 0) {
						System.out.println(
								"Declined Case => Has Driver 2 had their car insurance cancelled or refused at renewal in the past 5 years?"
										+ driver3HadTheirCarInsuranceCancelledOrRefused);
						ReportsClass.logStat(Status.FAIL,
								"Declined Case => Has Driver 2 had their car insurance cancelled or refused at renewal in the past 5 years?"
										+ driver3HadTheirCarInsuranceCancelledOrRefused);
						return false;
					}
				}

				// Has Driver 3, regardless of who was at fault, had any claims
				// in
				// the past 5 years?
				String sTempDriver3HadAnyClaimPast5Year = driver3HadAnyClaimInThePast5Year.toString().trim();
				if (sTempDriver3HadAnyClaimPast5Year.equalsIgnoreCase("No")
						|| sTempDriver3HadAnyClaimPast5Year.trim().length() == 0) {
					driver3HadAnyClaimInLast5YearNo_field.click();
				} else {
					driver3HadAnyClaimInLast5YearNo_field.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

					String strClaimTypeDriver3[] = driver3ClaimType.split(";");
					String strClaimMonthDriver3[] = driver3ClaimMonth.split(";");
					String strClaimYearDriver3[] = driver3ClaimYear.split(";");
					String strPayExcessDriver3[] = driver3DidYouAnExcess.split(";");
					try {
						for (int nCnt = 0; nCnt < strClaimTypeDriver3.length; nCnt++) {

							addClaimButtonDriver3_field.click();
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

							int dynamicXpathCountDriverThree = nCnt + 1;

							// Claim Type Driver 3
							System.out.println("Claim Type : " + strClaimTypeDriver3[nCnt]);
							Select drpClaimType = new Select(driver.findElement(
									By.xpath("//select[@id='ctri-5-" + dynamicXpathCountDriverThree + "-15']")));
							drpClaimType.selectByValue(
									CommonFunction.capitaliseFirstLetterSingleWord(strClaimTypeDriver3[nCnt]));

							// Claim Month Driver 3
							System.out.println("Claim Month : " + strClaimMonthDriver3[nCnt]);
							Select drpClaimMonth = new Select(driver.findElement(
									By.xpath("//select[@id='ctri-5-" + dynamicXpathCountDriverThree + "-16']")));
							drpClaimMonth.selectByValue(
									CommonFunction.capitaliseFirstLetterSingleWord(strClaimMonthDriver3[nCnt]));

							// Claim Year Driver 3
							System.out.println("Claim Type : " + strClaimYearDriver3[nCnt]);
							Select drpClaimYear = new Select(driver.findElement(
									By.xpath("//select[@id='ctri-5-" + dynamicXpathCountDriverThree + "-17']")));
							drpClaimYear.selectByValue(strClaimYearDriver3[nCnt]);

							// Did you pay an excess Driver 3
							String didYouPayAnExcessOption = strPayExcessDriver3[nCnt].trim();
							if (didYouPayAnExcessOption.equalsIgnoreCase("No")
									|| didYouPayAnExcessOption.trim().length() == 0) {
								driver.findElement(By
										.xpath("//li[@class='DELETE_ROW_BUTTON_CLASS nform_li ct_column_control5 show_hide_custom_table custom_table5 CUSTOM_TABLE_FIELD_POSITION inline5 custom_table_row_"
												+ dynamicXpathCountDriverThree
												+ "-5 left']//span[@class='input_cover text_cover ng-scope']//span[2]//input[1]"))
										.click();
							} else {
								driver.findElement(By
										.xpath("//li[@class='DELETE_ROW_BUTTON_CLASS nform_li ct_column_control5 show_hide_custom_table custom_table5 CUSTOM_TABLE_FIELD_POSITION inline5 custom_table_row_"
												+ dynamicXpathCountDriverThree + "-5 left']//span[1]//input[1]"))
										.click();
								driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
							}
						}
					} catch (Exception e) {
						System.out.println("");
					}
				}

				// Driver 3 - Criminal history
				String sTempDriverdCriminalHistory = driver3CriminalHistory.toString().trim();
				if (sTempDriverdCriminalHistory.equalsIgnoreCase("No")
						|| sTempDriverdCriminalHistory.trim().length() == 0) {
					driver3CriminalHistoryNo_field.click();
				} else {
					driver3CriminalHistoryYes_field.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

					// Declined Case for Driver 3 Criminal history
					String declined_driver2_Option4 = "//a[@id='Next26']";
					String declined_driver2_Option44 = driver.findElement(By.xpath(declined_driver2_Option4)).getText();
					if (driverDetailNextButton.compareToIgnoreCase(declined_driver2_Option44) != 0) {
						System.out.println("Declined Case => Driver 3 Criminal History" + driver3CriminalHistory);
						ReportsClass.logStat(Status.FAIL,
								"Declined Case => Driver 3 Criminal History" + driver3CriminalHistory);
						return false;
					}
				}

				// Driver 3 - Bankruptcy
				String sTempDriver3Bankruptcy = driver3Bankruptcy.toString().trim();
				if (sTempDriver3Bankruptcy.equalsIgnoreCase("No") || sTempDriver3Bankruptcy.trim().length() == 0) {
					driver3BankruptcyNo_field.click();
				} else {
					driver3BankruptcyYes_field.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

					// Declined Case for Driver 3 Bankruptcy
					String declined_driver2_Option5 = "//a[@id='Next26']";
					String declined_driver2_Option55 = driver.findElement(By.xpath(declined_driver2_Option5)).getText();
					if (driverDetailNextButton.compareToIgnoreCase(declined_driver2_Option55) != 0) {
						System.out.println("Declined Case => Driver 3 Bankruptcy" + driver3Bankruptcy);
						ReportsClass.logStat(Status.FAIL, "Declined Case => Driver 3 Bankruptcy" + driver3Bankruptcy);
						return false;
					}
				}

				// Is there a 4th driver?
				String sTempIsThere4rdDriver = isThere4ThDriver.toString().trim();
				if (sTempIsThere4rdDriver.equalsIgnoreCase("No") || sTempIsThere4rdDriver.trim().length() == 0) {
					isThere4thDriverNo_field.click();
				} else {
					isThere4thDriverYes_field.click();
				}
			}
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		return true;
	}

	// Option and Pay
	public boolean optionsAndPayForRenewal_Step6(String sheetName, int rowNum) {
		String paymentType = reader1.getCellData(sheetName, "Payment type", rowNum);
		String adjustExcessBy = reader1.getCellData(sheetName, "Adjust excess by", rowNum);
		String optionalWindscreenBenefit = reader1.getCellData(sheetName, "Reduce windscreen excess", rowNum);
		String addHireCarAccidentBenefit = reader1.getCellData(sheetName, "Add hire car", rowNum);

		DecimalFormat df = new DecimalFormat("#.##");
		df.setMaximumFractionDigits(2);

		// Payment type

		String sTemp_PaymentType = CommonFunction.capitaliseFirstLetterSingleWord(paymentType);

		System.out.println("Payment Type is : " + sTemp_PaymentType);
		Select drpPaymentType = new Select(paymentType_field);
		try {
			drpPaymentType.selectByValue(sTemp_PaymentType);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Payment type
		String getPaymentType = drpPaymentType.getFirstSelectedOption().getText().trim();
		if (sTemp_PaymentType.compareToIgnoreCase(getPaymentType) != 0) {
			System.out.println("Invalid Value => Payment type" + " : " + paymentType);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Payment type" + " : " + paymentType);
			return false;
		}

		// Adjust excess by
		String sTemp_AdjustExcessBy = adjustExcessBy.toString();
		String sTemp_AdjustExcessByTrim = sTemp_AdjustExcessBy.trim();

		if (sTemp_AdjustExcessByTrim.length() > 0) {
			if (sTemp_AdjustExcessBy.substring(0, 1) != "$") {
				sTemp_AdjustExcessBy = ("$" + sTemp_AdjustExcessBy);
			}
		}

		int index_AdjustExcessBy = 0;

		if (sTemp_AdjustExcessBy.equalsIgnoreCase("$-200")) {
			index_AdjustExcessBy = 1;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$-100")) {
			index_AdjustExcessBy = 2;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$0")) {
			index_AdjustExcessBy = 3;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$100")) {
			index_AdjustExcessBy = 4;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$200")) {
			index_AdjustExcessBy = 5;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$500")) {
			index_AdjustExcessBy = 6;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$1,000")) {
			index_AdjustExcessBy = 7;
		}

		System.out.println("Adjust Excess By Value is : " + sTemp_AdjustExcessBy);

		System.out.println("Adjust Excess By Index is : " + index_AdjustExcessBy);

		Select drpAdjustExcessBy = new Select(adjustExcessBy_field);
		try {
			drpAdjustExcessBy.selectByIndex(index_AdjustExcessBy);
		} catch (Exception e) {
			System.out.println("");
		}

		// Optional windscreen benefit
		if (optionalWindscreenBenefit.equalsIgnoreCase("No") || optionalWindscreenBenefit.trim().length() == 0) {
			optionalWindScreenBenefitNo_field.click();
		} else {
			optionalWindScreenBenefitYes_field.click();
		}

		// Add hire car after an accident benefit
		try {
			if (addHireCarAccidentBenefit.equalsIgnoreCase("No") || addHireCarAccidentBenefit.trim().length() == 0) {
				addHireCarAccidentBenefitNo_field.click();
			} else {
				addHireCarAccidentBenefitYes_field.click();
			}
		} catch (Exception e) {
		}

		return true;
	}

	// Option and Pay
	public boolean optionsAndPay_Step6(String sheetName, int rowNum) {
		String paymentType = reader.getCellData(sheetName, "Payment type", rowNum);
		String adjustExcessBy = reader.getCellData(sheetName, "Adjust excess by", rowNum);
		String optionalWindscreenBenefit = reader.getCellData(sheetName, "Reduce windscreen excess", rowNum);
		String addHireCarAccidentBenefit = reader.getCellData(sheetName, "Add hire car", rowNum);
		String annualPayment = reader.getCellData(sheetName, "Payment", rowNum);
		String standardExcess = reader.getCellData(sheetName, "Standard excess", rowNum);
		String choosenExcess = reader.getCellData(sheetName, "Chosen excess", rowNum);
		String premium = reader.getCellData(sheetName, "Premium", rowNum);
		String gst = reader.getCellData(sheetName, "GST", rowNum);
		String stampDuty = reader.getCellData(sheetName, "Stamp duty", rowNum);
		String totalCost = reader.getCellData(sheetName, "Total cost", rowNum);
		String roadSafetyFees = reader.getCellData(sheetName, "Road safety fee incl. GST", rowNum);
		String merchantFees = reader.getCellData(sheetName, "Merchant fee incl. GST", rowNum);

		DecimalFormat df = new DecimalFormat("#.##");
		df.setMaximumFractionDigits(2);

		// Payment type

		String sTemp_PaymentType = CommonFunction.capitaliseFirstLetterSingleWord(paymentType);

		System.out.println("Payment Type is : " + sTemp_PaymentType);
		Select drpPaymentType = new Select(paymentType_field);
		try {
			drpPaymentType.selectByValue(sTemp_PaymentType);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Invalid Case for Payment type
		String getPaymentType = drpPaymentType.getFirstSelectedOption().getText().trim();
		if (sTemp_PaymentType.compareToIgnoreCase(getPaymentType) != 0) {
			System.out.println("Invalid Value => Payment type" + " : " + paymentType);
			ReportsClass.logStat(Status.FAIL, "Invalid Case => Payment type" + " : " + paymentType);
			return false;
		}

		// Adjust excess by
		String sTemp_AdjustExcessBy = adjustExcessBy.toString();
		String sTemp_AdjustExcessByTrim = sTemp_AdjustExcessBy.trim();

		if (sTemp_AdjustExcessByTrim.length() > 0) {
			if (sTemp_AdjustExcessBy.substring(0, 1) != "$") {
				sTemp_AdjustExcessBy = ("$" + sTemp_AdjustExcessBy);
			}
		}

		int index_AdjustExcessBy = 0;

		if (sTemp_AdjustExcessBy.equalsIgnoreCase("$-200")) {
			index_AdjustExcessBy = 1;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$-100")) {
			index_AdjustExcessBy = 2;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$0")) {
			index_AdjustExcessBy = 3;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$100")) {
			index_AdjustExcessBy = 4;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$200")) {
			index_AdjustExcessBy = 5;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$500")) {
			index_AdjustExcessBy = 6;
		} else if (sTemp_AdjustExcessBy.equalsIgnoreCase("$1,000")) {
			index_AdjustExcessBy = 7;
		}

		System.out.println("Adjust Excess By Value is : " + sTemp_AdjustExcessBy);

		System.out.println("Adjust Excess By Index is : " + index_AdjustExcessBy);

		Select drpAdjustExcessBy = new Select(adjustExcessBy_field);
		try {
			drpAdjustExcessBy.selectByIndex(index_AdjustExcessBy);
		} catch (Exception e) {
			System.out.println("");
		}

		// Optional windscreen benefit
		if (optionalWindscreenBenefit.equalsIgnoreCase("No") || optionalWindscreenBenefit.trim().length() == 0) {
			optionalWindScreenBenefitNo_field.click();
		} else {
			optionalWindScreenBenefitYes_field.click();
		}

		// Add hire car after an accident benefit
		try {
			if (addHireCarAccidentBenefit.equalsIgnoreCase("No") || addHireCarAccidentBenefit.trim().length() == 0) {
				addHireCarAccidentBenefitNo_field.click();
			} else {
				addHireCarAccidentBenefitYes_field.click();
			}
		} catch (Exception e) {
		}
		try {
			// Verifying Payment Amount Details
			String temp_PaymentType = paymentType.toString().trim();
			if (temp_PaymentType.equalsIgnoreCase("Annual")) {
				String annualPaymentType_getText = driver
						.findElement(By.xpath("//input[@name='Annual payment_text__false___field221_']"))
						.getAttribute("value");
				String annualPayment_FormValue = String.valueOf(annualPaymentType_getText).replace("[$,]", "");

				Double dFormValueAnnualPayment = Double
						.parseDouble(df.format(Double.parseDouble(annualPayment_FormValue)));
				Double dXLSValueAnnualPayment = Double.parseDouble(df.format(Double.parseDouble(annualPayment)));

				if (Double.compare(dFormValueAnnualPayment, dXLSValueAnnualPayment) == 0) {
					System.out.println("Annual Payment is Matched : " + "" + "Form Value : " + ""
							+ annualPayment_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
					ReportsClass.logStat(Status.PASS, "Annual Payment is Matched : " + "" + "Form Value : " + "$"
							+ annualPayment_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
				} else {
					System.out.println("Annual Payment is Not Matched : " + "" + "Form Value : " + "$"
							+ annualPayment_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
					ReportsClass.logStat(Status.WARNING, "Annual Payment is Not Matched : " + "" + "Form Value : " + ""
							+ annualPayment_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
				}

			} else if (temp_PaymentType.equalsIgnoreCase("Monthly")) {
				String monthlyPaymentType_getText = driver
						.findElement(By.xpath("//input[@name='Monthly payment_text__false___field222_']"))
						.getAttribute("value");
				String monthlyPaymentType_FormValue = String.valueOf(monthlyPaymentType_getText).replace("[$,]", "");
				if (monthlyPaymentType_FormValue == annualPayment) {
					System.out.println("Mothly Payment is Matched : " + "" + "Form Value : " + "$"
							+ monthlyPaymentType_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
					ReportsClass.logStat(Status.PASS, "Mothly Payment is Matched : " + "" + "Form Value : " + "$"
							+ monthlyPaymentType_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
				} else {
					System.out.println("Mothly Payment is Not Matched : " + "" + "Form Value : " + ""
							+ monthlyPaymentType_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
					ReportsClass.logStat(Status.WARNING, "Mothly Payment is Not Matched : " + "" + "Form Value : " + ""
							+ monthlyPaymentType_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
				}
			} else if (temp_PaymentType.equalsIgnoreCase("Fortnightly")) {
				String fortnightlyPaymentType_getText = driver
						.findElement(By.xpath("//input[@name='Fortnightly payment_text__false___field223_']"))
						.getAttribute("value");
				String fortnightlyType_FormValue = String.valueOf(fortnightlyPaymentType_getText).replace("[$,]", "");
				if (fortnightlyType_FormValue == annualPayment) {
					System.out.println("Fortnightly Payment is Matched : " + "" + "Form Value : " + "$"
							+ fortnightlyType_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
					ReportsClass.logStat(Status.PASS, "Fortnightly Payment is Matched : " + "" + "Form Value : " + "$"
							+ fortnightlyType_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
				} else {
					System.out.println("Fortnightly Payment is Not Matched : " + "" + "Form Value : " + "$"
							+ fortnightlyType_FormValue + ", " + " " + " Excel Value : " + "" + "$" + annualPayment);
					ReportsClass.logStat(Status.WARNING,
							"Fortnightly Payment is Not Matched : " + "" + "Form Value : " + ""
									+ fortnightlyType_FormValue + ", " + " " + " Excel Value : " + "" + "$"
									+ annualPayment);
				}
			}
		} catch (Exception e) {
			System.out.println("");
		}

		try {
			// Verify Standard Excess Amount
			String standardExcessField = driver
					.findElement(By.xpath("//input[@name='Standard excess_text__0___field227_']"))
					.getAttribute("value");

			String standardExcess_FormValue = String.valueOf(standardExcessField).replaceAll("[$,]", "").toString()
					.trim();
			Double dFormValueStandardExcess = Double
					.parseDouble(df.format(Double.parseDouble(standardExcess_FormValue)));
			Double dXLSValueStandardExcess = Double.parseDouble(df.format(Double.parseDouble(standardExcess)));

			if (Double.compare(dFormValueStandardExcess, dXLSValueStandardExcess) == 0) {
				System.out.println("Standard Excess Value is Matched : " + "" + "Form Value : " + "$"
						+ standardExcess_FormValue + ", " + " " + " Excel Value : " + "" + "$" + standardExcess);
				ReportsClass.logStat(Status.PASS, "Standard Excess is Matched : " + "" + "Form Value : " + "$"
						+ standardExcess_FormValue + ", " + " " + " Excel Value : " + "" + "$" + standardExcess);
			} else {
				System.out.println("Standard Excess Value is Not Matched : " + "" + "Form Value : " + "$"
						+ standardExcess_FormValue + ", " + " " + " Excel Value : " + "" + "$" + standardExcess);
				ReportsClass.logStat(Status.WARNING, "Standard Excess is Not Matched : " + "" + "Form Value : " + ""
						+ standardExcess_FormValue + ", " + " " + " Excel Value : " + "" + "$" + standardExcess);
			}

			// Verify Choosen Excess Amount
			String choosenExcessField = driver
					.findElement(By.xpath("//input[@name='Chosen excess_text__0___field229_']")).getAttribute("value");

			String choosenExcess_FormValue = String.valueOf(choosenExcessField).replaceAll("[$,]", "").toString()
					.trim();
			Double dFormValueChoosenExcess = Double.parseDouble(df.format(Double.parseDouble(choosenExcess_FormValue)));
			Double dXLSValueChoosenExcess = Double.parseDouble(df.format(Double.parseDouble(choosenExcess)));

			if (Double.compare(dFormValueChoosenExcess, dXLSValueChoosenExcess) == 0) {
				System.out.println("Choosen Excess Value is Matched : " + "" + "Form Value : " + "$"
						+ choosenExcess_FormValue + ", " + " " + " Excel Value : " + "" + "$" + choosenExcess);
				ReportsClass.logStat(Status.PASS, "Choosen Excess is Matched : " + "" + "Form Value : " + "$"
						+ choosenExcess_FormValue + ", " + " " + " Excel Value : " + "" + "$" + choosenExcess);
			} else {
				System.out.println("Choosen Excess Value is Not Matched : " + "" + "Form Value : " + "$"
						+ choosenExcess_FormValue + ", " + " " + " Excel Value : " + "" + "$" + choosenExcess);
				ReportsClass.logStat(Status.WARNING, "Choosen Excess is Not Matched : " + "" + "Form Value : " + ""
						+ choosenExcess_FormValue + ", " + " " + " Excel Value : " + "" + "$" + choosenExcess);
			}

			// Verify Preimum
			String premiumField = driver.findElement(By.xpath("//input[@name='Premium_text__0___field233_']"))
					.getAttribute("value");

			String premium_FormValue = String.valueOf(premiumField).replaceAll("[$,]", "").toString().trim();
			Double dFormValuePremium = Double.parseDouble(df.format(Double.parseDouble(premium_FormValue)));
			Double dXLSValuePremium = Double.parseDouble(df.format(Double.parseDouble(premium)));

			if (Double.compare(dFormValuePremium, dXLSValuePremium) == 0) {
				System.out.println("Premium Matched : " + "" + "Form Value : " + "$" + premium_FormValue + ", " + " "
						+ " Excel Value : " + "" + "$" + premium);
				ReportsClass.logStat(Status.PASS, "Premium is Matched : " + "" + "Form Value : " + "$"
						+ premium_FormValue + ", " + " " + " Excel Value : " + "" + "$" + premium);
			} else {
				System.out.println("Premium is Not Matched : " + "" + "Form Value : " + "$" + premium_FormValue + ", "
						+ " " + " Excel Value : " + "" + "$" + premium);
				ReportsClass.logStat(Status.WARNING, "Premium is Not Matched : " + "" + "Form Value : " + "$"
						+ premium_FormValue + ", " + " " + " Excel Value : " + "" + "$" + premium);
			}

			// Verify GST
			String gstField = driver.findElement(By.xpath("//input[@name='GST_text__0___field235_']"))
					.getAttribute("value");

			String gst_FormValue = String.valueOf(gstField).replaceAll("[$,]", "").toString().trim();
			Double dFormValueGst = Double.parseDouble(df.format(Double.parseDouble(gst_FormValue)));
			Double dXLSValueGst = Double.parseDouble(df.format(Double.parseDouble(gst)));

			if (Double.compare(dFormValueGst, dXLSValueGst) == 0) {
				System.out.println("GST Matched : " + "" + "Form Value : " + "$" + gst_FormValue + ", " + " "
						+ " Excel Value : " + "" + "$" + gst);
				ReportsClass.logStat(Status.PASS, "GST is Matched : " + "" + "Form Value : " + "$" + gst_FormValue
						+ ", " + " " + " Excel Value : " + "" + "$" + gst);
			} else {
				System.out.println("GST is Not Matched : " + "" + "Form Value : " + "$" + gst_FormValue + ", " + " "
						+ " Excel Value : " + "" + "$" + gst);
				ReportsClass.logStat(Status.WARNING, "GST is Not Matched : " + "" + "Form Value : " + "$"
						+ gst_FormValue + ", " + " " + " Excel Value : " + "" + "$" + gst);
			}

			// Verify Stamp duty
			String stampDutyField = driver.findElement(By.xpath("//input[@name='Stamp duty_text__0___field236_']"))
					.getAttribute("value");

			String stampDuty_FormValue = String.valueOf(stampDutyField).replaceAll("[$,]", "").toString().trim();
			Double dFormValueStampDuty = Double.parseDouble(df.format(Double.parseDouble(stampDuty_FormValue)));
			Double dXLSValueStampDuty = Double.parseDouble(df.format(Double.parseDouble(stampDuty)));

			if (Double.compare(dFormValueStampDuty, dXLSValueStampDuty) == 0) {
				System.out.println("Stamp Duty Matched : " + "" + "Form Value : " + "$" + stampDuty_FormValue + ", "
						+ " " + " Excel Value : " + "" + "$" + stampDuty);
				ReportsClass.logStat(Status.PASS, "Stamp Duty is Matched : " + "" + "Form Value : " + "$"
						+ stampDuty_FormValue + ", " + " " + " Excel Value : " + "" + "$" + stampDuty);
			} else {
				System.out.println("Stamp Duty is Not Matched : " + "" + "Form Value : " + "$" + stampDuty_FormValue
						+ ", " + " " + " Excel Value : " + "" + "$" + stampDuty);
				ReportsClass.logStat(Status.WARNING, "Stamp Duty is Not Matched : " + "" + "Form Value : " + "$"
						+ stampDuty_FormValue + ", " + " " + " Excel Value : " + "" + "$" + stampDuty);
			}

			// Verify Carpeesh driver safety app fee incl. GST
			String roadSafetyFeesField = driver.findElement(By.xpath("//*[@id='fe_238_1238']/div/span[2]/input"))
					.getAttribute("value");

			String roadSafetyFees_FormValue = String.valueOf(roadSafetyFeesField).replaceAll("[$,]", "").toString()
					.trim();
			Double dFormValueroadSafetyFees = Double
					.parseDouble(df.format(Double.parseDouble(roadSafetyFees_FormValue)));
			Double dXLSValueroadSafetyFees = Double.parseDouble(df.format(Double.parseDouble(roadSafetyFees)));

			if (Double.compare(dFormValueroadSafetyFees, dXLSValueroadSafetyFees) == 0) {
				System.out.println("Carpeesh Driver Safety App Fee Incl. GST Matched : " + "" + "Form Value : " + "$"
						+ roadSafetyFees_FormValue + ", " + " " + " Excel Value : " + "" + "$" + roadSafetyFees);
				ReportsClass.logStat(Status.PASS,
						"Carpeesh Driver Safety App Fee Incl. GST is Matched : " + "" + "Form Value : " + "$"
								+ roadSafetyFees_FormValue + ", " + " " + " Excel Value : " + "" + "$"
								+ roadSafetyFees);
			} else {
				System.out.println("Carpeesh Driver Safety App Fee Incl. GST is Not Matched : " + "" + "Form Value : "
						+ "$" + roadSafetyFees_FormValue + ", " + " " + " Excel Value : " + "" + "$" + roadSafetyFees);
				ReportsClass.logStat(Status.WARNING,
						"Carpeesh Driver Safety App Fee Incl. GST is Not Matched : " + "" + "Form Value : " + "$"
								+ roadSafetyFees_FormValue + ", " + " " + " Excel Value : " + "" + "$"
								+ roadSafetyFees);
			}

			// Verify Merchant fee incl. GST
			String merchantyFeesField = driver.findElement(By.xpath("//*[@id='fe_240_1238']/div/span[2]/input"))
					.getAttribute("value");

			String merchantFees_FormValue = String.valueOf(merchantyFeesField).replaceAll("[$,]", "").toString().trim();
			Double dFormValueMerchatFees = Double.parseDouble(df.format(Double.parseDouble(merchantFees_FormValue)));
			Double dXLSValueMerchantFees = Double.parseDouble(df.format(Double.parseDouble(merchantFees)));

			if (Double.compare(dFormValueMerchatFees, dXLSValueMerchantFees) == 0) {
				System.out.println("Merchant fee incl. GST Matched : " + "" + "Form Value : " + "$"
						+ merchantFees_FormValue + ", " + " " + " Excel Value : " + "" + "$" + merchantFees);
				ReportsClass.logStat(Status.PASS, "Merchant fee incl. GST is Matched : " + "" + "Form Value : " + "$"
						+ merchantFees_FormValue + ", " + " " + " Excel Value : " + "" + "$" + merchantFees);
			} else {
				System.out.println("Merchant fee incl. GST is Not Matched : " + "" + "Form Value : " + "$"
						+ merchantFees_FormValue + ", " + " " + " Excel Value : " + "" + "$" + merchantFees);
				ReportsClass.logStat(Status.WARNING, "Merchant fee incl. GST is Not Matched : " + "" + "Form Value : "
						+ "$" + merchantFees_FormValue + ", " + " " + " Excel Value : " + "" + "$" + merchantFees);
			}

			// Verify Total Cost
			String gstTotalCost = driver.findElement(By.xpath("//input[@name='Total cost_text__0___field241_']"))
					.getAttribute("value");

			String totalCost_FormValue = String.valueOf(gstTotalCost).replaceAll("[$,]", "").toString().trim();
			Double dFormValueTotalCost = Double.parseDouble(df.format(Double.parseDouble(totalCost_FormValue)));
			Double dXLSValueTotalCost = Double.parseDouble(df.format(Double.parseDouble(totalCost)));

			if (Double.compare(dFormValueTotalCost, dXLSValueTotalCost) == 0) {
				System.out.println("Total Cost Matched : " + "" + "Form Value : " + "$" + totalCost_FormValue + ", "
						+ " " + " Excel Value : " + "" + "$" + totalCost);
				ReportsClass.logStat(Status.PASS, "Total Cost is Matched : " + "" + "Form Value : " + "$"
						+ totalCost_FormValue + ", " + " " + " Excel Value : " + "" + "$" + totalCost);
			} else {
				System.out.println("Total Cost is Not Matched : " + "" + "Form Value : " + "$" + totalCost_FormValue
						+ ", " + " " + " Excel Value : " + "" + "$" + totalCost);
				ReportsClass.logStat(Status.WARNING, "Total Cost is Not Matched : " + "" + "Form Value : " + "$"
						+ totalCost_FormValue + ", " + " " + " Excel Value : " + "" + "$" + totalCost);
			}
		} catch (Exception e) {
			System.out.println("");
		}

		return true;
	}

	// Click on Continue to Payment Button
	public void clickOnContinueToPaymentButton() {
		continueToPaymentButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	// Payment Page
	public void clickOnPayNowButton() {
		payNowButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	// Enter Card Details
	public void enterCardDetails(String cardNumber, String mmYY, String csc, String cardHolderName) throws Exception {
		cardNumber_field.sendKeys(cardNumber);
		expiryDate_field.sendKeys(mmYY);
		cardVerificationCode_field.sendKeys(csc);
		cardHolderName_field.sendKeys(cardHolderName);
		waitFor1Sec();
		payButton_field.click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}

	// Create new policies for Renewal Prior 365 Days
	public void createNewPolicyForRenewalPrior365Days(String type, String clientName, String policyClass,
			String townVillage, String sumInsured, String sheetName, int rowNum, int days) throws Exception {

		basicProcessing.contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFunction.navigateToClient(clientName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.addTransactionButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	// Create new client for Renewal Policies
	public void createNewClientForCarpeesh(String sheetName, int rowNum) throws Exception {
		String salutation = reader1.getCellData(sheetName, "Salutation", rowNum);
		String firstName = reader1.getCellData(sheetName, "First Name", rowNum);
		String lastName = reader1.getCellData(sheetName, "Last Name", rowNum);
		String gender = reader1.getCellData(sheetName, "Gender", rowNum);
		String emailAddress = reader1.getCellData(sheetName, "Email_Address", rowNum);
		String clientDOB = reader1.getCellData(sheetName, "Date Of Birth", rowNum);
		String mailingAddress = reader1.getCellData(sheetName, "Mailing Address", rowNum);
		String city = reader1.getCellData(sheetName, "City", rowNum);
		String state = reader1.getCellData(sheetName, "State", rowNum);
		String postalCode = reader1.getCellData(sheetName, "Postalcode", rowNum);
		String mobile = reader1.getCellData(sheetName, "Mobile", rowNum);
		String appendFamilyName = new SimpleDateFormat("dd-MM-yyyy").format(new Date());

		System.out.println("City : " + city);
		System.out.println("State : " + state);
		System.out.println("Postal Code : " + postalCode);

		cpPolicyPage.contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		cpPolicyPage.clientButton_field.click();
		Thread.sleep(2000);

		// Create new client
		String sTempSalutation = CommonFunction.capitaliseFirstLetterSingleWord(salutation);
		System.out.println("Salutation : " + sTempSalutation);
		Select drpSalutation = new Select(cpPolicyPage.salutation_field);
		drpSalutation.selectByValue(sTempSalutation);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		cpPolicyPage.firstName_field.sendKeys(firstName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		cpPolicyPage.lastName_field.sendKeys(lastName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		waitFor1Sec();

		String sTempGender = CommonFunction.capitaliseFirstLetterSingleWord(gender);
		System.out.println("Gender : " + sTempGender);

		Select drpGender = new Select(gender_field);
		drpGender.selectByValue(sTempGender);
		cpPolicyPage.emailAddress_field.sendKeys(emailAddress);
		cpPolicyPage.clientDOB_field.sendKeys(clientDOB, Keys.ESCAPE);
		Thread.sleep(2000);

		cpPolicyPage.mailingAddress1_field.sendKeys(mailingAddress);
		Thread.sleep(2000);
		cpPolicyPage.mailingAddress1_field.sendKeys(Keys.ESCAPE);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@id='mailing_city_client']")).sendKeys(city);
		Thread.sleep(2000);

		driver.findElement(By.xpath("//input[@id='mailing_state']")).sendKeys(state);
		Thread.sleep(2000);

		driver.findElement(By.xpath("//input[@id='mailing_postcode_client']")).sendKeys(postalCode);
		Thread.sleep(2000);

		cpPolicyPage.mailingAddressSameAsPhysicalAddress_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		cpPolicyPage.mobileNumber_field.sendKeys(0+mobile);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		cpPolicyPage.saveChangesButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		Select drpVulnerablePerson = new Select(cpPolicyPage.vulnerablePerson_field);
		try {
			drpVulnerablePerson.selectByValue("Mental Illness");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		cpPolicyPage.clientAdditionalFieldSubmitButton.click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	}

	// Create the new policy for Renewals
	public void createNewPolicyTillAddLobForRenewal(String sheetName, int rowNum) throws Exception {
		String policyStartDate = reader1.getCellData(sheetName, "Policy start date", rowNum);

		System.out.println("policyStartDate" + policyStartDate);
		driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
		Thread.sleep(3000);

		cpPolicyPage.addTransactionButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		cpPolicyPage.newPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// String getPrevDate = CommonFunction.getPreviousYear(days);
		cpPolicyPage.inceptionDate_field.sendKeys(policyStartDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.newPolicyContinueButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		cpPolicyPage.addLobButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		cpPolicyPage.additionalFieldButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	// Verify renewal data from policy view
	public void verifyRenewalData(String sheetName, int rowNum) throws Exception {
		String premium = renewalVerificationReader.getCellData(sheetName, "Premium", rowNum);
		String esl = renewalVerificationReader.getCellData(sheetName, "ESL", rowNum);
		String gst = renewalVerificationReader.getCellData(sheetName, "GST", rowNum);
		String stampDuty = renewalVerificationReader.getCellData(sheetName, "Stamp duty", rowNum);
		String carpeeshDriverSafetyAppFeeInclGST = renewalVerificationReader.getCellData(sheetName,
				"Road safety fee incl. GST", rowNum);
		String merchantFeeInclGST = renewalVerificationReader.getCellData(sheetName, "Merchant fee incl. GST", rowNum);
		String instalmentFeeInclGST = renewalVerificationReader.getCellData(sheetName, "Premium finance incl. GST",
				rowNum);
		String totalCost = renewalVerificationReader.getCellData(sheetName, "Total cost", rowNum);

		DecimalFormat df = new DecimalFormat("#.##");
		df.setMaximumFractionDigits(2);

		// Verify Premium
		String premiumLabel = driver.findElement(By.xpath("//div//label[contains(text(),'Premium')]")).getText().trim();
		String premiumValue = driver
				.findElement(By.xpath("//label[text()='Premium:']/parent::div/following-sibling::div")).getText()
				.trim();

		Double dFormValuePremium = Double.parseDouble(df.format(Double.parseDouble(premiumValue)));
		Double dXlsValuePremium = Double.parseDouble(df.format(Double.parseDouble(premium)));
		if (premiumLabel.equalsIgnoreCase("Premium:")) {
			try {
				if (Double.compare(dFormValuePremium, dXlsValuePremium) == 0) {
					System.out.println("Premim is Matched : " + "" + "Form Value : " + "" + dFormValuePremium + ", "
							+ " " + " Excel Value : " + "" + "$" + dXlsValuePremium);
					ReportsClass.logStat(Status.PASS, "Premium is Matched : " + "" + "Form Value : " + "$"
							+ dFormValuePremium + ", " + " " + " Excel Value : " + "" + "$" + dXlsValuePremium);
				} else {
					System.out.println("Premim is Not Matched : " + "" + "Form Value : " + "" + dFormValuePremium + ", "
							+ " " + " Excel Value : " + "" + "$" + dXlsValuePremium);
					ReportsClass.logStat(Status.FAIL, "Premium is Not Matched : " + "" + "Form Value : " + "$"
							+ dFormValuePremium + ", " + " " + " Excel Value : " + "" + "$" + dXlsValuePremium);
				}
			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			System.out.println(premiumLabel + "Label is not Matched !!!");
		}

		// Verify ESL
		String eslLabel = driver.findElement(By.xpath("//div//label[contains(text(),'ESL')]")).getText().trim();
		String eslValue = driver.findElement(By.xpath("//label[text()='ESL:']/parent::div/following-sibling::div"))
				.getText().trim();

		Double dFormValueESL = Double.parseDouble(df.format(Double.parseDouble(eslValue)));
		Double dXlsValueESL = Double.parseDouble(df.format(Double.parseDouble(esl)));

		if (eslLabel.equalsIgnoreCase("ESL:")) {
			try {
				if (Double.compare(dFormValueESL, dXlsValueESL) == 0) {
					System.out.println("ESL is Matched : " + "" + "Form Value : " + "" + dFormValueESL + ", " + " "
							+ " Excel Value : " + "" + "$" + dXlsValueESL);
					ReportsClass.logStat(Status.PASS, "ESL is Matched : " + "" + "Form Value : " + "$" + dFormValueESL
							+ ", " + " " + " Excel Value : " + "" + "$" + dXlsValueESL);
				} else {
					System.out.println("ESL is Not Matched : " + "" + "Form Value : " + "" + dFormValueESL + ", " + " "
							+ " Excel Value : " + "" + "$" + dXlsValueESL);
					ReportsClass.logStat(Status.FAIL, "ESL is Not Matched : " + "" + "Form Value : " + "$"
							+ dFormValueESL + ", " + " " + " Excel Value : " + "" + "$" + dXlsValueESL);
				}
			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			System.out.println(eslLabel + "Label is not Matched !!!");
		}

		// Verify GST
		String gstLabel = driver.findElement(By.xpath("//div//label[contains(text(),'GST')]")).getText().trim();
		String gstValue = driver.findElement(By.xpath("//label[text()='GST:']/parent::div/following-sibling::div"))
				.getText().trim();

		Double dFormValueGST = Double.parseDouble(df.format(Double.parseDouble(gstValue)));
		Double dXlsValueGST = Double.parseDouble(df.format(Double.parseDouble(gst)));

		if (gstLabel.equalsIgnoreCase("GST:")) {
			try {
				if (Double.compare(dFormValueGST, dXlsValueGST) == 0) {
					System.out.println("GST is Matched : " + "" + "Form Value : " + "" + dFormValueGST + ", " + " "
							+ " Excel Value : " + "" + "$" + dXlsValueGST);
					ReportsClass.logStat(Status.PASS, "GST is Matched : " + "" + "Form Value : " + "$" + dFormValueGST
							+ ", " + " " + " Excel Value : " + "" + "$" + dXlsValueGST);
				} else {
					System.out.println("GST is Not Matched : " + "" + "Form Value : " + "" + dFormValueGST + ", " + " "
							+ " Excel Value : " + "" + "$" + dXlsValueGST);
					ReportsClass.logStat(Status.FAIL, "GST is Not Matched : " + "" + "Form Value : " + "$"
							+ dFormValueGST + ", " + " " + " Excel Value : " + "" + "$" + dXlsValueGST);
				}
			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			System.out.println(gstLabel + "Label is not Matched !!!");
		}

		// Verify Stamp Duty
		String stampDutyLabel = driver.findElement(By.xpath("//div//label[contains(text(),'Stamp duty')]")).getText()
				.trim();
		String stampDutyValue = driver
				.findElement(By.xpath("//label[text()='Stamp duty:']/parent::div/following-sibling::div")).getText()
				.trim();

		Double dFormValueStampDuty = Double.parseDouble(df.format(Double.parseDouble(stampDutyValue)));
		Double dXlsValueStampDuty = Double.parseDouble(df.format(Double.parseDouble(stampDuty)));

		if (stampDutyLabel.equalsIgnoreCase("Stamp duty:")) {
			try {
				if (Double.compare(dFormValueStampDuty, dXlsValueStampDuty) == 0) {
					System.out.println("Stamp duty is Matched : " + "" + "Form Value : " + "" + dFormValueStampDuty
							+ ", " + " " + " Excel Value : " + "" + "$" + dXlsValueStampDuty);
					ReportsClass.logStat(Status.PASS, "Stamp duty is Matched : " + "" + "Form Value : " + "$"
							+ dFormValueStampDuty + ", " + " " + " Excel Value : " + "" + "$" + dXlsValueStampDuty);
				} else {
					System.out.println("Stamp duty is Not Matched : " + "" + "Form Value : " + "" + dFormValueStampDuty
							+ ", " + " " + " Excel Value : " + "" + "$" + dXlsValueStampDuty);
					ReportsClass.logStat(Status.FAIL, "Stamp duty is Not Matched : " + "" + "Form Value : " + "$"
							+ dFormValueStampDuty + ", " + " " + " Excel Value : " + "" + "$" + dXlsValueStampDuty);
				}
			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			System.out.println(stampDutyLabel + "Label is not Matched !!!");
		}

		// Verify Carpeesh driver safety app fee incl GST:
		String carpeeshDriverSafetyAppFeeInclGSTLabel = driver
				.findElement(By.xpath("//div//label[contains(text(),'Carpeesh driver safety app fee incl GST')]"))
				.getText().trim();
		String carpeeshDriverSafetyAppFeeInclGSTValue = driver
				.findElement(By
						.xpath("//label[text()='Carpeesh driver safety app fee incl GST:']/parent::div/following-sibling::div"))
				.getText().trim();
		Double dFormValueCarpeeshDriverSafetyAppFeeInclGST = Double
				.parseDouble(df.format(Double.parseDouble(carpeeshDriverSafetyAppFeeInclGSTValue)));
		Double dXlsValueCarpeeshDriverSafetyAppFeeInclGST = Double
				.parseDouble(df.format(Double.parseDouble(carpeeshDriverSafetyAppFeeInclGST)));

		if (carpeeshDriverSafetyAppFeeInclGSTLabel.equalsIgnoreCase("Carpeesh driver safety app fee incl GST:")) {
			try {
				if (Double.compare(dFormValueCarpeeshDriverSafetyAppFeeInclGST,
						dXlsValueCarpeeshDriverSafetyAppFeeInclGST) == 0) {
					System.out.println("Carpeesh driver safety app fee incl GST is Matched : " + "" + "Form Value : "
							+ "" + dFormValueCarpeeshDriverSafetyAppFeeInclGST + ", " + " " + " Excel Value : " + ""
							+ "$" + dXlsValueCarpeeshDriverSafetyAppFeeInclGST);
					ReportsClass.logStat(Status.PASS,
							"Carpeesh driver safety app fee incl GST is Matched : " + "" + "Form Value : " + "$"
									+ dFormValueCarpeeshDriverSafetyAppFeeInclGST + ", " + " " + " Excel Value : " + ""
									+ "$" + dXlsValueCarpeeshDriverSafetyAppFeeInclGST);
				} else {
					System.out.println("Carpeesh driver safety app fee incl GST is Not Matched : " + ""
							+ "Form Value : " + "" + dFormValueCarpeeshDriverSafetyAppFeeInclGST + ", " + " "
							+ " Excel Value : " + "" + "$" + dXlsValueCarpeeshDriverSafetyAppFeeInclGST);
					ReportsClass.logStat(Status.FAIL,
							"Carpeesh driver safety app fee incl GST is Not Matched : " + "" + "Form Value : " + "$"
									+ dFormValueCarpeeshDriverSafetyAppFeeInclGST + ", " + " " + " Excel Value : " + ""
									+ "$" + dXlsValueCarpeeshDriverSafetyAppFeeInclGST);
				}
			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			System.out.println(carpeeshDriverSafetyAppFeeInclGSTLabel + "Label is not Matched !!!");
		}

		// Verify Merchant fee incl GST:
		String merchantFeeInclGSTLabel = driver
				.findElement(By.xpath("//div//label[contains(text(),'Merchant fee incl GST')]")).getText().trim();
		String merchantFeeInclGSTValue = driver
				.findElement(By.xpath("//label[text()='Merchant fee incl GST:']/parent::div/following-sibling::div"))
				.getText().trim();
		Double dFormValueMerchantFeeInclGST = Double
				.parseDouble(df.format(Double.parseDouble(merchantFeeInclGSTValue)));
		Double dXlsValueMerchantFeeInclGST = Double.parseDouble(df.format(Double.parseDouble(merchantFeeInclGST)));

		if (merchantFeeInclGSTLabel.equalsIgnoreCase("Merchant fee incl GST:")) {
			try {
				if (Double.compare(dFormValueMerchantFeeInclGST, dXlsValueMerchantFeeInclGST) == 0) {
					System.out.println("Merchant fee incl GST is Matched : " + "" + "Form Value : " + ""
							+ dFormValueMerchantFeeInclGST + ", " + " " + " Excel Value : " + "" + "$"
							+ dXlsValueMerchantFeeInclGST);
					ReportsClass.logStat(Status.PASS,
							"Merchant fee incl GST is Matched : " + "" + "Form Value : " + "$"
									+ dFormValueMerchantFeeInclGST + ", " + " " + " Excel Value : " + "" + "$"
									+ dXlsValueMerchantFeeInclGST);
				} else {
					System.out.println("Merchant fee incl GST is Not Matched : " + "" + "Form Value : " + ""
							+ dFormValueMerchantFeeInclGST + ", " + " " + " Excel Value : " + "" + "$"
							+ dXlsValueMerchantFeeInclGST);
					ReportsClass.logStat(Status.FAIL,
							"Merchant fee incl GST is Not Matched : " + "" + "Form Value : " + "$"
									+ dFormValueMerchantFeeInclGST + ", " + " " + " Excel Value : " + "" + "$"
									+ dXlsValueMerchantFeeInclGST);
				}
			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			System.out.println(merchantFeeInclGSTLabel + "Label is not Matched !!!");
		}

		// Verify Instalment fee incl GST:
		String instalmentFeeInclGSTLabel = driver
				.findElement(By.xpath("//div//label[contains(text(),'Instalment fee incl GST')]")).getText().trim();
		String instalmentFeeInclGSTValue = driver
				.findElement(By.xpath("//label[text()='Instalment fee incl GST:']/parent::div/following-sibling::div"))
				.getText().trim();

		Double dFormValueInstalmentFeeInclGST = Double
				.parseDouble(df.format(Double.parseDouble(instalmentFeeInclGSTValue)));
		Double dXlsValueInstalmentFeeInclGST = Double.parseDouble(df.format(Double.parseDouble(instalmentFeeInclGST)));

		if (instalmentFeeInclGSTLabel.equalsIgnoreCase("Instalment fee incl GST:")) {
			try {
				if (Double.compare(dFormValueInstalmentFeeInclGST, dXlsValueInstalmentFeeInclGST) == 0) {
					System.out.println("Instalment fee incl GST is Matched : " + "" + "Form Value : " + ""
							+ dFormValueInstalmentFeeInclGST + ", " + " " + " Excel Value : " + "" + "$"
							+ dXlsValueInstalmentFeeInclGST);
					ReportsClass.logStat(Status.PASS,
							"Instalment fee incl GST is Matched : " + "" + "Form Value : " + "$"
									+ dFormValueInstalmentFeeInclGST + ", " + " " + " Excel Value : " + "" + "$"
									+ dXlsValueInstalmentFeeInclGST);
				} else {
					System.out.println("Instalment fee incl GST is Not Matched : " + "" + "Form Value : " + ""
							+ dFormValueInstalmentFeeInclGST + ", " + " " + " Excel Value : " + "" + "$"
							+ dXlsValueInstalmentFeeInclGST);
					ReportsClass.logStat(Status.FAIL,
							"Instalment fee incl GST is Not Matched : " + "" + "Form Value : " + "$"
									+ dFormValueInstalmentFeeInclGST + ", " + " " + " Excel Value : " + "" + "$"
									+ dXlsValueInstalmentFeeInclGST);
				}
			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			System.out.println(instalmentFeeInclGSTLabel + "Label is not Matched !!!");
		}

		// Verify Total Cost:
		String totalCostLabel = driver.findElement(By.xpath("//div//label[contains(text(),'Total cost')]")).getText()
				.trim();
		String totalCostValue = driver
				.findElement(By.xpath("//label[text()='Total cost:']/parent::div/following-sibling::div")).getText()
				.trim();

		Double dFormValueTotalCost = Double.parseDouble(df.format(Double.parseDouble(totalCostValue)));
		Double dXlsValueTotalCost = Double.parseDouble(df.format(Double.parseDouble(totalCost)));

		if (totalCostLabel.equalsIgnoreCase("Total cost:")) {
			try {
				if (Double.compare(dFormValueTotalCost, dXlsValueTotalCost) == 0) {
					System.out.println("Total Cost is Matched : " + "" + "Form Value : " + "" + dFormValueTotalCost
							+ ", " + " " + " Excel Value : " + "" + "$" + dXlsValueTotalCost);
					ReportsClass.logStat(Status.PASS, "Total Cost is Matched : " + "" + "Form Value : " + "$"
							+ dFormValueTotalCost + ", " + " " + " Excel Value : " + "" + "$" + dXlsValueTotalCost);
				} else {
					System.out.println("Total Cost is Not Matched : " + "" + "Form Value : " + "" + dFormValueTotalCost
							+ ", " + " " + " Excel Value : " + "" + "$" + dXlsValueTotalCost);
					ReportsClass.logStat(Status.FAIL, "Total Cost is Not Matched : " + "" + "Form Value : " + "$"
							+ dFormValueTotalCost + ", " + " " + " Excel Value : " + "" + "$" + dXlsValueTotalCost);
				}
			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			System.out.println(totalCostLabel + "Label is not Matched !!!");
		}

		driver.navigate().back();
		Thread.sleep(2000);
	}
}