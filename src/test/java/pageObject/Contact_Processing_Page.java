package pageObject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import baseClass.TestBase;
import locators.Locators.contactProcessingLocators;
import utility.CommonFunction;
import utility.ReportsClass;

public class Contact_Processing_Page extends TestBase implements contactProcessingLocators {

	Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
	CommonFunction commonFunction = new CommonFunction();
	CP_Client_Policy_Page cpPolicyPage = new CP_Client_Policy_Page();

	@FindBy(xpath = CREDITOR_TASKS_TAB)
	WebElement creditorTasksTab_field;

	@FindBy(xpath = CANCEL_PAYMENT_ICON)
	WebElement cancelPaymentIcon_field;
	@FindBy(xpath = REASON_FOR_CANCELLATION)
	WebElement reasonForCancellation_field;
	@FindBy(xpath = CANCEL_PAYMENT_BUTTON)
	WebElement cancelPaymentButton_field;

	// Hide Copy Policy Locators
	@FindBy(xpath = SETTINGS_TAB)
	WebElement settingsTab_field;
	@FindBy(xpath = SETTINGS_POLICY_CLASS)
	WebElement settingsPolicyClass_field;
	@FindBy(xpath = EDIT_POLICY_CLASS_GHOST)
	WebElement editPolicyClassGhost_field;
	@FindBy(xpath = EDIT_POLICY_FIRE)
	WebElement editPolicyFire_field;
	@FindBy(xpath = HIDE_COPY_POLICY)
	WebElement hideCopyPolicy_field;
	@FindBy(xpath = POLICY_CLASS_SAVE_CHANGE_BUTTON)
	WebElement policyClassSaveChangeButton_field;
	@FindBy(xpath = HIDE_COPY_QUOTE)
	WebElement hideCopyQuote_field;

	// Add Document Cateogry locator in Setting
	@FindBy(xpath = SETTING_DOCUMENT_MANAGEMENT)
	WebElement setting_document_management;
	@FindBy(xpath = ADD_CATEGORY)
	WebElement add_category;
	@FindBy(xpath = STATUS)
	WebElement status;
	@FindBy(xpath = SAVE)
	WebElement save;
	@FindBy(xpath = REMOVE_CATEGORY)
	WebElement remove_Category;

	// Add Document history locator in Contact
	@FindBy(xpath = DOCUMENT_HISTORY_TAB)
	WebElement documentHistoryTab_field;
	@FindBy(xpath = CATEGORY)
	WebElement category_field;
	@FindBy(xpath = DESCRIPTION)
	WebElement description_field;
	@FindBy(xpath = EXPIRY_DATE)
	WebElement expiryDate_field;
	@FindBy(xpath = REASON_FOR_EXPIRY)
	WebElement reasonForExpiry_field;
	@FindBy(xpath = AVAILABLE_ON_CLIENT_PORTAL)
	WebElement availableOnClientPortal_field;
	@FindBy(xpath = AVAILABLE_ON_BROKER_PORTAL)
	WebElement availableOnBrokerPortal_field;
	@FindBy(xpath = SUBMIT)
	WebElement submit_field;
	@FindBy(xpath = UPLOAD_BUTTON)
	WebElement uploadButton_field;
	@FindBy(xpath = SUCCESS_MESSAGE)
	WebElement successMessage_field;

	// Add Installment to Policy Locators
	@FindBy(xpath = INSTALLMENTS_BUTTON)
	WebElement installmentsButton_field;
	@FindBy(xpath = INSTALLMENT_EVERY_DROPDOWN)
	WebElement installmentEveryDropdown_field;
	@FindBy(xpath = INCLUDE_POLICY_FEE_IN_INSTALLMENT_CHECKBOX)
	WebElement includePolicyFeeInInstallmentCheckbox_field;
	@FindBy(xpath = INCLUDE_BROKER_FEE_IN_INSTALLMENT_CHECKBOX)
	WebElement includeBrokerFeeInInstallmentCheckbox_field;
	@FindBy(xpath = CHECK_INSTALLMENT_PLAN_BUTTON)
	WebElement checkInstallmentPlanButton_field;
	@FindBy(xpath = POST_INSTALLMENTS_BUTTON)
	WebElement postInstallmentsButton_field;

	// Client Fields Locators
	@FindBy(xpath = SETTINGS_CLIENT_FIELDS)
	WebElement settingsClientFields_field;
	@FindBy(xpath = CLIENT_CODE_RULE)
	WebElement clientCodeRule_field;

	public Contact_Processing_Page() {
		PageFactory.initElements(driver, this);
	}

	// Add Task for Creditor
	public void addTaskForCreditor(String creditorName, String assignedTo, String noteSection, String subject,
			String description) throws Exception {
		basicProcessing.creditorTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		basicProcessing.creditorTextInput_field.sendKeys(creditorName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		basicProcessing.creditorSearchButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		basicProcessing.creditorListManageIcon_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		creditorTasksTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		// Click on Add Task Button
		basicProcessing.addTaskButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Assigned Task To
		Select drpAssignedTaskTo = new Select(basicProcessing.assignedTo_field);

		try {
			drpAssignedTaskTo.selectByValue(assignedTo);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Select Note Section
		Select drpNoteSection = new Select(basicProcessing.addTaskNoteSection_field);

		try {
			drpNoteSection.selectByValue(noteSection);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Enter Time
		basicProcessing.addTaskTime_field.sendKeys("10:00");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Enter Subject
		basicProcessing.addTaskSubject_field.sendKeys(subject);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Enter Description
		basicProcessing.addTaskDescription_field.sendKeys(description);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Click on Save button
		basicProcessing.addTaskSaveButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
		String alertMessage = driver.switchTo().alert().getText();

		if (alertMessage.contains("Manual Task added")) {
			ReportsClass.logStat(Status.PASS, "Task Added Successfully !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "Task is Not Added Successfully !!!");
		}

		// Verify Task is added successfully Or Not
		Assert.assertTrue(alertMessage.contains("Manual Task added"), "Task is Not Added Successfully !!!");

		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
	}

	// Cancel payment
	public void cancelPayment(String reasonForCancellation) throws Exception {
		cancelPaymentIcon_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		reasonForCancellation_field.sendKeys(reasonForCancellation);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cancelPaymentButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		String getCancelMsg = driver.findElement(By.xpath("//div[@id='payments']//tbody//tr[1]")).getText();
		if (getCancelMsg.contains("CANPAY")) {
			ReportsClass.logStat(Status.PASS, "Cancel Payment Successfully !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "Failed to Cancel Payment !!!");
		}

		// Verify that Cancel Payment is successfull or not
		Assert.assertTrue(getCancelMsg.contains("CANPAY"), "Failed to Cancel Payment !!!");
	}

	// Hide Copy Policy Settings
	public void hideCopyPolicySettings(String hideCopyPolicyValue) throws Exception {
		settingsTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		settingsPolicyClass_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		editPolicyClassGhost_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Value from Hide Copy Policy Dropdown
		Select drpHideCopyPolicy = new Select(hideCopyPolicy_field);

		try {
			drpHideCopyPolicy.selectByValue(hideCopyPolicyValue);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		policyClassSaveChangeButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	// Verify Hide Copy Policy
	public void verifyHideCopyPolicyOrQuote(String text) throws Exception {
		basicProcessing.manageIconForEndorsePolicy_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Verify Hide Copy Policy Or Quote
		commonFunction.isElementPresent(By.xpath("//a[contains(text(),'" + text + "')]"), text);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		if (text.equalsIgnoreCase("Copy Policy")) {
			driver.findElement(By.xpath("//tbody/tr[1]/td[16]/div[2]/div[1]/div[1]/div[1]/button[1]/span[1]")).click();
		} else {
			driver.findElement(By.xpath("//tbody/tr[1]/td[16]/div[3]/div[1]/div[1]/div[1]/button[1]/span[1]")).click();
		}
	}

	// Add Document category in Setting
	public void addDocumentCategory(String categoryName) {
		settingsTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		setting_document_management.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		add_category.sendKeys(categoryName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		status.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		save.click();
	}

	// Add Installment To Policy
	public void addInstallmentToPolicy(String clientName, String policyClass, String townVillage, String sumInsured,
			String futureAnnualPremier, String installmentEveryOption) throws Exception {
		basicProcessing.contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFunction.navigateToClient(clientName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.addTransactionButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		cpPolicyPage.newPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		Select drpSelectPolicyClass = new Select(basicProcessing.selectPolicyClass_field);

		try {
			drpSelectPolicyClass.selectByValue(policyClass);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		String getCurrentDate = CommonFunction.getCurrentDate();
		cpPolicyPage.inceptionDate_field.sendKeys(getCurrentDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.newPolicyContinueButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		cpPolicyPage.addLobButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		basicProcessing.addLobTownVillage_field.clear();
		basicProcessing.addLobTownVillage_field.sendKeys(townVillage);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		basicProcessing.addLobSumInsured_field.sendKeys(Keys.CONTROL, "a");
		basicProcessing.addLobSumInsured_field.sendKeys(Keys.BACK_SPACE);
		basicProcessing.addLobSumInsured_field.sendKeys(sumInsured);
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		basicProcessing.addLobFutureAnnualPremier_field.sendKeys(Keys.CONTROL, "a");
		basicProcessing.addLobFutureAnnualPremier_field.sendKeys(Keys.BACK_SPACE);
		basicProcessing.addLobFutureAnnualPremier_field.sendKeys(futureAnnualPremier);
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Add Broker
		commonFunction.addBroker("179_No", "Broker_7011", "5000", "100");

		// Add Insurer
		commonFunction.addInsurer("131", "11", "AAA1212", "30 Days", "11", "10.00");

		basicProcessing.addLobSaveButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(4000);

		// Add Installment
		installmentsButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);

		// Select Installment Every Option
		Select drpInstallmentEveryOption = new Select(installmentEveryDropdown_field);

		try {
			drpInstallmentEveryOption.selectByValue(installmentEveryOption);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		includePolicyFeeInInstallmentCheckbox_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		includeBrokerFeeInInstallmentCheckbox_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		checkInstallmentPlanButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);

		postInstallmentsButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(3000);

		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.switchTo().alert().accept();
		Thread.sleep(3000);

		// Verify Add Installment Policy is created successfully or Not
		commonFunction.verifyAddTransactionTypeCreatedSuccessfully("Add Installment Policy");

		driver.findElement(By.xpath("//button[contains(text(),'Policy Overview')]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(4000);
	}

	// Set Client Code Rule as Auto Generate If Blank
	public void setClientCodeRuleFromClientFieldsSettings(String clientCodeRule) throws Exception {
		settingsTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		settingsClientFields_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		// Set Client Code Rule
		Select drpClientCodeRule = new Select(clientCodeRule_field);

		try {
			drpClientCodeRule.selectByValue(clientCodeRule);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		policyClassSaveChangeButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		String getClientCodeRuleSuccess = driver
				.findElement(
						By.xpath("//div[contains(text(),'Client settings fields have been successfully updated.')]"))
				.getText();

		if (getClientCodeRuleSuccess.contains("Client settings fields have been successfully updated.")) {
			ReportsClass.logStat(Status.PASS, "Client Code Rule is successfully set as = " + clientCodeRule + "");
		} else {
			ReportsClass.logStat(Status.FAIL, "Failed to Set Client Code Rule as = " + clientCodeRule + "");
		}

		// Verify that Client Code Rule is Set Successfully Or Not
		Assert.assertTrue(getClientCodeRuleSuccess.contains("Client settings fields have been successfully updated."),
				"Failed to Set Client Code Rule as " + clientCodeRule + "");
	}

	public void hideCopyQuoteSetting(String hideCopyPolicyValue) {
		// TODO Auto-generated method stub
		settingsTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		settingsPolicyClass_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		editPolicyFire_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Select Value from Hide Copy Policy Dropdown
		Select drpHideCopyPolicy = new Select(hideCopyQuote_field);

		try {
			drpHideCopyPolicy.selectByValue(hideCopyPolicyValue);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		policyClassSaveChangeButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	// Add Document history and verify selected category
	public void addDocumentHistory(String clientName, String categoryName, String descriptioText, String expiryDate,
			String reasonForExpiry, String filePath) throws Exception {
		basicProcessing.contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		commonFunction.navigateToClient(clientName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		documentHistoryTab_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Select last added category from dropdown
		Select drpCategory = new Select(category_field);
		try {
			drpCategory.selectByVisibleText(categoryName);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}
		description_field.sendKeys(descriptioText);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		expiryDate_field.sendKeys(expiryDate);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		reasonForExpiry_field.sendKeys(reasonForExpiry);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		availableOnClientPortal_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		availableOnBrokerPortal_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		uploadButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);
		commonFunction.uploadFile(filePath);
		Thread.sleep(3000);
		submit_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		String actualMessage = successMessage_field.getText();

		if (actualMessage.contains("Documents uploaded successfully")) {
			System.out.println("Document Uploaded successfully !!!");
			ReportsClass.logStat(Status.PASS, "Document Uploaded successfully !!!");
		} else {
			System.out.println("Failed to Upload Document  !!!");
			ReportsClass.logStat(Status.PASS, "Failed to Upload Document  !!!");
		}

		// Verify that Document Uploaded successfully or Not
		Assert.assertTrue(actualMessage.contains("Documents uploaded successfully"), "Failed to Upload Document  !!!");
	}

	public void removeDocumentCategory(String categoryName) throws InterruptedException {
		settingsTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		setting_document_management.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		remove_Category.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		String actualDeleteMessage = driver.findElement(By.xpath("(//div[@class='alert alert-success'])[1]")).getText();

		if (actualDeleteMessage.contains("Category has been successfully deleted")) {
			System.out.println("Category has been successfully deleted !!!");
		} else {
			System.out.println("Failed to Delete Category !!!");
		}
	}
}