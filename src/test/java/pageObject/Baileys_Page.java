package pageObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.aventstack.extentreports.Status;

import baseClass.TestBase;
import locators.Locators.baileysLocators;
import utility.CommonFunction;
import utility.Global_VARS;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

public class Baileys_Page extends TestBase implements baileysLocators {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
	CP_Client_Policy_Page cpPolicyPage = new CP_Client_Policy_Page();
	CommonFunction commonFunction = new CommonFunction();
	String baileysExcelPath = readPro.getBaileysExcelFile();
	WebDriverWait wait = new WebDriverWait(driver, 500);
	Xls_Reader reader = new Xls_Reader(baileysExcelPath);

	// Create new client Locators
	@FindBy(xpath = CLIENT_CODE)
	WebElement clientCode_field;
	@FindBy(xpath = BRANCH)
	WebElement branch_field;

	// Pleasure Craft Page Locators Step 1
	@FindBy(id = "salutation")
	WebElement salutation_field;
	@FindBy(id = "first_name")
	WebElement firstName_field;
	@FindBy(id = "last_name")
	WebElement lastName_field;
	@FindBy(id = "client_dob")
	WebElement dateOfBirth_field;
	@FindBy(id = "phoneareacode")
	WebElement phoneAreaCode_field;
	@FindBy(id = "phone")
	WebElement phone_field;
	@FindBy(id = "client_email")
	WebElement emailAddress_field;
	@FindBy(id = "confirm_email")
	WebElement confirmEmailAddress_field;
	@FindBy(xpath = B_NEXT_BUTTON)
	WebElement nextButton_field;

	// Pleasure Craft Page Locators Step 2
	@FindBy(xpath = IS_YOUR_BOAT_HOME_BUILT)
	WebElement isYourBoatHomeBuilt_field;
	@FindBy(xpath = IS_THIS_VESSEL_CURRENTLY_VERO_INSURED_YES)
	WebElement isThisVesselCurrentlyVeroInsuredYes_field;
	@FindBy(xpath = IS_THIS_VESSEL_CURRENTLY_VERO_INSURED_NO)
	WebElement isThisVesselCurrentlyVeroInsuredNo_field;
	@FindBy(xpath = TYPE_OF_BOAT)
	WebElement typeOfBoat_field;
	@FindBy(xpath = TYPE_OF_MOORING)
	WebElement typeOfMooring_field;
	@FindBy(xpath = DO_YOU_HAVE_A_TRAILER_YES)
	WebElement doYouHaveATrailerYes_field;
	@FindBy(xpath = DO_YOU_HAVE_A_TRAILER_NO)
	WebElement doYouHaveATrailerNo_field;
	@FindBy(xpath = MOORING_LOCATION)
	WebElement mooringLocation_field;
	@FindBy(xpath = TRAILER_REGISTRATION_NO)
	WebElement trailerRegistrationNumber_field;
	@FindBy(xpath = SUBMIT_BUTTON)
	WebElement submitButton_field;
	@FindBy(xpath = DATE_YOU_PURCHASED_YOUR_BOAT)
	WebElement pleaseAdviseTheDateYouPurchasedYourBoat_field;
	@FindBy(xpath = ADVISE_THE_PRICE_PAID_FOR_YOUR_BOAT)
	WebElement pleaseAdviseThePriceYouPaidForYourBoat_field;
	@FindBy(xpath = TOTAL_BOATING_EXPERIENCE_DO_YOU_HAVE)
	WebElement howManyYearsTotalBoatinExperienceYouHave_field;
	@FindBy(xpath = ARE_YOU_AN_EXISTING_CLIENT_OF_BAILEYS_YES)
	WebElement areYouAnExistingClientOfBaileysYes_field;
	@FindBy(xpath = ARE_YOU_AN_EXISTING_CLIENT_OF_BAILEYS_NO)
	WebElement areYouAnExistingClientOfBaileysNo_field;

	// Boat Use Section Locators
	@FindBy(xpath = IS_YOUR_BOAT_USED_FOR_BUSINESS_CHARTER_PURPOSE_YES)
	WebElement isYourBoatUsedForBusinessCharterPurposeYes_field;
	@FindBy(xpath = IS_YOUR_BOAT_USED_FOR_BUSINESS_CHARTER_PURPOSE_NO)
	WebElement isYourBoatUsedForBusinessCharterPurposeNo_field;
	@FindBy(xpath = COMMERICIAL_YES)
	WebElement commericialYes_field;
	@FindBy(xpath = COMMERICIAL_NO)
	WebElement commericialNo_field;
	@FindBy(xpath = CHARTER_YES)
	WebElement charterYes_field;
	@FindBy(xpath = CHARTER_NO)
	WebElement charterNo_field;
	@FindBy(xpath = SHARED_OWNERSHIP_YES)
	WebElement sharedOwnershipYes_field;
	@FindBy(xpath = SHARED_OWNERSHIP_NO)
	WebElement sharedOwnershipNo_field;
	@FindBy(xpath = DO_YOU_CURRENTLY_LIVE_ABOARD_VESSEL_YES)
	WebElement doYouCurrentlyLiveAboardVesselYes_field;
	@FindBy(xpath = DO_YOU_CURRENTLY_LIVE_ABOARD_VESSEL_NO)
	WebElement doYouCurrentlyLiveAboardVesselNo_field;

	// Policy Commencement Date Section Locators
	@FindBy(xpath = INSURANCE_FROM)
	WebElement insuranceFrom_field;
	@FindBy(xpath = INSURANCE_TO)
	WebElement insuranceTo_field;

	// Sum Insured (NZD) Section Locators
	@FindBy(xpath = FIXITURE_FITTING_MACHINERY_ACCESSORIES_VESSEL_NZD)
	WebElement fixitureFittingMachineryAcceesoriesVessel_field;
	@FindBy(xpath = TRAILER_NZD)
	WebElement trailerNZD_field;
	@FindBy(xpath = DO_YOU_NEED_COVER_FISHING_DRIVING_WHEN_LEFT_YOUR_BOAT_YES)
	WebElement doYouNeedCoverFishingDrivingLeftYourBoatYes_field;
	@FindBy(xpath = DO_YOU_NEED_COVER_FISHING_DRIVING_WHEN_LEFT_YOUR_BOAT_NO)
	WebElement doYouNeedCoverFishingDrivingLeftYourBoatNo_field;
	@FindBy(xpath = CLAIMS_EXCESS)
	WebElement claimExcess_field;

	// Vessel Details Section Locators
	@FindBy(xpath = PLEASE_ADVISE_THE_YEAR_BOAT_WAS_ORIGINALLY_BUILT)
	WebElement pleaseAdviseTheYearBoatWasOriginallyBuild_field;
	@FindBy(xpath = CONSTRUCTION)
	WebElement construction_field;
	@FindBy(xpath = MAX_SPEED_IN_KNOTS)
	WebElement maxSpeedInKnots_field;
	@FindBy(xpath = BOAT_NAME)
	WebElement boatName_field;
	@FindBy(xpath = MAKE_MODEL)
	WebElement makeModel_field;
	@FindBy(xpath = LENGTH)
	WebElement length_field;
	@FindBy(xpath = UNIT_YES)
	WebElement unitYes_field;
	@FindBy(xpath = UNIT_NO)
	WebElement unitNo_field;
	@FindBy(xpath = TYPE_OF_MOTOR)
	WebElement typeOfMotor_field;
	@FindBy(xpath = CONVERTED_CAR_MOTOR_YES)
	WebElement convertedCarMotorYes_field;
	@FindBy(xpath = CONVERTED_CAR_MOTOR_NO)
	WebElement convertedCarMotorNo_field;
	@FindBy(xpath = HAS_THIS_MOTOR_PROFESSIONALLY_INSTALLED_YES)
	WebElement hasThisMotorProfessionalInstalledYes_field;
	@FindBy(xpath = HAS_THIS_MOTOR_PRODESSIONALLY_INSTALLED_NO)
	WebElement hasThisMotorProfessionalInstalledNo_field;
	@FindBy(xpath = MAKE_OF_MOTOR)
	WebElement makeOfMotor_field;
	@FindBy(xpath = YEAR_OF_MANUFACTURE)
	WebElement yearOfManufacture_field;
	@FindBy(xpath = HP)
	WebElement hp_field;
	@FindBy(xpath = FUEL_PETROL)
	WebElement fuelPetro_field;
	@FindBy(xpath = FUEL_DIESEL)
	WebElement fuelDiesel_field;

	// Cover Specifics Section Locators
	@FindBy(xpath = IS_THERE_INTERESTED_PARTY_INVOLVED_YES)
	WebElement isThereInterstedPartyInvolvedYes_field;
	@FindBy(xpath = IS_THERE_INTERESTED_PARTY_INVOLVED_NO)
	WebElement isThereInterstedPartyInvolvedNo_field;
	@FindBy(xpath = NAME_OF_INTERESTED_PARTY)
	WebElement nameOfnterstedParty_field;
	@FindBy(xpath = DO_YOU_NEED_ADDITIONAL_OWNER_POLICY_YES)
	WebElement doYouNeedAdditionalOwnerPolicyYes_field;
	@FindBy(xpath = DO_YOU_NEED_ADDITIONAL_OWNER_POLICY_NO)
	WebElement doYouNeedAdditionalOwnerPolicyNo_field;
	@FindBy(xpath = ADDITIONAL_OWNER_NAME)
	WebElement additionalOwnerName_field;
	@FindBy(xpath = PREVIOUS_BOAT_INSURANCE)
	WebElement previousBoatInsurance_field;
	@FindBy(xpath = COASTGUARD_MASTER_YES)
	WebElement coastGuardMasterYes_field;
	@FindBy(xpath = COASTGUARD_MASTER_NO)
	WebElement coastGuardMasterNo_field;
	@FindBy(xpath = COASTGUARD_DISCOUNT)
	WebElement coastGuardDiscount_field;
	@FindBy(xpath = DO_YOU_RACE_WITH_USE_OF_EXTRAS_YES)
	WebElement doYouRaceWithUseOfExtraYes_field;
	@FindBy(xpath = DO_YOU_RACE_WITH_USE_OF_EXTRAS_NO)
	WebElement doYouRaceWithUseOfExtraNo_field;

	// Customer Referral Programme Section Locators
	@FindBy(xpath = HOW_DID_YOU_HEAR_ABOUT_BAILEYS)
	WebElement howDidYouHearAboutBaileys_field;

	// Declaration Section Locators
	@FindBy(xpath = DECLARATION_OPTION_A_YES)
	WebElement declarationOption_A_Yes_field;
	@FindBy(xpath = DECLARATION_OPTION_A_NO)
	WebElement declarationOption_A_No_field;
	@FindBy(xpath = DECLARATION_OPTION_B_YES)
	WebElement declarationOption_B_Yes_field;
	@FindBy(xpath = DECLARATION_OPTION_B_NO)
	WebElement declarationOption_B_No_field;
	@FindBy(xpath = DECLARATION_OPTION_C_YES)
	WebElement declarationOption_C_Yes_field;
	@FindBy(xpath = DECLARATION_OPTION_C_NO)
	WebElement declarationOption_C_No_field;
	@FindBy(xpath = DECLARATION_OPTION_D_YES)
	WebElement declarationOption_D_Yes_field;
	@FindBy(xpath = DECLARATION_OPTION_D_NO)
	WebElement declarationOption_D_No_field;
	@FindBy(xpath = CANCEL_BUTTON)
	WebElement cancelButton_field;

	public Baileys_Page() {
		PageFactory.initElements(driver, this);
	}

	// Create new client for Baileys
	public void createNewClientForBaileys(String sheetName, int rowNum) throws Exception {
		String salutation = reader.getCellData(sheetName, "Salutation", rowNum);
		String firstName = reader.getCellData(sheetName, "First Name", rowNum);
		String lastName = reader.getCellData(sheetName, "Last Name", rowNum);
		String gender = reader.getCellData(sheetName, "Gender", rowNum);
		String emailAddress = reader.getCellData(sheetName, "Email Address", rowNum);
		String mailingAddress = reader.getCellData(sheetName, "Mailing Address", rowNum);
		String clientCode = reader.getCellData(sheetName, "Client Code", rowNum);
		String branch = reader.getCellData(sheetName, "Branch", rowNum);

		String appendFamilyName = new SimpleDateFormat("dd-MM-yyyy HH:mm aaa").format(new Date());
		basicProcessing.contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		basicProcessing.clientButton_field.click();
		Thread.sleep(2000);

		// Create new client
		String sTempSalutation = CommonFunction.capitaliseFirstLetterSingleWord(salutation);
		Select drpSalutation = new Select(salutation_field);
		drpSalutation.selectByValue(sTempSalutation);
		firstName_field.sendKeys(firstName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		lastName_field.sendKeys(lastName + "_" + appendFamilyName, Keys.TAB);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		Select drpGender = new Select(basicProcessing.gender_field);
		drpGender.selectByValue(gender);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		emailAddress_field.sendKeys(emailAddress);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		basicProcessing.clientDOB_field.sendKeys("18-08-1985", Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		basicProcessing.mailingAddress1_field.sendKeys(mailingAddress);
		Thread.sleep(2000);
		basicProcessing.mailingAddress1_field.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		basicProcessing.mailingAddress1_field.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		basicProcessing.mailingAddressSameAsPhysicalAddress_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		clientCode_field.sendKeys(clientCode);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		Select drpBranch = new Select(branch_field);

		try {
			drpBranch.selectByValue(branch);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		basicProcessing.saveChangesButton_field.click();
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String clientAddedMessage = driver
				.findElement(By.xpath("//div[contains(text(),'Client Has Been Successfully Added')]")).getText();
		if (clientAddedMessage.contains("Client Has Been Successfully Added")) {
			ReportsClass.logStat(Status.PASS, "Client " + firstName + "_" + lastName + "_" + appendFamilyName
					+ " Has Been Successfully Added !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "Client " + firstName + "_" + lastName + "_" + appendFamilyName
					+ " Has Not Been Successfully added !!!");
		}

		// Verify the client has been successfully added or not
		Assert.assertTrue(clientAddedMessage.contains("Client Has Been Successfully Added"), "Client " + firstName + "_"
				+ lastName + "_" + appendFamilyName + " Has Not Been Successfully added !!!");
		Thread.sleep(2000);
	}

	// Create Policy Till Update Additional Form
	public void createPolicyForBaileys(String type, String policyClass) throws Exception {
		cpPolicyPage.addTransactionButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		if (type.equalsIgnoreCase("Quote")) {
			basicProcessing.newQuoteButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if (type.equalsIgnoreCase("Policy")) {
			cpPolicyPage.newPolicyButton_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else {
			Assert.assertTrue(type.equalsIgnoreCase("Quote") || type.equalsIgnoreCase("Policy"),
					"Failed to click on Quote OR Policy from Add Transaction button !!!");
		}

		Select drpSelectPolicyClass = new Select(basicProcessing.selectPolicyClass_field);

		try {
			drpSelectPolicyClass.selectByValue(policyClass);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		String getCurrentDate = CommonFunction.getCurrentDate();
		cpPolicyPage.inceptionDate_field.sendKeys(getCurrentDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		cpPolicyPage.newPolicyContinueButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		cpPolicyPage.addLobButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		driver.findElement(
				By.xpath("//div[@class='text-right']//button[@class='btn btn-primary'][contains(text(),'No')]"))
				.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		basicProcessing.updateAdditionalFieldButton_field.click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(3000);
	}

	// Pleasure Craft Form Page Step2
	public boolean pleasureCraftDetails_step2(String sheetName, int rowNum) {
		String isThisVesselInuredWithVero = reader.getCellData(sheetName, "Is this vessel insured with Vero by you",
				rowNum);
		String typeOfBoat = reader.getCellData(sheetName, "Type of Boat", rowNum);
		String typeOfMooring = reader.getCellData(sheetName, "Type of Mooring", rowNum);
		String doYouHaveATrailer = reader.getCellData(sheetName, "Do you have a trailer?", rowNum);
		String mooringLocation = reader.getCellData(sheetName, "Mooring Location", rowNum);
		String trailerRegistrationNumber = reader.getCellData(sheetName, "Trailer Registration No", rowNum);
		String pleaseAdviseThePriceYouPaidForYourBoat = reader.getCellData(sheetName,
				"Please advise the price you paid for your boat", rowNum);
		String howManyYearTotalBoatingExperienceDoYouHave = reader.getCellData(sheetName,
				"How many years total boating experience do you have?", rowNum);
		String areYouAnExistingClientOfBaileys = reader.getCellData(sheetName, "Are you an existing client of Baileys?",
				rowNum);

		// Is your boat Home Built?
		isYourBoatHomeBuilt_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Is this vessel currently insured with Vero Insurance by you?
		if (isThisVesselInuredWithVero.equalsIgnoreCase("Yes")) {
			String isThisVessleCurrentlyDeclinedMessage = "Sorry we cannot quote you if you are an existing Vero, Mariner or Nautical customer.";
			isThisVesselCurrentlyVeroInsuredYes_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			if (Global_VARS.submitButtonText.compareToIgnoreCase(Global_VARS.submitButtonGetText) != 0) {
				System.out.println(
						"Declined Case => Is this vessel currently insured with Vero, Mariner or Nautical Insurance by you?"
								+ " : " + isThisVessleCurrentlyDeclinedMessage);
				ReportsClass.logStat(Status.FATAL,
						"Declined Case => Is this vessel currently insured with Vero Insurance by you?" + " : "
								+ isThisVessleCurrentlyDeclinedMessage);
				return false;
			}
		} else {
			isThisVesselCurrentlyVeroInsuredNo_field.click();
		}

		// Type of Boat
		if (typeOfBoat.equalsIgnoreCase("Runabout")) {
			// Select Type of Boat Value
			Select drpTypeOfBoat1 = new Select(typeOfBoat_field);
			try {
				drpTypeOfBoat1.selectByValue(typeOfBoat);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}

			// Select Type of Mooring Value
			Select drpTypeOfMooring1 = new Select(typeOfMooring_field);
			try {
				drpTypeOfMooring1.selectByValue(typeOfMooring);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}

			if (typeOfMooring.equalsIgnoreCase("Dry Stack")) {
				if (doYouHaveATrailer.equalsIgnoreCase("Yes")) {
					doYouHaveATrailerYes_field.click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

					// Select Trailer Registration Number
					trailerRegistrationNumber_field.sendKeys(trailerRegistrationNumber);
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				} else {
					doYouHaveATrailerNo_field.click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}

				// Select Mooring Location Value
				Select drpMooringLocation1 = new Select(mooringLocation_field);
				try {
					drpMooringLocation1.selectByValue(mooringLocation);
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				} catch (Exception e) {
					System.out.println("");
				}
			} else {
				trailerRegistrationNumber_field.sendKeys(trailerRegistrationNumber);
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}

		} else if (typeOfBoat.equalsIgnoreCase("Launch")) {
			// Select Type of Boat Value
			Select drpTypeOfBoat2 = new Select(typeOfBoat_field);
			try {
				drpTypeOfBoat2.selectByValue(typeOfBoat);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}

			// Select Type Of Mooring Value
			Select drpTypeOfMooring2 = new Select(typeOfMooring_field);
			try {
				drpTypeOfMooring2.selectByValue(typeOfMooring);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.err.println("");
			}

			// Type of Mooring
			if (typeOfMooring.equalsIgnoreCase("Dry Stack")) {
				if (doYouHaveATrailer.equalsIgnoreCase("Yes")) {
					doYouHaveATrailerYes_field.click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

					// Select Trailer Registration Number Value
					trailerRegistrationNumber_field.sendKeys(trailerRegistrationNumber);
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				} else {
					doYouHaveATrailerNo_field.click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}

				// Select Mooring Location Value
				Select drpMooringLocation1 = new Select(mooringLocation_field);
				try {
					drpMooringLocation1.selectByValue(mooringLocation);
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				} catch (Exception e) {
					System.out.println("");
				}
			} else if (typeOfMooring.equalsIgnoreCase("Trailered")) {
				// Select Trailer Registration Number Value
				trailerRegistrationNumber_field.sendKeys(trailerRegistrationNumber);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			} else if (typeOfMooring.equalsIgnoreCase("Pile") || typeOfMooring.equalsIgnoreCase("Marina")
					|| typeOfMooring.equalsIgnoreCase("Swing")) {
				// Select Mooring Location Value
				Select drpMooringLocation1 = new Select(mooringLocation_field);
				try {
					drpMooringLocation1.selectByValue(mooringLocation);
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				} catch (Exception e) {
					System.out.println("");
				}
			}
		} else if (typeOfBoat.equalsIgnoreCase("Yacht")) {
			// Select Type Of Boat Value
			Select drpTypeOfBoat3 = new Select(typeOfBoat_field);
			try {
				drpTypeOfBoat3.selectByValue(typeOfBoat);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}

			// Select Type Of Mooring Value
			Select drpTypeOfMooring3 = new Select(typeOfMooring_field);
			try {
				drpTypeOfMooring3.selectByValue(typeOfMooring);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}

			if (typeOfMooring.equalsIgnoreCase("Swing") || typeOfMooring.equalsIgnoreCase("Marina")
					|| typeOfMooring.equalsIgnoreCase("Pile")) {
				// Select Mooring Location
				Select drpMooringLocation3 = new Select(mooringLocation_field);
				try {
					drpMooringLocation3.selectByValue(mooringLocation);
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				} catch (Exception e) {
					System.out.println("");
				}
			} else if (typeOfMooring.equalsIgnoreCase("Trailered")) {
				// Select Trailer Registration Number Value
				trailerRegistrationNumber_field.sendKeys(trailerRegistrationNumber);
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			} else if (typeOfMooring.equalsIgnoreCase("Dry Stack")) {
				if (doYouHaveATrailer.equalsIgnoreCase("Yes")) {
					doYouHaveATrailerYes_field.click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

					// Select Trailer Registration Number Value
					trailerRegistrationNumber_field.sendKeys(trailerRegistrationNumber);
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				} else {
					doYouHaveATrailerNo_field.click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}

				// Select Mooring Location
				Select drpMooringLocation3 = new Select(mooringLocation_field);
				try {
					drpMooringLocation3.selectByValue(mooringLocation);
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				} catch (Exception e) {
					System.out.println("");
				}
			}
		}

		// Please advise the date you purchased your boat
		String todayDate = CommonFunction.getCurrentDate();
		pleaseAdviseTheDateYouPurchasedYourBoat_field.sendKeys(todayDate, Keys.ESCAPE);

		// Please advise the price you paid for your boat
		pleaseAdviseThePriceYouPaidForYourBoat_field.sendKeys(pleaseAdviseThePriceYouPaidForYourBoat);

		// How many years total boating experience do you have?
		Select drpBoatingExperienceYouHave = new Select(howManyYearsTotalBoatinExperienceYouHave_field);
		drpBoatingExperienceYouHave.selectByValue(howManyYearTotalBoatingExperienceDoYouHave);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Are you an existing client of Baileys?
		if (areYouAnExistingClientOfBaileys.equalsIgnoreCase("Yes")) {
			areYouAnExistingClientOfBaileysYes_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		} else {
			areYouAnExistingClientOfBaileysNo_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		}
		return true;
	}

	// Boat Use Section
	public boolean boatUse(String sheetName, int rowNum) {
		String isYourBoatEverUsedForBusinessCharterPurpose = reader.getCellData(sheetName,
				"Is your boat ever used for business or charter purposes", rowNum);
		String doYouCurrentlyLiveAbroadYourVessel = reader.getCellData(sheetName,
				"Do you currently live aboard your vessel", rowNum);

		// Is your boat ever used for business or charter purposes

		if (isYourBoatEverUsedForBusinessCharterPurpose.equalsIgnoreCase("Yes")) {
			isYourBoatUsedForBusinessCharterPurposeYes_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		} else {
			isYourBoatUsedForBusinessCharterPurposeNo_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		}

		// Do you currently live aboard your vessel
		if (doYouCurrentlyLiveAbroadYourVessel.equalsIgnoreCase("Yes")) {
			String doYouCurrentlyLiveAbroadDeclinedMessage = "Sorry we cannot cover you as you live on board your vessel";
			doYouCurrentlyLiveAboardVesselYes_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			if (Global_VARS.submitButtonText.compareToIgnoreCase(Global_VARS.submitButtonGetText) != 0) {
				System.out.println("Declined Case => Do you currently live aboard your vessel" + " : "
						+ doYouCurrentlyLiveAbroadDeclinedMessage);
				ReportsClass.logStat(Status.FATAL, "Declined Case => Do you currently live aboard your vessel" + " : "
						+ doYouCurrentlyLiveAbroadDeclinedMessage);
				return false;
			}

		} else {
			doYouCurrentlyLiveAboardVesselNo_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		}

		return true;
	}

	// Policy Commenncement Date
	public void policyCommencementDate() throws Exception {
		insuranceFrom_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[5]/div[1]/table/tbody/tr[5]/td[2]")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	// Sum Insured (NZD)
	public void sumInsuredNZD(String sheetName, int rowNum) throws Exception {
		String hullFixtureFittingAndMachineryAccessories = reader.getCellData(sheetName, "Sum Insured", rowNum);
		String trailerNZD = reader.getCellData(sheetName, "Trailer: NZD", rowNum);
		String doYouNeedCoverFishingOrDivingGearBoardYourBoat = reader.getCellData(sheetName,
				"Do you need cover for Fishing or Diving Gear when left on board your boat?", rowNum);
		String typeOfBoat = reader.getCellData(sheetName, "Type of Boat", rowNum);
		String typeOfMooring = reader.getCellData(sheetName, "Type of Mooring", rowNum);

		// Hull, fixtures, fittings and machinery including tender equipment and
		// all other accessories relating to the vessel: NZD $
		fixitureFittingMachineryAcceesoriesVessel_field.clear();
		fixitureFittingMachineryAcceesoriesVessel_field.sendKeys(hullFixtureFittingAndMachineryAccessories, Keys.TAB);
		Thread.sleep(2000);

		try {
			trailerNZD_field.clear();
			trailerNZD_field.sendKeys(trailerNZD);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(2000);
		} catch (Exception e) {
			System.out.println("");
		}

		// Do you need cover for Fishing or Diving Gear when left on board
		// your boat?

		System.out.println("Do you need cover for Fishing or Diving Gear when left on board your boat?"
				+ doYouNeedCoverFishingOrDivingGearBoardYourBoat);

		if ((typeOfBoat.contains("Launch") && typeOfMooring.contains("Dry Stack"))
				|| typeOfBoat.contains("Launch") && typeOfMooring.contains("Pile")
				|| typeOfBoat.contains("Launch") && typeOfMooring.contains("Marina")
				|| typeOfBoat.contains("Launch") && typeOfMooring.contains("Swing")
				|| typeOfBoat.contains("Yacht") && typeOfMooring.contains("Swing")
				|| typeOfBoat.contains("Yacht") && typeOfMooring.contains("Marina")
				|| typeOfBoat.contains("Yacht") && typeOfMooring.contains("Pile")
				|| typeOfBoat.contains("Yacht") && typeOfMooring.contains("Dry Stack")) {
			if (doYouNeedCoverFishingOrDivingGearBoardYourBoat.equalsIgnoreCase("Yes")) {
				driver.findElement(By.xpath("//*[@id='fe_22_1233']/div/span[2]/span/div[1]/label")).click();
				driver.findElement(By.xpath("//*[@id='fe_22_1233']/div/span[2]/span/div[1]/label")).click();
				driver.findElement(By.xpath("//*[@id='fe_22_1233']/div/span[2]/span/div[1]/label")).click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			} else if (doYouNeedCoverFishingOrDivingGearBoardYourBoat.equalsIgnoreCase("No")) {
				driver.findElement(By.xpath("//*[@id='fe_22_1233']/div/span[2]/span/div[2]/label")).click();
				driver.findElement(By.xpath("//*[@id='fe_22_1233']/div/span[2]/span/div[2]/label")).click();
				driver.findElement(By.xpath("//*[@id='fe_22_1233']/div/span[2]/span/div[2]/label")).click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}
		} else {
			System.out.println(doYouNeedCoverFishingOrDivingGearBoardYourBoat + "Field is not There");
		}
	}

	// Vessel Details
	public boolean vesselDetails(String sheetName, int rowNum) {
		String pleaseAdviseTheYearBoatWasOrginallyBuild = reader.getCellData(sheetName,
				"Please advise the year your boat was originally built", rowNum);
		String construction = reader.getCellData(sheetName, "Construction", rowNum);
		// String maxSpeedInKnots = reader.getCellData(sheetName, "Max Speed in
		// Knots", rowNum);
		String boatName = reader.getCellData(sheetName, "Boat Name", rowNum);
		String makeModel = reader.getCellData(sheetName, "MakeModel", rowNum);
		String length = reader.getCellData(sheetName, "Length", rowNum);
		String unit = reader.getCellData(sheetName, "Unit", rowNum);
		String typeOfMotor = reader.getCellData(sheetName, "Type of Motor", rowNum);
		String convertedCarMotor = reader.getCellData(sheetName, "Converted Car Motor", rowNum);
		String hasMotorBeenProfessionallyInstalled = reader.getCellData(sheetName,
				"Has this motor been professionally installed", rowNum);

		// Please advise the year your boat was originally built
		Select drpPleaseAdviseYourBoatOriginallyBuild = new Select(pleaseAdviseTheYearBoatWasOriginallyBuild_field);

		try {
			drpPleaseAdviseYourBoatOriginallyBuild.selectByValue(pleaseAdviseTheYearBoatWasOrginallyBuild);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Verify Declined message over 30 years age for Please advise the
		// year
		// your boat was originally built IHQ-6234

		String boatWasBuildDeclineMessage = "Sorry, but as your vessel is over 30 years of age, we are unable to offer any cover for you.";

		String submitButtonText = "Submit";
		String submitButtonTextXpath = "//button[@class='submit_button nform_btn boots']";
		String submitButtonGetText = driver.findElement(By.xpath(submitButtonTextXpath)).getText();

		if (submitButtonText.compareToIgnoreCase(submitButtonGetText) != 0) {
			System.out.println("Declined Case => Please advise the year your boat was originally built" + " : "
					+ boatWasBuildDeclineMessage);
			ReportsClass.logStat(Status.FATAL, "Declined Case => Please advise the year your boat was originally built"
					+ " : " + boatWasBuildDeclineMessage);
			return false;
		}

		// Construction
		System.out.println("Construction : " + construction);
		Select drpConstruction = new Select(construction_field);

		try {
			drpConstruction.selectByValue(construction);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Boat Name
		System.out.println("Boat Name : " + boatName);
		boatName_field.sendKeys(boatName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Make/Model
		System.out.println("Make / Model : " + makeModel);
		makeModel_field.sendKeys(makeModel);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Length
		System.out.println("Length : " + length);
		length_field.sendKeys(length);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Unit
		System.out.println("Unit : " + unit);
		if (unit.equalsIgnoreCase("Metres")) {
			unitYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} else {
			unitNo_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		// Type of Motor
		System.out.println("Type of Motor : " + typeOfMotor);
		if (typeOfMotor.equalsIgnoreCase("Inboard")) {
			Select drpInboardTypeOfMotor = new Select(typeOfMotor_field);
			try {
				drpInboardTypeOfMotor.selectByValue(typeOfMotor);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}
			// Converted Car Motor?
			if (convertedCarMotor.equalsIgnoreCase("Yes")) {
				convertedCarMotorYes_field.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				// Has this motor been professionally installed
				if (hasMotorBeenProfessionallyInstalled.equalsIgnoreCase("Yes")) {
					hasThisMotorProfessionalInstalledYes_field.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} else {
					hasThisMotorProfessionalInstalledNo_field.click();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				}
			} else {
				convertedCarMotorNo_field.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}

		} else {
			Select drpTypeOfMotor = new Select(typeOfMotor_field);
			try {
				drpTypeOfMotor.selectByValue(typeOfMotor);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("");
			}
		}
		return true;

	}

	// Cover Specifics
	public void coverSpecifics(String sheetName, int rowNum) {
		String isThereAnInterestedPartyInvolved = reader.getCellData(sheetName, "Is there an interested party", rowNum);
		String nameOfInterestedParty = reader.getCellData(sheetName, "Name of interested party", rowNum);
		String doYouNeedToShowAnAdditionalOwnerOnThePolicy = reader.getCellData(sheetName,
				"Do you need to show an additional owner on the policy", rowNum);
		String additionalOwnerName = reader.getCellData(sheetName, "Additional Owner Name", rowNum);
		String previousBoatInsurance = reader.getCellData(sheetName, "Previous Boat Insurance", rowNum);
		String coastguardMember = reader.getCellData(sheetName, "Coastguard Member", rowNum);
		String doYouRaceWithTheUseOfExtra = reader.getCellData(sheetName, "Do you race with the use of extras", rowNum);
		String typeOfBoat = "Yacht";

		System.out.println("Type Of Boat : " + typeOfBoat);

		System.out.println("Do You Race With Use Of Extra : " + doYouRaceWithTheUseOfExtra);

		try {
			if (typeOfBoat.equalsIgnoreCase("Yacht")) {
				if (doYouRaceWithTheUseOfExtra.equalsIgnoreCase("Yes")) {
					doYouRaceWithUseOfExtraYes_field.click();
					doYouRaceWithUseOfExtraYes_field.click();
					doYouRaceWithUseOfExtraYes_field.click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				} else if (doYouRaceWithTheUseOfExtra.equalsIgnoreCase("No")) {
					doYouRaceWithUseOfExtraNo_field.click();
					doYouRaceWithUseOfExtraNo_field.click();
					doYouRaceWithUseOfExtraNo_field.click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				} else {
					System.out.println("Not Found : " + doYouRaceWithTheUseOfExtra);
				}
			}
		} catch (Exception e) {
			System.out.println("");
		}

		// Is there an interested party (any finance) involved?
		if (isThereAnInterestedPartyInvolved.equalsIgnoreCase("Yes")) {
			isThereInterstedPartyInvolvedYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			nameOfnterstedParty_field.sendKeys(nameOfInterestedParty);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} else {
			isThereInterstedPartyInvolvedNo_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		// Do you need to show an additional owner on the policy?
		if (doYouNeedToShowAnAdditionalOwnerOnThePolicy.equalsIgnoreCase("Yes")) {
			doYouNeedAdditionalOwnerPolicyYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			additionalOwnerName_field.sendKeys(additionalOwnerName);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} else {
			doYouNeedAdditionalOwnerPolicyNo_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		// Previous Boat Insurance
		previousBoatInsurance_field.sendKeys(previousBoatInsurance);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// CoastGuard Member
		if (coastguardMember.equalsIgnoreCase("Yes")) {
			coastGuardMasterYes_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			coastGuardDiscount_field.sendKeys(coastguardMember);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} else {
			coastGuardMasterNo_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}

	// Customer Referral Programme
	public void customerReferralProgramme(String sheetName, int rowNum) {
		String howDidYouHearAboutBaileys = reader.getCellData(sheetName, "How did you hear about Baileys?", rowNum);

		Select drpHowDidYouHearBaileys = new Select(howDidYouHearAboutBaileys_field);
		try {
			drpHowDidYouHearBaileys.selectByValue(howDidYouHearAboutBaileys);
		} catch (Exception e) {
			System.out.println("");
		}
	}

	// Declaration
	public void declaration(String sheetName, int rowNum) throws Exception {
		String declarationA = reader.getCellData(sheetName, "Declaration A", rowNum);
		String declarationB = reader.getCellData(sheetName, "Declaration B", rowNum);
		String declarationC = reader.getCellData(sheetName, "Declaration C", rowNum);
		String declarationD = reader.getCellData(sheetName, "Declaration D", rowNum);
		String pleaseProvideDetails = reader.getCellData(sheetName, "Please provide details", rowNum);
		String pleaseAdviseDetails = reader.getCellData(sheetName, "Please advise details", rowNum);
		String pleaseOutlineDetails = reader.getCellData(sheetName, "Please outline details", rowNum);
		String pleaseSpecifyDetails = reader.getCellData(sheetName, "Please specify details", rowNum);

		// Declaration A
		if (declarationA.equalsIgnoreCase("Yes")) {
			driver.findElement(By.xpath("//*[@id='fe_83_1233']/div/span[2]/span/div[1]/label"))
					.sendKeys(pleaseProvideDetails);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} else {
			declarationOption_A_No_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		// Declaration B
		if (declarationB.equalsIgnoreCase("Yes")) {
			driver.findElement(By.xpath("//textarea[@name='Please advise details_para__0___field89_']"))
					.sendKeys(pleaseAdviseDetails);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} else {
			declarationOption_B_No_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		// Declaration C
		if (declarationC.equalsIgnoreCase("Yes")) {
			driver.findElement(By.xpath("//textarea[@name='Please outline details_para__0___field91_']"))
					.sendKeys(pleaseOutlineDetails);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} else {
			declarationOption_C_No_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		// Declaration D
		if (declarationD.equalsIgnoreCase("Yes")) {
			driver.findElement(By.xpath("//textarea[@name='Please specify details_para__0___field93_']"))
					.sendKeys(pleaseSpecifyDetails);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} else {
			declarationOption_D_No_field.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		submitButton_field.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.findElement(
				By.xpath("//div[@class='text-right']//button[@class='btn btn-primary'][contains(text(),'No')]"))
				.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		basicProcessing.addLobDescription_field.sendKeys("New Quote_Policy_Baileys");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		basicProcessing.addLobSaveButton_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(4000);

		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		cpPolicyPage.bindPolicyButton_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.switchTo().alert().accept();
		Thread.sleep(3000);

		// Verify that Policy, Quote is Created successfully or Not
		commonFunction.verifyAddTransactionTypeCreatedSuccessfully("Policy");

		driver.findElement(By.xpath("//button[contains(text(),'Policy Overview')]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(4000);
	}

	// Navigate to IHQ Contact field for Declined case from Additional Fields
	// form. IHQ-6234
	public void navigateToIHQClientForDeclinedCase() throws Exception {
		cancelButton_field.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		Thread.sleep(3000);

		driver.findElement(
				By.xpath("//div[@class='text-right']//button[@class='btn btn-primary'][contains(text(),'No')]"))
				.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);
	}
}