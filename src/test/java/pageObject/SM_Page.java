package pageObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import baseClass.TestBase;
import locators.Locators.carpeeshClientPolicyLocators;
import locators.Locators.seguromukusLocators;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

public class SM_Page extends TestBase implements carpeeshClientPolicyLocators, seguromukusLocators {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String excelPath = readPro.getSMExcel();
	Xls_Reader reader = new Xls_Reader(excelPath);

	// Create new client locators
	@FindBy(xpath = CONTACT_NAV)
	WebElement contactNavigation_field;
	@FindBy(xpath = CLIENT_BUTTON)
	WebElement clientButton_field;
	@FindBy(id = "salutation")
	WebElement salutation_field;
	@FindBy(id = "first_name")
	WebElement firstName_field;
	@FindBy(id = "last_name")
	WebElement lastName_field;
	@FindBy(id = "gender")
	WebElement gender_field;
	@FindBy(id = "client_email")
	WebElement emailAddress_field;
	@FindBy(id = "client_dob")
	WebElement clientDOB_field;
	@FindBy(id = "physical_same_as_mailing_client")
	WebElement mailingAddressSameAsPhysicalAddress_field;
	@FindBy(id = "mailing_address1_client")
	WebElement mailingAddress1_field;
	@FindBy(id = "mobileareacode")
	WebElement mobileNumber_field;
	@FindBy(id = "save_client")
	WebElement saveChangesButton_field;
	@FindBy(xpath = VULNERABLE_PERSON)
	WebElement vulnerablePerson_field;
	@FindBy(xpath = CLIENT_ADDITIONAL_FIELD_SUBMIT_BUTTON)
	WebElement clientAdditionalFieldSubmitButton;
	@FindBy(xpath = NATIONALITY)
	WebElement nationality_field;
	@FindBy(xpath = MARITAL_STATUS_SINGLE)
	WebElement maritalStatusSingle_field;
	@FindBy(xpath = MARITAL_STATUS_MARRIED)
	WebElement maritalMarried_field;
	@FindBy(xpath = MARITAL_STATUS_WINDOWED)
	WebElement maritalStatusWindowed_field;
	@FindBy(id = "date_input_2")
	WebElement additionalFieldBirthday_field;
	@FindBy(xpath = DO_YOU_HAVE_CHILDREN_YES)
	WebElement doYouHaveChildrenYes_field;
	@FindBy(xpath = DO_YOU_HAVE_CHILDREN_NO)
	WebElement doYouHaveChildrenNo_field;
	@FindBy(xpath = ARE_YOU_A_HOME_OWNER_YES)
	WebElement areYouAHomeOwnerYes_field;
	@FindBy(xpath = ARE_YOU_A_HOME_OWNER_NO)
	WebElement areYouAHomeOwnerNo_field;
	@FindBy(xpath = ARE_YOU_A_CAR_OWNER_YES)
	WebElement areYouACarOwnerYes_field;
	@FindBy(xpath = ARE_YOU_A_CAR_OWNER_NO)
	WebElement areYouACarOwnerNo_field;
	@FindBy(xpath = ARE_YOU_AVAILABLE_TO_BE_WHATSAPP)
	WebElement areYouAvailableOnWhatsapp_field;
	@FindBy(xpath = ADDITIONAL_FIELD_SUBMIT_BUTTON)
	WebElement additionalfieldSubmitButton_field;

	// Bind Policy Button
	@FindBy(xpath = BIND_POLICY_BUTTON)
	WebElement bindPolicyButton_field;

	// Driver Details Online Quote Form locators for SM.
	@FindBy(id = "date_input_5")
	WebElement smDateOfBirth_field;
	@FindBy(xpath = DRIVER_LICENCE_NO)
	WebElement driverLicenceNo_field;
	@FindBy(xpath = SM_QUOTE_PHONE)
	WebElement phone_field;
	@FindBy(xpath = CATEGORY)
	WebElement category_field;
	@FindBy(xpath = SM_QUOTE_GENDER_MALE)
	WebElement genderMale_field;
	@FindBy(xpath = SM_QUOTE_GENDER_FEMALE)
	WebElement genderFemale_field;

	// Vehicle Details Online Quote Form locators for SM.
	@FindBy(xpath = SM_MAKE)
	WebElement make_field;
	@FindBy(xpath = SM_MODEL)
	WebElement model_field;
	@FindBy(xpath = SM_CAR_NEW)
	WebElement carNew_field;
	@FindBy(xpath = SM_VEHICLE_TYPE)
	WebElement vehicleType_field;
	@FindBy(xpath = SM_YEAR_OF_MANUFACTURE)
	WebElement yearOfManufacture_field;
	@FindBy(xpath = SM_YEAR_OF_MODEL)
	WebElement yearOfModel_field;
	@FindBy(xpath = SM_REGISTRATION_NUMBER)
	WebElement registrationNumber_field;
	@FindBy(xpath = SM_ENGINE_CHASSIS_NUMBER)
	WebElement engineChassisNumber_field;

	public SM_Page() {
		PageFactory.initElements(driver, this);
	}

	// Create new client for SM
	public void createNewClientForSM(String sheetName, int rowNum) throws Exception {
		String salutation = reader.getCellData(sheetName, "Salutation", rowNum);
		String firstName = reader.getCellData(sheetName, "First Name", rowNum);
		String lastName = reader.getCellData(sheetName, "Last Name", rowNum);
		String gender = reader.getCellData(sheetName, "Gender", rowNum);
		String emailAddress = reader.getCellData(sheetName, "Email_Address", rowNum);
		String clientDOB = reader.getCellData(sheetName, "Date Of Birth", rowNum);
		String mailingAddress = reader.getCellData(sheetName, "Mailing Address", rowNum);
		String mobile = reader.getCellData(sheetName, "Mobile", rowNum);
		String nationality = reader.getCellData(sheetName, "Nationality", rowNum);
		String marritalStatus = reader.getCellData(sheetName, "Marital Status", rowNum);

		String appendFamilyName = new SimpleDateFormat("dd-MM-yyyy HH:mm aaa").format(new Date());

		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		clientButton_field.click();
		Thread.sleep(2000);

		// Create new client

		String sTempSalutation = CommonFunction.capitaliseFirstLetterSingleWord(salutation);
		System.out.println("Salutation : " + sTempSalutation);
		Select drpSalutation = new Select(salutation_field);
		drpSalutation.selectByValue(sTempSalutation);
		firstName_field.sendKeys(firstName);
		lastName_field.sendKeys(lastName + "_" + appendFamilyName, Keys.TAB);
		waitFor1Sec();

		String sTempGender = CommonFunction.capitaliseFirstLetterSingleWord(gender);
		System.out.println("Gender : " + sTempGender);

		Select drpGender = new Select(gender_field);
		drpGender.selectByValue(sTempGender);
		emailAddress_field.sendKeys(emailAddress);
		clientDOB_field.sendKeys(clientDOB, Keys.ESCAPE);
		Thread.sleep(2000);

		mailingAddress1_field.sendKeys(mailingAddress);
		Thread.sleep(2000);
		mailingAddress1_field.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		mailingAddress1_field.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		mailingAddressSameAsPhysicalAddress_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		mobileNumber_field.sendKeys(mobile);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		saveChangesButton_field.click();
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Additional Client Fields

		// Nationality
		Select drpNationality = new Select(nationality_field);
		try {
			drpNationality.selectByValue(nationality);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Marital Status
		if (marritalStatus.equalsIgnoreCase("Single")) {
			maritalStatusSingle_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		} else if (marritalStatus.equalsIgnoreCase("Married")) {
			maritalMarried_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		} else {
			maritalStatusWindowed_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		}

		// Birthdate
		additionalFieldBirthday_field.sendKeys(clientDOB, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Do you have children?
		doYouHaveChildrenNo_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Are you a home owner
		areYouAHomeOwnerNo_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Are you a Car owner
		areYouACarOwnerNo_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		// Are you available to be contacted through Whatsapp?
		areYouAvailableOnWhatsapp_field.click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		additionalfieldSubmitButton_field.click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		Thread.sleep(4000);
	}

	// Driver Details
	public void driverDetails_SM(String sheetName, int rowNum) throws Exception {
		String dob = reader.getCellData(sheetName, "DOB", rowNum);
		String driverLicenceNo = reader.getCellData(sheetName, "Driver Licence No", rowNum);
		String phone = reader.getCellData(sheetName, "Phone", rowNum);
		String gender = reader.getCellData(sheetName, "Gender", rowNum);
		String category = reader.getCellData(sheetName, "Category", rowNum);

		// Date Of Birth
		driver.findElement(By.id("date_input_5")).click();
		Thread.sleep(1000);
		smDateOfBirth_field.sendKeys(dob, Keys.ESCAPE);
		Thread.sleep(1000);
		driverLicenceNo_field.sendKeys(driverLicenceNo);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Date of issue
		java.util.Date date = new java.util.Date(System.currentTimeMillis());
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String getCurrentDate = df.format(date);
		System.out.println("Current Date : " + getCurrentDate);
		driver.findElement(By.id("date_input_8")).sendKeys(getCurrentDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Date of Expiry
		LocalDate mydate = LocalDate.now();
		mydate = mydate.plusMonths(1);
		String nextMonthDate = mydate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
		System.out.println("Next Month Date : " + nextMonthDate);
		driver.findElement(By.id("date_input_10")).sendKeys(nextMonthDate, Keys.ESCAPE);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Phone
		phone_field.click();
		phone_field.clear();
		phone_field.sendKeys(phone);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Gender
		if (gender.equalsIgnoreCase("Male")) {
			genderMale_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else {
			genderFemale_field.click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}

		// Category
		category_field.sendKeys(category);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	// Vehicle Details
	public void vehicleDetails_SM(String sheetName, int rowNum) throws Exception {
		String make = reader.getCellData(sheetName, "Make", rowNum);
		String model = reader.getCellData(sheetName, "Model", rowNum);
		String vehicleType = reader.getCellData(sheetName, "Vehicle Type", rowNum);
		String yearOfManufacture = reader.getCellData(sheetName, "Year of Manufacture", rowNum);
		String yearOfModel = reader.getCellData(sheetName, "Year Of Model", rowNum);
		String registrationNumber = reader.getCellData(sheetName, "Registration Number", rowNum);
		String engineChassisNumber = reader.getCellData(sheetName, "Engine/Chassis Number", rowNum);

		// Make
		Select drpMake = new Select(make_field);
		try {
			drpMake.selectByValue(make);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Car New/Used
		carNew_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Model
		Select drpModel = new Select(model_field);
		try {
			drpModel.selectByValue(model);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		// Vehicle Type
		Select drpVehicleType = new Select(vehicleType_field);
		try {
			drpVehicleType.selectByValue(vehicleType);
		} catch (Exception e) {
			System.out.println("");
		}

		// Year of Manufacture
		yearOfManufacture_field.sendKeys(yearOfManufacture);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Year of Model
		Select drpYearOfModel = new Select(yearOfModel_field);
		try {
			drpYearOfModel.selectByValue(yearOfModel);
		} catch (Exception e) {
			System.out.println("");
		}

		// Registration Number
		registrationNumber_field.sendKeys(registrationNumber);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// Engine / Chassis Number
		engineChassisNumber_field.sendKeys(engineChassisNumber);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
	}

	// Premium Summary
	public void premiumSummary_SM(String sheetName, int rowNum) {

		// Premium Summary Details
		String conversationRate = reader.getCellData(sheetName, "Conversion Rate", rowNum);
		String catalogueValueUSD = reader.getCellData(sheetName, "Catalogue value (USD)", rowNum);
		String catalogueValueNalfs = reader.getCellData(sheetName, "Catalogue value (Nalfs)", rowNum);
		String catalogueValueNalfsInWords = reader.getCellData(sheetName, "Catalogue value (Nalfs) in words", rowNum);
		String excess = reader.getCellData(sheetName, "Excess", rowNum);

		DecimalFormat df = new DecimalFormat("#.##");
		df.setMaximumFractionDigits(2);

		// Verify Premium Summary
		// Verify Conversation Rate
		String conversation_Rate = driver.findElement(By.xpath("//input[@name='Conversion Rate_text__0___field28_']"))
				.getAttribute("value");

		Double formValueConversationRate = Double.parseDouble(df.format(Double.parseDouble(conversation_Rate)));
		Double xLSValueConversationRate = Double.parseDouble(df.format(Double.parseDouble(conversationRate)));

		if (Double.compare(formValueConversationRate, xLSValueConversationRate) == 0) {
			System.out.println("Conversation Rate Matched : " + "" + "Form Value : " + formValueConversationRate + ", "
					+ " " + " Excel Value : " + "" + xLSValueConversationRate);
			ReportsClass.logStat(Status.PASS, "Conversation Rate is Matched : " + "" + "Form Value : "
					+ formValueConversationRate + ", " + " " + " Excel Value : " + "" + xLSValueConversationRate);
		} else {
			System.out.println("Conversation Rate is Not Matched : " + "" + "Form Value : " + formValueConversationRate
					+ ", " + " " + " Excel Value : " + "" + xLSValueConversationRate);
			ReportsClass.logStat(Status.FAIL, "Conversation Rate is Not Matched : " + "" + "Form Value : "
					+ formValueConversationRate + ", " + " " + " Excel Value : " + "" + xLSValueConversationRate);
		}

		// Verify Catalogue value (USD)
		String catalogueValue_USD = driver
				.findElement(By.xpath("//input[@name='Catalogue value (USD)_text__1___field30_']"))
				.getAttribute("value");

		Double formValueCatalogueValueUSD = Double.parseDouble(df.format(Double.parseDouble(catalogueValue_USD)));
		Double xlsValueCatalogueValueUSD = Double.parseDouble(df.format(Double.parseDouble(catalogueValueUSD)));

		if (Double.compare(formValueCatalogueValueUSD, xlsValueCatalogueValueUSD) == 0) {
			System.out.println("Catalogue Value (USD) is Matched : " + "" + "Form Value : " + formValueCatalogueValueUSD
					+ ", " + " " + " Excel Value : " + "" + xlsValueCatalogueValueUSD);
			ReportsClass.logStat(Status.PASS, "Catalogue Value (USD) is Matched : " + "" + "Form Value : "
					+ formValueCatalogueValueUSD + ", " + " " + " Excel Value : " + "" + xlsValueCatalogueValueUSD);
		} else {
			System.out.println("Catalogue Value (USD) is Not Matched : " + "" + "Form Value : "
					+ formValueCatalogueValueUSD + ", " + " " + " Excel Value : " + "" + xlsValueCatalogueValueUSD);
			ReportsClass.logStat(Status.FAIL, "Catalogue Value (USD) is Not Matched : " + "" + "Form Value : "
					+ formValueCatalogueValueUSD + ", " + " " + " Excel Value : " + "" + xlsValueCatalogueValueUSD);
		}

		// Verify Catalogue Value (Nalfs)
		String catelogueValue_Nalfs = driver
				.findElement(By.xpath("//input[@name='Catalogue value (Nalfs)_text__1___field31_']"))
				.getAttribute("value");

		Double formValueCatelogueValueNAFLS = Double.parseDouble(df.format(Double.parseDouble(catelogueValue_Nalfs)));
		Double xlsValueCatelogueValueNAFLS = Double.parseDouble(df.format(Double.parseDouble(catalogueValueNalfs)));

		if (Double.compare(formValueCatelogueValueNAFLS, xlsValueCatelogueValueNAFLS) == 0) {
			System.out.println("Catalogue Value (Nalfs) is Matched : " + "" + "Form Value : "
					+ formValueCatelogueValueNAFLS + ", " + " " + " Excel Value : " + "" + xlsValueCatelogueValueNAFLS);
			ReportsClass.logStat(Status.PASS, "Catalogue Value (Nalfs) is Matched : " + "" + "Form Value : "
					+ formValueCatelogueValueNAFLS + ", " + " " + " Excel Value : " + "" + xlsValueCatelogueValueNAFLS);
		} else {
			System.out.println("Catalogue Value (Nalfs) is Not Matched : " + "" + "Form Value : "
					+ formValueCatelogueValueNAFLS + ", " + " " + " Excel Value : " + "" + xlsValueCatelogueValueNAFLS);
			ReportsClass.logStat(Status.FAIL, "Catalogue Value (Nalfs) is Not Matched : " + "" + "Form Value : "
					+ formValueCatelogueValueNAFLS + ", " + " " + " Excel Value : " + "" + xlsValueCatelogueValueNAFLS);
		}

		// Catalogue value (Nalfs) in words
		String getCatelogueValueInWords = driver
				.findElement(By.xpath("//input[@name='Catalogue value (Nalfs) in words_text__true___field32_']"))
				.getAttribute("value");

		if (getCatelogueValueInWords.equalsIgnoreCase(catalogueValueNalfsInWords)) {
			System.out.println("Catelogue Value (Nalfs) In Words value is Matched : " + "" + "Form Value : "
					+ getCatelogueValueInWords + ", " + " " + " Excel Value : " + "" + catalogueValueNalfsInWords);
			ReportsClass.logStat(Status.PASS,
					"Catelogue Value (Nalfs) In Words value is Matched : " + "" + "Form Value : "
							+ getCatelogueValueInWords + ", " + " " + " Excel Value : " + ""
							+ catalogueValueNalfsInWords);
		} else {
			System.out.println("Catelogue Value (Nalfs) In Words value is Not Matched : " + "" + "Form Value : "
					+ getCatelogueValueInWords + ", " + " " + " Excel Value : " + "" + catalogueValueNalfsInWords);
			ReportsClass.logStat(Status.FAIL,
					"Catelogue Value (Nalfs) In Words value is Not Matched : " + "" + "Form Value : "
							+ getCatelogueValueInWords + ", " + " " + " Excel Value : " + ""
							+ catalogueValueNalfsInWords);
		}

		// Verify Excess
		String excessVerify = driver.findElement(By.xpath("//input[@name='Excess_text__0___field33_']"))
				.getAttribute("value");

		Double formValueExcess = Double.parseDouble(df.format(Double.parseDouble(excessVerify)));
		Double xlsValueExcess = Double.parseDouble(df.format(Double.parseDouble(excess)));

		if (Double.compare(formValueExcess, xlsValueExcess) == 0) {
			System.out.println("Excess Value is Matched : " + "" + "Form Value : " + formValueExcess + ", " + " "
					+ " Excel Value : " + "" + xlsValueExcess);
			ReportsClass.logStat(Status.PASS, "Excess Value is Matched : " + "" + "Form Value : " + formValueExcess
					+ ", " + " " + " Excel Value : " + "" + xlsValueExcess);
		} else {
			System.out.println("Excess Value is Not Matched : " + "" + "Form Value : " + formValueExcess + ", " + " "
					+ " Excel Value : " + "" + xlsValueExcess);
			ReportsClass.logStat(Status.FAIL, "Excess Value is Not Matched : " + "" + "Form Value : " + formValueExcess
					+ ", " + " " + " Excel Value : " + "" + xlsValueExcess);
		}
	}

	// Premium / Tax / Levy Summary
	public void premiumTaxLevySummary_SM(String sheetName, int rowNum) {

		String totalPremium = reader.getCellData(sheetName, "Total Premium", rowNum);
		String proRatePremium = reader.getCellData(sheetName, "Pro-rata premium", rowNum);
		String crs = reader.getCellData(sheetName, "CRS", rowNum);
		String documentCost = reader.getCellData(sheetName, "Document Cost", rowNum);
		String adminFee = reader.getCellData(sheetName, "Admin Fee", rowNum);
		String totalFixedAdditions = reader.getCellData(sheetName, "Total Fixed Additions", rowNum);
		String totalDB = reader.getCellData(sheetName, "Total O.B.", rowNum);

		DecimalFormat df = new DecimalFormat("#.##");
		df.setMaximumFractionDigits(2);

		// Verify Total Premium

		String getTotalPremium = driver.findElement(By.xpath("//input[@name='Total Premium_text__0___field86_']"))
				.getAttribute("value");

		Double formValueTotalPremium = Double.parseDouble(df.format(Double.parseDouble(getTotalPremium)));
		Double xlsValueTotalPremium = Double.parseDouble(df.format(Double.parseDouble(totalPremium)));

		if (Double.compare(formValueTotalPremium, xlsValueTotalPremium) == 0) {
			System.out.println("Total Premium is Matched : " + "" + "Form Value : " + formValueTotalPremium + ", " + " "
					+ " Excel Value : " + "" + xlsValueTotalPremium);
			ReportsClass.logStat(Status.PASS, "Total Premium is Matched : " + "" + "Form Value : "
					+ formValueTotalPremium + ", " + " " + " Excel Value : " + "" + xlsValueTotalPremium);
		} else {
			System.out.println("Total Premium is Not Matched : " + "" + "Form Value : " + formValueTotalPremium + ", "
					+ " " + " Excel Value : " + "" + xlsValueTotalPremium);
			ReportsClass.logStat(Status.FAIL, "Total Premium is Not Matched : " + "" + "Form Value : "
					+ formValueTotalPremium + ", " + " " + " Excel Value : " + "" + xlsValueTotalPremium);
		}

		// Verify Pro-rata Premium
		String getProRatePremium = driver.findElement(By.xpath("//input[@name='Pro-rata Premium_text__0___field87_']"))
				.getAttribute("value");

		Double formValueProRatePremium = Double.parseDouble(df.format(Double.parseDouble(getProRatePremium)));
		Double xLSValueProRatePremium = Double.parseDouble(df.format(Double.parseDouble(proRatePremium)));

		if (Double.compare(formValueProRatePremium, xLSValueProRatePremium) == 0) {
			System.out.println("Pro-rate Premium is Matched : " + "" + "Form Value : " + formValueProRatePremium + ", "
					+ " " + " Excel Value : " + "" + xLSValueProRatePremium);
			ReportsClass.logStat(Status.PASS, "Pro-rata Premium is Matched : " + "" + "Form Value : "
					+ formValueProRatePremium + ", " + " " + " Excel Value : " + "" + xLSValueProRatePremium);
		} else {
			System.out.println("Pro-rate Premium is Not Matched : " + "" + "Form Value : " + formValueProRatePremium
					+ ", " + " " + " Excel Value : " + "" + xLSValueProRatePremium);
			ReportsClass.logStat(Status.FAIL, "Pro-rata Premium is Not Matched : " + "" + "Form Value : "
					+ formValueProRatePremium + ", " + " " + " Excel Value : " + "" + xLSValueProRatePremium);
		}

		// Verify CRS
		String getCRS = driver.findElement(By.xpath("//input[@name='CRS_text__0___field88_']")).getAttribute("value");

		Double formCRS = Double.parseDouble(df.format(Double.parseDouble(getCRS)));
		Double xlsCRS = Double.parseDouble(df.format(Double.parseDouble(crs)));

		if (Double.compare(formCRS, xlsCRS) == 0) {
			System.out.println("CRS is Matched : " + "" + "Form Value : " + formCRS + ", " + " " + " Excel Value : "
					+ "" + xlsCRS);
			ReportsClass.logStat(Status.PASS, "CRS Premium is Matched : " + "" + "Form Value : " + formCRS + ", " + " "
					+ " Excel Value : " + "" + xlsCRS);
		} else {
			System.out.println("CRS is Not Matched : " + "" + "Form Value : " + formCRS + ", " + " " + " Excel Value : "
					+ "" + xlsCRS);
			ReportsClass.logStat(Status.FAIL, "CRS Premium is Not Matched : " + "" + "Form Value : " + formCRS + ", "
					+ " " + " Excel Value : " + "" + xlsCRS);
		}

		// Verify Document Cost
		String getDocumentCost = driver.findElement(By.xpath("//input[@name='Document Cost_text__0___field89_']"))
				.getAttribute("value");

		Double formDocumentCost = Double.parseDouble(df.format(Double.parseDouble(getDocumentCost)));
		Double xlsDocumentCost = Double.parseDouble(df.format(Double.parseDouble(documentCost)));

		if (Double.compare(formDocumentCost, xlsDocumentCost) == 0) {
			System.out.println("Document Cost is Matched : " + "" + "Form Value : " + formDocumentCost + ", " + " "
					+ " Excel Value : " + "" + xlsDocumentCost);
			ReportsClass.logStat(Status.PASS, "Document Cost is Matched : " + "" + "Form Value : " + formDocumentCost
					+ ", " + " " + " Excel Value : " + "" + xlsDocumentCost);
		} else {
			System.out.println("Document Cost is Not Matched : " + "" + "Form Value : " + formDocumentCost + ", " + " "
					+ " Excel Value : " + "" + xlsDocumentCost);
			ReportsClass.logStat(Status.FAIL, "Document Cost is Not Matched : " + "" + "Form Value : "
					+ formDocumentCost + ", " + " " + " Excel Value : " + "" + xlsDocumentCost);
		}

		// Verify Admin Fee
		String getAdminFee = driver.findElement(By.xpath("//input[@name='Admin Fee_text__0___field90_']"))
				.getAttribute("value");

		Double formAdminFee = Double.parseDouble(df.format(Double.parseDouble(getAdminFee)));
		Double xlsAdminFee = Double.parseDouble(df.format(Double.parseDouble(adminFee)));

		if (Double.compare(formAdminFee, xlsAdminFee) == 0) {
			System.out.println("Admin Fee is Matched : " + "" + "Form Value : " + formAdminFee + ", " + " "
					+ " Excel Value : " + "" + xlsAdminFee);
			ReportsClass.logStat(Status.PASS, "Admin Fee is Matched : " + "" + "Form Value : " + formAdminFee + ", "
					+ " " + " Excel Value : " + "" + xlsAdminFee);
		} else {
			System.out.println("Admin Fee is Not Matched : " + "" + "Form Value : " + formAdminFee + ", " + " "
					+ " Excel Value : " + "" + xlsAdminFee);
			ReportsClass.logStat(Status.FAIL, "Admin Fee is Not Matched : " + "" + "Form Value : " + formAdminFee + ", "
					+ " " + " Excel Value : " + "" + xlsAdminFee);
		}

		// Verify Total Fixed Additions
		String getTotalFixedAdditions = driver
				.findElement(By.xpath("//input[@name='Total Fixed Additions_text__0___field91_']"))
				.getAttribute("value");

		Double formTotalFixedAddition = Double.parseDouble(df.format(Double.parseDouble(getTotalFixedAdditions)));
		Double xlsTotalFixedAddition = Double.parseDouble(df.format(Double.parseDouble(totalFixedAdditions)));

		if (Double.compare(formTotalFixedAddition, xlsTotalFixedAddition) == 0) {
			System.out.println("Total Fixed Addition is Matched : " + "" + "Form Value : " + formTotalFixedAddition
					+ ", " + " " + " Excel Value : " + "" + xlsTotalFixedAddition);
			ReportsClass.logStat(Status.PASS, "Total Fixed Addition is Matched : " + "" + "Form Value : "
					+ formTotalFixedAddition + ", " + " " + " Excel Value : " + "" + xlsTotalFixedAddition);
		} else {
			System.out.println("Total Fixed Addition is Not Matched : " + "" + "Form Value : " + formTotalFixedAddition
					+ ", " + " " + " Excel Value : " + "" + xlsTotalFixedAddition);
			ReportsClass.logStat(Status.FAIL, "Total Fixed Addition is Not Matched : " + "" + "Form Value : "
					+ formTotalFixedAddition + ", " + " " + " Excel Value : " + "" + xlsTotalFixedAddition);
		}

		// Verify Total O.B.
		String getTotalOB = driver.findElement(By.xpath("//input[@name='Total O.B._text__0___field92_']"))
				.getAttribute("value");

		Double formTotalOB = Double.parseDouble(df.format(Double.parseDouble(getTotalOB)));
		Double xlsTotalOB = Double.parseDouble(df.format(Double.parseDouble(totalDB)));

		if (Double.compare(formTotalOB, xlsTotalOB) == 0) {
			System.out.println("Total O.B. is Matched : " + "" + "Form Value : " + formTotalOB + ", " + " "
					+ " Excel Value : " + "" + xlsTotalOB);
			ReportsClass.logStat(Status.PASS, "Total O.B. is Matched : " + "" + "Form Value : " + formTotalOB + ", "
					+ " " + " Excel Value : " + "" + xlsTotalOB);
		} else {
			System.out.println("Total O.B. is Not Matched : " + "" + "Form Value : " + formTotalOB + ", " + " "
					+ " Excel Value : " + "" + xlsTotalOB);
			ReportsClass.logStat(Status.FAIL, "Total O.B. is Not Matched : " + "" + "Form Value : " + formTotalOB + ", "
					+ " " + " Excel Value : " + "" + xlsTotalOB);
		}
	}

	// Grand Total
	public void grandTotal_SM(String sheetName, int rowNum) throws Exception {
		String stampDuty = reader.getCellData(sheetName, "Stamp Duty", rowNum);
		String totalPremiumTaxesLevies = reader.getCellData(sheetName, "Total Premium inc Taxes/Levies", rowNum);
		String totalInWords = reader.getCellData(sheetName, "Total in words", rowNum);

		DecimalFormat df = new DecimalFormat("#.##");
		df.setMaximumFractionDigits(2);

		// Verify Stamp Duty
		String getStampDuty = driver.findElement(By.xpath("//input[@name='Stamp Duty_text__0___field101_']"))
				.getAttribute("value");

		Double formStampDuty = Double.parseDouble(df.format(Double.parseDouble(getStampDuty)));
		Double xlsStampDuty = Double.parseDouble(df.format(Double.parseDouble(stampDuty)));

		if (Double.compare(formStampDuty, xlsStampDuty) == 0) {
			System.out.println("Stamp Duty is Matched : " + "" + "Form Value : " + formStampDuty + ", " + " "
					+ " Excel Value : " + "" + xlsStampDuty);
			ReportsClass.logStat(Status.PASS, "Stamp Duty is Matched : " + "" + "Form Value : " + formStampDuty + ", "
					+ " " + " Excel Value : " + "" + xlsStampDuty);
		} else {
			System.out.println("Stamp Duty is Not Matched : " + "" + "Form Value : " + formStampDuty + ", " + " "
					+ " Excel Value : " + "" + xlsStampDuty);
			ReportsClass.logStat(Status.FAIL, "Stamp Duty is Not Matched : " + "" + "Form Value : " + formStampDuty
					+ ", " + " " + " Excel Value : " + "" + xlsStampDuty);
		}

		// Verify Total Premium inc Taxes/Levies
		String getTotalPremiumIncTax = driver
				.findElement(By.xpath("//input[@name='Total Premium inc Taxes/Levies_text__0___field102_']"))
				.getAttribute("value");

		Double formValueGetTotalPremimIncTax = Double.parseDouble(df.format(Double.parseDouble(getTotalPremiumIncTax)));
		Double xlsValueGetTotalPremimIncTax = Double
				.parseDouble(df.format(Double.parseDouble(totalPremiumTaxesLevies)));

		if (Double.compare(formValueGetTotalPremimIncTax, xlsValueGetTotalPremimIncTax) == 0) {
			System.out.println("Total Premium inc Taxes/Levies is Matched : " + "" + "Form Value : "
					+ formValueGetTotalPremimIncTax + ", " + " " + " Excel Value : " + ""
					+ xlsValueGetTotalPremimIncTax);
			ReportsClass.logStat(Status.PASS,
					"Total Premium inc Taxes/Levies is Matched : " + "" + "Form Value : "
							+ formValueGetTotalPremimIncTax + ", " + " " + " Excel Value : " + ""
							+ xlsValueGetTotalPremimIncTax);
		} else {
			System.out.println("Total Premium inc Taxes/Levies is Not Matched : " + "" + "Form Value : "
					+ formValueGetTotalPremimIncTax + ", " + " " + " Excel Value : " + ""
					+ xlsValueGetTotalPremimIncTax);
			ReportsClass.logStat(Status.FAIL,
					"Total Premium inc Taxes/Levies is Not Matched : " + "" + "Form Value : "
							+ formValueGetTotalPremimIncTax + ", " + " " + " Excel Value : " + ""
							+ xlsValueGetTotalPremimIncTax);
		}

		// Verify Total in words
		String getTotalWords = driver.findElement(By.xpath("//input[@name='Total in words_text__0___field103_']"))
				.getAttribute("value");

		if (getTotalWords.equalsIgnoreCase(totalInWords)) {
			System.out.println("Total in words is Matched : " + "" + "Form Value : " + getTotalWords + ", " + " "
					+ " Excel Value : " + "" + totalInWords);
			ReportsClass.logStat(Status.PASS, "Total in words is Matched : " + "" + "Form Value : " + getTotalWords
					+ ", " + " " + " Excel Value : " + "" + totalInWords);
		} else {
			System.out.println("Total in words is Not Matched : " + "" + "Form Value : " + getTotalWords + ", " + " "
					+ " Excel Value : " + "" + totalInWords);
			ReportsClass.logStat(Status.FAIL, "Total in words is Not Matched : " + "" + "Form Value : " + getTotalWords
					+ ", " + " " + " Excel Value : " + "" + totalInWords);
		}

		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@class='submit_button nform_btn boots']")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(4000);
	}

	// Verify Lob Summary from IHQ
	public void verifyLOBSummaryIHQ(String sheetName, int rowNum) throws Exception {
		String premiumPreDiscount = reader.getCellData(sheetName, "Pro-rata premium", rowNum);
		String premium = reader.getCellData(sheetName, "Total Premium", rowNum);
		String premiumIncludingTaxesAndFee = reader.getCellData(sheetName, "Total Premium inc Taxes/Levies", rowNum);

		DecimalFormat df = new DecimalFormat("#.##");
		df.setMaximumFractionDigits(2);

		// Verify Premium pre-discount

		String getPremiumPreDiscount2 = driver
				.findElement(By.xpath("//*[@id='lob_summery']/div/table/tbody/tr[2]/td[3]")).getText();
		String strPremimDiscount[] = getPremiumPreDiscount2.split(" ");
		String premiumDiscountValue = strPremimDiscount[1];

		String actualPremiumDiscount = premiumDiscountValue.replace(",", "");

		Double formValuePremiumDiscount = Double.parseDouble(df.format(Double.parseDouble(actualPremiumDiscount)));
		Double xlsValuePremiumDiscount = Double.parseDouble(df.format(Double.parseDouble(premiumPreDiscount)));

		if (Double.compare(formValuePremiumDiscount, xlsValuePremiumDiscount) == 0) {
			System.out.println("Premium Pre-Discount is Matched from IHQ : " + "" + "Form Value : "
					+ formValuePremiumDiscount + ", " + " " + " Excel Value : " + "" + xlsValuePremiumDiscount);
			ReportsClass.logStat(Status.PASS, "Premium Pre-Discount is Matched from IHQ : " + "" + "Form Value : "
					+ formValuePremiumDiscount + ", " + " " + " Excel Value : " + "" + xlsValuePremiumDiscount);
		} else {
			System.out.println("Premium Pre-Discount is Not Matched from IHQ : " + "" + "Form Value : "
					+ formValuePremiumDiscount + ", " + " " + " Excel Value : " + "" + xlsValuePremiumDiscount);
			ReportsClass.logStat(Status.FAIL, "Premium Pre-Discount is Not Matched from IHQ : " + "" + "Form Value : "
					+ formValuePremiumDiscount + ", " + " " + " Excel Value : " + "" + xlsValuePremiumDiscount);
		}

		// Premium
		String getPremium = driver.findElement(By.xpath("//*[@id='lob_summery']/div/table/tbody/tr[4]/td[3]"))
				.getText();

		String strPremium[] = getPremium.split(" ");
		String premiumValue = strPremium[1];

		String actualPremium = premium.replace(",", "");

		Double formValuePremium = Double.parseDouble(df.format(Double.parseDouble(actualPremium)));
		Double xlsValuePremium = Double.parseDouble(df.format(Double.parseDouble(premium)));

		if (Double.compare(formValuePremium, xlsValuePremium) == 0) {
			System.out.println("Premium is Matched from IHQ : " + "" + "Form Value : " + formValuePremium + ", " + " "
					+ " Excel Value : " + "" + xlsValuePremium);
			ReportsClass.logStat(Status.PASS, "Premium is Matched from IHQ : " + "" + "Form Value : " + formValuePremium
					+ ", " + " " + " Excel Value : " + "" + xlsValuePremium);
		} else {
			System.out.println("Premium is Not Matched from IHQ : " + "" + "Form Value : " + formValuePremium + ", "
					+ " " + " Excel Value : " + "" + xlsValuePremium);
			ReportsClass.logStat(Status.FAIL, "Premium is Not Matched from IHQ : " + "" + "Form Value : "
					+ formValuePremium + ", " + " " + " Excel Value : " + "" + xlsValuePremium);
		}

		// Premium Including taxes and fee
		String getPremiumTaxFee = driver
				.findElement(By.xpath("//*[@id='lob_summery']/div/table/tbody/tr[10]/td[3]/strong")).getText();

		String strPremiumTaxFee[] = getPremiumTaxFee.split(" ");
		String premiumTaxesFee = strPremiumTaxFee[1];

		String actualPremiumTaxesFee = premiumTaxesFee.replace(",", "");

		Double formValuePremiumTaxFee = Double.parseDouble(df.format(Double.parseDouble(actualPremiumTaxesFee)));
		Double xlsValuePremiumTaxFee = Double.parseDouble(df.format(Double.parseDouble(premiumIncludingTaxesAndFee)));

		if (Double.compare(formValuePremiumTaxFee, xlsValuePremiumTaxFee) == 0) {
			System.out.println("Premium including taxes and fee is Matched from IHQ : " + "" + "Form Value : "
					+ formValuePremiumTaxFee + ", " + " " + " Excel Value : " + "" + xlsValuePremiumTaxFee);
			ReportsClass.logStat(Status.PASS,
					"Premium including taxes and fee is Matched from IHQ : " + "" + "Form Value : "
							+ formValuePremiumTaxFee + ", " + " " + " Excel Value : " + "" + xlsValuePremiumTaxFee);
		} else {
			System.out.println("Premium including taxes and fee is Not Matched from IHQ : " + "" + "Form Value : "
					+ formValuePremiumTaxFee + ", " + " " + " Excel Value : " + "" + xlsValuePremiumTaxFee);
			ReportsClass.logStat(Status.FAIL,
					"Premium including taxes and fee is Not Matched from IHQ : " + "" + "Form Value : "
							+ formValuePremiumTaxFee + ", " + " " + " Excel Value : " + "" + xlsValuePremiumTaxFee);
		}

		driver.findElement(By.xpath("//button[@id='save_lob_transaction']")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);
		for (int i = 1; i <= 2; i++) {
			bindPolicyButton_field.click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Thread.sleep(2000);
		}
		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);
	}
}