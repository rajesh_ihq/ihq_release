package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import locators.Locators.basicProcessingLocators;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Create_A_New_Insurer_Quote extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String policyClass = "16";
	String sumInsured = "5000";
	String totalExcess = "100";
	String dev_se_Client = "26";
	String creditorName = "131";
	String brokerage = "10";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Create_A_New_Insurer_Quote_TC_12");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void create_A_New_Insurer_Quote() throws Exception {
		ReportsClass.initialisation("Create_A_New_Insurer_Quote_TC_12");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Create New Insurer Quote
		basicProcessing.createNewInsurerQuote(clientName, policyClass, sumInsured, totalExcess, creditorName,
				brokerage);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Create_A_New_Insurer_Quote_TC_12");
		closeBrowser();
	}
}
