package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Creditor_Add_Manual_Invoice extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String creditorName = "Gray ReInsurance";
	String transactionType = readPro.getInvoiceTransactionType();
	String currency = readPro.getInvoiceCurrency();
	String branch = readPro.getInvoiceBranch();
	String detail = readPro.getInvoiceDetail();
	String description = readPro.getInvoiceDescription();
	String exclusiveAmount = readPro.getInvoiceExclusiveAmount();
	String creditor = "Creditor";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Creditor_Add_Manual_Invoice_TC_25");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void creditor_Add_Manual_Invoice() throws Exception {
		ReportsClass.initialisation("Creditor_Add_Manual_Invoice_TC_25");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Create a Manual Invoice for Creditor
		basicProcessing.createManualInvoice(creditor, creditorName, transactionType, currency, branch, detail,
				description, exclusiveAmount);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Creditor_Add_Manual_Invoice_TC_25");
		closeBrowser();
	}
}
