package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Renew_Policy extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String clearClientTransactionURL = readPro.getClearClientTransactionDataURL();
	String key = readPro.getClearClientKey();
	String sheetName = readPro.getSheetOne();
	String type = "Policy";
	String convertPolicy = "Renew";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Renew_Policy_TC_15");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void renew_Policy() throws Exception {

		ReportsClass.initialisation("Renew_Policy_TC_15");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Renew a Policy
		basicProcessing.convertPolicy(convertPolicy, "50000", "500");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Renew_Policy_TC_15");
		closeBrowser();
	}
}
