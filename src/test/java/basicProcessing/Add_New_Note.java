package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Add_New_Note extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getClientName();
	String subject = readPro.getTaskSubject();
	String noteSection = readPro.getNoteSection();
	String description = readPro.getTaskDescription();
	String taskOrNote = "Note";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Add_New_Note_TC_05");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void add_New_Note() throws Exception {
		ReportsClass.initialisation("Add_New_Note_TC_05");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Add New Task
		basicProcessing.addNewTaskNotes(taskOrNote, clientName, null, subject, noteSection, description);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Add_New_Note_TC_05");
		closeBrowser();
	}
}
