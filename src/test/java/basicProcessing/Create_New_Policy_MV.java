package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Create_New_Policy_MV extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String sumInsured = "50000.00";
	String futureAnnualPremier = "1000.00";
	String policyClass = "16";
	String type = "Policy";
	String sheetName = readPro.getSheetOne();
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: New_Policy_MV_TC_08");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void new_Policy_MV() throws Exception {
		ReportsClass.initialisation("New_Policy_MV_TC_08");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Create New Policy
		basicProcessing.createNewPolicy_Quote_Legacy_CoverNote(type, clientName, policyClass, null, sumInsured,
				futureAnnualPremier, sheetName, 2);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: New_Policy_MV_TC_08");
		closeBrowser();
	}
}