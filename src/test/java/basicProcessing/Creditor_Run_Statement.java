package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Creditor_Run_Statement extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String creditorName = "Tribute Insurance";
	String runStatementFor = "Creditor";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Creditor_Run_Statement_TC_24");

		// Launch MasterQA URL
		openBrowser(masterQAURL);

	}

	@Test
	public void creditor_Run_Statement() throws Exception {
		ReportsClass.initialisation("Creditor_Run_Statement_TC_24");
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
		CommonFunction commonFunction = new CommonFunction();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Creditor Run Statement
		basicProcessing.runStatement(runStatementFor, creditorName);

	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Creditor_Run_Statement_TC_24");
		closeBrowser();
	}
}
