package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Refund_Payment extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String paymentAmount = readPro.getRefundPaymentAmount();
	String paymentType = readPro.getRefundPaymentType();
	String department = readPro.getDepartment();
	String reference = readPro.getRefundReference();
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Refund_Payment_TC_20");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void refund_Payment() throws Exception {
		ReportsClass.initialisation("Refund_Payment_TC_20");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Refund Payment
		basicProcessing.refundPayment(clientName, paymentAmount, paymentType, department, reference);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Refund_Payment_TC_20");
		closeBrowser();
	}
}
