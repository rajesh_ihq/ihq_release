package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Client_Run_Statement extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();

	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = "John Whitney";
	String runStatementFor = "Client";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Run_Statement_TC_19");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void runStatement() throws Exception {
		ReportsClass.initialisation("Run_Statement_TC_19");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Client Run Statement
		basicProcessing.runStatement(runStatementFor, clientName);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Run_Statement_TC_19");
		closeBrowser();
	}
}
