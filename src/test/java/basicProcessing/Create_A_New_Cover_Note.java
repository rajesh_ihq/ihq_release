package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Create_A_New_Cover_Note extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String type = "CoverNote";
	String policyClass = "16";
	String townOrVillage = "Sydney";
	String sumInsured = "50000.00";
	String futureAnnualPremier = "1000.00";
	String sheetName = readPro.getSheetOne();
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Create_A_New_Cover_Note_TC_11");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void create_A_New_Cover_Note() throws Exception {
		ReportsClass.initialisation("Create_A_New_Cover_Note_TC_11");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Create New Cover Note
		basicProcessing.createNewPolicy_Quote_Legacy_CoverNote(type, clientName, policyClass, townOrVillage, sumInsured,
				futureAnnualPremier, "", 0);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Create_A_New_Cover_Note_TC_11");
		//closeBrowser();
	}
}
