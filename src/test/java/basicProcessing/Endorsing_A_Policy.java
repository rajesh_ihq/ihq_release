package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Endorsing_A_Policy extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String clearClientTransactionURL = readPro.getClearClientTransactionDataURL();
	String key = readPro.getClearClientKey();
	String sheetName = readPro.getSheetOne();
	String type = "Policy";
	String convertPolicy = "Endorse";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {

		System.out.println("Start Test: Endorsing_A_Policy_TC_13");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void endorsing_A_Policy() throws Exception {
		ReportsClass.initialisation("Endorsing_A_Policy_TC_13");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Launch Clear Client Transaction Data URL
		openBrowser(clearClientTransactionURL);
		basicProcessing.clearClientTransactionData(clientName, key);

		// Create New Policy - MV
		basicProcessing.createNewPolicy_Quote_Legacy_CoverNote(type, clientName, "16", null, "50000.00", "1000.00",
				sheetName, 2);

		// Endorsing a Policy
		basicProcessing.convertPolicy(convertPolicy,"50000", "500");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Endorsing_A_Policy_TC_13");
		closeBrowser();
	}
}