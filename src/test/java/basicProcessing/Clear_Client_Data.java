package basicProcessing;

import org.testng.annotations.Test;

import atu.testrecorder.ATUTestRecorder;
import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterMethod;

public class Clear_Client_Data extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String clearClientTransactionURL = readPro.getClearClientTransactionDataURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String dev_se_Client = "26";
	String clientName = readPro.getGhostClient();
	String key = readPro.getClearClientKey();
	ATUTestRecorder recorder;
	
	@BeforeMethod
	public synchronized void beforeMethod(Method method) {
		System.out.println("Start Test: Clear_Client_Data_TC_01");
		CommonFunction.deleteVideos(System.getProperty("user.dir") + "\\ScriptVideos\\");
		DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH-mm-ss");
		Date date = new Date();

		try {
			recorder = new ATUTestRecorder(System.getProperty("user.dir") + "\\ScriptVideos\\",
					method.getName() + "-" + dateFormat.format(date), false);
		} catch (Exception e) {
			System.out.println("Error in finding the location of the video.");
		}

		// To Start Recording
		try {
			recorder.start();
		} catch (Exception e) {
			System.out.println("Error in starting the video");
		}
		
		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void clearClientData_TC_01() throws Exception {
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
		CommonFunction commonFunction = new CommonFunction();
		ReportsClass.initialisation("Clear Client Data TC_01");
		
		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Launch Clear Client Transaction Data URL
		openBrowser(clearClientTransactionURL);
		basicProcessing.clearClientTransactionData(clientName, key);

	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Clear_Client_Data_TC_01");
		try {
			recorder.stop();
		} catch (Exception e) {
			System.out.println("Unable to stop the screen recording.");
		}
		closeBrowser();
	}
}