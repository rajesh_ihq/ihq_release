package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Unapproved_Receipt_Payment extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();

	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String paymentAmount = readPro.getUnApprovedPaymentAmount();
	String paymentType = readPro.getPaymentType();
	String department = readPro.getDepartment();
	String reference = readPro.getUnApprovedReceiptReference();
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Unapproved_Receipt_Payment_TC_18");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void unapproved_Receipt_Payment() throws Exception {
		ReportsClass.initialisation("Unapproved_Receipt_Payment_TC_18");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Unapproved Receipt Payment
		basicProcessing.unapprovedReceipt(clientName, paymentAmount, paymentType, department, reference);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Unapproved_Receipt_Payment_TC_18");
		closeBrowser();
	}
}
