package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Creditor_Add_Edit_And_Delete extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String creditorType = readPro.getCreditorType();
	String tradeTerms = readPro.getTradeTerms();
	String creditorCode = readPro.getCreditorCode();
	String branch = readPro.getCreditorBranch();
	String companyName = readPro.getCompanyName();
	String emailAddress = readPro.getEmailAddress();
	String mailingAddress = readPro.getMailingAddress();
	String editCreditorType = readPro.getEditCreditorType();
	String creditorEmail = readPro.getCreditorEmail();
	String operation = "Delete Creditor";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Creditor_Add_Edit_And_Delete_TC_23");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void creditor_Add_Edit_And_Delete() throws Exception {
		ReportsClass.initialisation("Creditor_Add_Edit_And_Delete_TC_23");

		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Create New Creditor
		basicProcessing.addNewCreditor(creditorType, tradeTerms, "Test7011", branch, companyName, emailAddress,
				mailingAddress);

		// Edit Creditor
		basicProcessing.edit_Delete_Creditor("", companyName, editCreditorType, creditorEmail);

		// Delete Creditor
		basicProcessing.edit_Delete_Creditor(operation, companyName, "", "");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Creditor_Add_Edit_And_Delete_TC_23");
		closeBrowser();
	}
}
