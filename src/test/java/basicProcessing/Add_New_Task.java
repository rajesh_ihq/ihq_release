package basicProcessing;

import org.testng.annotations.Test;

import atu.testrecorder.ATUTestRecorder;
import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterMethod;

public class Add_New_Task extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getClientName();
	String assignedTo = readPro.getAssignedToTask();
	String subject = readPro.getTaskSubject();
	String noteSection = readPro.getNoteSection();
	String description = readPro.getTaskDescription();
	String taskOrNote = "Task";
	String dev_se_Client = "26";
	ATUTestRecorder recorder;

	@BeforeMethod
	public synchronized  void beforeMethod(Method method) {
		System.out.println("Start Test: Add_New_Task_TC_03");

		DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH-mm-ss");
		Date date = new Date();

		try {
			recorder = new ATUTestRecorder(System.getProperty("user.dir") + "\\ScriptVideos\\",
					method.getName() + "-" + dateFormat.format(date), false);
		} catch (Exception e) {
			System.out.println("Error in finding the location of the video.");
		}

		// To Start Recording
		try {
			recorder.start();
		} catch (Exception e) {
			System.out.println("Error in starting the video");
		}
		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void add_New_Task() throws Exception {
		ReportsClass.initialisation("Add_New_Task_TC_03");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Add New Task
		basicProcessing.addNewTaskNotes(taskOrNote, clientName, assignedTo, subject, noteSection, description);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Add_New_Task_TC_03");
		try {
			recorder.stop();
		} catch (Exception e) {
			System.out.println("Unable to stop the screen recording.");
		}
		closeBrowser();
	}
}