package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class New_Claim extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: New_Claim_TC_21");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void new_Claim() throws Exception {
		ReportsClass.initialisation("New_Claim_TC_21");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// New Claim
		basicProcessing.createNewClaim_OR_AddReserve_OR_AddPayment("", clientName, "", "", "", "", "", "");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: New_Claim_TC_21");
		closeBrowser();
	}
}
