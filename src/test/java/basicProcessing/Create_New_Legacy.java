package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Create_New_Legacy extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String sumInsured = "50000.00";
	String futureAnnualPremier = "1000.00";
	String policyClass = "16";
	String type = "Legacy";
	String sheetName = readPro.getSheetOne();
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Create_New_Legacy_TC_09");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void create_New_Legacy() throws Exception {
		ReportsClass.initialisation("Create_New_Legacy_TC_09");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Create New Legacy
		basicProcessing.createNewPolicy_Quote_Legacy_CoverNote(type, clientName, policyClass, null, sumInsured,
				futureAnnualPremier, sheetName, 2);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Create_New_Legacy_TC_09");
		closeBrowser();
	}
}
