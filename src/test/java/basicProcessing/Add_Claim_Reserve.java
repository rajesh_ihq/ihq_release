package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Add_Claim_Reserve extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String type = "Add Reserve";
	String description = readPro.getAddReserveDescription();
	String reserveAmount = readPro.getAddReserveAmount();
	String excess = readPro.getAddReserveExcess();
	String reasonForReserveAdjustment = readPro.getReasonForReserveAdjustment();
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Add_Claim_Reserve_TC_22");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void add_Claim_Reserve() throws Exception {
		ReportsClass.initialisation("Add_Claim_Reserve_TC_22");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Add Claim Reserve
		basicProcessing.createNewClaim_OR_AddReserve_OR_AddPayment(type, clientName, description, reserveAmount, excess,
				reasonForReserveAdjustment, reserveAmount, "Cheque");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Add_Claim_Reserve_TC_22");
	//	closeBrowser();
	}
}
