package basicProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Add_Contact_To_Ghost_Client extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getClientName();
	String fName = readPro.getFirstName();
	String lName = readPro.getLastName();
	String position = readPro.getPosition();
	String mobile = readPro.getMobile();
	String email = readPro.getEmailAddress();
	String address = readPro.getMailingAddress();
	String dob = readPro.getClientDateOfBirth();
	String typeClient = "Client";
	String contactRelationship = readPro.getContactRelationship();

	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Add_Contact_To_Ghost_Client_TC_06");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void add_Contact_To_Ghost_Client() throws Exception {
		ReportsClass.initialisation("Add_Contact_To_Ghost_Client_TC_06");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Add Contact To Ghost Client
		basicProcessing.createContactToGhostClient_Or_Creditor(typeClient, clientName, fName, lName, position, mobile, email,
				address, dob, contactRelationship);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Add_Contact_To_Ghost_Client_TC_06");
		closeBrowser();
	}
}
