package SM;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.CP_Client_Policy_Page;
import pageObject.SM_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class SM_Test extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String sheetOne = readPro.getSheetOne();
	String sheetTwo = readPro.getSheetTwo();
	String seguromuskus_Client = "55";

	@BeforeMethod
	public void beforeMethod() throws Exception {
		CommonFunction commonFunction = new CommonFunction();
		System.out.println("beforeMethod");

		// Launch MasterQA URL
		openBrowser(masterQAURL);

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, seguromuskus_Client);
	}

	@Test
	public void smTest() throws Exception {
		SM_Page smPage = new SM_Page();
		CP_Client_Policy_Page cpClientPage = new CP_Client_Policy_Page();
		String excelPath = readPro.getSMExcel();
		Xls_Reader reader = new Xls_Reader(excelPath);
		int getRowCount = reader.getRowCount(sheetOne);
		System.out.println("Total Number of Row is : " + getRowCount);

		for (int i = 2; i <= 5; i++) {
			String firstName = reader.getCellData(sheetOne, "First Name", i);
			String lastName = reader.getCellData(sheetOne, "Last Name", i);
			int testCount = i - 1;
			ReportsClass.initialisation("" + firstName + " " + lastName + " _0" + testCount);
			try {
				System.out.println("");
				System.out.println("Started Test Step :=> " + testCount);
				System.out.println("");
				smPage.createNewClientForSM(sheetOne, i);
				cpClientPage.createNewPolicyTillAddLob();
				smPage.driverDetails_SM(sheetTwo, i);
				smPage.vehicleDetails_SM(sheetTwo, i);
				smPage.premiumSummary_SM(sheetTwo, i);
				smPage.premiumTaxLevySummary_SM(sheetTwo, i);
				smPage.grandTotal_SM(sheetTwo, i);
				smPage.verifyLOBSummaryIHQ(sheetTwo, i);
			} catch (Exception e) {
				System.out.println("");
			}
		}
	}

	@AfterMethod
	public void afterMethod() {
		ReportsClass.endTest();
		System.out.println("End Test: SM_Test");
		closeBrowser();
	}
}