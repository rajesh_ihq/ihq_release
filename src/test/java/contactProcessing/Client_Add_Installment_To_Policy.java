package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Contact_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Client_Add_Installment_To_Policy extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String sumInsured = "50000.00";
	String futureAnnualPremier = "1000.00";
	String installmentEveryOption = "Weekly";
	String townOrVillage = "Melbourne";
	String policyClass = "40";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Client_Add_Installment_To_Policy_TC_13");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void client_Add_Installment_To_Policy() throws Exception {
		ReportsClass.initialisation("Client_Add_Installment_To_Policy_TC_13");
		Contact_Processing_Page contactProcessing = new Contact_Processing_Page();
		CommonFunction commonFunction = new CommonFunction();

		// Login to IHQ with Usersupport
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Add Installment To Policy
		contactProcessing.addInstallmentToPolicy(clientName, policyClass, townOrVillage, sumInsured,
				futureAnnualPremier, installmentEveryOption);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Client_Add_Installment_To_Policy_TC_13");
		closeBrowser();
	}
}