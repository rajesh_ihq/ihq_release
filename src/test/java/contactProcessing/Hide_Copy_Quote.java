package contactProcessing;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import pageObject.Contact_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

public class Hide_Copy_Quote extends TestBase {
	ReadPropertyConfig readPro = new ReadPropertyConfig();

	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String sheetName = readPro.getSheetOne();
	String clearClientTransactionURL = readPro.getClearClientTransactionDataURL();
	String key = readPro.getClearClientKey();
	String sumInsured = "5000.00";
	String futureAnnualPremier = "1000.00";
	String townVillage = "Sydney";
	String policyClass = "30";
	String hideCopyQuoteYes = "Yes";
	String hideCopyQuoteNo = "No";
	String dev_client_Name = "26";
	String Quote = "Quote";
	String copyQuote = "Copy Quote";
	@BeforeMethod
	public void beforeMethod() throws Exception {
		System.out.println("Start Test: Hide_Copy_Quote_TC_12");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void hide_copy_quote() throws Exception {
		Contact_Processing_Page contactProcessing = new Contact_Processing_Page();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
		CommonFunction commonFunction = new CommonFunction();

		ReportsClass.initialisation("Hide_Copy_Quote_TC_12");

		// IHQ Login with Usersupport
		commonFunction.loginIHQ(userName, password, dev_client_Name);

		// Launch Clear Client Transaction Data URL
		openBrowser(clearClientTransactionURL);
		basicProcessing.clearClientTransactionData(clientName, key);

		// Hide Copy Policy Yes
		contactProcessing.hideCopyQuoteSetting(hideCopyQuoteYes);

		// Create New Ghost Quote
		basicProcessing.createNewPolicy_Quote_Legacy_CoverNote(Quote, clientName, policyClass, townVillage, sumInsured,
				futureAnnualPremier, "", 0);

		// Verify Copy Hide Quote
		contactProcessing.verifyHideCopyPolicyOrQuote(copyQuote);

		// Hide Copy Quote NO
		contactProcessing.hideCopyQuoteSetting(hideCopyQuoteNo);

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		System.out.println("End Test: Hide_Copy_Policy_TC_11");
		closeBrowser();
	}

}
