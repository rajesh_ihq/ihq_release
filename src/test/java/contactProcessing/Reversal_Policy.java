package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Reversal_Policy extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();
	String convertPolicyOption = "Reversal";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Reversal_Policy_TC_02");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void reversal_Policy() throws Exception {
		ReportsClass.initialisation("Reversal_Policy_TC_02");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Reversal Policy
		basicProcessing.convertPolicy(convertPolicyOption, "", "");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Reversal_Policy_TC_02");
		closeBrowser();
	}
}
