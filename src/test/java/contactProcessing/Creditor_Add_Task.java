package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import pageObject.Contact_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Creditor_Add_Task extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String creditorName = readPro.getCompanyName();
	String assignedTo = readPro.getAssignedToTask();
	String noteSection = readPro.getNoteSection();
	String description = readPro.getTaskDescription();
	String subject = readPro.getTaskSubject();
	String companyName = readPro.getCompanyName();
	String operation = "Delete Creditor";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Creditor_Add_Task_TC_08");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void creditor_Add_Task() throws Exception {
		ReportsClass.initialisation("Creditor_Add_Task_TC_08");
		Contact_Processing_Page contactProcessing = new Contact_Processing_Page();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
		CommonFunction commonFunction = new CommonFunction();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Creditor Add Tasks
		contactProcessing.addTaskForCreditor(creditorName, assignedTo, noteSection, subject, description);
		
		// Delete Creditor
		basicProcessing.edit_Delete_Creditor(operation, companyName, "", "");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Creditor_Add_Task_TC_08");
		closeBrowser();
	}
}
