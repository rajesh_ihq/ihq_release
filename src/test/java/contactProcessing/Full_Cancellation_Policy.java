package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Full_Cancellation_Policy extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String convertPolicyOption = "Full Cancellation";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Full_Cancellation_Policy_TC_04");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void full_Cancellation_Policy() throws Exception {
		ReportsClass.initialisation("Full_Cancellation_Policy_TC_04");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Reinstate Policy
		basicProcessing.convertPolicy(convertPolicyOption, "", "");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Full_Cancellation_Policy_TC_04");
		closeBrowser();
	}

}
