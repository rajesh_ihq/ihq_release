package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import pageObject.Contact_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class ClientCodeRule_AutoGenerate_IF_Blank extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientCodeRule = readPro.getClientCodeRule();
	String salutation = "Mr";
	String firstName = "Peter";
	String lastName = "Spidey";
	String legalName = "Peter Spidey";
	String gender = readPro.getGender();
	String emailAddress = readPro.getEmailAddress();
	String clientDOB = readPro.getClientDateOfBirth();
	String mailingAddress = readPro.getMailingAddress();
	String dev_se_Client = "16";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: ClientCodeRule_AutoGenerate_IF_Blank_TC_14");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void clientCodeRule_AutoGenerate_IF_Blank() throws Exception {
		ReportsClass.initialisation("ClientCodeRule_AutoGenerate_IF_Blank_TC_14");
		CommonFunction commonFunction = new CommonFunction();
		Contact_Processing_Page contactProcessing = new Contact_Processing_Page();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// Login to IHQ with Usersupport
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Make Client Code Rule as Auto Generate If Blank
		contactProcessing.setClientCodeRuleFromClientFieldsSettings(clientCodeRule);

		// Create Contact
		basicProcessing.createNewClient(salutation, firstName, lastName, legalName, gender, emailAddress, clientDOB,
				mailingAddress);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: ClientCodeRule_AutoGenerate_IF_Blank_TC_14");
		closeBrowser();
	}
}
