package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class CreateNewPolicy_ReceiptPayment extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clearClientTransactionURL = readPro.getClearClientTransactionDataURL();
	String key = readPro.getClearClientKey();
	String clientName = readPro.getGhostClient();
	String paymentAmount = readPro.getPaymentAmount();
	String paymentType = readPro.getPaymentType();
	String department = readPro.getDepartment();
	String reference = readPro.getPaymentReference();
	String clientForPayment = "Peter Parker";
	String type = "Policy";
	String sheetName = readPro.getSheetOne();
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: CreateNewPolicy_ReceiptPayment_AddPayment_TC_09");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void createNewPolicy_ReceiptPayment_AddPayment() throws Exception {
		ReportsClass.initialisation("CreateNewPolicy_ReceiptPayment_AddPayment_TC_09");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Launch Clear Client Transaction Data URL
		openBrowser(clearClientTransactionURL);
		basicProcessing.clearClientTransactionData(clientName, key);

		// Create New Policy
		basicProcessing.createNewPolicy_Quote_Legacy_CoverNote(type, clientForPayment, "16", null, "50000.00", "1000.00",
				sheetName, 2);

		// Client Receipt Payment
		basicProcessing.receiptPayment("", clientForPayment, paymentAmount, paymentType, department, reference);

	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: CreateNewPolicy_ReceiptPayment_AddPayment_TC_09");
		closeBrowser();
	}
}