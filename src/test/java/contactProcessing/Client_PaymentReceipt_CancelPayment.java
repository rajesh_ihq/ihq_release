package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import pageObject.Contact_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Client_PaymentReceipt_CancelPayment extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String paymentAmount = readPro.getPaymentAmount();
	String paymentType = readPro.getPaymentType();
	String department = readPro.getDepartment();
	String reference = readPro.getPaymentReference();
	String reasonforCancellation = "Cancel Payment";
	String clientName = "Peter Parker";
	String dev_client_Name = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Client_PaymentReceipt_CancelPayment_TC_10");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void client_PaymentReceipt_CancelPayment() throws Exception {
		ReportsClass.initialisation("Client_PaymentReceipt_CancelPayment_TC_10");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
		Contact_Processing_Page contactProcessing = new Contact_Processing_Page();

		// IHQ Login with Usersupport
		commonFunction.loginIHQ(userName, password, dev_client_Name);

		// Client Payment Receipt
		basicProcessing.receiptPayment("", clientName, paymentAmount, paymentType, department, reference);

		// Cancel Payment
		contactProcessing.cancelPayment(reasonforCancellation);

	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Client_PaymentReceipt_CancelPayment_TC_10");
		closeBrowser();
	}
}
