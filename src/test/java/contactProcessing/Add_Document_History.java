package contactProcessing;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import pageObject.Contact_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

public class Add_Document_History extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();

	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String clientName = readPro.getGhostClient();

	String sheetName = readPro.getSheetOne();
	String dev_client_Name = "26";
	String categoryName = "Test CategoryName";
	String descriptioText = "Test Description";
	String expiryDate = "23/10/2020";
	//String filepath = readPro.getUploadImage();
	String path = System.getProperty("user.dir");
	String filepath = path + "\\Images\\butterfly.jpg";
	String reasonForExpiry = "Test Reason For Expiry";
	String clearClientTransactionURL = readPro.getClearClientTransactionDataURL();
	String key = readPro.getClearClientKey();

	@BeforeMethod
	public void beforeMethod() throws Exception {
		System.out.println("Start Test: Add_Document_History_TC_15");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void add_Document_History() throws Exception {
		Contact_Processing_Page contactProcessing = new Contact_Processing_Page();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
		CommonFunction commonFunction = new CommonFunction();

		ReportsClass.initialisation("Add_Document_History_TC_15");

		// IHQ Login with Usersupport
		commonFunction.loginIHQ(userName, password, dev_client_Name);

		// Launch Clear Client Transaction Data URL
		openBrowser(clearClientTransactionURL);
		basicProcessing.clearClientTransactionData(clientName, key);

		// Add Document Category in Setting
		contactProcessing.addDocumentCategory(categoryName);

		// Add Document History
		contactProcessing.addDocumentHistory(clientName, categoryName, descriptioText, expiryDate, reasonForExpiry,
				filepath);

		// Remove Added Category in setting
		contactProcessing.removeDocumentCategory(categoryName);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Add_Document_History_TC_15");
		closeBrowser();
	}
}