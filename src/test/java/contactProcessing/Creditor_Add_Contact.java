package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Creditor_Add_Contact extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String dev_se_Client = "26";
	String typeClient = "Creditor";
	String fName = readPro.getCreditorContactFirstName();
	String lName = readPro.getCreditorContactLastName();
	String creditorName = readPro.getCompanyName();
	String position = readPro.getCreditorContactPosition();
	String mobile = readPro.getMobile();
	String email = readPro.getEmailAddress();
	String address = readPro.getMailingAddress();
	String dob = readPro.getClientDateOfBirth();
	String contactRelationship = readPro.getContactRelationship();

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Creditor_Add_Contact_TC_07");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void creditor_Add_Contact() throws Exception {
		ReportsClass.initialisation("Creditor_Add_Contact_TC_07");

		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Add Contact To Ghost Creditor
		basicProcessing.createContactToGhostClient_Or_Creditor(typeClient, creditorName, fName, lName, position, mobile,
				email, address, dob, contactRelationship);
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Creditor_Add_Contact_TC_07");
		closeBrowser();
	}
}
