package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import pageObject.Contact_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Hide_Copy_Policy extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();

	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String type = "Ghost Policy";
	String clientName = readPro.getGhostClient();
	String sheetName = readPro.getSheetOne();
	String clearClientTransactionURL = readPro.getClearClientTransactionDataURL();
	String key = readPro.getClearClientKey();
	String sumInsured = "5000.00";
	String futureAnnualPremier = "1000.00";
	String townVillage = "Sydney";
	String policyClass = "40";
	String hideCopyPolicyYes = "Yes";
	String hideCopyPolicyNo = "No";
	String dev_client_Name = "26";
	String copyPolicy = "Copy Policy";

	@BeforeMethod
	public void beforeMethod() throws Exception {
		System.out.println("Start Test: Hide_Copy_Policy_TC_11");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void hide_Copy_Policy() throws Exception {
		Contact_Processing_Page contactProcessing = new Contact_Processing_Page();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();
		CommonFunction commonFunction = new CommonFunction();

		ReportsClass.initialisation("Hide_Copy_Policy_TC_11");

		// IHQ Login with Usersupport
		commonFunction.loginIHQ(userName, password, dev_client_Name);

		// Launch Clear Client Transaction Data URL
		openBrowser(clearClientTransactionURL);
		basicProcessing.clearClientTransactionData(clientName, key);

		// Hide Copy Policy Yes
		contactProcessing.hideCopyPolicySettings(hideCopyPolicyYes);

		// Create New Ghost Policy
		basicProcessing.createNewPolicy_Quote_Legacy_CoverNote(type, clientName, policyClass, townVillage, sumInsured,
				futureAnnualPremier, "", 0);

		// Verify to Hide Copy Policy Option Is Showing or Not
		contactProcessing.verifyHideCopyPolicyOrQuote(copyPolicy);

		// Hide Copy Policy No
		contactProcessing.hideCopyPolicySettings(hideCopyPolicyNo);
	}

	@AfterMethod
	public void afterMethod() throws Exception {
		System.out.println("End Test: Hide_Copy_Policy_TC_11");
		closeBrowser();
	}

	
}