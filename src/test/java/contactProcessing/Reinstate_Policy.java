package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Reinstate_Policy extends TestBase {
	
	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String convertPolicyOption = "Reinstate";
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Reinstate_Policy_TC_03");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void reinstate_Policy() throws Exception {
		ReportsClass.initialisation("Reinstate_Policy_TC_03");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Reinstate Policy
		basicProcessing.convertPolicy(convertPolicyOption, "", "");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Reinstate_Policy_TC_03");
		closeBrowser();
	}
}
