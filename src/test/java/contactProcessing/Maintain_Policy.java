package contactProcessing;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Basic_Processing_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Maintain_Policy extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();

	String masterQAURL = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String dev_se_Client = "26";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Maintain_Policy_TC_05");

		// Launch MasterQA URL
		openBrowser(masterQAURL);
	}

	@Test
	public void maintain_Policy() throws Exception {
		ReportsClass.initialisation("Maintain_Policy_TC_05");
		CommonFunction commonFunction = new CommonFunction();
		Basic_Processing_Page basicProcessing = new Basic_Processing_Page();

		// IHQ Login with Support User
		commonFunction.loginIHQ(userName, password, dev_se_Client);

		// Maintain a Policy
		basicProcessing.maintainPolicy();

	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Maintain_Policy_TC_05");
		closeBrowser();
	}
}
