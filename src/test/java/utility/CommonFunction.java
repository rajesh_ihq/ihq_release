package utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import atu.testrecorder.ATUTestRecorder;
import baseClass.TestBase;
import locators.Locators.basicProcessingLocators;
import locators.Locators.carpeeshClientPolicyLocators;

public class CommonFunction extends TestBase implements carpeeshClientPolicyLocators, basicProcessingLocators {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String renewalExcel = readPro.getRenewalVerificationExcel();
	Xls_Reader xlsReaderForRenewal = new Xls_Reader(renewalExcel);

	// IHQ Login locators
	@FindBy(xpath = CLICK_HERE_LOGIN)
	WebElement clickHereLogin_field;
	@FindBy(xpath = IHQ_USERNAME)
	WebElement ihqUserName_field;
	@FindBy(xpath = IHQ_PASSWORD)
	WebElement ihqPassword_field;
	@FindBy(xpath = IHQ_ACCOUNT_SELECT)
	WebElement ihqAccountSelect_field;
	@FindBy(xpath = IHQ_SIGNIN_BUTTON)
	WebElement ihqSignInButton_field;

	// basicProcessing Page Locators
	@FindBy(xpath = IHQ_CLIENT_NAME)
	WebElement ihqClientName_field;
	@FindBy(xpath = CONTACT_NAV)
	WebElement contactNavigation_field;
	@FindBy(xpath = ACCOUNT_TRANSACTIONS_TAB)
	WebElement accountTransactionTab_field;
	@FindBy(xpath = CLAIMS_TAB)
	WebElement claimsTab_field;
	@FindBy(xpath = VIEW_POLICY)
	WebElement viewPolicy_field;

	// Add insurer from Add LOB Locators
	@FindBy(xpath = ADD_LOB_INSURER_PLUS_ICON)
	WebElement addLobInsurerPlusIcon_field;
	@FindBy(xpath = ADD_INSURER_INSURER)
	WebElement addInsurer_field;
	@FindBy(xpath = ADD_INSURER_POLICY_FEE_ALLOCATION)
	WebElement addInsurerPolicyFeeAllocation_field;
	@FindBy(xpath = ADD_INSURER_POLICY_NUMBER)
	WebElement addInsurerPolicyNumber_field;
	@FindBy(xpath = ADD_INSURER_CREDIT_TERM)
	WebElement addInsurerCreditTerm_field;
	@FindBy(xpath = ADD_INSURER_BROKERAGE)
	WebElement addInsurerBrokerage_field;
	@FindBy(xpath = ADD_INSURER_FLAT_BROKERAGE)
	WebElement addInsurerFlatBrokerage_field;
	@FindBy(xpath = ADD_INSURER_SAVE_BUTTON)
	WebElement addInsurerSaveButton_field;

	// Add Broker from Add LOB Locators
	@FindBy(xpath = ADD_BROKER_PLUS_ICON)
	WebElement addBrokerPlusIcon_field;
	@FindBy(xpath = SELECT_BROKER)
	WebElement selectBroker_field;
	@FindBy(xpath = BROKER_TRANSACTION_NUMBER)
	WebElement brokerTransactionNumber_field;
	@FindBy(xpath = BROKER_FLAT_COMISSION)
	WebElement brokerFlatComission_field;
	@FindBy(xpath = BROKER_FEE)
	WebElement brokerFee_field;
	@FindBy(xpath = ADD_BROKER_SAVE_BUTTON)
	WebElement addBrokerSaveButton_field;

	// Clear Session Lock Locators
	@FindBy(xpath = ADMIN_SECTION)
	WebElement adminSection_field;
	@FindBy(xpath = CLEAR_SESSION_LOCK)
	WebElement clearSessionLock_field;

	public CommonFunction() {
		PageFactory.initElements(driver, this);
	}

	// Login to IHQ
	public void loginIHQ(String userName, String password, String value) throws Exception {
		//clickHereLogin_field.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ihqUserName_field.clear();
		ihqUserName_field.sendKeys(userName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ihqPassword_field.clear();
		ihqPassword_field.sendKeys(password);
		ihqSignInButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
		// when we run on master qa please uncomment below lines
		/*Select drpchooseAccount = new Select(ihqAccountSelect_field);
		try {
			drpchooseAccount.selectByValue(value);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement option = drpchooseAccount.getFirstSelectedOption();
			String selectedTest = option.getText();
			System.out.println("Client : " + selectedTest);
		} catch (Exception e) {
			System.out.println("");
		}*/
	}

	// Select Licence Type from Dropdown
	public static boolean selectLicenceType(String licenceType, WebElement xpath) {
		String sTempLicenceType = licenceType.toString().trim();
		int index_LicenceType = 0;

		if (sTempLicenceType.equalsIgnoreCase("Full licence")) {
			index_LicenceType = 1;
		} else if (sTempLicenceType.equalsIgnoreCase("Probationary P1")) {
			index_LicenceType = 2;
		} else if (sTempLicenceType.equalsIgnoreCase("Probationary P2")) {
			index_LicenceType = 3;
		} else if (sTempLicenceType.equalsIgnoreCase("International or overseas licence")) {
			index_LicenceType = 4;
		}

		if (index_LicenceType == 0) {
			return false;
		}

		// Invalid Case
		Select drpLicenceType = new Select((WebElement) xpath);
		try {
			drpLicenceType.selectByIndex(index_LicenceType);
		} catch (Exception e) {
			System.out.println("");
		}

		return true;
	}

	// Select Licence Year from Dropdown
	public static boolean selectLicenceYear(String licenceType, String licenceYear, WebElement xpath) {
		String sTempLicenceYear = licenceYear.toString().trim();
		int index_LicenceYear = 0;

		String sTemLicenceType = licenceType.toString().trim();
		if (sTemLicenceType.equalsIgnoreCase("Full licence")) {
			if (sTempLicenceYear.equalsIgnoreCase("3")) {
				index_LicenceYear = 1;
			} else if (sTempLicenceYear.equalsIgnoreCase("4")) {
				index_LicenceYear = 2;
			} else if (sTempLicenceYear.equalsIgnoreCase("5")) {
				index_LicenceYear = 3;
			} else if (sTempLicenceYear.equalsIgnoreCase("6+")) {
				index_LicenceYear = 4;
			}
		} else if (sTemLicenceType.equalsIgnoreCase("Probationary P1")) {
			if (sTempLicenceYear.equalsIgnoreCase("Less than 1")) {
				index_LicenceYear = 1;
			} else if (sTempLicenceYear.equalsIgnoreCase("1")) {
				index_LicenceYear = 2;
			} else if (sTempLicenceYear.equalsIgnoreCase("2")) {
				index_LicenceYear = 3;
			}
		} else if (sTemLicenceType.equalsIgnoreCase("Probationary P2")) {
			if (sTempLicenceYear.equalsIgnoreCase("1")) {
				index_LicenceYear = 1;
			} else if (sTempLicenceYear.equalsIgnoreCase("2")) {
				index_LicenceYear = 2;
			} else if (sTempLicenceYear.equalsIgnoreCase("3")) {
				index_LicenceYear = 3;
			}
		} else if (sTemLicenceType.equalsIgnoreCase("International or overseas licence")) {
			if (sTempLicenceYear.equalsIgnoreCase("Less than 1")) {
				index_LicenceYear = 1;
			} else if (sTempLicenceYear.equalsIgnoreCase("1")) {
				index_LicenceYear = 2;
			} else if (sTempLicenceYear.equalsIgnoreCase("2")) {
				index_LicenceYear = 3;
			} else if (sTempLicenceYear.equalsIgnoreCase("3")) {
				index_LicenceYear = 4;
			} else if (sTempLicenceYear.equalsIgnoreCase("4")) {
				index_LicenceYear = 5;
			} else if (sTempLicenceYear.equalsIgnoreCase("5")) {
				index_LicenceYear = 6;
			} else if (sTempLicenceYear.equalsIgnoreCase("6+")) {
				index_LicenceYear = 7;
			}
		}
		System.out.println("Licence Type is : " + sTemLicenceType);
		System.out.println("Licence Year is : " + sTempLicenceYear);
		System.out.println("Licence Year Index is : " + index_LicenceYear);

		if (index_LicenceYear == 0) {
			return false;
		}

		Select drpLicenceYear = new Select(xpath);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		try {
			drpLicenceYear.selectByIndex(index_LicenceYear);
		} catch (Exception e) {
			System.out.println("");
		}
		return true;

	}

	// Select Licence Country from dropdown
	public static boolean selectLicenceCountry(String value, WebElement xpath) {
		String sTempLicenceCountry = value.toString().trim();
		int index_LicenceCountry = 0;

		if (sTempLicenceCountry.equalsIgnoreCase("Australia")) {
			index_LicenceCountry = 1;
		} else if (sTempLicenceCountry.equalsIgnoreCase("Ireland")) {
			index_LicenceCountry = 2;
		} else if (sTempLicenceCountry.equalsIgnoreCase("New Zealand")) {
			index_LicenceCountry = 3;
		} else if (sTempLicenceCountry.equalsIgnoreCase("United Kingdom")) {
			index_LicenceCountry = 4;
		} else if (sTempLicenceCountry.equalsIgnoreCase("Other")) {
			index_LicenceCountry = 5;
		}

		if (index_LicenceCountry == 0) {
			return false;
		}

		Select drpLicenceCountry = new Select(xpath);
		try {
			drpLicenceCountry.selectByIndex(index_LicenceCountry);
		} catch (Exception e) {
			System.out.println("");
		}

		return true;
	}

	// Convert String into Case Sensitive Format for Single Word
	public static String capitaliseFirstLetterSingleWord(String str) {

		// Create a char array of given String
		char ch[] = str.toCharArray();
		for (int i = 0; i < str.length(); i++) {

			// If first character of a word is found
			if (i == 0 && ch[i] != ' ' || ch[i] != ' ' && ch[i - 1] == ' ') {

				// If it is in lower-case
				if (ch[i] >= 'a' && ch[i] <= 'z') {

					// Convert into Upper-case
					ch[i] = (char) (ch[i] - 'a' + 'A');
				}
			}

			// If apart from first character
			// Any one is in Upper-case
			else if (ch[i] >= 'A' && ch[i] <= 'Z')

				// Convert into Lower-Case
				ch[i] = (char) (ch[i] + 'a' - 'A');
		}

		// Convert the char array to equivalent String
		String st = new String(ch);
		return st;
	}

	// Every String first Letter Convert to Capital
	public static String capitaliseFirstLetterEveryWordInString(String string) {
		char[] chars = string.toLowerCase().toCharArray();
		boolean found = false;
		for (int i = 0; i < chars.length; i++) {
			if (!found && Character.isLetter(chars[i])) {
				chars[i] = Character.toUpperCase(chars[i]);
				found = true;
			} else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '\'') {
				found = false;
			}
		}
		return String.valueOf(chars);
	}

	// Convert only first letter capital to entire String.
	public static String capitaliseFirstLetterEntireString(String s) {
		if (s.length() == 0)
			return s;
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

	// Get Current Day
	public static String getCurrentDay() {
		// Create a Calendar Object
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

		// Get Current Day as a number
		int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
		// Integer to String Conversion
		String todayStr = Integer.toString(todayInt);
		return todayStr;
	}

	// Get Current Date
	public static String getCurrentDate() {
		java.util.Date date = new java.util.Date(System.currentTimeMillis());
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String getCurrentDate = df.format(date);
		return getCurrentDate;
	}

	// Get Previous Year
	public static String getPreviousYear(int days) {
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		Date todate1 = cal.getTime();
		String fromdate = dateFormat.format(todate1);

		return fromdate;
	}

	// Navigate to Client Policy Overview Page
	public void navigateToClient(String clientName) throws Exception {
		ihqClientName_field.sendKeys(clientName);
		Thread.sleep(2000);
		ihqClientName_field.sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);
	}

	// Add Insurer from ADD LOB
	public void addInsurer(String insurerValue, String policyFeeAllocation, String policyNumber, String creditTerm,
			String brokerage, String flatBrokerage) throws Exception {
		addLobInsurerPlusIcon_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);

		Select drpInsurer = new Select(addInsurer_field);

		try {
			drpInsurer.selectByValue(insurerValue);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		addInsurerPolicyFeeAllocation_field.sendKeys(policyFeeAllocation);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		addInsurerPolicyNumber_field.sendKeys(policyNumber);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		Select drpCreditTerms = new Select(addInsurerCreditTerm_field);

		try {
			drpCreditTerms.selectByValue(creditTerm);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		addInsurerBrokerage_field.sendKeys(brokerage);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		addInsurerFlatBrokerage_field.sendKeys(flatBrokerage);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		addInsurerSaveButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);
	}

	// Add Broker from ADD LOB
	public void addBroker(String broker, String brokerTransactionNumber, String flatComission, String brokerFee)
			throws Exception {
		addBrokerPlusIcon_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(2000);

		// Select Broker
		Select drpBroker = new Select(selectBroker_field);

		try {
			drpBroker.selectByValue(broker);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("");
		}

		brokerTransactionNumber_field.sendKeys(brokerTransactionNumber);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		// Enter Broker Flat Comission
		brokerFlatComission_field.sendKeys(Keys.CONTROL, "a");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		brokerFlatComission_field.sendKeys(Keys.BACK_SPACE);
		brokerFlatComission_field.sendKeys(Keys.BACK_SPACE);
		brokerFlatComission_field.sendKeys(Keys.BACK_SPACE);
		brokerFlatComission_field.sendKeys(flatComission);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		// Enter Broker Fee%
		brokerFee_field.sendKeys(Keys.CONTROL, "a");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);
		brokerFee_field.sendKeys(Keys.BACK_SPACE);
		brokerFee_field.sendKeys(Keys.BACK_SPACE);
		brokerFee_field.sendKeys(Keys.BACK_SPACE);
		brokerFee_field.sendKeys(brokerFee);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(1000);

		addBrokerSaveButton_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Thread.sleep(3000);
	}

	// Navigate To Client Account Transaction TAB
	public void navigateToAccountTransaction(String clientName) throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		ihqClientName_field.sendKeys(clientName);
		Thread.sleep(2000);
		ihqClientName_field.sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);

		accountTransactionTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	// Navigate To Client Claims TAB
	public void navigateToClaims(String clientName) throws Exception {
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		ihqClientName_field.sendKeys(clientName);
		Thread.sleep(2000);
		ihqClientName_field.sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(2000);

		claimsTab_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	// Verify Client Account Transaction Module Functionality
	public void verifyAccountTransactionModuleFunctionality(String value1, String value2, String msgPassed,
			String msgFailed) {
		String getRefundPaymentSuccess = driver
				.findElement(By.xpath("//div[@class='x_content']//table[@class='table table-striped']")).getText();
		if (getRefundPaymentSuccess.contains(value1) && getRefundPaymentSuccess.contains(value2)) {
			ReportsClass.logStat(Status.PASS, msgPassed);
		} else {
			ReportsClass.logStat(Status.FAIL, msgFailed);
		}

		// Verify that Refund Payment Successful or Not
		Assert.assertTrue(getRefundPaymentSuccess.contains(value1) && getRefundPaymentSuccess.contains(value2),
				msgFailed);
	}

	// Verify that Convert Policy is successfully or not.
	public void verifyConvertPolicy(String convertPolicyOption) throws Exception {
		String endorseSuccessful = driver.findElement(By.xpath("//div//h3[contains(text(),'Policy Documents')]"))
				.getText();
		if (convertPolicyOption.contains(convertPolicyOption)) {
			if (endorseSuccessful.contains("Policy Documents")) {
				ReportsClass.logStat(Status.PASS, "" + convertPolicyOption + " a Policy Successfully !!!");
			} else {
				ReportsClass.logStat(Status.FAIL, "Failed to " + convertPolicyOption + " a Policy !!!");
			}
			// Verify to Converting Policy success or not
			Assert.assertTrue(endorseSuccessful.contains("Policy Documents"),
					"Failed to " + convertPolicyOption + " a Policy !!!");
			driver.findElement(By.xpath("//button[contains(text(),'Policy Overview')]")).click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			Thread.sleep(4000);
		} else if (convertPolicyOption.contains("Cancel ")) {
			if (endorseSuccessful.contains("Policy Documents")) {
				ReportsClass.logStat(Status.PASS, "Cancel a Policy Successfully !!!");
			} else {
				ReportsClass.logStat(Status.FAIL, "Failed to Cancel a Policy !!!");
			}
			// Verify to Cancel a Policy success or not
			Assert.assertTrue(endorseSuccessful.contains("Policy Documents"), "Failed to Cancel a Policy !!!");
			driver.findElement(By.xpath("//button[contains(text(),'Policy Overview')]")).click();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else {
			System.out.println("Failed to Bind Policy !!!");
		}
	}

	// Verify that Policy, Quote, New Insurer Quote, Legancy is Created
	// successfully or Not.
	public void verifyAddTransactionTypeCreatedSuccessfully(String addTransactionType) {
		String addTransactionGetText = driver.findElement(By.xpath("//div//h3[contains(text(),'Policy Documents')]"))
				.getText();
		if (addTransactionGetText.contains("Policy Documents")) {
			ReportsClass.logStat(Status.PASS, "New " + addTransactionType + " Successfully Created !!!");
		} else {
			ReportsClass.logStat(Status.FAIL, "New " + addTransactionType + " Failed to Create !!!");
		}

		// Verify to Create New Policy Or Not
		Assert.assertTrue(addTransactionGetText.contains("Policy Documents"),
				"New " + addTransactionType + " Failed to Create !!!");
	}

	// Verify that Element is present or Not,
	public boolean isElementPresent(By locatorKey, String msg) {
		try {
			driver.findElement(locatorKey);
			System.out.println("Element " + msg + " is present on the screen");
			ReportsClass.logStat(Status.INFO, "Element " + msg + " is present on the screen");
			System.out.println("");
			return true;
		} catch (Exception e) {
			System.out.println("Element " + msg + " is not present on the screen");
			ReportsClass.logStat(Status.INFO, "Element " + msg + " is not present on the screen");
			System.out.println("");
			return false;
		}
	}

	public void uploadFile(String fileLocation) throws AWTException {
		StringSelection strSelection = new StringSelection(fileLocation);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(strSelection, null);

		Robot robot = new Robot();

		robot.delay(300);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(200);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	// Clear Session Lock
	public void clearSessionLock() {
		adminSection_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		clearSessionLock_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	// Search client and View Policy
	public void searchAndViewPolicy(String sheetName, int rowNum) throws Exception {
		String clientName = xlsReaderForRenewal.getCellData(sheetName, "ClientName", rowNum);

		System.out.println("Client Name is: " + clientName);
		System.out.println("========================================");

		// Navigate to Contact
		contactNavigation_field.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		navigateToClient(clientName);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		// View Policy
		viewPolicy_field.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public static void deleteVideos(String path){
		File directory = new File(path);
		File[] files = directory.listFiles();
		for(File file : files){
			file.delete();
		}
	}
}
