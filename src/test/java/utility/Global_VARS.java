package utility;

import org.openqa.selenium.By;

import baseClass.TestBase;

public class Global_VARS extends TestBase {

	// Baileys Global Variables
	public static final String submitButtonGetText = driver
			.findElement(By.xpath("//button[@class='submit_button nform_btn boots']")).getText().trim();
	public static final String submitButtonText = "Submit";

}
