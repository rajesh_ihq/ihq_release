package utility;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.util.Properties;

import junit.extensions.TestDecorator;

public class ReadPropertyConfig {
	Properties configProperty;
	Properties dataProperty;

	public ReadPropertyConfig() {
		File src = new File("./Configuration/config.properties");
		File src1 = new File("./Configuration/testdata.properties");

		try {
			FileInputStream fis = new FileInputStream(src);
			configProperty = new Properties();
			configProperty.load(fis);

			FileInputStream fis1 = new FileInputStream(src1);
			dataProperty = new Properties();
			dataProperty.load(fis1);
		} catch (Exception e) {
			System.out.println("Exception is" + e.getMessage());
		}
	}

	// Excel Property
	public String getExcelPath() {
		String excelPath = configProperty.getProperty("EXCEL_FILE");
		return excelPath;
	}

	public String getJSONExcelFile() {
		String jsonExcelFile = configProperty.getProperty("JSON_EXCEL_FILE");
		return jsonExcelFile;
	}

	public String getSheetOne() {
		String sheetOne = dataProperty.getProperty("PART_ONE");
		return sheetOne;
	}

	public String getSheetTwo() {
		String sheetTwo = dataProperty.getProperty("PART_TWO");
		return sheetTwo;
	}

	public String getSheetThree() {
		String sheetThree = dataProperty.getProperty("PART_Three");
		return sheetThree;
	}

	public String getSheetFour() {
		String sheetFour = dataProperty.getProperty("PART_FOUR");
		return sheetFour;
	}

	public String getCarpeeshUrl() {
		String carpeeshUrl = dataProperty.getProperty("CARPEESH_URL");
		return carpeeshUrl;
	}

	// Extent Report get Property for Basic
	public String getExtentReportPath() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    String extentReport = ".//ExtentReport\\Basic\\Report"+ timestamp.getTime()+".html";
		return extentReport;
	}
	
	// Extent Report get Property for CP
		public String getExtentReportPath_CP() {
			//String extentReport = configProperty.getProperty("EXTENT_REPORT_PATH");
		    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		    String extentReport = ".//ExtentReport\\Carpeesh\\Report_"+ timestamp.getTime()+".html";
			return extentReport;
					}
		
		// Extent Report get Property for SM
		public String getExtentReportPath_SM() {
			//String extentReport = configProperty.getProperty("EXTENT_REPORT_PATH");
		    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		    String extentReport = ".//ExtentReport\\SM\\Report_"+ timestamp.getTime()+".html";
			return extentReport;
		}

	// Pdf File get Property
	public String getPdfFilePath() {
		String pdfFile = configProperty.getProperty("PDF_FILE_PATH");
		return pdfFile;
	}

	public String getPdfURLPath() {
		String pdfFile = configProperty.getProperty("PDF_URL_PATH");
		return pdfFile;
	}

	// Chrome Driver get Property
	public String getChromeDriver() {
		String chromeDriver = configProperty.getProperty("CHROME_DRIVER");
		return chromeDriver;
	}

	// CTM get Property
	public String getCtmExcel() {
		String ctmExcelPath = configProperty.getProperty("CTM_TEST_EXCEL");
		return ctmExcelPath;
	}

	public String getCtmUrl() {
		String ctmURL = dataProperty.getProperty("CTM_URL");
		return ctmURL;
	}

	public String getCTMPassword() {
		String ctmPassword = dataProperty.getProperty("CTM_PASSWORD");
		return ctmPassword;
	}

	public String getCarpeeshProectedPassword() {
		String carpeeshProtectedPassword = dataProperty.getProperty("CARPEESH_PROTECTED_PASSWORD");
		return carpeeshProtectedPassword;
	}

	public String getCTMSheet() {
		String ctmSheet = dataProperty.getProperty("CTM_SHEET");
		return ctmSheet;
	}

	// Payment Card Details get Property
	public String getCardNumber() {
		String cardNumber = dataProperty.getProperty("CARD_NUMBER");
		return cardNumber;
	}

	public String getExpiryDate() {
		String expiryDate = dataProperty.getProperty("EXPIRY_DATE");
		return expiryDate;
	}

	public String getCardVerificationCode() {
		String cardVerificationCode = dataProperty.getProperty("CSC");
		return cardVerificationCode;
	}

	public String getCardHolderName() {
		String cardHolderName = dataProperty.getProperty("CARD_HOLDER_NAME");
		return cardHolderName;
	}

	// Baileys Details Get Property
	public String getBaileysExcelFile() {
		String baileysExcel = configProperty.getProperty("BAILEYS_TEST_EXCEL");
		return baileysExcel;
	}

	public String getBaileysURL() {
		String baileysURL = dataProperty.getProperty("BAILEYS_URL");
		return baileysURL;
	}
	
	public String getProductBuiderURLForBaileys() {
		String productBuilderBaileysURL = dataProperty.getProperty("PRODUCT_BUILDER_BAILEYS_URL");
		return productBuilderBaileysURL;
	}

	public String getBaileySheetOne() {
		String baileySheetOne = dataProperty.getProperty("BAILETS_SHEET_ONE");
		return baileySheetOne;
	}

	public String getBaileySheetTwo() {
		String baileySheetTwo = dataProperty.getProperty("BAILETS_SHEET_TWO");
		return baileySheetTwo;
	}

	public String getBaileySheetThree() {
		String baileySheetThree = dataProperty.getProperty("BAILETS_SHEET_THREE");
		return baileySheetThree;
	}

	// SureVestor Details Get Property
	public String getSureVestorExcel() {
		String sureVestorExcel = configProperty.getProperty("SUREVESTOR_TEST_EXCEL");
		return sureVestorExcel;
	}

	public String getSureVestorURL() {
		String sureVestorURL = dataProperty.getProperty("SUREVESTOR_URL");
		return sureVestorURL;
	}

	// Carpeesh IHQ Automation
	public String getCarpeeshIHQExcel() {
		String carpeeshIHQExcel = configProperty.getProperty("CARPEESH_IHQ_AUTOMATION");
		return carpeeshIHQExcel;
	}

	public String getIHQServer() {
		String ihqServer = dataProperty.getProperty("TEST_SERVER");
		return ihqServer;
	}

	public String getIHQUserName() {
		String ihqUserName = dataProperty.getProperty("IHQ_USERNAME");
		return ihqUserName;
	}

	public String getIHQPassword() {
		String ihqPassword = dataProperty.getProperty("IHQ_PASSWORD");
		return ihqPassword;
	}

	// Release Automation Run on MasterQA
	public String getMasterQAURL() {
		String masterQAUrl = dataProperty.getProperty("MASTERQA_SERVER");
		return masterQAUrl;
	}

	public String getClearClientTransactionDataURL() {
		String clearClientTrasactionData = dataProperty.getProperty("CLEAR_CLIENT_TRANSACTION_DATA_URL");
		return clearClientTrasactionData;
	}

	public String getGhostClient() {
		String clearClientName = dataProperty.getProperty("GHOST_CLIENT_NAME");
		return clearClientName;
	}

	public String getClearClientKey() {
		String clearClientKey = dataProperty.getProperty("CLEAR_CLIENT_KEY");
		return clearClientKey;
	}

	public String getSalutation() {
		String salutation = dataProperty.getProperty("SALUTATION");
		return salutation;
	}

	public String getFirstName() {
		String firstName = dataProperty.getProperty("FIRST_NAME");
		return firstName;
	}

	public String getLastName() {
		String lastName = dataProperty.getProperty("LAST_NAME");
		return lastName;
	}

	public String getLegalName() {
		String getLegalName = dataProperty.getProperty("LEGAL_NAME");
		return getLegalName;
	}

	public String getGender() {
		String gender = dataProperty.getProperty("GENDER");
		return gender;
	}

	public String getEmailAddress() {
		String emailAddress = dataProperty.getProperty("EMAIL_ADDRESS");
		return emailAddress;
	}

	public String getClientDateOfBirth() {
		String clientDOB = dataProperty.getProperty("CLIENT_DATE_OF_BIRTH");
		return clientDOB;
	}

	public String getMailingAddress() {
		String mailingAddress = dataProperty.getProperty("MAILING_ADDRESS");
		return mailingAddress;
	}

	public String getPosition() {
		String position = dataProperty.getProperty("POSITION");
		return position;
	}

	public String getMobile() {
		String mobile = dataProperty.getProperty("MOBILE");
		return mobile;
	}

	public String getContactRelationship() {
		String contactRelationship = dataProperty.getProperty("CONTACT_RELEATIONSHIP");
		return contactRelationship;
	}

	public String getReleaseAutomationAdditionalFormMV() {
		String additionalFormMVExcel = configProperty.getProperty("RELEASE_AUTOMATION_ADDITIONAL_FORM_MV_EXCEL");
		return additionalFormMVExcel;
	}

	// Add New Task Get Details
	public String getClientName() {
		String clientName = dataProperty.getProperty("TASK_CLIENT_NAME");
		return clientName;
	}

	public String getAssignedToTask() {
		String assignedToTask = dataProperty.getProperty("ASSIGNED_TASK_TO");
		return assignedToTask;
	}

	public String getNoteSection() {
		String noteSection = dataProperty.getProperty("TASK_NOTE_SECTION");
		return noteSection;
	}

	public String getTaskSubject() {
		String taskSubject = dataProperty.getProperty("TASK_SUBJECT");
		return taskSubject;
	}

	public String getTaskDescription() {
		String getTaskDescription = dataProperty.getProperty("TASK_DESCRIPTION");
		return getTaskDescription;
	}

	// SM Test
	public String getSMExcel() {
		String smExcel = configProperty.getProperty("SM_TEST_EXCEL");
		return smExcel;
	}

	// Create Manual Invoice get Data.
	public String getInvoiceTransactionType() {
		String transactionType = dataProperty.getProperty("TRANSACTION_TYPE");
		return transactionType;
	}

	public String getInvoiceCurrency() {
		String currency = dataProperty.getProperty("CURRENCY");
		return currency;
	}

	public String getInvoiceBranch() {
		String branch = dataProperty.getProperty("BRANCH");
		return branch;
	}

	public String getInvoiceDetail() {
		String detail = dataProperty.getProperty("DETAIL");
		return detail;
	}

	public String getInvoiceDescription() {
		String description = dataProperty.getProperty("DESCRIPTION");
		return description;
	}

	public String getInvoiceExclusiveAmount() {
		String exclusiveAmount = dataProperty.getProperty("EXCLUSIVE_AMOUNT");
		return exclusiveAmount;
	}

	// Receipt Payment get property
	public String getPaymentAmount() {
		String paymentAmount = dataProperty.getProperty("PAYMENT_AMOUNT");
		return paymentAmount;
	}

	public String getPaymentType() {
		String paymentType = dataProperty.getProperty("PAYMENT_TYPE");
		return paymentType;
	}

	public String getDepartment() {
		String department = dataProperty.getProperty("DEPARTMENT");
		return department;
	}

	public String getPaymentReference() {
		String paymentReference = dataProperty.getProperty("PAYMENT_REFERENCE");
		return paymentReference;
	}

	public String getUnApprovedPaymentAmount() {
		String unapprovedPaymentAmount = dataProperty.getProperty("UNAPPROVED_PAYMENT_AMOUNT");
		return unapprovedPaymentAmount;
	}

	public String getUnApprovedReceiptReference() {
		String unapprovedReceiptReference = dataProperty.getProperty("UNAPPROVED_RECEIPT_REFERENCE");
		return unapprovedReceiptReference;
	}

	public String getRefundPaymentAmount() {
		String refundPaymentAmount = dataProperty.getProperty("REFUND_PAYMENT_AMOUNT");
		return refundPaymentAmount;
	}

	public String getRefundPaymentType() {
		String refundPaymentType = dataProperty.getProperty("REFUND_PAYMENT_TYPE");
		return refundPaymentType;
	}

	public String getRefundReference() {
		String refundReference = dataProperty.getProperty("REFUND_REFERENC");
		return refundReference;
	}

	// Add Reserve Get Property
	public String getAddReserveDescription() {
		String addReserveDescription = dataProperty.getProperty("ADD_RESERVE_DESCRIPTION");
		return addReserveDescription;
	}

	public String getAddReserveAmount() {
		String reserveAmount = dataProperty.getProperty("RESERVE_AMOUNT");
		return reserveAmount;
	}

	public String getAddReserveExcess() {
		String addReserveExcess = dataProperty.getProperty("EXCESS");
		return addReserveExcess;
	}

	public String getReasonForReserveAdjustment() {
		String reasonForReserveAjdustment = dataProperty.getProperty("REASON_FOR_RESERVE_ADJUSTMENT");
		return reasonForReserveAjdustment;
	}

	// Add New Creditor Get Property.
	public String getCreditorType() {
		String creditorType = dataProperty.getProperty("CREDITOR_TYPE");
		return creditorType;
	}

	public String getTradeTerms() {
		String tradeTerms = dataProperty.getProperty("TRADE_TERMS");
		return tradeTerms;
	}

	public String getCreditorCode() {
		String creditorCode = dataProperty.getProperty("CREDITOR_CODE");
		return creditorCode;
	}

	public String getCreditorBranch() {
		String creditorBranch = dataProperty.getProperty("CREDITOR_BRANCH");
		return creditorBranch;
	}

	public String getCompanyName() {
		String companyName = dataProperty.getProperty("COMPANY_NAME");
		return companyName;
	}

	public String getEditCreditorType() {
		String editCreditorType = dataProperty.getProperty("EDIT_CREDITOR_TYPE");
		return editCreditorType;
	}

	public String getCreditorEmail() {
		String creditorEmail = dataProperty.getProperty("CREDITOR_EMAIL");
		return creditorEmail;
	}

	public String getCreditorContactFirstName() {
		String creditorContactFirstName = dataProperty.getProperty("CREDITOR_CONTACT_FIRST_NAME");
		return creditorContactFirstName;
	}

	public String getCreditorContactLastName() {
		String creditorContactLastName = dataProperty.getProperty("CREDITOR_CONTACT_LAST_NAME");
		return creditorContactLastName;
	}

	public String getCreditorContactPosition() {
		String creditorContactPosition = dataProperty.getProperty("CREDITOR_CONTACT_POSITION");
		return creditorContactPosition;
	}

	public String getClientCodeRule() {
		String clientCodeRule = dataProperty.getProperty("CLIENT_CODE_RULE");
		return clientCodeRule;
	}

	// Images Get Property
	public String getUploadImage() {
		String uploadImage = configProperty.getProperty("IMAGES_PATH");
		return uploadImage;
	}

	// Get Excel Path for Renewal Policies Prior 365 Days
	public String getRenewalPolicesDataExcel() {
		String renewalPolicyExcel = configProperty.getProperty("RENEWAL_POLICIES_DATA_EXCEL");
		return renewalPolicyExcel;
	}

	// Get Excel Path for Renewal Policies Data Verification
	public String getRenewalVerificationExcel() {
		String renewalVerificationExcel = configProperty.getProperty("RENEWAL_DATA_VERIFICATION_EXCEL");
		return renewalVerificationExcel;
	}

	// Get NCB Import File
	public String getNCBImportExcel() {
		String ncbImportExcel = configProperty.getProperty("NCB_IMPORT");
		return ncbImportExcel;
	}

	// Get Renewal Cron URL
	public String getRenewalCronURL() {
		String renewalCronURL = dataProperty.getProperty("RENEWAL_CRON_URL");
		return renewalCronURL;
	}
}
