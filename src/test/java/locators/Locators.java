package locators;

import org.openqa.selenium.By;

public interface Locators {
	public interface carpeeshLocators {
		/* Car Detail Locators **********/
		String I_AGREE_BUTTON = "//button[@name='btn_accept_dod']";
		String INITIAL_I_AGREE_BUTTON = "//button[@id='btn_accept_initial']";
		String POLICY_START_DAY = "//select[@id='dp3_ds_0']";
		String POLICY_START_MONTH = "//select[@id='dp3_ms_0']";
		String POLICY_START_YEAR = "//select[@id='dp3_yt_0']";
		String PREVIOUS_INSURER = "//select[@name='Previous insurer_dropdown__0___field4']";
		String ESTIMATED_ANNUAL_KILOMETER = "//select[@name='Estimated annual kilometres_dropdown__0___field5']";
		String CAR_REGISTRATION_PLAT = "//input[@name='Car registration plate_text__0___field6_']";
		String CAR_MAKE = "//select[@name='Car make_dropdown__0___field7']";
		String CAR_MODEL = "//select[@name='Car model_dropdown__0___field8']";
		String CAR_YEAR_MANUFACTURE = "//select[@name='Car year of manufacture_dropdown__0___field9']";
		String CAR_DESCRIPTION = "//select[@name='Car description_dropdown__0___field10']";
		String CAR_REGISTRATION_MONTH = "//select[@name='Car registration month_dropdown__0___field11']";
		String MARKET_VALUE = "//li[@id='fe_14_1238']//div//div[1]//label[1]";
		String AGREED_VALUE = "//li[@id='fe_14_1238']//div[2]//label[1]";
		String CAR_FINANCE = "//select[@name='Car finance_dropdown__0___field17']";
		String CAR_ANTI_THEFT_DEVICE = "//select[@name='Car anti-theft device_dropdown__0___field18']";
		String DOES_CAR_HAVE_NON_STANDARD_MODIFICATIONS_OR_ACCESSORIES_YES = "//li[@id='fe_19_1238']//div//div[1]//label[1]";
		String DOES_CAR_HAVE_NON_STANDARD_MODIFICATIONS_OR_ACCESSORIES_NO = "//li[@id='fe_19_1238']//div[2]//label[1]";
		String DOES_CAR_HAVE_PRE_EXISTING_DAMAGE_YES = "//li[@id='fe_21_1238']//div//div[1]//label[1]";
		String DOES_CAR_HAVE_PRE_EXISTING_DAMAGE_NO = "//li[@id='fe_21_1238']//div[2]//label[1]";
		String CAR_HAS_RUST_DAMAGE_YES = "//*[@id='fe_24_1238']/div/span[2]/span/div[1]/label";
		String CAR_HAS_RUST_DAMAGE_NO = "//*[@id='fe_24_1238']/div/span[2]/span/div[2]/label";
		String CAR_HAS_WINDSCREEN_DAMAGE_YES = "//li[@id='fe_25_1238']//div//div[1]//label[1]";
		String CAR_HAS_WINDSCREEN_DAMAGE_NO = "//li[@id='fe_25_1238']//div[2]//label[1]";
		String CAR_HAS_HAIL_DAMAGE_YES = "//li[@id='fe_26_1238']//div//div[1]//label[1]";
		String CAR_HAS_HAIL_DAMAGE_NO = "//li[@id='fe_26_1238']//div[2]//label[1]";
		String AGREED_VALUE_TEXTBOX = "//input[@name='Agreed value_text__0___field16_']";
		String CAR_DAMAGE_AREA = "//*[@id='fe_22_1238']/div/span[2]/select";
		String IS_DAMAGE_LARGER_THAN_ONE_HAND_SIZE_YES = "//*[@id='fe_23_1238']/div/span[2]/span/div[1]/label";
		String IS_DAMAGE_LARGER_THAN_ONE_HAND_SIZE_NO = "//*[@id='fe_23_1238']/div/span[2]/span/div[2]/label";

		// Does the car have non-standard modifications or accessories? Locators
		String ALCOHOL_INTERLOCK = "//*[@id='form_ul']/li[23]/div/span[2]/table/tbody/tr[1]/td[1]/div/label";
		String ALLOY_MAG_WHEELS = "//*[@id='form_ul']/li[23]/div/span[2]/table/tbody/tr[1]/td[2]/div/label";

		/* Car Usage Page Locators **********/
		String CAR_USAGE = "//select[@name='Car usage_dropdown__0___field27']";
		String BUSINESS_USE = "//select[@name='Business use_dropdown__0___field28']";
		String GREY_FLEET = "//*[@id='fe_30_1238']/div/span[2]/select";
		String RIDE_SHARE = "//select[@name='Rideshare_dropdown__0___field29']";
		String CAR_USAGE_NEXT_BUTTON = "//a[@class='ct_next_step']";

		/* Kept address details Locators */
		String SAME_AS_POSTAL_ADDRESS = "//li[@id='fe_32_1238']//label[@class='label_check default']";
		String KEPT_AT_NIGHT_ADDRESS = "*//span//input[@placeholder = 'Enter Address Here...']";
		String KEPT_AT_NIGHT_CITY = "*//span//input[@name= 'Kept at night city_text__0___field36_']";
		String KEPT_AT_NIGHT_STATE = "*//span//input[@name= 'Kept at night state_text__0___field37_']";
		String KEPT_AT_NIGHT_POSTCODE = "*//span//input[@name= 'Kept at night postcode_text__0___field38_']";
		String SAME_AS_KEPT_AT_NIGHT_ADDRESS = "//li[@id='fe_40_1238']//label[@class='label_check default']";
		String KEPT_AT_NIGHT_SITUATION = "//select[@name='Kept at night situation_dropdown__0___field33']";
		String KEPT_DURING_THE_DAY_SITUATION = "//select[@name='Kept during the day situation_dropdown__0___field41']";

		// Driver Details page Locators

		// Driver 1 Locators
		String PERMITTED_DRIVER_AGE_RANGE = "//select[@name='Permitted driver age range_dropdown__0___field47']";
		String LICENCE_COUNTRY = "//select[@name='Licence country_dropdown__0___field57']";
		String LICENCE_TYPE = "//select[@name='Licence type_dropdown__0___field56']";
		String LICENCE_YEARS = "//select[@name='Licence years_dropdown__0___field58']";
		String HAS_THIS_DRIVER_LICENCE_SUSPENSION_OR_CANCELLATION_YES = "//li[@id='fe_59_1238']//div//div[1]//label[1]";
		String HAS_THIS_DRIVER_LICENCE_SUSPENSION_OR_CANCELLATION_NO = "//li[@id='fe_59_1238']//div//div[2]//label[1]";
		String HAS_THIS_DRIVER_ANY_ALCOHOL_OR_DRUG_YES = "//li[@id='fe_60_1238']//div//div[1]//label[1]";
		String HAS_THIS_DRIVER_ANY_ALCOHOL_OR_DRUG_NO = "//li[@id='fe_60_1238']//div//div[2]//label[1]";
		String HAS_THIS_DRIVER_THEIR_CAR_INSURANCE_CANCELLED_YES = "//li[@id='fe_62_1238']//div//div[1]//label[1]";
		String HAS_THIS_DRIVER_THEIR_CAR_INSURANCE_CANCELLED_NO = "//li[@id='fe_62_1238']//div//div[2]//label[1]";
		String HAS_THIS_DRIVER_HAD_ANY_CLAIM_PAST_5YEAR_YES = "//li[@id='fe_63_1238']//div//div[1]//label[1]";
		String HAS_THIS_DRIVER_HAD_ANY_CLAIM_PAST_5YEAR_NO = "//li[@id='fe_63_1238']//div//div[2]//label[1]";
		String CRIMINAL_HISTORY_YES = "//li[@id='fe_64_1238']//div//div[1]//label[1]";
		String CRIMINAL_HISTORY_NO = "//li[@id='fe_64_1238']//div//div[2]//label[1]";
		String BANKRUPTCY_YES = "//li[@id='fe_65_1238']//div//div[1]//label[1]";
		String BANKRUPTCY_NO = "//li[@id='fe_65_1238']//div//div[2]//label[1]";
		String IS_THERE_2ND_DRIVER_YES = "//li[@id='fe_66_1238']//div//div[1]//label[1]";
		String IS_THERE_2ND_DRIVER_NO = "//li[@id='fe_66_1238']//div//div[2]//label[1]";
		String DRIVER_DETAIL_NEXT_BUTTON = "//a[@class='ct_next_step']";
		String REVIEW_PAGE_NEXT_BUTTON = "//a[@class='ct_next_step']";
		String ADD_CLAIM_BUTTON = "//input[@id='custom_table3']";
		String CLAIM_TYPE = "//select[@id='ctri-3-1-9']";
		String CLAIM_MONTH = "//select[@id='ctri-3-1-10']";
		String CLAIM_YEAR = "//select[@id='ctri-3-1-11']";
		String DID_YOU_PAY_AN_EXCESS_YES = "//li[@class='nform_li ct_column_control3 show_hide_custom_table custom_table3 CUSTOM_TABLE_FIELD_POSITION inline5 custom_table_row_1-3 left']//span[1]//input[1]";
		String DID_YOU_PAY_AN_EXCESS_NO = "//span[@class='input_cover text_cover ng-scope']//span[2]//input[1]";

		// Driver 2 Locators
		String REMOVE_2ND_DRIVER_FROM_POLICY_YES = "//li[@id='fe_67_1238']//div//div[1]//label[1]";
		String REMOVE_2ND_DRIVER_FROM_POLICY_NO = "//li[@id='fe_67_1238']//div//div[2]//label[1]";
		String DRIVER2_SALUTATION = "//select[@name='Driver 2 Salutation_dropdown__0___field68']";
		String DRIVER2_FIRSTNAME = "//input[contains(@name,'First name_text__0___field69_')]";
		String DRIVER2_FAMILYNAME = "//input[contains(@name,'Driver 2 Family name_text__0___field70_')]";
		String DRIVER2_GENDER = "//select[contains(@name,'Driver 2 Gender_dropdown__0___field71')]";
		String DRIVER2_DAY = "//select[@id='dp3_ds_3']";
		String DRIVER2_MONTH = "//select[@id='dp3_ms_3']";
		String DRIVER2_YEAR = "//select[@id='dp3_yt_3']";
		String DRIVER2_MOBILE = "//input[contains(@name,'Driver 2 Mobile_text__0___field73_')]";
		String DRIVER2_LICENCE_TYPE = "//select[contains(@name,'Driver 2 Licence type_dropdown__0___field75')]";
		String DRIVER2_LICENCE_COUNTRY = "//select[contains(@name,'Driver 2 Licence country_dropdown__0___field76')]";
		String DRIVER2_LICENCE_YEAR = "//select[contains(@name,'Driver 2 Licence years_dropdown__0___field77')]";
		String DRIVER2_HAS_BEEN_LICENCE_SUSPENSION_OR_CANCELLATION_YES = "//li[@id='fe_78_1238']//div//div[1]//label[1]";
		String DRIVER2_HAS_BEEN_LICENCE_SUSPENSION_OR_CANCELLATION_NO = "//li[@id='fe_78_1238']//div//div[2]//label[1]";
		String DRIVER2_HAS_DRIVER_ALCOHOL_OR_DRUG_YES = "//li[@id='fe_79_1238']//div//div[1]//label[1]";
		String DRIVER2_HAS_DRIVER_ALCOHOL_OR_DRUG_NO = "//li[@id='fe_79_1238']//div//div[2]//label[1]";
		String DRIVER2_HAS_DRIVER_CAR_INSURANCE_CANCELLED_YES = "//li[@id='fe_81_1238']//div//div[1]//label[1]";
		String DRIVER2_HAS_DRIVER_CAR_INSURANCE_CANCELLED_NO = "//li[@id='fe_81_1238']//div//div[2]//label[1]";
		String DRIVER2_HAS_DRIVER_HAD_ANY_CLAIM_PAST_5YEAR_YES = "//li[@id='fe_82_1238']//div//div[1]//label[1]";
		String DRIVER2_HAS_DRIVER_HAD_ANY_CLAIM_PAST_5YEAR_NO = "//li[@id='fe_82_1238']//div//div[2]//label[1]";
		String DRIVER2_CRIMINAL_HISTORY_YES = "//li[@id='fe_83_1238']//div//div[1]//label[1]";
		String DRIVER2_CRIMINAL_HISTORY_NO = "//li[@id='fe_83_1238']//div//div[2]//label[1]";
		String DRIVER2_BANKRUPTCY_YES = "//li[@id='fe_84_1238']//div//div[1]//label[1]";
		String DRIVER2_BANKRUPTCY_NO = "//li[@id='fe_84_1238']//div//div[2]//label[1]";
		String IS_THERE_3RD_DRIVER_YES = "//li[@id='fe_85_1238']//div//div[1]//label[1]";
		String IS_THERE_3RD_DRIVER_NO = "//li[@id='fe_85_1238']//div//div[2]//label[1]";
		String ADD_CLAIM_BUTTON_DRIVER2 = "//input[@id='custom_table4']";
		String DRIVER2_CLAIM_TYPE = "//*[@id='ctri-4-1-7']";
		String DRIVER2_CLAIM_MONTH = "//*[@id='ctri-4-1-8']";
		String DRIVER2_CLAIM_YEAR = "//*[@id='ctri-4-1-13']";
		String DRIVER2_DID_YOU_PAY_AN_EXCESS_YES = "//li[@class='nform_li ct_column_control4 show_hide_custom_table custom_table4 CUSTOM_TABLE_FIELD_POSITION inline5 custom_table_row_1-4 left']//span[1]//input[1]";
		String DRIVER2_DID_YOU_PAY_AN_EXCESS_NO = "//span//input[@id='ctri-4-1-14' and @value='No']";

		// Driver 3 Locators
		String DRIVER3_REMOVE_3RD_DRIVER_FROM_POLICY_YES = "//li[@id='fe_86_1238']//div//div[1]//label[1]";
		String DRIVER3_REMOVE_3RD_DRIVER_FROM_POLICY_NO = "//li[@id='fe_86_1238']//div[2]//label[1]";
		String DRIVER3_SALUTATION = "//select[@name='Driver 3 Salutation_dropdown__0___field87']";
		String DRIVER3_FIRST_NAME = "//input[@name='Driver 3 First name_text__0___field88_']";
		String DRIVER3_FAMILY_NAME = "//input[@name='Driver 3 Family name_text__0___field89_']";
		String DRIVER3_GENDER = "//select[@name='Driver 3 Gender_dropdown__0___field90']";
		String DRIVER3_DAY = "//select[@id='dp3_ds_4']";
		String DRIVER3_MONTH = "//select[@id='dp3_ms_4']";
		String DRIVER3_YEAR = "//select[@id='dp3_yt_4']";
		String DRIVER3_MOBILE = "//input[@name='Driver 3 Mobile_text__0___field92_']";
		String DRIVER3_LICENCE_TYPE = "//select[@name='Driver 3 Licence type_dropdown__0___field94']";
		String DRIVER3_LICENCE_COUNTRY = "//select[@name='Driver 3 Licence country_dropdown__0___field95']";
		String DRIVER3_LICENCE_YEAR = "//select[@name='Driver 3 Licence years_dropdown__0___field96']";
		String DRIVER3_HAS_BEEN_UNDER_SUSPENSION_OR_CANCELLATION_YES = "//li[@id='fe_97_1238']//div//div[1]//label[1]";
		String DRIVER3_HAS_BEEN_UNDER_SUSPENSION_OR_CANCELLATION_NO = "//li[@id='fe_97_1238']//div//div[2]//label[1]";
		String DRIVER3_HAD_ANY_ALCOHOL_OR_DRUG_YES = "//li[@id='fe_98_1238']//div//div[1]//label[1]";
		String DRIVER3_HAD_ANY_ALCOHOL_OR_DRUG_NO = "//li[@id='fe_98_1238']//div//div[2]//label[1]";
		String DRIVER3_HAD_THEIR_CAR_INSURANCE_CANCELLED_YES = "//li[@id='fe_100_1238']//div//div[1]//label[1]";
		String DRIVER3_HAD_THEIR_CAR_INSURANCE_CANCELLED_NO = "//li[@id='fe_100_1238']//div//div[2]//label[1]";
		String DRIVER3_HAD_ANY_CLAIM_IN_LAST_5_YEAR_YES = "//li[@id='fe_101_1238']//div//div[1]//label[1]";
		String DRIVER3_HAD_ANY_CLAIM_IN_LAST_5_YEAR_NO = "//li[@id='fe_101_1238']//div//div[2]//label[1]";
		String DRIVER3_CRIMINAL_HISTORY_YES = "//li[@id='fe_102_1238']//div//div[1]//label[1]";
		String DRIVER3_CRIMINAL_HISTORY_NO = "//li[@id='fe_102_1238']//div//div[2]//label[1]";
		String DRIVER3_BANKRUPTCY_YES = "//li[@id='fe_103_1238']//div//div[1]//label[1]";
		String DRIVER3_BANKRUPTCY_NO = "//li[@id='fe_103_1238']//div//div[2]//label[1]";
		String IS_THERE_4TH_DRIVER_YES = "//li[@id='fe_104_1238']//div//div[1]//label[1]";
		String IS_THERE_4TH_DRIVER_NO = "//li[@id='fe_104_1238']//div//div[2]//label[1]";
		String DRIVER3_CLAIM_TYPE = "//*[@id='ctri-5-1-15']";
		String DRIVER3_CLAIM_MONTH = "//*[@id='ctri-5-1-16']";
		String DRIVER3_CLAIM_YEAR = "//*[@id='ctri-5-1-17']";
		String DRIVER3_DID_YOU_PAY_EXCESS_YES = "/html/body/div[1]/div/div[3]/form/ul/li[131]/div/span[2]/span[1]/input";
		String DRIVER3_DID_YOU_PAY_EXCESS_NO = "/html/body/div[1]/div/div[3]/form/ul/li[131]/div/span[2]/span[2]/input";
		String ADD_CLAIM_BUTTON_DRIVER3 = "//input[@id='custom_table5']";

		// Option and Pay Page Locators
		String PAYMENT_TYPE = "//select[@name='Payment type_dropdown__0___field226']";
		String ADJUST_EXCESS_BY = "//select[@name='Adjust excess by_dropdown__0___field228']";
		String OPTIONAL_WINDSCREEN_BENEFIT_YES = "//li[@id='fe_230_1238']//div//div[1]//label[1]";
		String OPTIONAL_WINDSCREEN_BENEFIT_NO = "//li[@id='fe_230_1238']//div//div[2]//label[1]";
		String ADD_HIRE_CAR_ACCIDENT_BENEFIT_YES = "//li[@id='fe_231_1238']//div//div[1]//label[1]";
		String ADD_HIRE_CAR_ACCIDENT_BENEFIT_NO = "//li[@id='fe_231_1238']//div//div[2]//label[1]";
		String PROMO_CODE = "//input[@name='Unused field11_text__false___field292_']";
		String CONTINUE_TO_PAYMENT_BUTTON = "//button[@class='submit_button nform_btn boots']";

		// Payment Button
		String PAY_NOW_BUTTON = "//td[@class='bottom_buttons']//button[@name='accept'][contains(text(),'Pay NOW')]";
		String CARD_NUMBER = "//input[@id='cardnumber']";
		String EXPIRY_DATE = "//input[@id='expirydate']";
		String CARD_VERIFICATION_CODE = "//input[@id='cardverificationcode']";
		String CARD_HOLDER_NAME = "//input[@id='cardholder']";
		String PAY_BUTTON = "//button[@class='payment-button']";
	}

	public interface ctmLocators {
		// CTM JSON Form Locators
		String CTM_JSON_TEXTAREA = "//textarea[@id='ctm_json']";
		String CTM_PASSCODE = "//input[@id='passcode']";
		String CTM_SUBMIT_BUTTON = "//input[@name='select_carpeesh']";
		String CARPEESH_ENTER_BUTTON = "//*[@id='cb-content']/form/p[2]/input";

		// Online Quote Form Locators
		String CTM_I_AGREE_BUTTON = "/html/body/div/div/div/div/div/div/form/table/tbody/tr[2]/td/button";

		/* Car Usage Locators */
		String CTM_CAR_USAGE_NEXT_BUTTON = "//*[@id='custom_tab_p_n_button_li']/a[2]";

		/* Kept address details Locators */
		String CTM_SAME_AS_POSTAL_ADDRESS = "//*[@id='fe_32_1238']/div/span[2]/span/div/label";
		String CTM_SAME_AS_KEPT_AT_NIGHT_ADDRESS = "//*[@id='fe_40_1238']/div/span[2]/span/div/label";
		String CTM_KEPT_DURING_THE_DAY_SITUATION = "//*[@id='fe_41_1238']/div/span[2]/select";

		/* Driver details Locators */
		String CTM_LICENCE_COUNTRY = "//*[@id='fe_57_1238']/div/span[2]/select";
		String CTM_LICENCE_TYPE = "//*[@id='fe_56_1238']/div/span[2]/select";
		String CTM_LICENCE_YEAR = "//*[@id='fe_58_1238']/div/span[2]/select";
		String CTM_DRIVER_HAS_BEEN_UNDER_LICENCE_SUSPENSIION_PAST_5_YEAR_YES = "//*[@id='fe_59_1238']/div/span[2]/span/div[1]/label";
		String CTM_DRIVER_HAS_BEEN_UNDER_LICENCE_SUSPENSIION_PAST_5_YEAR_NO = "//*[@id='fe_59_1238']/div/span[2]/span/div[2]/label";
		String CTM_DRIVER_HAD_ALCOHOL_DRUG_PAST_5_YEAR_YES = "//*[@id='fe_60_1238']/div/span[2]/span/div[1]/label";
		String CTM_DRIVER_HAD_ALCOHOL_DRUG_PAST_5_YEAR_NO = "//*[@id='fe_60_1238']/div/span[2]/span/div[2]/label";
		String CTM_DRIVER_HAD_THEIR_CAR_INSURANCE_CANCELLED_YES = "//*[@id='fe_62_1238']/div/span[2]/span/div[1]/label";
		String CTM_DRIVER_HAD_THEIR_CAR_INSURANCE_CANCELLED_NO = "//*[@id='fe_62_1238']/div/span[2]/span/div[2]/label";
		String CTM_CRIMINAL_HISTORY_YES = "//*[@id='fe_64_1238']/div/span[2]/span/div[1]/label";
		String CTM_CRIMINAL_HISTORY_NO = "//*[@id='fe_64_1238']/div/span[2]/span/div[2]/label";
		String CTM_BANKRUPTCY_YES = "//*[@id='fe_65_1238']/div/span[2]/span/div[1]/label";
		String CTM_BANKRUPTCY_NO = "//*[@id='fe_65_1238']/div/span[2]/span/div[2]/label";
		String CTM_IS_THERE_2ND_DRIVER_YES = "//*[@id='fe_66_1238']/div/span[2]/span/div[1]/label";
		String CTM_IS_THERE_2ND_DRIVER_NO = "//*[@id='fe_66_1238']/div/span[2]/span/div[2]/label";

		// Option and Pay Page Locators
		String CTM_OPTIONAL_WINDSCREEN_BENEFIT_YES = "//*[@id='fe_230_1238']/div/span[2]/span/div[1]/label";
		String CTM_OPTIONAL_WINDSCREEN_BENEFIT_NO = "//*[@id='fe_230_1238']/div/span[2]/span/div[2]/label";
		String CTM_ADD_HIRE_CAN_AFTER_ACCIDENT_BENEFIT_YES = "//*[@id='fe_231_1238']/div/span[2]/span/div[1]/label";
		String CTM_ADD_HIRE_CAN_AFTER_ACCIDENT_BENEFIT_NO = "//*[@id='fe_231_1238']/div/span[2]/span/div[2]/label";
		String CTM_CONTINUE_TO_PAYMENT_BUTTON = "//*[@id='fe_248_1238']/div/button";

		// Payment Page
		String CTM_PAY_NOW_BUTTON = "//*[@id='form_ul']/div[2]/table/tbody/tr/td/button";
		String CTM_PAY_BUTTON = "//*[@id='CREDIT_CARD']/form/div[3]/button";
	}

	public interface baileysLocators {
		// Create New Client for Baileys Locators
		String CLIENT_CODE = "//input[@id='client_code']";
		String BRANCH = "//select[@id='branch']";

		// Pleasure Craft Page Locators
		String B_NEXT_BUTTON = "//button[@id='btn_save_client']";
		String IS_YOUR_BOAT_HOME_BUILT = "//*[@id='fe_99_1233']/div/span[2]/span/div[2]/label";
		String IS_THIS_VESSEL_CURRENTLY_VERO_INSURED_YES = "//li[@id='fe_0_1233']//div//div[1]//label[1]";
		String IS_THIS_VESSEL_CURRENTLY_VERO_INSURED_NO = "//li[@id='fe_0_1233']//div[2]//label[1]";
		String TYPE_OF_BOAT = "//select[@name='Type of Boat_dropdown__1___field2']";
		String TYPE_OF_MOORING = "//select[@name='Type of Mooring_dropdown__1___field4']";
		String MOORING_LOCATION = "//select[@name='Mooring Location_dropdown__0___field5']";
		String DO_YOU_HAVE_A_TRAILER_YES = "//*[@id='do_you_have_a_trailer_li']/div/span[2]/span/div[1]/label";
		String DO_YOU_HAVE_A_TRAILER_NO = "//*[@id='do_you_have_a_trailer_li']/div/span[2]/span/div[2]/label";
		String TRAILER_REGISTRATION_NO = "//input[@name='Trailer Registration No._text__false___field8_']";
		String DATE_YOU_PURCHASED_YOUR_BOAT = "//input[@id='date_input_9']";
		String ADVISE_THE_PRICE_PAID_FOR_YOUR_BOAT = "//input[@name='Please advise the price you paid for your boat_text__0___field10_']";
		String TOTAL_BOATING_EXPERIENCE_DO_YOU_HAVE = "//select[@name='How many years total boating experience do you have?_dropdown__1___field11']";
		String ARE_YOU_AN_EXISTING_CLIENT_OF_BAILEYS_YES = "//li[@id='fe_12_1233']//div//div[1]//label[1]";
		String ARE_YOU_AN_EXISTING_CLIENT_OF_BAILEYS_NO = "//li[@id='fe_12_1233']//div[2]//label[1]";

		// Boat Use Locators
		String IS_YOUR_BOAT_USED_FOR_BUSINESS_CHARTER_PURPOSE_YES = "//li[@id='fe_14_1233']//div//div[1]//label[1]";
		String IS_YOUR_BOAT_USED_FOR_BUSINESS_CHARTER_PURPOSE_NO = "//li[@id='fe_14_1233']//div[2]//label[1]";
		String COMMERICIAL_YES = "//li[@id='fe_15_1233']//div//div[1]//label[1]";
		String COMMERICIAL_NO = "//li[@id='fe_15_1233']//div[2]//label[1]";
		String CHARTER_YES = "//li[@id='fe_16_1233']//div//div[1]//label[1]";
		String CHARTER_NO = "//li[@id='fe_16_1233']//div[2]//label[1]";
		String SHARED_OWNERSHIP_YES = "//li[@id='fe_17_1233']//div//div[1]//label[1]";
		String SHARED_OWNERSHIP_NO = "//li[@id='fe_17_1233']//div[2]//label[1]";
		String DO_YOU_CURRENTLY_LIVE_ABOARD_VESSEL_YES = "//*[@id='fe_15_1233']/div/span[2]/span/div[1]/label";
		String DO_YOU_CURRENTLY_LIVE_ABOARD_VESSEL_NO = "//*[@id='fe_15_1233']/div/span[2]/span/div[2]/label";

		// Policy Commencement Date Locators
		String INSURANCE_FROM = "//input[@id='date_input_17']";
		String INSURANCE_TO = "//input[@id='date_input_21']";

		// Sum Insured (NZD) Locators
		String FIXITURE_FITTING_MACHINERY_ACCESSORIES_VESSEL_NZD = "//*[@id='fe_20_1233']/div/span[2]/input";
		String TRAILER_NZD = "//*[@id='fe_21_1233']/div/span[2]/input";
		String DO_YOU_NEED_COVER_FISHING_DRIVING_WHEN_LEFT_YOUR_BOAT_YES = "//body/div[1]/form[1]/ul[1]/li[26]/div[1]/span[2]/span[1]/div[1]/label[1]";
		String DO_YOU_NEED_COVER_FISHING_DRIVING_WHEN_LEFT_YOUR_BOAT_NO = "//body/div[1]/form[1]/ul[1]/li[26]/div[1]/span[2]/span[1]/div[2]/label[1]";
		String CLAIMS_EXCESS = "//*[@id='fe_37_1233']/div/span[2]/select";
		String FISHING_OR_DIVING_GEAR_NZD = "//input[@name='Fishing or Diving Gear NZD $_text__false___field27_']";
		String ARE_ANY_INDIVIDUAL_ITEM_VALUED_AT_OVER_YES = "//li[@id='fe_28_1233']//label[@class='label_radio circle-ticked']";

		// Vessel Details Locators
		String PLEASE_ADVISE_THE_YEAR_BOAT_WAS_ORIGINALLY_BUILT = "//*[@id='fe_39_1233']/div/span[2]/select";
		String CONSTRUCTION = "//*[@id='fe_42_1233']/div/span[2]/select";
		String BOAT_NAME = "//*[@id='fe_44_1233']/div/span[2]/input";
		String MAKE_MODEL = "//*[@id='fe_45_1233']/div/span[2]/input";
		String LENGTH = "//*[@id='fe_46_1233']/div/span[2]/input";
		String UNIT_YES = "//*[@id='fe_47_1233']/div/span[2]/span/div[1]/label";
		String UNIT_NO = "//*[@id='fe_47_1233']/div/span[2]/span/div[2]/label";
		String TYPE_OF_MOTOR = "//*[@id='fe_48_1233']/div/span[2]/select";
		String CONVERTED_CAR_MOTOR_YES = "//*[@id='fe_49_1233']/div/span[2]/span/div[1]/label";
		String CONVERTED_CAR_MOTOR_NO = "//*[@id='fe_49_1233']/div/span[2]/span/div[2]/label";
		String HAS_THIS_MOTOR_PROFESSIONALLY_INSTALLED_YES = "//*[@id='fe_50_1233']/div/span[2]/span/div[1]/label";
		String HAS_THIS_MOTOR_PRODESSIONALLY_INSTALLED_NO = "//*[@id='fe_50_1233']/div/span[2]/span/div[2]/label";
		String MAKE_OF_MOTOR = "//*[@id='fe_51_1233']/div/span[2]/input";
		String YEAR_OF_MANUFACTURE = "//*[@id='fe_52_1233']/div/span[2]/input";
		String HP = "//*[@id='fe_53_1233']/div/span[2]/input";
		String FUEL_PETROL = "//*[@id='fe_54_1233']/div/span[2]/span/div[1]/label";
		String FUEL_DIESEL = "//*[@id='fe_54_1233']/div/span[2]/span/div[2]/label";
		String MAX_SPEED_IN_KNOTS = "//select[@name='Max Speed in Knots_dropdown__0___field46']";

		// Cover Specifics Locators
		String IS_THERE_INTERESTED_PARTY_INVOLVED_YES = "//*[@id='fe_66_1233']/div/span[2]/span/div[1]/label";
		String IS_THERE_INTERESTED_PARTY_INVOLVED_NO = "//*[@id='fe_66_1233']/div/span[2]/span/div[2]/label";
		String NAME_OF_INTERESTED_PARTY = "//*[@id='fe_67_1233']/div/span[2]/input";
		String DO_YOU_NEED_ADDITIONAL_OWNER_POLICY_YES = "//*[@id='fe_68_1233']/div/span[2]/span/div[1]/label";
		String DO_YOU_NEED_ADDITIONAL_OWNER_POLICY_NO = "//*[@id='fe_68_1233']/div/span[2]/span/div[2]/label";
		String ADDITIONAL_OWNER_NAME = "//*[@id='fe_69_1233']/div/span[2]/input";
		String PREVIOUS_BOAT_INSURANCE = "//*[@id='fe_70_1233']/div/span[2]/input";
		String COASTGUARD_MASTER_YES = "//*[@id='fe_71_1233']/div/span[2]/span/div[1]/label";
		String COASTGUARD_MASTER_NO = "//*[@id='fe_71_1233']/div/span[2]/span/div[2]/label";
		String COASTGUARD_DISCOUNT = "//*[@id='fe_72_1233']/div/span[2]/input";
		String DO_YOU_RACE_WITH_USE_OF_EXTRAS_YES = "//*[@id='fe_58_1233']/div/span[2]/span/div[1]/label";
		String DO_YOU_RACE_WITH_USE_OF_EXTRAS_NO = "//*[@id='fe_58_1233']/div/span[2]/span/div[2]/label";

		// Customer Referral Programme Locators
		String HOW_DID_YOU_HEAR_ABOUT_BAILEYS = "//*[@id='fe_74_1233']/div/span[2]/select";

		// Declaration Section Locators
		String DECLARATION_OPTION_A_YES = "//*[@id='fe_83_1233']/div/span[2]/span/div[1]/label";
		String DECLARATION_OPTION_A_NO = "//*[@id='fe_83_1233']/div/span[2]/span/div[2]/label";
		String DECLARATION_OPTION_B_YES = "//li[@id='fe_88_1233']//div//div[1]//label[1]";
		String DECLARATION_OPTION_B_NO = "//*[@id='fe_85_1233']/div/span[2]/span/div[2]/label";
		String DECLARATION_OPTION_C_YES = "//li[@id='fe_90_1233']//div//div[1]//label[1]";
		String DECLARATION_OPTION_C_NO = "//*[@id='fe_87_1233']/div/span[2]/span/div[2]/label";
		String DECLARATION_OPTION_D_YES = "//li[@id='fe_92_1233']//div//div[1]//label[1]";
		String DECLARATION_OPTION_D_NO = "//*[@id='fe_89_1233']/div/span[2]/span/div[2]/label";
		String SUBMIT_BUTTON = "//button[@class='submit_button nform_btn boots']";
		String CANCEL_BUTTON = "//*[@id='form_ul']/input";
	}

	public interface sureVestorLocators {
		// Scheer Landlord Protection Insurance Policy
		String S_NEXT_BUTTON = "//button[@id='btn_save_client']";

		// Property Management Agent
		String NAME_OF_PROPERTY_MANAGEMENT_AGENCY = "//input[@name='Name of Property Management Agency_text__1___field2_']";
		String PROPERTY_MANAGER = "//select[@name='Property Manager_dropdown__0___field3']";

		// Residential Rental Property to be Insured
		String TYPE_OF_DWELLING = "//select[@name='Type of Dwelling_dropdown__1___field5']";
		String ADDRESS1 = "//input[@placeholder='Enter Address Here...']";
		String CITY = "//input[@name='City_text__0___field9_']";
		String STATE = "//select[@name='State_dropdown__1___field10']";
		String STATE_CODE = "//input[@name='State Code_text__0___field11_']";
		String ZIP_CODE = "//input[@name='ZIP Code_text__0___field12_']";

		// Please answer the following questions.
		String HAS_THIS_TENANT_BEEN_BEHIND_MORE_THAN_5_DAYS_LAST_2_MONTH_NO = "//*[@id='fe_16_1231']/div/span[2]/span/div[1]/label";
		String WHAT_IS_THE_MONTHLY_RENT_FOR_RENTAL_PROPERTY = "//input[@name='What is the monthly rent for the residential rental property?_text_integers_1___field18_']";
		String HAS_THE_RENTAL_PROPERTY_UNOCCUPIED_FOR_60_DAYS_MORE_NO = "//*[@id='fe_20_1231']/div/span[2]/span/div[1]/label";

		// Insurance Benefits Required
		String LEVEL_OF_BENEFITS_REQUIRED = "//select[@name='Level of benefits required_dropdown__1___field24']";
		String DO_YOU_WISH_TO_ADD_TERRORISM_POLICY_AN_ADDITIONAL_PREMIUM_NO = "//*[@id='fe_26_1231']/div/span[2]/span/div[1]/label";
		String DO_YOU_WISH_TO_ADD_TERRORISM_POLICY_AN_ADDITIONAL_PREMIUM_YES = "//*[@id='fe_26_1231']/div/span[2]/span/div[2]/label";
	}

	public interface carpeeshClientPolicyLocators {
		// IHQ Login
		String CLICK_HERE_LOGIN = "//a[text()='Click here'][1]";
		String IHQ_USERNAME = "//input[@id='username']";
		String IHQ_PASSWORD = "//input[@id='password']";
		String IHQ_ACCOUNT_SELECT = "//select[@id='post_master_user_id']";
		String IHQ_SIGNIN_BUTTON = "//input[@id='login-submit']";

		// Create new client locators
		String CONTACT_NAV = "//a[contains(text(),'Contacts')]";
		String CLIENT_BUTTON = "//div[@class='x_title']//a[3]";
		String VULNERABLE_PERSON = "//select[@name='Vulnerable Person_dropdown__0___field0']";
		String CLIENT_ADDITIONAL_FIELD_SUBMIT_BUTTON = "//button[@class='submit_button nform_btn boots']";

		// Create new policy locators
		String ADD_TRANSACTION_BUTTON = "//*[normalize-space()='Add Transaction']";
		String NEW_POLICY = "//a[contains(text(),'New Policy')]";
		String INCEPTION_DATE = "//input[@id='inception_date']";
		String NEW_POLICY_CONTINUE_BUTTON = "//button[@name='do_edit']";
		String ADD_LOB_BUTTON = "//button[@name='do_edit']";
		String ADD_LOB_EDIT_ICON = "//i[contains(@class,'fa fa-pencil')]";
		String ADDITIONAL_FIELD_BUTTON = "//button[contains(text(),'Additional Fields')]";

		// Create new legacy Locators
		String NEW_LEGACY = "//a[contains(text(),'New Legacy')]";

		// Create New Cover Note Locators
		String COVER_NOTE = "//a[contains(text(),'New Cover Note')]";

		// Create New Insurer Quote Locators
		String NEW_INSURER_QUOTE = "//a[contains(text(),'New Insurer Quote')]";
		String EXCESS = "//input[@id='total_excess']";
		String ADD_INSURER_BUTTON = "//button[@name='add_insurer']";
		String SELECT_CREDITOR_FROM_ADD_INSURER = "//select[@id='creditor_id']";
		String SAVE_BUTTON_ADD_INSURER_FROM_QUOTE = "//button[@id='add_note_btn']";

		// Clear Session Lock
		String ADMIN_SECTION = "//*[@id='nav_admin_tasks']/a";
		String CLEAR_SESSION_LOCK = "//a[contains(text(),'Clear Session Lock')]";
	}

	public interface basicProcessingLocators {
		// Clear Client Transaction Data
		String CLIENT_NAME = "//input[@id='client_name']";
		String KEY = "//input[@id='key']";
		String SUBMIT_BUTTON = "//button[@class='btn btn-primary']";

		// View Policy and Verify Policy Data Locators
		String VIEW_POLICY = "//td//a//i[@class='fa fa-eye']";

		// Editing A Client Locators
		String IHQ_CLIENT_NAME = "//input[@id='client']";
		String EDIT_BUTTON = "/html/body/div[1]/div/div[5]/div[3]/div[2]/div/div[1]/div/a[1]/text()";
		String GENDER = "//select[@id='gender']";
		String SAVE_CHANGES_BUTTON = "//button[@id='save_client']";
		String VIEW_ICON = "//div[@class='col-md-4 col-sm-12 col-xs-12 profile_details']//a[2]";
		String VIEW_MALE = "//div[contains(text(),'Male')]";

		// Add New Task Locators
		String NAVIGATE_DASHBOARD = "//a[contains(text(),'Dashboard')]";
		String ALL_TASKS = "//a[contains(text(),'All Tasks')]";
		String ADD_TASK_BUTTON = "//button[@id='btn_add_contact_note']";
		String ADD_TASK_CLIENT_NAME = "//input[@id='client_name_note']";
		String ASSIGNED_TO = "//select[@id='assing_to']";
		String ADD_TASK_SUBJECT = "//input[@id='email_subject']";
		String ADD_TASK_NOTE_SECTION = "//select[@id='note_section']";
		String ADD_TASK_DESCRIPTION = "//textarea[@id='email_body']";
		String ADD_TASK_TIME = "//input[@id='note_time']";
		String ADD_TASK_SAVE_BUTTON = "//button[@id='_saveTask']";
		String SEARCH_BUTTON = "//button[@name='submit2']";

		// Add New Note Locators
		String ALL_NOTES = "//a[contains(text(),'All Notes')]";

		// Create Contact To Ghost Client Locators
		String CLIENT_CONTACT_TAB = "//li[@class='dynamic-tab']//a[contains(text(),'Contacts')]";
		String ADD_CONTACT_BUTTON = "//button[@name='add contact']";

		// Add New Quote Locators
		String NEW_QUOTE_BUTTON = "//a[contains(text(),'New Quote')]";
		String SELECT_POLICY_CLASS = "//select[@id='policy_class']";
		String ADD_LOB_TOWN_VILLAGE = "//input[@id='town_or_village']";
		String ADD_LOB_SUM_INSURED = "//input[@id='sum_insured']";
		String ADD_LOB_FUTURE_ANNUAL_PREMIER = "//input[@id='annual_premium']";
		String ADD_LOB_INSURER_PLUS_ICON = "//div[@class='panel_toolbox text-right']//i[@class='fa fa-plus-circle']";
		String ADD_INSURER_INSURER = "//select[@id='insurer']";
		String ADD_INSURER_POLICY_FEE_ALLOCATION = "//input[@id='policy_fee']";
		String ADD_INSURER_POLICY_NUMBER = "//input[@id='policy_number']";
		String ADD_INSURER_CREDIT_TERM = "//select[@id='credit_terms']";
		String ADD_INSURER_BROKERAGE = "//input[@id='brokerage']";
		String ADD_INSURER_FLAT_BROKERAGE = "//input[@id='brokerage']";
		String ADD_INSURER_SAVE_BUTTON = "//button[@id='add_underwriter_btn']";
		String ADD_LOB_SAVE_BUTTON = "//button[@id='save_lob_transaction']";

		// Add New Broker Locators
		String ADD_BROKER_PLUS_ICON = "//div[@class='panel_toolbox right-text']//i[@class='fa fa-plus-circle']";
		String SELECT_BROKER = "//select[@id='broker_all']";
		String BROKER_TRANSACTION_NUMBER = "//input[@id='broker_reference']";
		String BROKER_FLAT_COMISSION = "//input[@id='flat']";
		String BROKER_FEE = "//input[@id='fee']";
		String ADD_BROKER_SAVE_BUTTON = "//button[@id='add_broker_btn']";

		// Add New Policy Additional Field Locators for SM
		String ADD_LOB_DESCRIPTION = "//input[@id='description']";
		String UPDATE_ADDITIONAL_FIELDS_BUTTON = "//button[contains(text(),'Update Additional Fields')]";
		String SM_ADDITIONAL_FIELD_MAKE_01 = "//select[@name='Make_dropdown__0___field0']";
		String SM_ADDITIONAL_FIELD_MAKE_02 = "//select[@name='Make_dropdown__0___field1']";
		String SM_ADDITIONAL_FIELD_YEAR = "//input[@name='Year_text__1___field2_']";
		String SM_ADDITIONAL_FIELD_MODIFICATION_YES = "//li[@id='fe_3_1238']//div//div[1]//label[1]";
		String SM_ADDITIONAL_FIELD_MODIFICATION_NO = "//li[@id='fe_3_1238']//div//div[2]//label[1]";
		String SM_ADDITIONAL_FIELD_ANY_FINANCE_ON_THE_CAR_YES = "//li[@id='fe_5_1238']//div//div[1]//label[1]";
		String SM_ADDITIONAL_FIELD_ANY_FINANCE_ON_THE_CAR_NO = "//li[@id='fe_5_1238']//div//div[2]//label[1]";
		String SM_ADDITIONAL_FIELD_DOES_THE_CAR_HAVE_IMMOBILISER_FACTORY = "//li[@id='fe_6_1238']//div//div[1]//label[1]";
		String SM_ADDITIONAL_FIELD_DOES_THE_CAR_HAVE_IMMOBILISER_MARKET = "//li[@id='fe_6_1238']//div//div[2]//label[1]";
		String SM_ADDITIONAL_FIELD_DOES_THE_CAR_HAVE_IMMOBILISER_NO = "//li[@id='fe_6_1238']//div//div[3]//label[1]";

		// Insurance History Additional Field Locators for SM
		String SM_ADDITIONAL_FIELD_HELD_LICENCE_OVER_10_YEAR_YES = "//li[@id='fe_10_1238']//div//div[1]//label[1]";
		String SM_ADDITIONAL_FIELD_HELD_LICENCE_OVER_10_YEAR_NO = "//li[@id='fe_10_1238']//div//div[2]//label[1]";
		String SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_NONE = "//li[@id='fe_11_1238']//div//div[1]//label[1]";
		String SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_1 = "//li[@id='fe_11_1238']//div//div[2]//label[1]";
		String SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_2 = "//li[@id='fe_11_1238']//div//div[3]//label[1]";
		String SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_3 = "//li[@id='fe_11_1238']//div//div[4]//label[1]";
		String SM_ADDITIONAL_FIELD_HOW_MANY_FALUT_CLAIM_HAD_IN_LAST_3_YEARS_4_OR_MORE = "//li[@id='fe_11_1238']//div//div[5]//label[1]";
		String HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_NONE = "//li[@id='fe_12_1238']//div//div[1]//label[1]";
		String HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_1 = "//li[@id='fe_12_1238']//div//div[2]//label[1]";
		String HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_2 = "//li[@id='fe_12_1238']//div//div[3]//label[1]";
		String HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_3 = "//li[@id='fe_12_1238']//div//div[4]//label[1]";
		String HOW_MANY_LICENCE_SUSPENSION_CALCELLATION_DISQUALIFICATION_RESTRICTION_HAD_IN_LAST_3_YEARS_4_OR_MORE = "//li[@id='fe_12_1238']//div//div[5]//label[1]";

		// Insurance Details Additional Field Locators for SM
		String SM_ADDITIONAL_FIELD_AGE_OF_MAIN_DRIVER = "//input[@name='Age of main driver_text_integers_1_0_300_field14_']";
		String SM_ADDITIONAL_FIELD_CAR_MAIN_USE_PERSONAL = "//li[@id='fe_15_1238']//div//div[1]//label[1]";
		String SM_ADDITIONAL_FIELD_CAR_MAIN_USE_BUSINESS = "//li[@id='fe_15_1238']//div//div[2]//label[1]";
		String SM_ADDITIONAL_FIELD_WHERE_IS_CAR_PARKED_GARAGED = "//li[@id='fe_16_1238']//div//div[1]//label[1]";
		String SM_ADDITIONAL_FIELD_WHERE_IS_CAR_PARKED_DRIVEWAY = "//li[@id='fe_16_1238']//div//div[2]//label[1]";
		String SM_ADDITIONAL_FIELD_WHERE_IS_CAR_PARKED_STREET = "//li[@id='fe_16_1238']//div//div[3]//label[1]";
		String SM_ADDITIONAL_FIELD_QUOTE_BUTTON = "//button[@class='submit_button nform_btn boots']";

		// Endorsing policy Locators
		String MANAGE_ICON_FOR_ENDORSE_POLICY = "//div[@id='policy_list']//i[@class='fa fa-random']";
		String ENDORSE_OPTION_FROM_CONVERT_POLICY = "//div[@id='policy_options_10773']//div[@class='modal-body']//div[1]";
		String CONFIRM_BUTTON_WHILE_CONVERT_POLICY = "//div[@id='policy_list']//button[@id='_btnConfirmConvertPolicy']";
		String BIND_POLICY_BUTTON_CONVERT_POLICY = "//button[@name='bind_policy']";

		// Create a Manual Invoices Locators
		String ACCOUNT_TRANSACTIONS_TAB = "//a[contains(text(),'Account Transactions')]";
		String CLAIMS_TAB = "//a[@class='nav-link'][contains(text(),'Claims')]";
		String CREATE_INVOICE_BUTTON = "//a[contains(text(),'Create Invoice')]";
		String TRANSACTION_TYPE = "//select[@id='transaction_type']";
		String CURRENCY = "//select[@id='srh_currency_id']";
		String SEARCH_FOR_CLIENT_DEBATOR = "//input[@id='client_name']";
		String BRANCH = "//select[@id='branch_id']";
		String INVOICE_DETAIL = "//input[@id='invoice_detail']";
		String ADD_INVOICE_LINE_BUTTON = "//a[contains(text(),'Add Invoice Line')]";
		String INVOICE_LINE_DESCRIPTION = "//input[@id='description_1']";
		String EXCLUSIVE_AMOUNT = "//input[@id='exclusive_amount_1']";
		String VAGST_AMOUNT = "//input[@id='gst_vat_amount_1']";
		String MANUAL_INVOICE_CREDIT_NOTE_BUTTON = "//button[@name='add_invoice']";

		// Client Receipt payment Locators
		String RECEIPT_PAYMENT_BUTTON = "//a[contains(text(),'Receipt Payment')]";
		String PAYMENT_AMOUNT = "//input[@id='payment_amount']";
		String PAYMENT_TYPE = "//select[@id='payment_type']";
		String DEPARTMENT = "//select[@id='department']";
		String REFERENCE = "//input[@id='reference']";
		String RECEIPT_PAYMENT_SUBMIT_BUTTON = "//button[@name='add_payment']";
		String ALLOCATE_CHECKBOX_1 = "//input[@id='allocate_chk_1']";

		// Unapproved Receipt Locators
		String UNAPPROVED_RECEIPT = "//a[contains(text(),'Unapproved Receipt')]";
		String CLIENT_PAID = "//input[@id='pay_client']";

		// Run Statement Locators
		String CLIENT_RUN_STATEMENT = "//a[contains(text(),'Run Statement')]";
		String CREDITOR_RUN_STATEMENT = "//button[@name='run_statement']";
		String PREVIEW_STATEMENT_BUTTON = "//button[@name='preview_pdf']";

		// Refund Payment Locators
		String REFUND_PAYMENT_BUTTONN = "//button[@id='add_payment']";
		String REFOUND_PAYMENT = "//a[contains(text(),'Refund Payment')]";

		// New Claim Locators
		String NEW_CLAIM = "//button[@id='btnnew_claim']";
		String SELECT_POLICY_TRANSACTION = "//button[@id='sel_policy_btn']";
		String SELECT_POLICY = "//tr[2]//td[8]//button[1]";
		String ADD_NEW_RESERVE = "//button[@id='add_new_reserve_btn']";
		String ADD_THIRD_PARTY = "//button[@id='add_claim_third_party_btn']";
		String ADD_NEW_THIRD_PARTY = "//button[@id='add_claim_third_party_new_btn']";
		String ADD_NOTE_IN_CLAIM = "//button[@id='btn_add_standard_note']";
		String ADD_TASK_IN_CLAIM = "//button[@id='btn_add_standard_task']";
		String SUBMIT_CLAIM = "//button[@id='do_submit_claim1']";
		String LOSS_SUMMARY = "//iframe[@id='loss_summary_ifr']";
		String ADD_RESERVE_DESCRIPTION = "//input[@id='description']";
		String ADD_RESERVE_AMOUNT = "//input[@id='reserve_amount']";
		String ADD_RESERVE_EXCESS = "//input[@id='total_excess']";
		String APPROVED_CHECKBOX = "//input[@id='approved']";
		String REASON_FOR_RESERVE_ADJUSTMENT = "//input[@id='reason_for_reserve_adjustment']";
		String ADD_RESERVE_SAVE_BUTTON = "//button[@id='add_reserve_btn']";
		String ADD_PAYMENT_ICON = "//i[@class='fa fa-money']";
		String AUTHORISE_CHECHBOX = "//input[@id='authorised']";
		String ADD_CLAIM_PAYMENT_SAVE_BUTTON = "//button[@id='add_claimpayment_btn']";

		// Create New Creditor
		String CREDITOR_TAB = "//a[contains(text(),'Creditors')]";
		String CREDITOR_BUTTON = "//*[@id='Creditors']/div[2]/div[1]/div[2]/a[3]";
		String CREDITOR_TYPE = "//select[@id='creditor_type']";
		String TRADE_TERMS = "//select[@id='trade_terms']";
		String CREDITOR_CODE = "//input[@id='creditor_code']";
		String CREDITOR_BRANCH = "//select[@id='branch']";
		String CREDITOR_COMPANY_NAME = "//input[@id='company_name_creditor']";
		String CREDITOR_EMAIL = "//input[@id='email']";
		String CREDITOR_MAILING_SAME_PHYSICAL_CHECKBOX = "//input[@id='physical_same_as_mailing_creditor']";
		String MAILING_ADDRESS_CREDITOR = "//input[@id='mailing_address1_creditor']";
		String CREDITOR_SUBMIT_BUTTON = "//button[@id='_btnSubmit']";
		String CREDITOR_TEXT_INPUT = "//input[@id='creditor']";
		String CREDITOR_LIST_MANAGE_ICON = "//i[@class='fa fa-wrench']";
		String EDIT_CREDITOR_ICON = "//i[@class='fa fa-edit fa-lg']";
		String CREDITOR_SAVE_CHANGE_BUTTON = "//button[contains(text(),'Save Changes')]";
		String CREDITOR_VIEW_ICON = "//i[@class='fa fa-eye fa-lg']";
		String CREDITOR_DELETE_ICON = "//i[@class='fa fa-trash']";
		String CREDITOR_DELETE_CONFIRM_BUTTON = "//button[@id='_btnConfirm']";
		String CREDITOR_SEARCH_BUTTON = "//a[@id='_btnSearch']";
		String CREDITOR_CREATE_INVOICE = "//button[@name='create_invoice']";
		String CREDITOR_CONTACTS_TAB = "//a[contains(text(),'Creditor Contacts')]";
	}

	public interface contactProcessingLocators {
		String CREDITOR_TASKS_TAB = "//a[contains(text(),'Tasks')]";
		String CANCEL_PAYMENT_ICON = "//i[@class='fa fa-minus-square-o']";
		String REASON_FOR_CANCELLATION = "//input[@id='cancel_reason']";
		String CANCEL_PAYMENT_BUTTON = "//button[@name='add_payment']";

		// Hide Copy Policy Locators
		String SETTINGS_TAB = "//li[@id='nav_settings']//a[contains(text(),'Settings')]";
		String SETTINGS_POLICY_CLASS = "//a[contains(text(),'Policy Class')]";
		String EDIT_POLICY_CLASS_GHOST = "//*[@id='view-policy']/table/tbody/tr[26]/td[6]/a/i";
		String EDIT_POLICY_FIRE = "//tbody/tr[18]/td[6]/a[1]/i[1]";
		String HIDE_COPY_POLICY = "//select[@id='hide_copy_policy']";
		String POLICY_CLASS_SAVE_CHANGE_BUTTON = "//button[contains(text(),'Save changes')]";

		// Hide Copy Quote Locators
		String HIDE_COPY_QUOTE = "//select[@id='hide_copy_quote']";

		// Add Document Category in setting Locator
		String SETTING_DOCUMENT_MANAGEMENT = "//a[contains(text(),'Document Management')]";
		String ADD_CATEGORY = "//input[@id='category_name']";
		String STATUS = "//input[@id='cat_status']";
		String SAVE = "//button[contains(text(),'Save')]";
		String DOCUMENT_HISTORY_TAB = "//a[contains(text(),'Documents History')]";
		String CATEGORY = "//select[@id='document_cat_id']";
		String DESCRIPTION = "//textarea[@id='description']";
		String EXPIRY_DATE = "//input[@id='exp_date']";
		String REASON_FOR_EXPIRY = "//textarea[@id='exp_reason']";
		String AVAILABLE_ON_CLIENT_PORTAL = "//input[@id='chk_available_broker_portal']";
		String AVAILABLE_ON_BROKER_PORTAL = "//input[@id='chk_available_client_portal']";
		String SUBMIT = "//button[@id='btn_savedocs']";
		String UPLOAD_BUTTON = "//div[@class='dz-default dz-message']";
		String SUCCESS_MESSAGE = "//div[@class='alert alert-success']";
		String REMOVE_CATEGORY = "//tbody/tr[2]/td[4]/a[2]";

		// Add Installment to Policy Locators
		String INSTALLMENTS_BUTTON = "//button[@name='btn_instalment']";
		String INSTALLMENT_EVERY_DROPDOWN = "//select[@id='instalment_every']";
		String INCLUDE_POLICY_FEE_IN_INSTALLMENT_CHECKBOX = "//input[@id='include_policy_fee']";
		String INCLUDE_BROKER_FEE_IN_INSTALLMENT_CHECKBOX = "//input[@id='include_broker_fee']";
		String CHECK_INSTALLMENT_PLAN_BUTTON = "//button[@name='setup_instalments']";
		String POST_INSTALLMENTS_BUTTON = "//button[@name='post_instalments']";

		// Client Fields Locators
		String SETTINGS_CLIENT_FIELDS = "//a[contains(text(),'Client Fields')]";
		String CLIENT_CODE_RULE = "//select[@id='client_code_rule']";
	}

	public interface seguromukusLocators {
		// Additional Client Fields
		String NATIONALITY = "//select[@name='Nationality_dropdown__0___field0']";
		String MARITAL_STATUS_SINGLE = "//li[@id='fe_1_12322']//div//div[1]//label[1]";
		String MARITAL_STATUS_MARRIED = "//li[@id='fe_1_12322']//div//div[2]//label[1]";
		String MARITAL_STATUS_WINDOWED = "//li[@id='fe_1_12322']//div//div[3]//label[1]";
		String DO_YOU_HAVE_CHILDREN_YES = "//li[@id='fe_3_12322']//div//div[1]//label[1]";
		String DO_YOU_HAVE_CHILDREN_NO = "//li[@id='fe_3_12322']//div//div[2]//label[1]";
		String ARE_YOU_A_HOME_OWNER_YES = "//li[@id='fe_5_12322']//div//div[1]//label[1]";
		String ARE_YOU_A_HOME_OWNER_NO = "//li[@id='fe_5_12322']//div//div[2]//label[1]";
		String ARE_YOU_A_CAR_OWNER_YES = "//li[@id='fe_6_12322']//div//div[1]//label[1]";
		String ARE_YOU_A_CAR_OWNER_NO = "//li[@id='fe_6_12322']//div//div[2]//label[1]";
		String ARE_YOU_AVAILABLE_TO_BE_WHATSAPP = "//li[@id='fe_7_12322']//div[2]//label[1]";
		String ADDITIONAL_FIELD_SUBMIT_BUTTON = "//button[@class='submit_button nform_btn boots']";

		// Driver Details Online Quote Form locators for SM.
		String DRIVER_LICENCE_NO = "//input[@name='Driver Licence No._text__0___field7_']";
		String SM_QUOTE_PHONE = "//input[@name='Phone_text__0___field9_']";
		String CATEGORY = "//input[@name='Category_text__0___field12_']";
		String SM_QUOTE_GENDER_MALE = "//li[@id='fe_11_1233']//div//div[1]//label[1]";
		String SM_QUOTE_GENDER_FEMALE = "//li[@id='fe_11_1233']//div//div[2]//label[1]";

		// Vehicle Details Online Quote Form locators for SM.
		String SM_MAKE = "//select[@name='Make_dropdown__1___field18']";
		String SM_CAR_NEW = "//li[@id='fe_19_1233']//label[@class='label_radio circle-ticked']";
		String SM_MODEL = "//select[@name='Model_dropdown__1___field20']";
		String SM_VEHICLE_TYPE = "//select[@name='Vehicle Type_dropdown__0___field21']";
		String SM_YEAR_OF_MANUFACTURE = "//input[@name='Year of Manufacture_text_integers_1___field22_']";
		String SM_YEAR_OF_MODEL = "//select[@name='Year of Model_dropdown__1___field23']";
		String SM_REGISTRATION_NUMBER = "//input[@name='Registration Number_text__1___field24_']";
		String SM_ENGINE_CHASSIS_NUMBER = "//input[@name='Engine / Chassis Number_text__1___field25_']";

		// Bind Policy Button
		String BIND_POLICY_BUTTON = "//button[@id='bind_policy']";
	}
}