package baseClass;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import utility.ReadPropertyConfig;
import utility.ReportsClass;

public class TestBase {
	public static WebDriver driver;
	static ReadPropertyConfig readPro = new ReadPropertyConfig();
	static String chromePath = readPro.getChromeDriver();

	@BeforeClass
	public static void setUp() {
		// WebDriverManager.chromedriver().setup(); 
		System.setProperty("webdriver.chrome.driver", chromePath);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--no-sandbox");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--aggressive-cache-discard");
		options.addArguments("--disable-cache");
		options.addArguments("--disable-application-cache");
		options.addArguments("--disable-offline-load-stale-cache");
		options.addArguments("--disk-cache-size=0");
		options.addArguments("--headless");
		options.addArguments("--disable-gpu");
		options.addArguments("--dns-prefetch-disable");
		options.addArguments("--no-proxy-server");
		options.addArguments("--log-level=3");
		options.addArguments("--silent");
		options.addArguments("--disable-browser-side-navigation");
		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
		options.setProxy(null);
		driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	}

	//@AfterClass
	public static void tearDown() {
		driver.quit();
	}

	@BeforeSuite
	public void beforeSuite() {
		ReportsClass.startUp();
	}

	@AfterSuite
	public void afterSuite() {
		ReportsClass.endTest();
	}

	public void switchToDefaultView() {
		driver.switchTo().parentFrame();

		driver.switchTo().frame(0);
	}

	public void openBrowser(String url) {
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}

	public void closeBrowser() {
		driver.quit();
	}

	public void openNewTab() {
		((JavascriptExecutor) driver).executeScript("window.open();");
	}

	public void waitFor1Sec() throws Exception {
		Thread.sleep(1000);
	}

	public void waitFor2Sec() throws Exception {
		Thread.sleep(2000);
	}

	public void waitFor3Sec() throws Exception {
		Thread.sleep(3000);
	}

	public void waitFor4Sec() throws Exception {
		Thread.sleep(4000);
	}

	public void waitFor5Sec() throws Exception {
		Thread.sleep(5000);
	}

	public void waitFor10Sec() throws Exception {
		Thread.sleep(10000);
	}

	public void clearCookie() {
		driver.manage().deleteAllCookies();
	}

	// User defined method to check the alert is present or not
	public boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isTextPresent(String text) {
		try {
			boolean b = driver.getPageSource().contains(text);
			return b;
		} catch (Exception e) {
			return false;
		}
	}

}
