package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.CP_Client_Policy_Page;
import pageObject.Carpeesh_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

public class Renewal_IHQ_6004_Test extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String path = readPro.getRenewalPolicesDataExcel();
	String sheetOne = readPro.getSheetOne();
	String sheetTwo = readPro.getSheetTwo();
	String sheetFour = readPro.getSheetFour();
	String url = readPro.getCarpeeshUrl();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	int days = -391;
	String client = "10016";
	String renewalCronURL = readPro.getRenewalCronURL();

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Renewal_IHQ_6004_Test");

		// Launch Browser and Open URL
		openBrowser(url);
	}

	@Test
	public void renewal_IHQ_6004_Test() throws Exception {
		ReportsClass.initialisation("Renewal_IHQ_6004_Test");
		Carpeesh_Page carpeesh = new Carpeesh_Page();
		CP_Client_Policy_Page cp_ClientPage = new CP_Client_Policy_Page();
		CommonFunction commonFunction = new CommonFunction();
		Xls_Reader reader = new Xls_Reader(path);
		int getRowCount = reader.getRowCount(sheetOne);
		System.out.println("Number of row: " + getRowCount);

		// Login IHQ with Usersupport
		commonFunction.loginIHQ(userName, password, client);
		for (int i = 2; i <= 6; i++) {
			int testCount = i - 1;
			System.out.println("");
			System.out.println("Started Test Step :=> " + testCount);
			// Create new client
			carpeesh.createNewClientForCarpeesh(sheetOne, i);

			// Create New Policy
			carpeesh.createNewPolicyTillAddLobForRenewal(sheetTwo, i);

			// Car Details Page
			if (cp_ClientPage.carDetails_Step1(sheetTwo, i) == false) {
				clearCookie();
				continue;
			}

			// Car Usage Page
			if (cp_ClientPage.carUsage_Step2(sheetTwo, i) == false) {
				clearCookie();
				continue;
			}

			// Car Kept Address Detail Page
			if (cp_ClientPage.keptAddressDetails_step3(sheetTwo, i) == false) {
				clearCookie();
				continue;
			}

			// Driver Detail Page
			if (cp_ClientPage.driverDetails_Step4(sheetTwo, i) == false) {
				clearCookie();
				continue;
			}

			/// Verifying Driver Detail Page Next Button is Present or Not

			String driverDetailButtonXpath = "//a[@id='Next26']";
			String driverDetailButton = driver.findElement(By.xpath(driverDetailButtonXpath)).getText();
			if (isTextPresent(driverDetailButton)) {
				System.out.println("Driver Detail Page Next Button is Not Present ===>>");
				clearCookie();
				continue;
			} else {
				driver.findElement(By.xpath(driverDetailButtonXpath)).click();
				waitFor2Sec();
			}

			// Click on Review Page Next Button
			driver.findElement(By.cssSelector("#custom_tab_p_n_button_li > a.ct_next_step")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Option and Pay page
			if (carpeesh.optionsAndPayForRenewal_Step6(sheetFour, i) == false) {
				clearCookie();
				continue;
			}
			carpeesh.clickOnContinueToPaymentButton();
			cp_ClientPage.afterAdditionalForm();
			System.out.println("");
		}
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("End Test: Renewal_IHQ_6004_Test");
		// closeBrowser();
	}
}