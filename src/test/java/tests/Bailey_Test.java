package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Baileys_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

public class Bailey_Test extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String url = readPro.getMasterQAURL();
	String sheet1 = readPro.getBaileySheetOne();
	String sheet2 = readPro.getBaileySheetTwo();
	String sheet3 = readPro.getBaileySheetThree();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String type = "Policy";
	String policyClass = "1";
	String baileysClient = "45";

	@BeforeMethod
	public void beforeMethod() throws Exception {
		CommonFunction commonFunction = new CommonFunction();
		System.out.println("beforeMethod");

		// Launch URL
		openBrowser(url);

		// Login to IHQ with Usersupport Credentials
		commonFunction.loginIHQ(userName, password, baileysClient);

		// Clear Session Lock
		commonFunction.clearSessionLock();
	}

	@Test
	public void carpeesh_Test() throws Exception {
		Baileys_Page baileyPage = new Baileys_Page();
		String excelPath = readPro.getBaileysExcelFile();
		Xls_Reader reader = new Xls_Reader(excelPath);
		int getRowCount = reader.getRowCount(sheet1);
		System.out.println("Total Number of Row is : " + getRowCount);
		for (int i = 2; i <= getRowCount; i++) {
			String firstName = reader.getCellData(sheet1, "First Name", i);
			String lastName = reader.getCellData(sheet1, "Last Name", i);

			int testCount = i - 1;
			ReportsClass.initialisation("" + firstName + " " + lastName + " _0" + testCount);
			try {
				System.out.println("");
				System.out.println("Started Test Step :=> " + testCount);
				System.out.println("");

				// Create new client for Baileys
				baileyPage.createNewClientForBaileys(sheet1, i);

				// Create new policy for Baileys
				baileyPage.createPolicyForBaileys(type, policyClass);

				if (baileyPage.pleasureCraftDetails_step2(sheet2, i) == false) {
					baileyPage.navigateToIHQClientForDeclinedCase();
					continue;
				}

				if (baileyPage.boatUse(sheet2, i) == false) {
					baileyPage.navigateToIHQClientForDeclinedCase();
					continue;
				}
				baileyPage.policyCommencementDate();
				baileyPage.sumInsuredNZD(sheet2, i);
				if (baileyPage.vesselDetails(sheet2, i)) {
					baileyPage.navigateToIHQClientForDeclinedCase();
					continue;
				}
				baileyPage.coverSpecifics(sheet3, i);
				baileyPage.customerReferralProgramme(sheet3, i);
				baileyPage.declaration(sheet3, i);
			} catch (Exception e) {
				System.out.println("");
			}
		}
	}

	@AfterMethod
	public void afterMethod() {
		ReportsClass.endTest();
		System.out.println("End Test: Carpeesh_Test");
		closeBrowser();
	}
}