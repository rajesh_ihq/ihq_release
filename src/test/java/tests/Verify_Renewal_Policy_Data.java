package tests;

import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.Carpeesh_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class Verify_Renewal_Policy_Data extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String url = readPro.getMasterQAURL();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String sheetOne = readPro.getSheetOne();
	String sheetFour = readPro.getSheetFour();
	String client = "10016";

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Start Test: Verify_Renewal_Policy_Data");

		// Launch Browser and Open URL
		openBrowser(url);

	}

	@Test
	public void verify_Renewal_Policy_Data() throws Exception {
		CommonFunction commonFunction = new CommonFunction();
		ReadPropertyConfig readPro = new ReadPropertyConfig();
		String renewalExcelPath = readPro.getRenewalPolicesDataExcel();
		Xls_Reader reader = new Xls_Reader(renewalExcelPath);
		Carpeesh_Page carpeesh = new Carpeesh_Page();

		// Login IHQ with Usersupport
		commonFunction.loginIHQ(userName, password, client);
		for (int i = 2; i <= 11; i++) {
			String firstName = reader.getCellData(sheetOne, "First Name", i);
			String lastName = reader.getCellData(sheetOne, "Last Name", i);
			ReportsClass.initialisation("" + firstName + " " + lastName);

			// Search and View Policy
			commonFunction.searchAndViewPolicy(sheetOne, i);

			// Verify Renewal Data
			carpeesh.verifyRenewalData(sheetOne, i);
		}
	}

	@AfterMethod
	public void afterMethod() {
		ReportsClass.endTest();
		System.out.println("========================================");
		System.out.println("End Test: Verify_Renewal_Policy_Data");
		// closeBrowser();
	}
}
