package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.CP_Client_Policy_Page;
import pageObject.Carpeesh_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

public class CP_Client_Policy_Test extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String excelPath = readPro.getRenewalPolicesDataExcel();
	String sheetOne = readPro.getSheetOne();
	String sheetTwo = readPro.getSheetTwo();
	String sheetFour = readPro.getSheetFour();
	String url = readPro.getCarpeeshUrl();
	String userName = readPro.getIHQUserName();
	String password = readPro.getIHQPassword();
	String carpeesh_Client = "10016";

	@BeforeMethod
	public void beforeMethod() throws Exception {
		System.out.println("beforeMethod");
		ReportsClass.reportStartUp_CP();
	}

	@Test
	public void cp_client_policy_Test() throws Exception {
		CP_Client_Policy_Page cp_ClientPage = new CP_Client_Policy_Page();
		Carpeesh_Page carpeeshPage = new Carpeesh_Page();
		CommonFunction commonFunction = new CommonFunction();
		Xls_Reader reader = new Xls_Reader(excelPath);
		int getRowCount = reader.getRowCount(sheetOne);
		System.out.println("Total Number of Row is : " + getRowCount);
		System.out.println("");
		System.out.println("");
		openBrowser(url);
		commonFunction.loginIHQ(userName, password, carpeesh_Client);

		for (int i = 2; i <=6; i++) {
			int testCount = i - 1;
			System.out.println("Started Test Step :=> " + testCount);
			String firstName = reader.getCellData(sheetOne, "First Name", i);
			String lastName = reader.getCellData(sheetOne, "Last Name", i);
			ReportsClass.initialisation("" + firstName + " " + lastName + " _0" + testCount);
			try {
				cp_ClientPage.createNewClientForCarpeesh(sheetOne, i);
				cp_ClientPage.createNewPolicyTillAddLob();
				// Car Details Page
				if (cp_ClientPage.carDetails_Step1(sheetTwo, i) == false) {
					clearCookie();
					continue;
				}

				// Car Usage Page
				if (cp_ClientPage.carUsage_Step2(sheetTwo, i) == false) {
					clearCookie();
					continue;
				}

				// Car Kept Address Detail Page
				if (cp_ClientPage.keptAddressDetails_step3(sheetTwo, i) == false) {
					clearCookie();
					continue;
				}

				// Driver Detail Page
				if (cp_ClientPage.driverDetails_Step4(sheetTwo, i) == false) {
					clearCookie();
					continue;
				}

				/// Verifying Driver Detail Page Next Button is Present or Not

				String driverDetailButtonXpath = "//a[@id='Next26']";
				String driverDetailButton = driver.findElement(By.xpath(driverDetailButtonXpath)).getText();
				if (isTextPresent(driverDetailButton)) {
					System.out.println("Driver Detail Page Next Button is Not Present ===>>");
					System.out.println("Ended Test Step :=> " + testCount);
					clearCookie();
					continue;
				} else {
					driver.findElement(By.xpath(driverDetailButtonXpath)).click();
					waitFor2Sec();
				}

				// Click on Review Page Next Button
				driver.findElement(By.cssSelector("#custom_tab_p_n_button_li > a.ct_next_step")).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				// Option and Pay page
				if (cp_ClientPage.optionsAndPay_Step6(sheetFour, i) == false) {
					clearCookie();
					continue;
				}
				carpeeshPage.clickOnContinueToPaymentButton();
				cp_ClientPage.afterAdditionalForm();
				System.out.println("Ended Test Step :=> " + testCount);
				System.out.println("");
			} catch (Exception e) {
				System.out.println("");
			}
		}
	}

	@AfterMethod
	public void afterMethod() {
		ReportsClass.endTest();
		System.out.println("End Test: Carpeesh_Test");
		closeBrowser();
	}
}
