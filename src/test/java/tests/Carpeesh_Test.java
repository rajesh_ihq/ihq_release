package tests;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import atu.testrecorder.ATUTestRecorder;
import baseClass.TestBase;
import pageObject.CP_Client_Policy_Page;
import pageObject.Carpeesh_Page;
import utility.CommonFunction;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

public class Carpeesh_Test extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String path = readPro.getExcelPath();
	Xls_Reader reader = new Xls_Reader(path);
	String url = readPro.getCarpeeshUrl();
	String sheetOne = readPro.getSheetOne();
	String sheetTwo = readPro.getSheetTwo();
	String sheetFour = readPro.getSheetFour();
	String cardNumber = readPro.getCardNumber();
	String expiryDate = readPro.getExpiryDate();
	String cardVerificationCode = readPro.getCardVerificationCode();
	String cardHolderName = readPro.getCardHolderName();
	ATUTestRecorder recorder;

	@BeforeMethod
	public synchronized void beforeMethod(Method method) throws Exception {
		System.out.println("beforeMethod");
		ReportsClass.startUp();
		CommonFunction.deleteVideos(System.getProperty("user.dir") + "\\ScriptVideos\\");
		/*DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH-mm-ss");
		Date date = new Date();

		try {
			recorder = new ATUTestRecorder(System.getProperty("user.dir") + "\\ScriptVideos\\",
					method.getName() + "-" + dateFormat.format(date), false);
		} catch (Exception e) {
			System.out.println("Error in finding the location of the video.");
		}

		// To Start Recording
		try {
			recorder.start();
		} catch (Exception e) {
			System.out.println("Error in starting the video");
		}*/
	}

	@Test
	public void carpeesh_Test() throws Exception {
		Carpeesh_Page carpeeshPage = new Carpeesh_Page();
		CP_Client_Policy_Page cp_ClientPage = new CP_Client_Policy_Page();
		Carpeesh_Page carpeesh = new Carpeesh_Page();
		String excelPath = readPro.getRenewalPolicesDataExcel();
		Xls_Reader reader = new Xls_Reader(excelPath);
		int getRowCount = reader.getRowCount(sheetOne);
		System.out.println("Total Number of Row is : " + getRowCount);

		for (int i = 2; i <= 2; i++) {
			String firstName = reader.getCellData(sheetOne, "First Name", i);
			String lastName = reader.getCellData(sheetOne, "Last Name", i);
			int testCount = i - 1;
			ReportsClass.initialisation("" + firstName + " " + lastName);
			try {
				System.out.println("");
				System.out.println("Started Test Step :=> " + testCount);
				System.out.println("");
				openBrowser(url);
				carpeeshPage.clickInitialIAgreeButton();
				carpeeshPage.clickIAgreeButton();
				carpeeshPage.policyHolderDetails(sheetOne, i);

				// Car Details Page
				if (carpeesh.carDetails_Step1(sheetTwo, i) == false) {
					clearCookie();
					continue;
				}

				// Car Usage Page
				if (cp_ClientPage.carUsage_Step2(sheetTwo, i) == false) {
					clearCookie();
					continue;
				}

				// Car Kept Address Detail Page
				if (cp_ClientPage.keptAddressDetails_step3(sheetTwo, i) == false) {
					clearCookie();
					continue;
				}

				// Driver Detail Page
				if (cp_ClientPage.driverDetails_Step4(sheetTwo, i) == false) {
					clearCookie();
					continue;
				}

				/// Verifying Driver Detail Page Next Button is Present or Not

				String driverDetailButtonXpath = "//a[@id='Next26']";
				String driverDetailButton = driver.findElement(By.xpath(driverDetailButtonXpath)).getText();
				if (isTextPresent(driverDetailButton)) {
					System.out.println("Driver Detail Page Next Button is Not Present ===>>");
					System.out.println("Ended Test Step :=> " + testCount);
					clearCookie();
					continue;
				} else {
					driver.findElement(By.xpath(driverDetailButtonXpath)).click();
					waitFor2Sec();
				}

				// Click on Review Page Next Button
				driver.findElement(By.cssSelector("#custom_tab_p_n_button_li > a.ct_next_step")).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				// Option and Pay page
				if (carpeesh.optionsAndPayForRenewal_Step6(sheetFour, i) == false) {
					clearCookie();
					continue;
				}
				carpeeshPage.clickOnContinueToPaymentButton();
				carpeeshPage.clickOnPayNowButton();
				carpeeshPage.enterCardDetails(cardNumber, expiryDate, cardVerificationCode, cardHolderName);

				// Verify payment successful and policy created successful
				String thankYouMessage = "//*[@id='form_ul']/div[2]/table/tbody/tr/td/table";
				if (driver.findElement(By.xpath(thankYouMessage)) != null) {
					System.out.println("");
					System.out.println("Policy is created == " + testCount);
					ReportsClass.logStat(Status.PASS, "Policy is Created for Records : " + testCount);
				} else {
					System.out.println("Policy is not created == " + testCount);
					ReportsClass.logStat(Status.FAIL, "Policy is Not Created for Records : " + testCount);
				}
				System.out.println("Ended Test Step :=> " + testCount);
				System.out.println("");
				clearCookie();
			} catch (Exception e) {
				System.out.println("");
			}
		}
	}

	@AfterMethod
	public void afterMethod() {
		ReportsClass.endTest();
		System.out.println("End Test: Carpeesh_Test");
		/*try {
			recorder.stop();
		} catch (Exception e) {
			System.out.println("Unable to stop the screen recording.");
		}*/
		//closeBrowser();
	}
}