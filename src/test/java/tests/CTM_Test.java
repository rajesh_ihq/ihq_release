package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.CTM_Page;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

public class CTM_Test extends TestBase {

	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String ctmURL = readPro.getCtmUrl();
	String ctmSheet = readPro.getCTMSheet();
	String ctmPassword = readPro.getCTMPassword();
	String carpeeshProtectedPassword = readPro.getCarpeeshProectedPassword();
	String sheetOne = readPro.getSheetOne();
	String sheetTwo = readPro.getSheetTwo();
	String sheetFour = readPro.getSheetFour();
	String cardNumber = readPro.getCardNumber();
	String expiryDate = readPro.getExpiryDate();
	String cardVerificationCode = readPro.getCardVerificationCode();
	String cardHolderName = readPro.getCardHolderName();
	JavascriptExecutor js = (JavascriptExecutor) driver;

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("beforeMethod !!!");
	}

	@Test
	public void ctmTest() throws Exception {
		CTM_Page ctmPage = new CTM_Page();
		ReportsClass.startUp();
		String excelPath = readPro.getCtmExcel();
		Xls_Reader reader = new Xls_Reader(excelPath);
		int getRowCount = reader.getRowCount(sheetOne);

		System.out.println("Total Number of Row is : " + getRowCount);

		for (int i = 3; i <= 3 ; i++) {
			try {
				int testCount = i - 1;
				ReportsClass.initialisation("CTM Test : " + testCount);
				openBrowser(ctmURL);
				ctmPage.enterCTMJson(ctmSheet, i, ctmPassword);
				ctmPage.enterCarpeeshProtectedPassword(carpeeshProtectedPassword);
				ctmPage.policyHolderDetails(sheetOne, i);
				ctmPage.carDetails_Step1(sheetTwo, i);

				// Click on Car Usage Next Button
				driver.switchTo().activeElement().click();
				driver.findElement(By.cssSelector("#custom_tab_p_n_button_li > a.ct_next_step")).click();

				// Kept Address Details
				ctmPage.keptAddressDetails_step3(sheetTwo, i);

				// Driver Details
				ctmPage.driverDetails_step4(sheetTwo, i);

				// Click on Review Page Next Button
				driver.findElement(By.cssSelector("#custom_tab_p_n_button_li > a.ct_next_step")).click();

				// Option and Pay Page
				ctmPage.optionAndPay_step6(sheetFour, i);
				ctmPage.clickOnPayNowButton();

				// Payment Card Page
				ctmPage.enterCardDetails(cardNumber, expiryDate, cardVerificationCode, cardHolderName);

				String thankYouMessage = "//div/table/tbody/tr/td/p[contains(text(),'Your policy number is')]";
				if (driver.findElement(By.xpath(thankYouMessage)) != null) {
					System.out.println("Policy is created == " + testCount);
				} else {
					System.out.println("Policy is not created == " + testCount);
				}
				ReportsClass.endTest();
				clearCookie();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("After Method : !!!");
		closeBrowser();
	}
}