package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import baseClass.TestBase;
import pageObject.SureVestor_Page;
import utility.ReadPropertyConfig;
import utility.ReportsClass;
import utility.Xls_Reader;

public class Surevestor_Test extends TestBase {
	ReadPropertyConfig readPro = new ReadPropertyConfig();
	String url = readPro.getSureVestorURL();
	String sheet1 = readPro.getBaileySheetOne();
	String sheet2 = readPro.getBaileySheetTwo();
	String sheet3 = readPro.getBaileySheetThree();

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("beforeMethod");
		ReportsClass.startUp();
	}

	@Test
	public void sureVestor_Test() {
		SureVestor_Page sureVestorPage = new SureVestor_Page();
		String excelPath = readPro.getBaileysExcelFile();
		Xls_Reader reader = new Xls_Reader(excelPath);
		int getRowCount = reader.getRowCount(sheet1);
		System.out.println("Total Number of Row is : " + getRowCount);
		for (int i = 2; i <= 2; i++) {
			String landLordName = reader.getCellData(sheet1, "Landlord Name", i);
			int testCount = i - 1;
			ReportsClass.initialisation("" + landLordName + "_0" + testCount);
			try {
				System.out.println("");
				System.out.println("Started Test Step :=> " + testCount);
				System.out.println("");
				openBrowser(url);
				sureVestorPage.scheerLandlordProtectionInsurancePolicy(sheet1, i);
				sureVestorPage.propertyManagementAgent(sheet2, i);
				sureVestorPage.residentialRentalPropertyToBeInsured(sheet2, i);
				sureVestorPage.pleaseAnswerFollowingQuestions(sheet2, i);
				sureVestorPage.insuranceBenefitsRequired(sheet3, i);
			} catch (Exception e) {
				System.out.println("");
			}
		}
	}

	@AfterMethod
	public void afterMethod() {
		ReportsClass.endTest();
		System.out.println("End Test: SureVestor_Test");
		//closeBrowser();
	}
}
